import Reactotron, { networking } from 'reactotron-react-native';
import { reactotronRedux } from 'reactotron-redux';
import sagaPlugin from 'reactotron-redux-saga';

const reactotron = Reactotron
    .configure({ 
        name: 'Safe Pay',
        host: '192.168.0.103',
        //host: '192.168.1.70', 
    })
    .use(networking())
    .use(reactotronRedux()) //  <- here i am!
    .use(sagaPlugin())
    .useReactNative()
    .connect(); //Don't forget about me!
  
export default reactotron;
