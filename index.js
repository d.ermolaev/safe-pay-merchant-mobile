/** @format */
//enable Reactotron
/*
if(__DEV__) {
    import('./ReactotronConfig').then(() => console.log('Reactotron Configured'));
}
*/

import { AppRegistry } from 'react-native';
import Entrypoint from './app/Entrypoint';
import { name as appName } from './app.json';

AppRegistry.registerComponent(appName, () => Entrypoint);
