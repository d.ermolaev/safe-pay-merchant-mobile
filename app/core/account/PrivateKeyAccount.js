import PublicKeyAccount from './PublicKeyAccount';
import { Base58 } from '../crypt/Base58';

export default class PrivateKeyAccount {

    publicKey = null;

    // keyPair: KeyPair 
    constructor(keyPair) {
        this.keyPair = keyPair;
        this.privateKey = this.keyPair.secretKey;
    }

    // key: string
    // return : Promise<boolean>
    static async validate(key) {
        try {
            const keyBytes = await Base58.decode(key);
            return keyBytes.length === 64;
        } catch (e) {
            return false;
        }
    }

    getPublicKey() {
        if (!this.publicKey) {
            this.publicKey = new PublicKeyAccount(this.keyPair.publicKey);
        }
        return this.publicKey;
    }

    // return : Int8Array
    getSecretKey() {
        return this.privateKey;
    }

    // return : IKeyPair
    getKeyPair() {
        return this.keyPair;
    }
}
