import { Base58 } from '../crypt/Base58';
import { CoreCrypto } from '../Crypto';
import { getAddressByPublicKey } from '../EraChainCrypto';

export default class PublicKeyAccount {
    static PUBLIC_KEY_LENGTH = CoreCrypto.HASH_LENGTH;
 
    _address = null;

    // publicKey: Int8Array
    constructor(publicKey) {
        this._publicKey = publicKey;
    }

    // return : Int8Array
    get publicKey() {
        return this._publicKey;
    }

    // return : Promise<string>
    async base58PublicKey() {
        return await Base58.encode(this._publicKey);
    }

    // return : Promise<string>
    async getAddress() {
        if (!this._address) {
            this._address = await getAddressByPublicKey(this.publicKey);
        }
        return this._address;
    }
}
