import { sign } from 'tweetnacl';
import { Base58 } from '../crypt/Base58';

export default class KeyPair {

    // secretKey?: IKeyPair | Int8Array | string, 
    // publicKey?: Int8Array | string
    constructor(secretKey = null, publicKey = null) {

        if (arguments.length === 0) {
            const keys = this.generate();
            secretKey = keys.secretKey;
            publicKey = keys.publicKey;
        }

        if (arguments.length === 1) {
            const keyPair = secretKey;
            secretKey = keyPair.secretKey;
            publicKey = keyPair.publicKey;
        }
        this.secretKey = new Int8Array(secretKey);
        this.publicKey = new Int8Array(publicKey);
    }

    toArray() {
        return {
            secretKey: Array.from(this.secretKey),
            publicKey: Array.from(this.publicKey),
        };
    }

    // return : Promise<IKeyPairString>
    async toString() {
        return {
            secretKey: await Base58.encode(this.secretKey),
            publicKey: await Base58.encode(this.publicKey),
        };
    }

    toJSON() {
        return this.toArray();
    }

    // return : IKeyPair
    generate() {
        const keys = sign.keyPair();
        return {
            secretKey: new Int8Array(keys.secretKey),
            publicKey: new Int8Array(keys.publicKey)
        };
    }
}

