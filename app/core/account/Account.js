export default class Account {

    static ADDRESS_LENGTH = 25;

    // address: string
    constructor(address) {
        this.address = address;
    }

    getAddress() {
        return this.address;
    }

    // address: string
    // return : boolean
    static isValidAddress(address) {
        return /^[\w\d]{34}$/.test(address);
    }
}
