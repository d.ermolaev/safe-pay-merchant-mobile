import {NativeModules} from 'react-native';

const RNUtil = NativeModules.ChainUtilities;

export class Bytes {

    // numbers: Int8Array, minLength: number, padding: number
    // return : Int8Array
    static ensureCapacity(numbers, minLength, padding) {
        const array = numbers.length < minLength ? [...new Array(numbers), ...new Array(minLength + padding - numbers.length).fill(0)] : numbers;
        return new Int8Array(array);
    }

    // int: number
    // return : Int8Array
    static async intToByteArray(int) {
        const int8Array = await RNUtil.intToByteArray(int);
        // debugger;
        return int8Array;
    }

    // bytes: Int8Array
    // return: Promise<number> 
    static async intFromByteArray(bytes) {
        const number = await RNUtil.intFromByteArray(Array.from(bytes));
        // debugger;
        return number;
    }

    // long: number
    // return: Promise<number>
    static async longToByteArray(long) {
        const int8Array = await RNUtil.longToByteArray(long.toFixed(0));
        // debugger;
        return int8Array;
    }

    // bytes: Int8Array
    // return: Promise<Int8Array>
    static async longFromByteArray(bytes) {
        const byteArray = await RNUtil.longFromByteArray(Array.from(bytes));
        // debugger;
        return parseFloat(byteArray);
    }

    // float: number
    // return: Promise<Int8Array>
    static async floatToByteArray(float) {
        const int8Array = await RNUtil.floatToByteArray(`${float}`);
        // debugger;
        return int8Array;
    }

    // bytes: Int8Array
    // return : Promise<number>
    static async floatFromByteArray(bytes)  {
        const byteArray = await RNUtil.floatFromByteArray(Array.from(bytes));
        // debugger;
        return parseFloat(byteArray);
    }

    // str: string
    // return : Promise<Int8Array>
    static async stringToByteArray(str) {
        const int8Array = await RNUtil.stringToByteArray(str);
        // debugger;
        return int8Array;
    }

    // bytes: Int8Array
    // return : Promise<string>
    static async stringFromByteArray(bytes) {
        const s = await RNUtil.stringFromByteArray(Array.from(bytes));
        // debugger;
        return s;
    }
}
