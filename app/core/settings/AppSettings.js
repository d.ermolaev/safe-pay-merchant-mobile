
class AppSettings {

    _environment = 'development';

    get settings() {
        if (!this._settings) {
            this._settings = this.loadSettings();
        }
        return this._settings;
    }

    // environment: 'stage' | 'production' | 'development'
    set environment(environment) {
        const validEnv = ['stage', 'production', 'development'];
        if (validEnv.indexOf(environment) < 0) {
            throw new Error(`Invalid environment. Valid values is on of '${validEnv.join('\', \'')}'.`);
        }
        this._environment = environment;
    }

    loadSettings() {
        let envName = this._environment;
        const env = require('../../resources/settings/env.json');
        if (!envName) {
            envName = env.environment;
        }

        const envs = {
            development: require('../../resources/settings/development.json'),
            production: require('../../resources/settings/production.json'),
            stage: require('../../resources/settings/stage.json'),
        };

        const loadedSettings = envs[envName];
        return {...this._defaultSettings, ...env, ...loadedSettings};
    }
}

export default AppSettings;
