import { Transaction } from './Transaction';
import Account from '../account/Account';
import { DataWriter } from '../DataWriter';
import { Base58 } from '../crypt/Base58';
import { Bytes } from '../Bytes';

export class TransactionAmount extends Transaction {
    static RECIPIENT_LENGTH = Account.ADDRESS_LENGTH;
    static AMOUNT_LENGTH = 8;
    static BASE_LENGTH_AS_PACK = Transaction.BASE_LENGTH_AS_PACK + TransactionAmount.RECIPIENT_LENGTH + Transaction.KEY_LENGTH + TransactionAmount.AMOUNT_LENGTH;
    static BASE_LENGTH = Transaction.BASE_LENGTH + TransactionAmount.RECIPIENT_LENGTH + Transaction.KEY_LENGTH + TransactionAmount.AMOUNT_LENGTH;

    amount = null;
    
    // typeBytes: Int8Array, 
    // name: string, 
    // creator: PrivateKeyAccount, 
    // feePow: number, 
    // recipient: Account, 
    // amount: BigDecimal | null, 
    // key: number, 
    // timestamp: number, 
    // reference: number
    constructor(typeBytes, name, creator, feePow, recipient, amount, key, timestamp, reference) {
        super(typeBytes, name, creator, feePow, timestamp, reference);
        this.recipient = recipient;
        if (amount === null || amount.num === 0) {
            // set version to 1
            typeBytes[2] = (typeBytes[2] | -128);
        } else {
            this.amount = amount;
        }
        this.key = key;
    }

    // withSign: boolean, releaserReference: number | null
    // return : Promise<Int8Array>
    async toBytes(withSign, releaserReference = null) {

        const data = new DataWriter();
        data.set(await super.toBytes(withSign, releaserReference));

        //WRITE RECIPIENT
        data.set(await Base58.decode(this.recipient.getAddress()));

        if (this.amount !== null) {
            //WRITE KEY
            let keyBytes = await Bytes.longToByteArray(this.key);
            keyBytes = Bytes.ensureCapacity(keyBytes, Transaction.KEY_LENGTH, 0);
            data.set(keyBytes);

            //WRITE AMOUNT
            //console.log({ amount: this.amount });
            let amountBytes = await Bytes.longToByteArray(this.amount.unscaledValue());
            amountBytes = Bytes.ensureCapacity(amountBytes, TransactionAmount.AMOUNT_LENGTH, 0);
            data.set(amountBytes);
        }

        return data.data;
    }

    // asPack: boolean
    // return : Promise<number>
    async getDataLength(asPack) {
        return (asPack ? TransactionAmount.BASE_LENGTH_AS_PACK : TransactionAmount.BASE_LENGTH) -
            (this.typeBytes[2] < 0 ? (Transaction.KEY_LENGTH + TransactionAmount.AMOUNT_LENGTH) : 0);
    }

}
