import { TransactionAmount } from './TransactionAmount';
import { Transaction } from './Transaction';
import { Bytes } from '../Bytes';
import { DataWriter } from '../DataWriter';

export default class R_Send extends TransactionAmount {
    
    static BASE_LENGTH = Transaction.IS_TEXT_LENGTH + Transaction.ENCRYPTED_LENGTH + Transaction.DATA_SIZE_LENGTH;
    static TYPE_ID = Transaction.SEND_ASSET_TRANSACTION;
    static NAME_ID = 'Send';

    // creator: PrivateKeyAccount, 
    // feePow: number, 
    // recipient: Account, 
    // key: number, 
    // amount: BigDecimal | null, 
    // head: string | null, 
    // data: Int8Array | null, 
    // isText: Int8Array, 
    // encrypted: Int8Array, 
    // timestamp: number, 
    // reference: number
    constructor(creator, feePow, recipient, key, amount = null, head = null, data = null, isText, encrypted, timestamp, reference) {
        const typeBytes = new Int8Array([R_Send.TYPE_ID, 0, 0, 0]);
        super(typeBytes, R_Send.NAME_ID, creator, feePow, recipient, amount, key, timestamp, reference);

        this.head = head || '';

        if (data === null || data.length === 0) {
            // set version byte
            typeBytes[3] = (typeBytes[3] | -128);
        } else {
            this.data = data;
            this.encrypted = encrypted;
            this.isText = isText;
        }
    }

    // asPack: boolean
    // return : Promise<number>
    async getDataLength(asPack) {
        const headBytes = await Bytes.stringToByteArray(this.head);
        const dataLen = await super.getDataLength(asPack) + 1 + headBytes.length;

        if (this.typeBytes[3] >= 0) {
            return dataLen + R_Send.BASE_LENGTH + this.data.length;
        } else {
            return dataLen;
        }
    }

    // return : number
    getFee() {
        return this.feePow;
    }

    // return : BigDecimal 
    getFeeBig(){
        return this.fee;
    }

    // withSign: boolean, releaserReference: number | null
    // return : Promise<Int8Array>
    async toBytes(withSign, releaserReference = null) {
        const data = new DataWriter();
        data.set(await super.toBytes(withSign, releaserReference));

        // WRITE HEAD
        const headBytes = await Bytes.stringToByteArray(this.head);
        //HEAD SIZE
        data.setNumber(headBytes.length);
        //HEAD
        data.set(headBytes);

        if (this.data !== null) {
            //WRITE DATA SIZE
            //console.log({ data: this.data });
            const dataSizeBytes = await Bytes.intToByteArray(this.data.length);
            data.set(dataSizeBytes);

            //WRITE DATA
            data.set(this.data);

            //WRITE ENCRYPTED
            data.set(this.encrypted);

            //WRITE ISTEXT
            data.set(this.isText);
        }
        return data.data;
    }
}
