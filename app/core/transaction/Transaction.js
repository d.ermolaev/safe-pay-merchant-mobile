import { CoreCrypto } from '../Crypto';
import PublicKeyAccount from '../account/PublicKeyAccount';
import { BlockChain } from '../BlockChain';
import { DataWriter } from '../DataWriter';
import { Bytes } from '../Bytes';
import { BigDecimal } from '../BigDecimal';
import { signMessage } from '../EraChainCrypto';

export class Transaction {
    
    static CERTIFY_PUB_KEYS_TRANSACTION = 36;

    static SIGNATURE_LENGTH = CoreCrypto.SIGNATURE_LENGTH;

    static TIMESTAMP_LENGTH = 8;

    static REFERENCE_LENGTH = Transaction.TIMESTAMP_LENGTH;

    static ISSUE_PERSON_TRANSACTION = 24;

    static SEND_ASSET_TRANSACTION = 31;

    static KEY_LENGTH = 8;

    static FEE_POWER_LENGTH = 1;

    static TYPE_LENGTH = 4;

    static CREATOR_LENGTH = PublicKeyAccount.PUBLIC_KEY_LENGTH;

    static BASE_LENGTH = Transaction.TYPE_LENGTH + Transaction.FEE_POWER_LENGTH + Transaction.REFERENCE_LENGTH + Transaction.TIMESTAMP_LENGTH + Transaction.CREATOR_LENGTH + Transaction.SIGNATURE_LENGTH;

    static BASE_LENGTH_AS_PACK = Transaction.TYPE_LENGTH + Transaction.CREATOR_LENGTH + /*REFERENCE_LENGTH*/ +Transaction.SIGNATURE_LENGTH;

    static IS_TEXT_LENGTH = 1;

    static ENCRYPTED_LENGTH = 1;

    static DATA_SIZE_LENGTH = 4;

    signature; //Int8Array;

    // typeBytes: Int8Array, 
    // typeName: string, 
    // creator: PrivateKeyAccount, 
    // feePow: number, 
    // timestamp: number, 
    // reference: number
    constructor(typeBytes, typeName, creator, feePow, timestamp, reference) {
        this.typeBytes = typeBytes;
        this.TYPE_NAME = typeName;
        this.creator = creator;
        //this.props = props;
        this.timestamp = timestamp;
        this.reference = reference;

        if (feePow < 0) {
            feePow = 0;
        } else if (feePow > BlockChain.FEE_POW_MAX) {
            feePow = BlockChain.FEE_POW_MAX;
        }
        this.feePow = feePow;
    }

    // return : Int8Array
    get Signature() {
        return this.signature;
    }

    // creator: PrivateKeyAccount, 
    // asPack: boolean
    // port: from stores.nodesStore.currentNode.port
    // return : Promise<void>
    async sign(creator, asPack, port) {
        // use this.reference in any case and for Pack too
        // but not with SIGN
        const data = new DataWriter();
        data.set(await this.toBytes(false, null));

        // all test a not valid for main test
        // all other network must be invalid here!
        //Если не ходят тразакции то возможно неверно указан порт. сейчас ок высчитывается при добавлении новой ноды как порт -1
        //console.log({ port });
        data.set(await Bytes.intToByteArray(port));
        this.signature = signMessage(data.data, creator.getSecretKey());
        if (!asPack) {
            // need for recalc! if not as a pack
            await this.calcFee();
        }
    }

    // withSign: boolean, releaserReference: number | null
    // return : Promise<Int8Array>
    async toBytes(withSign, releaserReference) {
        const asPack = releaserReference !== null;
        const data = new DataWriter();

        //WRITE TYPE
        data.set(this.typeBytes);

        if (!asPack) {
            //WRITE TIMESTAMP
            //[0, 0, 0, 0, 41, 0, 16, -113] iphone 5
            //[0, 0, 1, 108, 41, 5, -92, 18] 
            /*
                QRSP timestamp 1564056617948
                Bytes.js:33 QRSP longToByteArray 1564056617948
                Bytes.js:34 QRSP long.toFixed(0) 1564056617948
                Transaction.js:107 QRSP timestampBytes (8)[0, 0, 1, 108, 41, 10, 3, -36]

                QRSP timestamp 1564057001103
                Bytes.js:33 QRSP longToByteArray 1564057001103
                Bytes.js:34 QRSP long.toFixed(0) 1564057001103
                Transaction.js:114 QRSP timestampBytes (8)[0, 0, 0, 0, 41, 15, -36, -113]
            */

            //console.log('QRSP timestamp', this.timestamp);
            let timestampBytes = await Bytes.longToByteArray(this.timestamp);
            //console.log('QRSP timestampBytes', timestampBytes);
            timestampBytes = Bytes.ensureCapacity(timestampBytes, Transaction.TIMESTAMP_LENGTH, 0);
            //console.log('QRSP Transaction.TIMESTAMP_LENGTH', Transaction.TIMESTAMP_LENGTH);
            //console.log('QRSP ensureCapacity timestampBytes', timestampBytes);
            data.set(timestampBytes);
        }

        //WRITE REFERENCE - in any case as Pack or not
        if (this.reference !== null) {
            // NULL in imprints
            let referenceBytes = await Bytes.longToByteArray(this.reference);
            referenceBytes = Bytes.ensureCapacity(referenceBytes, Transaction.REFERENCE_LENGTH, 0);
            data.set(referenceBytes);
        }

        //WRITE CREATOR
        data.set(await this.creator.getPublicKey().publicKey);

        //WRITE FEE POWER
        if (!asPack) {
            const feePowBytes = new Int8Array([0]);
            feePowBytes[0] = this.feePow;
            data.set(feePowBytes);
        }

        //SIGNATURE
        if (withSign) {
            data.set(this.signature);
        }

        return data.data;
    }

    // return : Promise<number>
    async calcBaseFee() {
        return this.calcCommonFee();
    }

    // return : Promise<void>
    async calcFee() {
        const fee = new BigDecimal(await this.calcBaseFee())
            .setScale(8, BigDecimal.ROUND_UP);

        if (this.feePow > 0) {
            this.fee = fee
                .multiply(new BigDecimal(BlockChain.FEE_POW_BASE).pow(this.feePow))
                .setScale(8, BigDecimal.ROUND_UP);
        } else {
            this.fee = fee;
        }
    }

    // return : Promise<number>
    async calcCommonFee() {
        return await this.getDataLength(false) * BlockChain.FEE_PER_BYTE;
    }

    // abstract
    // asPack: boolean
    // return : Promise<number>
    async getDataLength(asPack = false) {
        if (asPack) return 0;
        return 0;
    }
}
