import { Issue_ItemRecord } from './Issue_ItemRecord';
import { Transaction } from './Transaction';


export class IssuePersonRecord extends Issue_ItemRecord {
    static TYPE_ID = Transaction.ISSUE_PERSON_TRANSACTION;
    static NAME_ID = 'Issue Person';

    // creator: PrivateKeyAccount, 
    // person: PersonCls, 
    // feePow: number, 
    // timestamp: number, 
    // reference: number
    constructor(creator, person, feePow, timestamp, reference) {
        super(new Int8Array([IssuePersonRecord.TYPE_ID, 0, 0, 0]), IssuePersonRecord.NAME_ID, creator, person, feePow, timestamp, reference);
    }

}
