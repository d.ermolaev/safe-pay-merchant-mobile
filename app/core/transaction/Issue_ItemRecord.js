import { Transaction } from './Transaction';
import { DataWriter } from '../DataWriter';

export class Issue_ItemRecord extends Transaction {

    // typeBytes: Int8Array, 
    // NAME_ID: string, 
    // creator: PrivateKeyAccount, 
    // item: ItemCls, 
    // feePow: number, 
    // timestamp: number, 
    // reference: number
    constructor(typeBytes, NAME_ID, creator, item, feePow, timestamp, reference) {
        super(typeBytes, NAME_ID, creator, feePow, timestamp, reference);
        this.item = item;
    }

    // asPack: boolean
    // return : Promise<number>
    async getDataLength(asPack) {
        // not include item reference
        if (asPack) {
            return Transaction.BASE_LENGTH_AS_PACK + await this.item.getDataLength(false);
        } else {
            return Transaction.BASE_LENGTH + await this.item.getDataLength(false);
        }
    }

    // withSign: boolean, 
    // releaserReference: number | null
    // return : Promise<Int8Array>
    async toBytes(withSign, releaserReference = null) {
        const dataWriter = new DataWriter();
        dataWriter.set(await super.toBytes(withSign, releaserReference));
        dataWriter.set(await this.item.toBytes(false, false));
        return dataWriter.data;
    }
}
