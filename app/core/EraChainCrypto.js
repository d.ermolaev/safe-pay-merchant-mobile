
import { Base58 } from './crypt/Base58';
import { sign, scalarMult } from 'tweetnacl';
import { Buffer } from 'buffer';
import md5 from 'md5';
import SHA256 from './crypt/SHA256';
import RIPEMD160 from './crypt/RIPEMD160';
import { wordsToByteArray, prepareAfterDecrypt } from './crypt/convert';
import { Bytes } from './Bytes';

const ed2curve = require('./crypt/ed2curve');
const CryptoJS = require('crypto-js');

const ADDRESS_VERSION = 15; 

const int32ToBytes = (word) => {
    let byteArray = [];
    for (let b = 0; b < 32; b += 8) {
        byteArray.push((word >>> (24 - b % 32)) & 0xFF);
    }
    return byteArray;
};

const generateAccountSeed = async (seed, nonce, returnBase58) => {
    if(typeof(seed) === 'string') {
        const pwd = seed.replace(/-/g, '').replace(/0/g, '');
        seed = await Base58.decode(pwd);
    }
    let nonceBytes = int32ToBytes(nonce);
    let resultSeed = new Uint8Array();
    resultSeed = appendBuffer(resultSeed, nonceBytes);
    resultSeed = appendBuffer(resultSeed, seed);
    resultSeed = appendBuffer(resultSeed, nonceBytes);
    if (returnBase58) {
        return Base58.encodeSync(SHA256.digest(SHA256.digest(resultSeed)));
    } else {
        return SHA256.digest(SHA256.digest(resultSeed));
    }
};

// return : SignKeyPair
const generateKeys = (seed) => {
    return sign.keyPair.fromSeed(seed);
};

// buffer1, buffer2: Int8Array
const appendBuffer = (buffer1, buffer2) => {
    buffer1 = new Int8Array(buffer1);
    buffer2 = new Int8Array(buffer2);
    const tmp = new Int8Array(buffer1.byteLength + buffer2.byteLength);
    tmp.set(buffer1, 0);
    tmp.set(buffer2, buffer1.byteLength);
    return tmp;
};

// publicKey: Int8Array
// return : Promise<string>
const getAccountAddressFromPublicKey = async (publicKey) => {
    // SHA256 PUBLICKEY FOR PROTECTION
    let publicKeyHash = SHA256.digest(publicKey);

    // RIPEMD160 TO CREATE A SHORTER ADDRESS
    const ripEmd160 = new RIPEMD160();
    publicKeyHash = ripEmd160.digest(publicKeyHash);

    // CONVERT TO LIST
    let addressList = new Int8Array(0);

    //ADD VERSION BYTE
    const versionByte = new Int8Array([ADDRESS_VERSION]);
    addressList = appendBuffer(addressList, versionByte);

    addressList = appendBuffer(addressList, publicKeyHash);

    //GENERATE CHECKSUM
    const checkSum = SHA256.digest(SHA256.digest(addressList));

    //ADD FIRST 4 BYTES OF CHECKSUM TO ADDRESS
    addressList = appendBuffer(addressList, new Int8Array([checkSum[0]]));
    addressList = appendBuffer(addressList, new Int8Array([checkSum[1]]));
    addressList = appendBuffer(addressList, new Int8Array([checkSum[2]]));
    addressList = appendBuffer(addressList, new Int8Array([checkSum[3]]));

    //BASE58 ENCODE ADDRESS
    return await Base58.encode(addressList);
};

// return : Promise<string
const createAddress = async () => {
    const keys = generateKeys();
    return getAccountAddressFromPublicKey(new Int8Array(keys.publicKey));
};

// message: Int8Array, secretKey: Int8Array
// return : Int8Array
const signMessage = (message, secretKey) => {
    const result = sign.detached(new Uint8Array(message), new Uint8Array(secretKey));
    return new Int8Array(result);
};

// message: Int8Array, 
// result: Int8Array, 
// publicKey: Int8Array
// return : boolean
const verifySignMessage = (message, result, publicKey) => {
    return sign.detached.verify(new Uint8Array(message), new Uint8Array(result), new Uint8Array(publicKey));
};

// publicKey: Int8Array | string
// privateKey: Int8Array | string
// result : Int8Array
const getPassword = async (publicKey, privateKey) => {
    const key = typeof(publicKey) === 'string'?new Buffer(await Base58.decode(publicKey)):new Buffer(publicKey); 
    const secret = typeof(privateKey) === 'string'?new Buffer(await Base58.decode(privateKey)):new Buffer(privateKey); 
    const theirDHPublicKey = ed2curve.convertPublicKey(key);
    const myDHSecretKey = ed2curve.convertSecretKey(secret);
    const password = SHA256.digest(scalarMult(myDHSecretKey, theirDHPublicKey));
    return password;
};

// message: string
// publicKey: Int8Array
// privateKey: Int8Array
// result : Int8Array
const encryptMessage = async (message, publicKey, privateKey) => {
    try {
        const iv = CryptoJS.enc.Hex.parse('06040308010201020702030805070101');
        const password = await getPassword(publicKey, privateKey);
        const sharedKey = CryptoJS.lib.WordArray.create(password);
    
        const encrypted = CryptoJS.AES.encrypt(message, sharedKey, { iv });
        const int8Array0 = wordsToByteArray(encrypted.ciphertext);
        const int8Array = new Int8Array([ 0x01, ...int8Array0]);
        return int8Array;
    } catch (e) {
        return false;
    }
};

// encryptedMessage: Int8Array
// publicKey: Int8Array
// privateKey: Int8Array
// result : string
const decryptMessage = async (encryptedMessage, publicKey, privateKey) => {
    try {
        const iv = CryptoJS.enc.Hex.parse('06040308010201020702030805070101');
        const password = await getPassword(publicKey, privateKey);
        const sharedKey = CryptoJS.lib.WordArray.create(password);

        const arrayMessage = await Base58.decode(encryptedMessage);
        const message = arrayMessage.slice(1);
        const words = CryptoJS.lib.WordArray.create(message);
        
        const decrypted = CryptoJS.AES.decrypt({ ciphertext: words }, sharedKey, { iv });
        const jsonString = await Bytes.stringFromByteArray(prepareAfterDecrypt(wordsToByteArray(decrypted)));
        return jsonString;
    } catch (e) {
        return false;
    }
};

const testCrypt = async () => {

    const secret1 = '2RSehSu6szKbhEap46fhLN4s2BCfD1N2PLqbKnVWCbPvzF9JRBJs8sbQFqqaGLs4x4TZZV8hD5irs2RiT7gsGVjq';
    const public1 = '3jPhyhgqry3J95PHRLmH27UhKvn93y3x6Z748uv2vKoH';
    const secret2 = 'kiAeqbBDqQ4Qa5X1hdpuWnzmX3GW3i9Bwk81VmNeFkXWhypZnxCYGnPDdTbbBFkgZHQkADowQhatRqYAT72nYrf';
    const public2 = '6jt933MGNLo8DFEtJX1HXsfu38h21PyWpBRstaNM1Q5q';
    const msg = JSON.stringify({name:1});

    const encrypted = await encryptMessage(msg, public2, secret1);
    const str = await Base58.encode(encrypted);
    const decrypted = await decryptMessage(str, public1, secret2);
    console.log({ decrypted: decrypted });
};

// publicKey: string | Int8Array
// return : Promise<string>
const getAddressByPublicKey = (publicKey) => {
    if (typeof publicKey === 'string') {
        publicKey = Base58.decode(publicKey);
    }
    publicKey = new Int8Array(publicKey);
    return getAccountAddressFromPublicKey(publicKey);
};

// secretKey: string | Int8Array
// return : Promise<string>
const getAddressBySecretKey = async (secretKey) => {
    const keys = await getKeyPair(secretKey);
    return getAddressByPublicKey(new Int8Array(keys.publicKey));
};

// secretKey: string | Int8Array
// return : Int8Array
const getPublicKeyBySecretKey = async (secretKey) => {
    const keys = await getKeyPair(secretKey);
    return new Int8Array(keys.publicKey);
};

const getKeyPair = async (secretKey) => {
    if (typeof secretKey === 'string') {
        secretKey = await Base58.decode(secretKey);
    }
    return sign.keyPair.fromSecretKey(new Uint8Array(secretKey));
};

// s : string
// return : hex string
const hashString = (s) => {
    return md5(s);
};

export { 
    testCrypt,
    hashString,
    generateKeys,
    generateAccountSeed,
    getAccountAddressFromPublicKey,
    createAddress,
    signMessage,
    verifySignMessage,
    getAddressBySecretKey,
    getPublicKeyBySecretKey,
    getKeyPair,
    encryptMessage,
    decryptMessage
};

