import { ItemCls } from '../ItemCls';
import { DataWriter } from '../../DataWriter';
import { Bytes } from '../../Bytes';

export class PersonCls extends ItemCls {
    static BIRTHDAY_LENGTH = ItemCls.TIMESTAMP_LENGTH;
    static DEATHDAY_LENGTH = ItemCls.TIMESTAMP_LENGTH;

    //  typeBytes: Int8Array,
    // owner: PublicKeyAccount,
    // name: string,
    // birthday: number,
    // deathday: number,
    // gender: number,
    // race: string,
    // birthLatitude: number,
    // birthLongitude: number,
    // skinColor: string,
    // eyeColor: string,
    // hairColor: string,
    // height: number,
    // icon: Int8Array,
    // image: Int8Array,
    // description: string
    constructor(typeBytes,
        owner,
        name,
        birthday,
        deathday,
        gender,
        race,
        birthLatitude,
        birthLongitude,
        skinColor,
        eyeColor,
        hairColor,
        height,
        icon,
        image,
        description) {
        super(typeBytes, owner, name, icon, image, description);

        this.birthday = birthday;
        this.deathday = deathday;
        this.gender = gender;
        this.race = race;
        this.birthLatitude = birthLatitude;
        this.birthLongitude = birthLongitude;
        this.skinColor = skinColor;
        this.eyeColor = eyeColor;
        this.hairColor = hairColor;
        this.height = height;
    }

    // includeReference: boolean, 
    // forOwnerSign: boolean
    // return : Promise<Int8Array>
    async toBytes(includeReference, forOwnerSign) {
        const dataWriter = new DataWriter();
        dataWriter.set(await super.toBytes(includeReference, forOwnerSign));

        // WRITE BIRTHDAY
        await this.birthdayToBytes(dataWriter);

        // WRITE DEATHDAY
        await this.deathdayToBytes(dataWriter);

        // WRITE GENDER
        this.genderToBytes(dataWriter);

        // WRITE RACE
        await this.raceToBytes(dataWriter);

        //WRITE BIRTH_LATITUDE
        await this.birthLatitudeToBytes(dataWriter);

        //WRITE BIRTH_LONGITUDE
        await this.birthLongitudeToBytes(dataWriter);

        //WRITE SKIN COLOR
        await this.skinColorToBytes(dataWriter);

        //WRITE EYE COLOR
        await this.eyeColorToBytes(dataWriter);

        //WRITE HAIR COLOR
        await this.hairColorToBytes(dataWriter);

        //WRITE HEIGHT
        this.heightToBytes(dataWriter);

        return dataWriter.data;
    }

    // dataWriter: DataWriter
    // return : Promise<void>
    async birthdayToBytes(dataWriter) {
        const dayBytes = await Bytes.longToByteArray(this.birthday);
        const bytes = Bytes.ensureCapacity(dayBytes, PersonCls.BIRTHDAY_LENGTH, 0);
        dataWriter.set(bytes);
    }

    // dataWriter: DataWriter
    // return : Promise<void>
    async deathdayToBytes(dataWriter) {
        const dayBytes = await Bytes.longToByteArray(this.deathday);
        const bytes = Bytes.ensureCapacity(dayBytes, PersonCls.DEATHDAY_LENGTH, 0);
        dataWriter.set(bytes);
    }

    // dataWriter: DataWriter
    genderToBytes(dataWriter) {
        dataWriter.setNumber(this.gender);
    }

    // dataWriter: DataWriter
    // return : Promise<void>
    async raceToBytes(dataWriter) {
        const bytes = await Bytes.stringToByteArray(this.race);
        dataWriter.setNumber(bytes.length);
        dataWriter.set(bytes);
    }

    // dataWriter: DataWriter
    // return : Promise<void>
    async birthLatitudeToBytes(dataWriter) {
        const bytes = await Bytes.floatToByteArray(this.birthLatitude);
        dataWriter.set(bytes);
    }

    // dataWriter: DataWriter
    // return : Promise<void>
    async birthLongitudeToBytes(dataWriter) {
        const bytes = await Bytes.floatToByteArray(this.birthLongitude);
        dataWriter.set(bytes);
    }

    // dataWriter: DataWriter
    // return : Promise<void>
    async skinColorToBytes(dataWriter) {
        const bytes = await Bytes.stringToByteArray(this.skinColor);
        dataWriter.setNumber(bytes.length);
        dataWriter.set(bytes);
    }

    // dataWriter: DataWriter
    // return : Promise<void>
    async eyeColorToBytes(dataWriter) {
        const bytes = await Bytes.stringToByteArray(this.eyeColor);
        dataWriter.setNumber(bytes.length);
        dataWriter.set(bytes);
    }

    // dataWriter: DataWriter
    // return : Promise<void>
    async hairColorToBytes(dataWriter) {
        const bytes = await Bytes.stringToByteArray(this.hairColor);
        dataWriter.setNumber(bytes.length);
        dataWriter.set(bytes);
    }

    // dataWriter: DataWriter
    heightToBytes(dataWriter) {
        dataWriter.setNumber(this.height);
    }
}
