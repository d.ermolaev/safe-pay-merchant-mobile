

const regexpPhone = new RegExp(/^\+?[^0][0-9]{0,1}[0-9]{10}$/);
const regexpPhoneWithoutCountry = new RegExp(/([0-9]{10})$/); // не менять!!!

const regexpRecipient = new RegExp(/^([\$\%\*\+\.\/\:\" a-zA-Z0-9А-Яа-я]{1,160})/);
const regexpBank = new RegExp(/^([\$\%\*\+\.\/\:\" a-zA-Z0-9А-Яа-я]{1,45})/);
const regexpAcct = new RegExp(/^([^0][0-9]{19})/);
const regexpCAcct = new RegExp(/^([0-9]{1,20})/);
const regexpBIK = new RegExp(/^([0-9]{9})/);
const regexpINN = new RegExp(/^([^0][0-9]{9,11})/);
const regexpKPP = new RegExp(/^([^0][0-9]{8})/);

export const validatePhone = (phone) => {
    return regexpPhone.test(phone);
};

export const validateEmptyPhone = (phone) => {
    if (phone.length === 0) {
        return true;
    }
    return regexpPhone.test(phone);
};

export const numberPhone = (phone) => {
    const arr = regexpPhoneWithoutCountry.exec(phone);
    if (arr && arr.length === 2) {
        return arr[1];
    } 
    return phone;
};

export const validateRecipient = (val) => {
    return regexpRecipient.test(val);
};

export const getRecipient = (val) => {
    const arr = regexpRecipient.exec(val);
    if (arr && arr.length === 2) {
        return arr[1];
    } 
    return val;
};

export const validateBank = (val) => {
    return regexpBank.test(val);
};

export const getBank = (val) => {
    const arr = regexpBank.exec(val);
    if (arr && arr.length === 2) {
        return arr[1];
    } 
    return val;
};

export const validateAcct = (val) => {
    return regexpAcct.test(val);
};

export const getAcct = (val) => {
    const arr = regexpAcct.exec(val);
    if (arr && arr.length === 2) {
        return arr[1];
    } 
    return val;
};

export const validateCAcct = (val) => {
    return regexpCAcct.test(val);
};

export const getCAcct = (val) => {
    const arr = regexpCAcct.exec(val);
    if (arr && arr.length === 2) {
        return arr[1];
    } 
    return val;
};

export const validateBIK = (val) => {
    return regexpBIK.test(val);
};

export const getBIK = (val) => {
    const arr = regexpBIK.exec(val);
    if (arr && arr.length === 2) {
        return arr[1];
    } 
    return val;
};

export const validateINN = (val) => {
    return regexpINN.test(val);
};

export const getINN = (val) => {
    const arr = regexpINN.exec(val);
    if (arr && arr.length === 2) {
        return arr[1];
    } 
    return val;
};

export const validateKPP = (val) => {
    return regexpKPP.test(val);
};

export const getKPP = (val) => {
    const arr = regexpKPP.exec(val);
    if (arr && arr.length === 2) {
        return arr[1];
    } 
    return val;
};