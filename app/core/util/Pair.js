export class Pair {

    a = null;
    b = null;

    constructor(a = null, b = null) {
        if (a) {
            this.a = a;
        }
        if (b) {
            this.b = b;
        }
    }

    setA(a) {
        this.a = a;
    }

    getA() {
        return this.a;
    }

    setB(b) {
        this.b = b;
    }

    getB() {
        return this.b;
    }
}
