import { nodeBaseRequest } from './NodeBaseRequest';

export const broadcastRequest = ({ baseUrl, transaction }) => {
    const uri = encodeURI(`${baseUrl}/broadcasttelegram/${transaction}`);
    //console.log('QRSP', uri);
    const headers = {
        'Content-Type': 'text/plain',
    };
    return nodeBaseRequest({ method: 'GET', url: uri, headers, body: transaction });
};
