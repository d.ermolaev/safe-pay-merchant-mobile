import RNFetchBlob from 'react-native-fetch-blob';

export const nodeBaseRequest = ({ method = 'GET', url, headers = null, body = null }) => {
    console.log(url);
    return RNFetchBlob
        .config({timeout: 20000})
        .fetch(method, url, headers, body)
        .then(r => {
            console.log('QRSP response', r);
            if (r.respInfo.status < 200 || r.respInfo.status >= 300) {
                throw new Error(r.respInfo.status.toString());
            }
            const data = r.json();
            console.log('QRSP', data);
            if (data.error) {
                throw new Error(data.error);
            }
            return data;
        })
        .catch(e => {
            console.log('QRSP', e);
            throw new Error(e);
        });
};

