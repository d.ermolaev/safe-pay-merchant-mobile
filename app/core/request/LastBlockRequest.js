import { baseRequest } from './BaseRequest';

export const lastBlockRequest = ({ baseUrl }) => {
    const uri = encodeURI(`${baseUrl}/height`);
    return baseRequest({ method: 'GET',  url: uri, headers: null, body: null });
};