import { nodeBaseRequest } from './NodeBaseRequest';

export const unconfirmedRequest = ({ baseUrl, address, type, count, timestamp }) => {
    const uri = encodeURI(`${baseUrl}/apirecords/unconfirmedincomes/${address}/?type=${type}&timestamp=${timestamp}&count=${count}&descending=true`);
    return nodeBaseRequest({ url: uri });
};
