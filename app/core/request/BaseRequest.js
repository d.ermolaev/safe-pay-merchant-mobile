import RNFetchBlob from 'react-native-fetch-blob';

export const baseRequest = ({ method, url, headers, body }) => {
    return RNFetchBlob
        .config({timeout: 20000})
        .fetch(method, url, headers, body)
        .then(r => {
            if (r.respInfo.status < 200 || r.respInfo.status >= 300) {
                throw new Error(r.respInfo.status.toString());
            }
            return r;
        })
        .catch(e => {
            throw new Error(e);
        });
};

