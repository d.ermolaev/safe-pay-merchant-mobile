import { nodeBaseRequest } from './NodeBaseRequest';

export const incomingRequest = ({ baseUrl, address, type, fromBlock }) => {
    const uri = encodeURI(`${baseUrl}/apirecords/incomingfromblock/${address}/${fromBlock}?type=${type}`);
    return nodeBaseRequest({ method: 'GET', url: uri });
};