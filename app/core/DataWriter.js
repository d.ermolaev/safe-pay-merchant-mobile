import { Bytes } from './Bytes';

export class DataWriter {

    _data = [];

    // return : Int8Array
    get data() {
        const dataLength = this._data.reduce((length, d) => length + d.length, 0);
        const data = new Int8Array(dataLength);
        let offset = 0;
        this._data.forEach(d => {
            data.set(d, offset);
            offset += d.length;
        });
        return data;
    }

    // data: Int8Array
    // return : DataWriter
    set (data) {
        this._data.push(data);
        return this;
    }

    // data: string
    // return : Promise<DataWriter>
    async setString(data) {
        this.set(new Int8Array(await Bytes.stringToByteArray(data)));
        return this;
    }

    // data: number
    // return : DataWriter
    setNumber(data) {
        this.set(new Int8Array([data]));
        return this;
    }
}
