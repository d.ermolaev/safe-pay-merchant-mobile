import SHA256 from '../SHA256';
import { fixtures } from './fixtures';

describe('sha256', () => {
    it('hash', () => {
        expect(SHA256.hash(fixtures.sha256Message)).toMatch(fixtures.sha256Hash);
    });

    it('digest', () => {
        expect(SHA256.digest(fixtures.sha256Message).toString()).toMatch(fixtures.sha256Digest.toString());
    });
});
