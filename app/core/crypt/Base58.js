import { NativeModules } from 'react-native';

const RNBase58 = NativeModules.ChainUtilities;

export class Base58 {
    
    // input: Int8Array
    // return : Promise<string>
    static async encode(input) {
        return RNBase58.base58encode(Array.from(input));
    }

    // input: Int8Array
    // return : string
    static encodeSync(input) {
        return RNBase58.base58encode(Array.from(input));
    }

    // input: string
    // return : Promise<Int8Array>
    static async decode(input) {
        const data = await RNBase58.base58decode(input);
        //console.log(`Base58 decode: ${data}`);
        return new Int8Array(data);
    }
}

