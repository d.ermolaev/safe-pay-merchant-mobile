export class BigDecimal {
    static ROUND_UP = 1;
    static ROUND_DOWN = 2;
    scale = 8;

    // num: number | string, scale?: number
    constructor(num, scale = undefined) {
        if (typeof num === 'string') {
            num = parseFloat(num);
        }
        if (scale !== undefined) {
            this.scale = scale;
        }
        this._num = num;
    }

    // return : number
    get intCompact() {
        return this._num * Math.pow(10, this.scale);
    }

    // return : string
    get stringCache() {
        const float = this.intCompact.toString();
        let repeat = 0;
        if (this.scale > float.length) {
            repeat = this.scale - float.length;
        }
        const zeros = '0'.repeat(repeat);

        return `${Math.floor(this._num)}.${zeros}${float}`;
    }

    // return : number
    get num() {
        return this._num;
    }


    // x: number | BigDecimal
    // return : BigDecimal
    multiply(x) {
        return new BigDecimal(this.num * this._numValue(x));
    }

    // newScale: number = this.scale, roundingMode: number = BigDecimal.ROUND_DOWN
    // return : BigDecimal
    setScale(newScale = this.scale, roundingMode = BigDecimal.ROUND_DOWN) {
        const precision = Math.pow(10, newScale + 1);
        let value = this.num * precision;
        value = roundingMode === BigDecimal.ROUND_UP ? Math.ceil(value) : Math.floor(value);
        value = value / precision;
        return new BigDecimal(value);
    }


    // x: number | BigDecimal
    // return : BigDecimal
    pow(x) {
        return new BigDecimal(Math.pow(this.num, this._numValue(x)));
    }

    // return: number
    valueOf() {
        return this.num;
    }

    // return : number
    unscaledValue() {
        return this.intCompact;
    }

    // x: number | BigDecimal
    // return : number
    _numValue(x) {
        if (x instanceof BigDecimal) {
            x = x.num;
        }
        return x;
    }
}
