import React, { Component } from 'react';
import { Text, View } from 'react-native';
import { 
    LargeLogo
} from '../../components';
import { primaryColorLight, styles } from '../../config/styles';
import { Btn } from '../../components/Btn/Btn';
import { strings } from '../../config/localize';
import * as navigationActions from '../../actions/navigationActions';

export default class CreateWallet extends Component {

    //noinspection JSUnusedGlobalSymbols
    static navigationOptions = {
        header: false,
        headerLeft: null,
    };

    navigateToLicense = () => {
        navigationActions.navigate('License');
    };

    navigateToImport = () => {
        navigationActions.navigate('Import');
    };

    render() {
        return (
            <View style={styles.container}>
                <View style={styles.login.containerTop}>
                    <LargeLogo/>
                </View>

                <View style={styles.login.containerBottom}>
                    <Text style={styles.login.message}>{strings.message_5}</Text>
                    <Btn title={strings['Restore old']} fill={true} onPress={this.navigateToImport}
                        fillColor={ primaryColorLight }
                        customBtnStyles={{marginBottom: 8}}/>
                    <Btn title={strings['Create new']} fill={true} onPress={this.navigateToLicense}/>
                </View>
            </View>
        );
    }
}

