import React from 'react';
import PropTypes from 'prop-types';
import { Clipboard, Image, Text, TextInput, TouchableOpacity, View } from 'react-native';
import { 
    LargeLogo,
    Loading,
    BackHandled,
    BackBtn,
    ImageResources,
    Toaster
} from '../../components';
import { styles, textPlaceholderColor } from '../../config/styles';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
// import {settings} from '../core/settings/AppSettings';
import Toast from 'react-native-simple-toast';
import { 
    getKeyPair,
    getAccountAddressFromPublicKey,
} from '../../core/EraChainCrypto';

// import { PrivateKeyAccount } from '../../core/account/PrivateKeyAccount';
import { Base58 } from '../../core/crypt/Base58';
import { strings } from '../../config/localize';
import * as navigationActions from '../../actions/navigationActions';
import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect';
import * as walletActions from '../../actions/walletActions';
import { makeSelectAuth } from '../../selectors/dbSelectors';

class Import extends BackHandled {

    //noinspection JSUnusedGlobalSymbols
    static navigationOptions = {
        header: false,
        headerLeft: null,
    };

    pasteFromClipboardSecret = async () => {
        const content = await Clipboard.getString();
        const keyBytes = await Base58.decode(content);
        const valid = keyBytes.length === 64;
        //const valid = await PrivateKeyAccount.validate(content);
        this.setState({secretKey: content, validKey: valid});
        Toast.show(`${strings['Copied from clipboard']} `);
    };

    done = async () => {
        this.setState({loading: true});
        try {
            //const keys = generateKeys(seed);
            const keyPair = await getKeyPair(this.state.secretKey);
            this.props.setKeyPair(keyPair);
            const address = await getAccountAddressFromPublicKey(keyPair.publicKey);
            this.props.setAddress(address);
            //console.log('QRSP', { secretKey: this.state.secretKey, keys: await keyPair.toString(), address });
            this.setState({loading: false}); 
            const { pin, hash } = this.props.auth;
            if (pin && hash) {
                this.props.createWallet();
                this.back();
            } else {
                navigationActions.navigate('ImportPin');
            }
        } catch (e) {
            Toaster.error(e);
            this.setState({loading: false});
        }
    };

    // props: IImportProps, 
    // context: any
    constructor(props, context) {
        super(props, context);

        this.state = {loading: false, secretKey: '', seed: '', validKey: false};
    }

    render() {
        const continueButton = this.state.validKey ? (
            <View style={styles.register.bottomContainer}>
                <TouchableOpacity onPress={this.done} style={styles.register.buttonContinue} activeOpacity={0.9}>
                    <Text style={styles.register.buttonText}>{`${strings['Continue']} `}</Text>
                </TouchableOpacity>
            </View>) :
            <View style={styles.register.bottomContainer}>
                <View style={{...styles.register.buttonContinue, ...{backgroundColor: '#D1D5DB'}}}>
                    <Text style={styles.register.buttonNextDisabledText}>{`${strings['Continue']} `}</Text>
                </View>
            </View>;

        return (
            <View style={styles.container}>
                <Loading state={this.state.loading}/>

                <KeyboardAwareScrollView viewIsInsideTabBar={true} alwaysBounceVertical={false}>
                    <View style={styles.register.top}>
                        <LargeLogo>
                            <BackBtn float={true} onPress={this.back}/>
                        </LargeLogo>
                    </View>
                    <View style={{...styles.register.bottom, ...{paddingVertical: 31}}}>
                        <View style={styles.register.formRow}>
                            <Text style={styles.register.message}>{`${strings['Insert secret key']} `}</Text>
                        </View>
                        <View 
                            style={[
                                styles.register.border, 
                                {
                                    flex: 1,
                                    flexDirection: 'row',
                                    paddingVertical: 8,
                                }]}
                        >
                            <TextInput
                                placeholder={'...'}
                                placeholderTextColor={textPlaceholderColor}
                                style={[
                                    styles.register.textEdit,
                                    {
                                        flex: 1,
                                    }
                                ]}
                                underlineColorAndroid={'transparent'}
                                value={this.state.secretKey}
                                editable={false}
                            />

                            <TouchableOpacity 
                                activeOpacity={0.9} 
                                onPress={this.pasteFromClipboardSecret}
                                style={styles.register.button1}
                            >
                                <Image 
                                    style={{ width: 28, height: 30 }}
                                    source={ImageResources.icon_insert}
                                />
                            </TouchableOpacity>
                        </View>
                    </View>
                </KeyboardAwareScrollView>
                {continueButton}
            </View>
        );
    }
}

Import.propTypes = {
    setKeyPair: PropTypes.func,
    setAddress: PropTypes.func,
    auth: PropTypes.object,
    createWallet: PropTypes.func,
};

const mapStateToProps = createStructuredSelector({
    auth: makeSelectAuth(),
});

function mapDispatchToProps(dispatch) {
    return {
        setKeyPair: (keys) => dispatch(walletActions.actionSetKeyPair(keys)),
        setAddress: (address) => dispatch(walletActions.actionSetAddress(address)),
        createWallet: () => dispatch(walletActions.actionCreateWallet()),
    };
}

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(Import);