import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { View } from 'react-native';
import { 
    DoublePinInput,
    Loading
} from '../../components';
import { createStructuredSelector } from 'reselect';
import { styles } from '../../config/styles';
import * as navigationActions from '../../actions/navigationActions';
import * as walletActions from '../../actions/walletActions';
import { makeSelectWallet } from '../../selectors/walletSelectors';
import Toast from 'react-native-simple-toast';
import { strings } from '../../config/localize';

class ImportPin extends Component {

    static navigationOptions = {
        header: false,
    };

    componentDidUpdate(prevProps) {
        const { wallet } = prevProps;
        if (wallet.creating && !this.props.wallet.creating && this.props.wallet.keyPair && !this.props.wallet.error) {
            navigationActions.navigateToHome();
        }
        if (!wallet.error && this.props.wallet.error) {
            this.setState({ pin: '' });
            Toast.show(strings['error_19']);
        }
    }
    
    navigateToWallet = async (pin) => {
        this.props.setPin(pin);
        this.props.createWallet();
    };

    render() {
        return (
            <View style={styles.container}>
                <Loading state={this.props.wallet.creating}/>
                <DoublePinInput onComplete={this.navigateToWallet}/>
            </View>
        );
    }
}

ImportPin.propTypes = {
    createWallet: PropTypes.func,
    setPin: PropTypes.func,
    wallet: PropTypes.object,
};

const mapStateToProps = createStructuredSelector({
    wallet: makeSelectWallet(),
});

function mapDispatchToProps(dispatch) {
    return {
        setPin: (pin) => dispatch(walletActions.actionSetPinCode(pin)),
        createWallet: () => dispatch(walletActions.actionCreateWallet()),
    };
}

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(ImportPin);