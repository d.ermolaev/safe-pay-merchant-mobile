import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { ScrollView, Text, TouchableOpacity, View } from 'react-native';
import { styles } from '../../config/styles';
import { 
    LargeLogo,
    Loading,
    Toaster,
    CopySecretKey
} from '../../components';
import { strings } from '../../config/localize';
import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect';
import { makeSelectWallet } from '../../selectors/walletSelectors';
import * as navigationActions from '../../actions/navigationActions';
import KeyPair from '../../core/account/KeyPair';

class Complete extends Component {
    
    static navigationOptions = {
        header: false,
    };

    complete = async () => {
        this.setState({loading: true});
        try {
            navigationActions.navigateToHome();
        } catch (e) {
            this.setState({loading: false});
            Toaster.error(e);
        }
    };
    
    onComplete = (valid) => {
        this.setState({valid: valid});
    };

    constructor(props, context) {
        super(props, context);
        this.state = {
            loading: false, 
            valid: false,
        };
    }

    async componentDidMount() {
        /*
        if (!stores.navigationStore.params.pin) {
            stores.navigationStore.goBack();
            return;
        }
        */
        this.setState({loading: true});
        let keys = new KeyPair(this.props.wallet.keyPair);
        keys = await keys.toString();
        this.setState({loading: false, keys});
    }

    render() {
        const { creating } = this.props.wallet;
        const { keys } = this.state;
        const continueButton = this.state.valid ? (
            <View style={styles.login.bottomContainer}>
                <TouchableOpacity onPress={this.complete} style={styles.register.buttonContinue}
                    activeOpacity={0.9}>
                    <Text style={styles.register.buttonText}>{`${strings['Next']} `}</Text>
                </TouchableOpacity>
            </View>) :
            <View style={styles.login.bottomContainer}>
                <View style={{...styles.register.buttonContinue, ...{backgroundColor: '#D1D5DB'}}}>
                    <Text style={styles.register.buttonNextDisabledText}>{`${strings['Next']} `}</Text>
                </View>
            </View>;

        return (
            <View style={styles.container}>
                <Loading state={creating || this.state.loading}/>

                <View style={styles.login.containerTop}><LargeLogo/></View>

                <View style={styles.login.containerBottomComplete}>
                    <ScrollView>
                        <View style={{paddingHorizontal: 16, paddingBottom: 16}}>
                            <Text style={styles.register.completeBigMessage}>{strings['secret key generated']}</Text>
                            {keys ? <CopySecretKey onComplete={this.onComplete} keys={keys} /> : null}
                        </View>
                    </ScrollView>
                </View>
                {continueButton}
            </View>
        );
    }
}

Complete.propTypes = {
    wallet: PropTypes.object,
    createWallet: PropTypes.func,
};

const mapStateToProps = createStructuredSelector({
    wallet: makeSelectWallet(),
});

export default connect(
    mapStateToProps,
    null
)(Complete);