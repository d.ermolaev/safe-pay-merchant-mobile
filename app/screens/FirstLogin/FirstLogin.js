import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { View } from 'react-native';
import { 
    DoublePinInput,
    Toaster 
} from '../../components';
import { styles } from '../../config/styles';
import * as navigationActions from '../../actions/navigationActions';
import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect';
import { makeSelectWallet } from '../../selectors/walletSelectors';
import * as walletActions from '../../actions/walletActions';
import { 
    generateAccountSeed, 
    generateKeys,
    getAccountAddressFromPublicKey 
} from '../../core/EraChainCrypto';
import uuid from 'react-native-uuid';

class FirstLogin extends Component {
    
    componentDidUpdate(prevProps) {
        if (prevProps.wallet.creating && !this.props.wallet.creating && !this.props.wallet.error) {
            navigationActions.navigate('Complete');
        }
        if (!prevProps.wallet.error && this.props.wallet.error) {
            Toaster.error(this.props.wallet.error);
        }
    }

    static navigationOptions = {
        header: false,
    };

    navigateToComplete = async (pin) => {
        try {
            this.props.setPin(pin);
            const pwd = pin + uuid.v4();
            const seed = await generateAccountSeed(pwd, 0, false);
            this.props.setSeed(seed);
            const keys = generateKeys(seed);
            this.props.setKeyPair(keys);
            const address = await getAccountAddressFromPublicKey(keys.publicKey);
            this.props.setAddress(address);
            this.props.createWallet();
        } catch (e) {
            console.log(e);
            Toaster.error(e);
        }
    };

    render() {
        return (
            <View style={styles.container}>
                <DoublePinInput onComplete={this.navigateToComplete}/>
            </View>
        );
    }
}

FirstLogin.propTypes = {
    setPin: PropTypes.func,
    setSeed: PropTypes.func,
    setKeyPair: PropTypes.func,
    setAddress: PropTypes.func,
    createWallet: PropTypes.func,
    wallet: PropTypes.object,
};

const mapStateToProps = createStructuredSelector({
    wallet: makeSelectWallet(),
});

function mapDispatchToProps(dispatch) {
    return {
        setPin: (pin) => dispatch(walletActions.actionSetPinCode(pin)),
        setSeed: (seed) => dispatch(walletActions.actionSetSeed(seed)),
        setKeyPair: (keys) => dispatch(walletActions.actionSetKeyPair(keys)),
        setAddress: (address) => dispatch(walletActions.actionSetAddress(address)),
        createWallet: () => dispatch(walletActions.actionCreateWallet()),
    };
}

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(FirstLogin);