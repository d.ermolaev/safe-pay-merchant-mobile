import React from 'react';
import PropTypes from 'prop-types';
import {
    ActivityIndicator, Image, Text, TouchableOpacity, View, FlatList
} from 'react-native';
import {
    BackHandled,
    ImageResources,
    NavigationBar,
} from '../../components';
import { marginHorizontal, primaryColorLight, secondaryColor, primaryColor3, styles, textPlaceholderColor, fontSize20, fontSize5, fontSize8 } from '../../config/styles';
import { FontNames } from '../../config/FontNames';
import { strings } from '../../config/localize';
import { TextAlign } from '../../config/RNStyles';
import * as navigationActions from '../../actions/navigationActions';
import * as dbActions from '../../actions/dbActions';
import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect';
import { 
    makeSelectPatterns,
    makeSelectDefaultTitleID,
    makeSelectDefaultDescID,
} from '../../selectors/dbSelectors';
import AwesomeAlert from 'react-native-awesome-alerts';
import { initAlert } from '../Common';

class Patterns extends BackHandled {

    constructor(props) {
        super(props);
        this.pattern = null;
        this.state = {
            alert: {
                ...initAlert,
            },
        };
    }

    componentDidMount() {
        this.props.getDefaultTitle();
        this.props.getDefaultDesc();
        this.props.getPatterns();
    }

    static navigationOptions = {
        header: false,
    };

    warning = (title, text) => {
        this.setState({
            alert: {
                title: `${title} `,
                message: `${text} `, 
                show: true,
                showProgress: false,
                closeOnTouchOutside: true,
                closeOnHardwareBackPress: true,
                showCancelButton: true,
                showConfirmButton: true,
                cancelText: `${strings['ButtonCancel']} `,
                confirmText: `${strings['ButtonOk']} `,
                onCancelPressed: this.hideAlert,
                onConfirmPressed: this.deletePattern,
            }
        });
    };

    hideAlert = () => {
        this.setState({
            alert: {
                ...this.state.alert,
                show: false,
            }
        });
    };

    add = () => {
        const pattern = {
            id: 0, 
            defaultTitle: false, 
            defaultDesc: false, 
            name: '', 
            pattern: '', 
        };
        navigationActions.navigate('EditPattern', pattern);
    };

    remove = (pattern) => () => {
        this.pattern = pattern;
        this.warning(strings['Title warning'], strings['Remove pattern']);
    };

    deletePattern = () => {
        this.hideAlert();
        setTimeout(() => {
            this.props.delete(this.pattern);
        }, 500);
    }

    editPattern = (pattern) => {
        const { defaultTitleID, defaultDescID } = this.props;
        navigationActions.navigate('EditPattern', { 
            ...pattern, 
            defaultTitle: defaultTitleID === pattern.id, 
            defaultDesc: defaultDescID === pattern.id,
        });
    }

    menu = () => {
        navigationActions.toggle();
    };

    keyExtractor = (item) => {
        return item.id.toString();
    };

    renderLoading = () => {
        const { loading } = this.props.patterns;
        if (loading) {
            return (
                <View style={{ justifyContent: 'center', alignItems: 'center' }}>
                    <ActivityIndicator animating={true} color={'#aaa'} size={'large'}/>
                </View>
            );
        } else {
            return null;
        }
    };

    // {item}
    renderItem = ({ item }) => {
        const { defaultTitleID, defaultDescID } = this.props;
        return (
            <View style={styles.settings.serverItemContainer}>
                <View style={styles.settings.serverItemSubContainer}>
                    {item.id === defaultTitleID ? <View style={styles.settings.dot}/> : null}
                    {item.id === defaultDescID ? <View style={styles.settings.dot2}/> : null}
                    <View>
                        <Text numberOfLines={1} style={styles.settings.serverName}>{item.name}</Text>
                        <Text numberOfLines={1}
                            style={styles.settings.serverConnectionString}>{item.pattern}</Text>
                    </View>
                </View>
                <View style={{ flexDirection: 'row', alignItems: 'center', marginLeft: fontSize8 }}>
                    <TouchableOpacity activeOpacity={0.9} onPress={() => this.editPattern(item)}>
                        <Image style={styles.settings.serverDelete} source={ImageResources.icon_user_edit}/>
                    </TouchableOpacity>
                    {item.id === defaultTitleID || item.id === defaultDescID ? null : (
                        <TouchableOpacity onPress={this.remove(item)} style={{ marginLeft: 16 }}>
                            <Image style={styles.settings.serverDelete}
                                source={ImageResources.icon_delete_gray}/>
                        </TouchableOpacity>)}
                </View>
            </View>
        );
    };

    render() {
        //const node = stores.nodesStore.nodes[this.state.checkingIndex]
        const { patterns } = this.props;
        //const { alert } = this.state;
        //console.log(alert.show);
        return (
            <View style={style.container}>
                <Image source={ImageResources.background} style={styles.shared.topBackgroundImage}/>
                <NavigationBar
                    title={strings['List patterns']}
                    leftButton={ImageResources.icon_menu}
                    onLeftButtonPress={this.menu}
                />

                <View style={style.cont}>
                    <View style={styles.settings.serverLabelContainer}>
                        <Text style={styles.settings.serverLabel}> {strings['Purpose of payment']}</Text>
                    </View>
                    <View style={{paddingVertical: 8}}>
                        {this.renderLoading()}
                        <FlatList
                            data={patterns.data}
                            renderItem={this.renderItem}
                            keyExtractor={this.keyExtractor}
                        />
                    </View>
                    <View style={styles.shared.line}/>
                    <TouchableOpacity onPress={this.add} style={styles.settings.serverAddContainer}>
                        <Image style={styles.settings.serverAddImage} source={ImageResources.icon_plus_server}/>
                        <Text style={styles.settings.serverAddLabel}>{`${strings['Add Pattern']} `}</Text>
                    </TouchableOpacity>

                </View>
                <AwesomeAlert
                    {...this.state.alert}
                    titleStyle={styles.alert.title}
                    messageStyle={styles.alert.text}
                    overlayStyle={styles.alert.overlayStyle}
                    cancelButtonColor={secondaryColor}
                    confirmButtonColor={primaryColorLight}
                />

            </View>
        );
    }
}

const style = {
    container: {
        backgroundColor: '#fff',
        flex: 1
    },

    cont: {
        padding: 16,
        paddingHorizontal: marginHorizontal,
    },

    text: {
        fontFamily: FontNames.ProstoLight,
        fontSize: fontSize20,
        color: textPlaceholderColor,
        marginBottom: 5,

    },
    textTitle: {
        fontFamily: FontNames.ProstoLight,
        fontSize: fontSize20,
        color: primaryColor3,
        marginBottom: 5,

    },
    version: {
        fontSize: fontSize5,
        textAlign: TextAlign.right
    }
};

Patterns.propTYpes = {
    getPatterns: PropTypes.func,
    insert: PropTypes.func,
    patterns: PropTypes.object,
    defaultTitleID: PropTypes.number,
    defaultDescID: PropTypes.number,
    getDefaultTitle: PropTypes.func,
    getDefaultDesc: PropTypes.func,
};

const mapStateToProps = createStructuredSelector({
    patterns: makeSelectPatterns(),
    defaultTitleID: makeSelectDefaultTitleID(),
    defaultDescID: makeSelectDefaultDescID(),
});

function mapDispatchToProps(dispatch) {
    return {
        getPatterns: () => dispatch(dbActions.actionPatternsQuery()),
        delete: (pattern) => dispatch(dbActions.actionPatternDelete(pattern)),
        getDefaultTitle: () => dispatch(dbActions.actionGetParam('DefaultTitle')),
        getDefaultDesc: () => dispatch(dbActions.actionGetParam('DefaultDesc')),
    };
}

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(Patterns);