import React from 'react';
import PropTypes from 'prop-types';
import { WebView } from 'react-native-webview';
import { Image, Text, TextInput, TouchableOpacity, View } from 'react-native';
import Switch from 'react-native-switch-pro';
import { marginHorizontal, styles, textPlaceholderColor, primaryColor3, dHeight, fontSizeInput } from '../../config/styles';
import {
    ImageResources,
    NavigationBar,
    BackHandled,
    FooterBtn,
    LineSeparator,
} from '../../components';
import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect';
import * as dbActions from '../../actions/dbActions';
import { makeSelectLang } from '../../selectors/configSelectors';
import { strings } from '../../config/localize';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import { initialState } from '../../reducers/invoiceReducer';
import moment from 'moment/min/moment-with-locales';

const height = Math.round(dHeight / 2);

class EditPattern extends BackHandled {
    
    static navigationOptions = {
        header: false,
    };

    onBack = () => {
        this.back();
    };
 
    getGuide = () => {
        if (this.props.lang === 'ru') {
            this.setState({guide: require('../../resources/pattern/ru.json')});
        } else {
            this.setState({guide: require('../../resources/pattern/en.json')});
        }
    };

    nameTextInput;
    editName = () => {
        if (this.nameTextInput) {
            this.nameTextInput.focus();
        }
    }

    onNameChange = (text) => {
        this.setState({ name: text });
    }

    onPatternChange = (text) => {
        this.setState({ pattern: text });
    }

    save = () => {
        const { id, name, defaultTitle, defaultDesc, pattern } = this.state;
        if (id === 0) {
            this.props.insert({ name, defaultTitle, defaultDesc, pattern });
        } else {
            this.props.update({ id, name, defaultTitle, defaultDesc, pattern });
        }
        this.back();
    }

    changeDefaultTitle(value) {
        //console.log({ defaultTitle: value });
        this.setState({ defaultTitle: value });
    }

    changeDefaultDesc(value) {
        //console.log({ defaultDesc: value });
        this.setState({ defaultDesc: value });
    }

    constructor(props) {
        super(props);
        this.changeDefaultTitle = this.changeDefaultTitle.bind(this);
        this.changeDefaultDesc = this.changeDefaultDesc.bind(this); 
        this.state = {
            ...this.props.navigation.state.params,
            lockButton: false,
            invoice: {
                ...initialState.get('invoice').toJS(),
                date: moment().valueOf(),
                user: '+7-999-9999999',
                sum: 999.99,
                novat: 'No VAT',
                vat: 'Including VAT',
            },
            guide: '',
        };
    }

    componentWillMount() {
        this.getGuide();
    }

    render() {
        const { name, pattern } = this.state;
        const isReady = name.length > 0 && pattern.length > 0;
        const labelStyle = {...styles.sent.titleMessage, color: textPlaceholderColor};
        const textStyle = {...styles.sent.textInput, textAlign: 'left', fontSize: fontSizeInput };
        const containerStyle = {
            ...styles.login.containerBottomLicence,
            justifyContent: 'space-around',
            height,
        };        
        //console.log({ height });
        const source = {html: this.state.guide ? this.state.guide.html : undefined};
        return (
            <View style={styles.container}>
                <Image source={ImageResources.background} style={styles.shared.topBackgroundImage}/>
                <NavigationBar
                    title={strings['String pattern']}
                    leftButton={ImageResources.icon_back}
                    onLeftButtonPress={this.onBack}
                />
                <View style={styles.sent.subContainer}>
                    <KeyboardAwareScrollView
                        contentContainerStyle={{paddingTop: 16, paddingBottom: 60}}
                        viewIsInsideTabBar={true}
                        showsVerticalScrollIndicator={false}
                        enableAutoAutomaticScroll={true}
                        ref={this.setScrollViewRef}>
                        <View style={{paddingHorizontal: marginHorizontal}}>
                            <View>
                                <View style={{ flexDirection: 'row', height: 50, justifyContent: 'space-between' }}>
                                    <View style={{ alignSelf: 'center' }}>
                                        <Text style={labelStyle}>{`${strings['Pattern name']}:`}</Text>
                                    </View>
                                    <View style={{ flexDirection: 'row', justifyContent: 'center', alignItems: 'center' }}>
                                        <TextInput
                                            ref={(input) => { this.nameTextInput = input; }}
                                            placeholder={strings['Pattern name']}
                                            style={{ textAlign: 'left', color: primaryColor3, fontSize: fontSizeInput }}
                                            multiline={false}
                                            numberOfLines={1}
                                            underlineColorAndroid={'transparent'}
                                            placeholderTextColor={'#A1ACBA'}
                                            value={name}
                                            onChangeText={this.onNameChange}
                                        />
                                        <TouchableOpacity activeOpacity={0.9} onPress={this.editName}>
                                            <Image style={{width: 28, height: 30}} source={ImageResources.icon_user_edit}/>
                                        </TouchableOpacity>
                                    </View>
                                </View>
                            </View>
                            <LineSeparator />
                            <View style={{ justifyContent: 'center', marginTop: 16 }}>
                                <View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
                                    <View style={{ flex: 4, alignSelf: 'center' }}>
                                        <Text style={labelStyle} numberOfLines={2}>{strings['Default title']}</Text>
                                    </View>
                                    <View style={{ flex: 1, justifyContent: 'center', alignItems: 'flex-end' }}>
                                        <Switch
                                            onSyncPress={this.changeDefaultTitle}
                                            value={this.state.defaultTitle}
                                        />
                                    </View>
                                </View>
                                <View style={{ flexDirection: 'row', justifyContent: 'space-between', marginTop: 16 }}>
                                    <View style={{ flex: 4, alignSelf: 'center' }}>
                                        <Text style={labelStyle} numberOfLines={2}>{strings['Default desc']}</Text>
                                    </View>
                                    <View style={{ flex: 1, justifyContent: 'center', alignItems: 'flex-end' }}>
                                        <Switch
                                            onSyncPress={this.changeDefaultDesc}
                                            value={this.state.defaultDesc}
                                        />
                                    </View>
                                </View>
                            </View>
                            <View style={[styles.sent.textContainer, { paddingTop: 16 }]}>
                                <TextInput
                                    placeholder={strings['Pattern']}
                                    multiline={false}
                                    numberOfLines={3}
                                    style={textStyle}
                                    underlineColorAndroid={'transparent'}
                                    placeholderTextColor={'#A1ACBA'}
                                    value={pattern}
                                    onChangeText={this.onPatternChange}
                                />
                            </View>
                            <LineSeparator />
                        </View>
                        <View style={[containerStyle, { paddingHorizontal: marginHorizontal }]}>
                            <Text style={styles.register.liceseText}>{strings['Pattern tip']}</Text>
                            <View style={{ ...styles.login.web }}>
                                <WebView source={source} />
                            </View>
                        </View>
                    </KeyboardAwareScrollView>
                </View>
                {isReady ? <FooterBtn onPress={this.save}>
                    <Image style={styles.myWallet.buttonImage180}
                        source={ImageResources.icon_sent}/>
                    <Text style={styles.myWallet.buttonText}>{strings['Save']}</Text>
                </FooterBtn>:null}
            </View>
        );
    }
}

EditPattern.propTypes = {
    lang: PropTypes.string,
    insert: PropTypes.func,
    update: PropTypes.func,
};

const mapStateToProps = createStructuredSelector({
    lang: makeSelectLang(),
});

function mapDispatchToProps(dispatch) {
    return {
        insert: (pattern) => dispatch(dbActions.actionPatternInsert(pattern)),
        update: (pattern) => dispatch(dbActions.actionPatternUpdate(pattern)),
    };
}

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(EditPattern);


