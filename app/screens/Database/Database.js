import React from 'react';
import PropTypes from 'prop-types';
import {
    Image, View, Platform, Text
} from 'react-native';
import {
    BackHandled,
    ImageResources,
    NavigationBar,
    EditText, 
} from '../../components';
import { Btn } from '../../components/Btn/Btn';
import { marginHorizontal, primaryColorLight, secondaryColor, primaryColor3, styles, textPlaceholderColor, fontSize20, fontSize5, fontSize14, fontSize7, fontSizeTitle } from '../../config/styles';
import { FontNames } from '../../config/FontNames';
import { strings } from '../../config/localize';
import { TextAlign } from '../../config/RNStyles';
import * as navigationActions from '../../actions/navigationActions';
import * as dbActions from '../../actions/dbActions';
import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect';
import AwesomeAlert from 'react-native-awesome-alerts';
import { initAlert } from '../Common';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import { makeSelectBackupName } from '../../selectors/configSelectors';
import { 
    makeSelectRestore,
    makeSelectBackup
} from '../../selectors/dbSelectors';
import LocalStore from '../../store/LocalStore';

class Database extends BackHandled {

    static navigationOptions = {
        header: false,
    };

    warning = (title, text) => {
        this.setState({
            alert: {
                title: `${title} `,
                message: `${text} `, 
                show: true,
                showProgress: false,
                confirmText: `${strings['ButtonOk']} `,
                closeOnTouchOutside: true,
                closeOnHardwareBackPress: true,
                showCancelButton: false,
                showConfirmButton: true,
                onCancelPressed: () => {
                    this.hideAlert();
                },
                onConfirmPressed: () => {
                    this.hideAlert();
                },
            }
        });
    };

    progress = (title) => {
        this.setState({
            alert: {
                title: `${title} `,
                show: true,
                showProgress: true,
                closeOnTouchOutside: false,
                closeOnHardwareBackPress: false,
                showCancelButton: false,
                showConfirmButton: false,
                progressSize: 'small',
                progressColor: 'gray',
            }
        });
    };

    hideAlert = () => {
        this.setState({
            alert: {
                ...this.state.alert,
                show: false,
            }
        });
        //this.props.restoreReset();
        //this.props.backupReset();
    };

    menu = () => {
        navigationActions.toggle();
    };

    backup = () => {
        if (Platform.OS === 'ios') {
            if (LocalStore.tokenICloud) {
                this.props.runBackup(this.props.backupName);
            } else {
                this.warning(strings['Warning'], `${strings['iCloud failed']}: ${LocalStore.error} `);
                LocalStore.getICloudToken();
            }
        } else {
            this.props.runBackup(this.props.backupName);
        }
    }

    restore = () => {
        if (Platform.OS === 'ios') {
            if (LocalStore.tokenICloud) {
                this.props.runRestore(this.props.backupName);
            } else {
                this.warning(strings['Warning'], `${strings['iCloud failed']}: ${LocalStore.error} `);
                LocalStore.getICloudToken();
            }
        } else {
            this.props.runRestore(this.props.backupName);
        }
    }

    onChangeName = (name) => {
        this.props.setParam({ name: 'BackupName', val: name });
    }

    constructor(props) {
        super(props);
        this.state = {
            alert: {
                ...initAlert,
            },
        };
    }

    componentWillMount() {
        this.props.backupReset();
        this.props.restoreReset();
        this.props.getParam('BackupName');
    }

    componentDidUpdate(prevProps) {
        //console.log({ prevProps, props: this.props });
        if (!prevProps.backup.executing && this.props.backup.executing) {
            this.progress(strings['Backup progress']);
        }
        if (!prevProps.restore.executing && this.props.restore.executing) {
            this.progress(strings['Restore progress']);
        }
        if (!prevProps.backup.error && this.props.backup.error) {
            this.warning(strings['Backup error'], this.props.backup.error);
        }
        if (!prevProps.restore.error && this.props.restore.error) {
            this.warning(strings['Restore error'], this.props.restore.error);
        }
        if (prevProps.restore.executing && !this.props.restore.executing && !this.props.restore.error) {
            this.hideAlert();
        }
        if (prevProps.backup.executing && !this.props.backup.executing && !this.props.backup.error) {
            this.warning(strings['Warning'], `${strings['Backup complete']}: ${this.props.backup.path} `);
        }
    }

    render() {
        //console.log(this.props);
        const disableBtn = this.props.backupName.length === 0 || this.props.backup.executing || this.props.restore.executing;
        return (
            <View style={style.container}>
                <Image source={ImageResources.background} style={styles.shared.topBackgroundImage}/>
                <NavigationBar
                    title={strings['Menu store']}
                    leftButton={ImageResources.icon_menu}
                    onLeftButtonPress={this.menu}
                />
                <KeyboardAwareScrollView
                    contentContainerStyle={{paddingTop: 16, paddingBottom: 60, paddingHorizontal: marginHorizontal}}
                    viewIsInsideTabBar={true}
                    showsVerticalScrollIndicator={false}
                    enableAutoAutomaticScroll={true}
                    ref={this.setScrollViewRef}>
                    <View style={style.cont}>
                        <EditText
                            title={strings['Backup file name']}
                            placeholder={this.props.backupName}
                            onTextChange={this.onChangeName}
                            allowPaste={true}
                            maxLength={35}
                        />
                        <Btn 
                            title={`${strings['Backup']} `} 
                            fill={true} 
                            onPress={this.backup}
                            fillColor={primaryColorLight}
                            customBtnStyles={{ flex: 1, marginLeft: fontSize14, marginRight: fontSize7, marginTop: fontSize14  }}
                            disabled={disableBtn}
                        />
                        <View style={{ marginTop: fontSize14 }}>
                            <Text style={styles.settings.serverLabel}> {`${strings['Remarks for restore']} `}</Text>
                        </View>
                        <Btn 
                            title={`${strings['Restore']} `} 
                            fill={true} 
                            onPress={this.restore}
                            customBtnStyles={{ flex: 1, marginLeft: fontSize14, marginRight: fontSize7, marginTop: fontSize14 }}
                            disabled={disableBtn}
                        />
                    </View>
                </KeyboardAwareScrollView>
                <AwesomeAlert
                    {...this.state.alert}
                    titleStyle={this.state.alert.showProgress?styles.alert.text:styles.alert.title}
                    messageStyle={styles.alert.text}
                    overlayStyle={styles.alert.overlayStyle}
                    cancelButtonColor={secondaryColor}
                    confirmButtonColor={primaryColorLight}
                />
            </View>
        );
    }
}

const style = {
    container: {
        backgroundColor: '#fff',
        flex: 1
    },

    cont: {
        margin: 16
    },
    text: {
        fontFamily: FontNames.ProstoLight,
        fontSize: fontSize20,
        color: textPlaceholderColor,
        marginBottom: 5,
    },
    textTitle: {
        fontFamily: FontNames.ProstoLight,
        fontSize: fontSizeTitle,
        color: primaryColor3,
        marginBottom: 5,
    },
    version: {
        fontSize: fontSize5,
        textAlign: TextAlign.right
    }, 
};

Database.propTYpes = {
    setParam: PropTypes.func,
    getParam: PropTypes.func,
    runBackup: PropTypes.func,
    runRestore: PropTypes.func,
    backupReset: PropTypes.func,
    restoreReset: PropTypes.func,
    backupName: PropTypes.string,
    backup: PropTypes.object,
    restore: PropTypes.object,
};

const mapStateToProps = createStructuredSelector({
    backupName: makeSelectBackupName(),
    backup: makeSelectBackup(),
    restore: makeSelectRestore(),
});

function mapDispatchToProps(dispatch) {
    return {
        setParam: (param) => dispatch(dbActions.actionSetParam(param)), 
        getParam: (name) => dispatch(dbActions.actionGetParam(name)),
        runBackup: (filename) => dispatch(dbActions.actionBackup(filename)),
        runRestore: (filename) => dispatch(dbActions.actionRestore(filename)),
        backupReset: () => dispatch(dbActions.actionBackupReset()),
        restoreReset: () => dispatch(dbActions.actionRestoreReset()),
    };
}

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(Database);