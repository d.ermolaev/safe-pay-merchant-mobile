import React from 'react';
import PropTypes from 'prop-types';
import { Image, View } from 'react-native';
import {
    BackHandled,
    ImageResources,
    NavigationBar,
    EditText,
    FooterBtn,
    Toaster
} from '../../components';
import { marginHorizontal, styles, primaryColorLight, secondaryColor } from '../../config/styles';
import { strings } from '../../config/localize';
import { settings } from '../../core/settings/AppSettings';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import * as serversActions from '../../actions/serversActions';
import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect';
import { 
    makeSelectPing,
    makeSelectServers 
} from '../../selectors/serverSelectors';
//import { AssetsRequest } from '../core/request/AssetsRequest';
import AwesomeAlert from 'react-native-awesome-alerts';
import { initAlert } from '../Common';

class AddServer extends BackHandled {

    static navigationOptions = {
        header: false,
    };

    notified = false;

    warning = (title, text) => {
        this.setState({
            alert: {
                title: `${title} `,
                message: `${text} `, 
                show: true,
                showProgress: false,
                confirmText: `${strings['ButtonOk']} `,
                closeOnTouchOutside: true,
                closeOnHardwareBackPress: true,
                showCancelButton: false,
                showConfirmButton: true,
                onCancelPressed: () => {
                    this.hideAlert();
                },
                onConfirmPressed: () => {
                    this.hideAlert();
                },
            }
        });
    };

    progress = (title) => {
        this.setState({
            alert: {
                title: `${title} `,
                show: true,
                showProgress: true,
                closeOnTouchOutside: false,
                closeOnHardwareBackPress: false,
                showCancelButton: false,
                showConfirmButton: false,
                progressSize: 'small',
                progressColor: 'gray',
            }
        });
    };

    hideAlert = () => {
        this.setState({
            alert: {
                ...this.state.alert,
                show: false,
            }
        });
    };

    add = async () => {
        const name = this.state.name;
        const conStr = this.state.conStr;
        if (name === '' || conStr === '' || name.split(' ').length === name.length + 1) {
            Toaster.error(strings['error_6']);
            return;
        }

        let valid = /^(ftp|http|https):\/\/[^ ']+$/.test(conStr);
        if (!valid) {
            Toaster.error(strings['error_13']);
            return;
        }
        const nodeData = this.validate(conStr.replace(' ', ''));
        if (nodeData === null) {
            Toaster.error(strings['error_13']);
        } else {

            const { connectionString } = nodeData;

            if (this.exists(connectionString)) {

                Toaster.error(strings['error_16']);

            } else {
            
                //console.log(connectionString);
                this.props.pingServer(connectionString, 'AddServer');
            }
        }
    };

    exists = (url) => {
        const { list } = this.props.servers;
        for (let key in list) {
            if (list[key].connectionString === url) {
                return true;
            }
        }
        return false;
    }

    constructor() {
        super();
        this.state = {
            name: '', 
            conStr: '',
            alert: {
                ...initAlert,
            },
        };
    }

    componentWillMount() {
        this.props.pingReset();
    }

    componentDidUpdate(prevProps) {
        //console.log({ props: this.props, this.props, state: this.state });
        const { ping } = prevProps;
        if (this.props.ping.source === 'AddServer' && !ping.fetching && this.props.ping.fetching) {
            this.progress(strings['Checking'] + ':\n' + this.state.name + '\n' + this.state.conStr);
        } 
        if (this.props.ping.source === 'AddServer' && ping.fetching && !this.props.ping.fetching) {
            this.hideAlert();
            this.props.pingReset();
        }
        if (this.props.ping.source === 'AddServer' && !ping.url && this.props.ping.url === this.state.conStr) {
            if (this.props.ping.error) {
                this.warning(strings['Warning'], strings['NodeNotRespond'] + ': ' + this.state.name);
            } else {
                this.props.addServer({ 
                    name: this.state.name, 
                    connectionString: this.state.conStr,
                    port: 0
                });
                this.props.pingReset();
                this.back();      
            }
        }
    }

    validate(_url) {
        try {
            const http = _url.includes('http://') ? '' : 'http://';
            let port = '';
            let nPort = 0;
            let url = _url.replace('http://', '').replace('/api', '');
            const api = _url.includes('/api') ? '' :
                url.charAt(url.length - 1) === '/' ? 'api' : '/api';
            const comp = url.split(':');
            if (comp.length !== 2) {
                port = settings.environment === 'production' ? ':9047' : ':9067';
                nPort = +port - 1;
            } else if (comp.length === 2) {
                const _port = comp[1];
                let valid = /^\d+$/.test(_port);
                if (!valid) {
                    return null;
                }
                nPort = +_port - 1;
            }
            return {connectionString: http + _url + port + api, port: nPort};
        } catch (e) {
            return null;
        }
    }

    render() {
        return (
            <View style={styles.container}>
                <Image source={ImageResources.background} style={styles.shared.topBackgroundImage}/>
                <NavigationBar
                    title={strings['Addd']}
                    leftButton={ImageResources.icon_back}
                    onLeftButtonPress={this.back}
                />
                <KeyboardAwareScrollView
                    contentContainerStyle={{paddingTop: 16, paddingBottom: 60, paddingHorizontal: marginHorizontal}}
                    viewIsInsideTabBar={true}
                    showsVerticalScrollIndicator={false}
                    enableAutoAutomaticScroll={true}>
                    <EditText
                        title={strings['Server\'s name']}
                        placeholder={strings['servNamepls']}
                        onTextChange={(name) => this.setState({name})}
                        allowPaste={false}
                        maxLength={35}
                    />
                    <EditText
                        title={strings['Connection string']}
                        placeholder={'http://myserver.com:5676'}
                        onTextChange={(conStr) => this.setState({conStr})}
                        allowPaste={false} 
                    />
                </KeyboardAwareScrollView>
                <FooterBtn title={strings['Addd']} onPress={this.add}/>
                <AwesomeAlert
                    titleStyle={this.state.alert.showProgress?styles.alert.text:styles.alert.title}
                    messageStyle={styles.alert.text}
                    overlayStyle={styles.alert.overlayStyle}
                    cancelButtonColor={secondaryColor}
                    confirmButtonColor={primaryColorLight}
                    {...this.state.alert}
                />
            </View>
        );
    }
}

AddServer.propTYpes = {
    servers: PropTypes.object,
    ping: PropTypes.object,
    addServer: PropTypes.func,
    pingServer: PropTypes.func,
    pingReset: PropTypes.func,
};

const mapStateToProps = createStructuredSelector({
    ping: makeSelectPing(),
    servers: makeSelectServers(),
});

function mapDispatchToProps(dispatch) {
    return {
        addServer: (node) => dispatch(serversActions.actionAddServer(node)),
        pingServer: (url, source) => dispatch(serversActions.actionPingServer(url, source)),
        pingReset: () => dispatch(serversActions.actionPingServerReset()),
    };
}

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(AddServer);

