import React from 'react';
import PropTypes from 'prop-types';
import {
    Image, ScrollView, Text, TouchableOpacity, View, FlatList
} from 'react-native';
import {
    BackHandled,
    ImageResources,
    NavigationBar,
    TextInfo, 
} from '../../components';
import { Btn } from '../../components/Btn/Btn';
import { marginHorizontal, dWidth, primaryColorLight, secondaryColor, primaryColor3, styles, textPlaceholderColor, fontSize20, fontSize5, fontSize14, fontSize7, fontSizeTitle, fontSize16 } from '../../config/styles';
import { FontNames } from '../../config/FontNames';
import { strings } from '../../config/localize';
import { TextAlign } from '../../config/RNStyles';
import * as navigationActions from '../../actions/navigationActions';
import * as serversActions from '../../actions/serversActions';
import * as dbActions from '../../actions/dbActions';
import * as pollActions from '../../actions/pollActions';
import * as invoiceActions from '../../actions/invoiceActions';
import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect';
import { 
    makeSelectSettings,
    makeSelectUrlEraChain,
    makeSelectLang, 
    makeSelectInvoiceExpire,
    makeSelectPollTimeout
} from '../../selectors/configSelectors';
import { 
    makeSelectServers,
    makeSelectPing,    
} from '../../selectors/serverSelectors';
import { 
    makeSelectDefaultServerID 
} from '../../selectors/dbSelectors';
import { makeSelectWallet } from '../../selectors/walletSelectors';
import { makeSelectRestore } from '../../selectors/invoiceSelectors';
import KeyPair from '../../core/account/KeyPair';
import DateTimePicker from 'react-native-modal-datetime-picker';
import moment from 'moment-timezone';
import AwesomeAlert from 'react-native-awesome-alerts';
import { initAlert } from '../Common';
import { Slider } from 'react-native-elements';

class Settings extends BackHandled {

    static navigationOptions = {
        header: false,
    };

    warning = (title, text) => {
        this.setState({
            alert: {
                title: `${title} `,
                message: `${text} `, 
                show: true,
                showProgress: false,
                confirmText: `${strings['ButtonOk']} `,
                closeOnTouchOutside: true,
                closeOnHardwareBackPress: true,
                showCancelButton: false,
                showConfirmButton: true,
                onCancelPressed: () => {
                    this.hideAlert();
                },
                onConfirmPressed: () => {
                    this.hideAlert();
                },
            }
        });
    };

    progress = (title) => {
        this.setState({
            alert: {
                title: `${title} `,
                show: true,
                showProgress: true,
                closeOnTouchOutside: false,
                closeOnHardwareBackPress: false,
                showCancelButton: false,
                showConfirmButton: false,
                progressSize: 'small',
                progressColor: 'gray',
            }
        });
    };


    hideAlert = () => {
        this.setState({
            alert: {
                ...this.state.alert,
                show: false,
            }
        });
    };

    importKey = () => {
        navigationActions.navigate('Import');
    }

    add = () => {
        navigationActions.navigate('AddServer');
    };

    remove = (item) => async () => {
        if (item.id !== this.props.defaultServerID) {
            //console.log(item);
            this.props.removeServer(item.id);
        }
    };

    switchTo = (item, index) => async () => {
        if (item.id !== this.props.defaultServerID) {
            const node = this.props.servers.list[index];
            this.setState({ node, pingServerID: item.id });
            this.props.pingServer(node.connectionString, 'Settings');
        }
    };

    close = () => {
        this.setState({visible: false});
    };

    closeCalendar = () => {
        this.setState({ showCalendar: false });
    }

    openCalendar = () => {
        this.setState({ showCalendar: true });
    }

    onDateSelect = (date) => {
        this.setState({ showCalendar: false });
        const fromTimestamp = moment(date).valueOf();
        this.props.recoveryInvoices(fromTimestamp);
    }

    menu = () => {
        navigationActions.toggle();
    };

    keyExtractor = (item) => {
        return item.connectionString;
    };

    // {item}
    renderItem = ({ item, index }) => {
        return (<TouchableOpacity onPress={this.switchTo(item, index)} key={item.id} style={styles.settings.serverItemContainer}>
            <View style={styles.settings.serverItemSubContainer}>
                {item.checked ?
                    <View style={styles.settings.dot}/> : <View style={styles.settings.nodot}/>}
                <View>
                    <Text numberOfLines={1} style={styles.settings.serverName}>{`${item.name} `}</Text>
                    <Text numberOfLines={1}
                        style={styles.settings.serverConnectionString}>{`${item.connectionString} `}</Text>
                </View>
            </View>
            {index === 0 || item.id === this.props.defaultServerID ? null : (
                <TouchableOpacity onPress={this.remove(item)}>
                    <Image style={styles.settings.serverDelete}
                        source={ImageResources.icon_delete_gray}/>
                </TouchableOpacity>)}
        </TouchableOpacity>);
    };

    onChangePollTimeout = (value) => {
        this.props.setParam({
            name: 'PollTimeout',
            val: value * 60,
        });
        this.props.stopSchedulePoll();
        if (this.timeoutRestartSchedulePoll) {
            clearTimeout(this.timeoutRestartSchedulePoll);
            this.timeoutRestartSchedulePoll = null;
        }
        this.timeoutRestartSchedulePoll = setTimeout(function () {
            this.props.startSchedulePoll();
            this.timeoutRestartSchedulePoll = null;
        }.bind(this), 30000);
    }

    onChangeInvoiceExpire = (value) => {
        this.props.setParam({
            name: 'InvoiceExpire',
            val: value * 3600,
        });
    }

    constructor(props) {
        super(props);
        this.timeoutRestartSchedulePoll = null;
        this.state = {
            visible: false,
            secretKey: '',  
            publicKey: '', 
            node: this.props.servers.list[0],
            pingServerID: 0,
            showCalendar: false,
            alert: {
                ...initAlert,
            },
        };
    }

    async componentWillMount() {
        //console.log('this.props.getServers()');
        await this.getKeys();
        this.props.getDefaultServerID();
        this.props.getServers();
        this.props.getParam('PollTimeout');
        this.props.getParam('InvoiceExpire');
        this.props.pingReset();
    }

    async componentDidUpdate(prevProps) {
        const { ping } = prevProps;
        if (this.props.ping.source === 'Settings' && !ping.fetching && this.props.ping.fetching) {
            this.progress(strings['Checking'] + ':\n' + this.state.node.name + '\n' + this.state.node.connectionString);
        } 
        if (this.props.ping.source === 'Settings' && ping.fetching && !this.props.ping.fetching) {
            this.hideAlert();
            this.props.pingReset();
        }
        if (this.props.ping.source === 'Settings' && (!ping.url || ping.url !== this.props.ping.url) && this.props.ping.url) {
            if (this.props.ping.error) {
                //console.log('Settings');
                this.props.pingReset();
                this.warning(strings['Title warning'], strings['NodeNotRespond'] + ': ' + this.state.node.name);
                
            } else {
                this.props.currentServer(this.state.pingServerID);
                this.warning(strings['Title warning'], strings['Switch to'] + ': ' + this.state.node.name);
                this.props.pingReset();
            }
        }
        const { error, result, executing } = prevProps.recovery;
        if (!executing && this.props.recovery.executing) {
            this.progress(strings['Wait recovery']);
        } 
        if (executing && !this.props.recovery.executing) {
            this.hideAlert();
        }
        if (!error && this.props.recovery.error) {
            this.warning(strings['Recovery error'], this.props.recovery.error);
        }
        if (!result && this.props.recovery.result) {
            this.warning(strings['Title warning'], `${strings['Recovery success']} ${this.props.recovery.result}`);
        }
        if (prevProps.wallet.keyPair !== this.props.wallet.keyPair) {
            await this.getKeys();
        }
    }

    getKeys = async () => {
        const { wallet } = this.props;
        //console.log({ wallet });
        const keyPair = new KeyPair(wallet.keyPair);
        const keys = await keyPair.toString();
        const secretKey = keys.secretKey;
        //const seed = await Base58.encode(wallet.seed);
        const publicKey = keys.publicKey;
        //const public32Key = Base32JS.encode(wallet.keyPair.publicKey);
        this.setState({  secretKey, publicKey });
    }

    renderCalendar = () => {
        const { showCalendar } = this.state;
        //console.log({ showCalendar });
        return (
            <DateTimePicker
                isVisible={showCalendar}
                onConfirm={this.onDateSelect}
                onCancel={this.closeCalendar}
            />
        );
    }

    render() {
        //const node = stores.nodesStore.nodes[this.state.checkingIndex]
        const { settings, servers } = this.props;
        const { secretKey, publicKey } = this.state;
        const invoiceExpire = Math.round(this.props.invoiceExpire / 3600);
        const pollTimeout = Math.round(this.props.pollTimeout / 60);
        //console.log({ defaultServerID: this.props.defaultServerID, invoiceExpire, pollTimeout });
        return (
            <View style={style.container}>
                <Image source={ImageResources.background} style={styles.shared.topBackgroundImage}/>
                <NavigationBar
                    title={strings['Settings']}
                    leftButton={ImageResources.icon_menu}
                    onLeftButtonPress={this.menu}
                />
                {this.renderCalendar()}
                <ScrollView contentContainerStyle={style.cont}>
                    <View>
                        <View style={{flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center'}}>
                            <Text style={style.textTitle}>{`${strings['Keys']} `}</Text>
                        </View>
                        <TextInfo title={strings['Open Key']} displayText={publicKey} allowCopy={true} qrCode={true}
                            titleColor={textPlaceholderColor} titleStyle={{fontFamily: FontNames.ProstoRegular}}/>
                        <TextInfo title={strings['Private key']} displayText={''}
                            copyText={secretKey} allowCopy={true} qrCode={true}
                            titleColor={textPlaceholderColor} titleStyle={{fontFamily: FontNames.ProstoRegular}}/>
                        <Btn 
                            title={`${strings['Import secret']} `} 
                            fill={true} 
                            onPress={this.importKey}
                            customBtnStyles={{ flex: 1, marginTop: fontSize14, marginLeft: fontSize14, marginRight: fontSize14 }}
                        />
                        <View style={{...styles.shared.hr, marginVertical: 13}}/>
                    </View>
                    <View>
                        <View style={{flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center', marginBottom: fontSize7}}>
                            <Text style={style.textTitle}>{`${strings['Invoices recovery']} `}</Text>
                        </View>
                        <Btn 
                            title={`${strings['Recovery from']} `} 
                            fill={true} 
                            onPress={this.openCalendar}
                            fillColor={primaryColorLight}
                            customBtnStyles={{ flex: 1, marginLeft: fontSize14, marginRight: fontSize14 }}
                        />
                        <View style={{...styles.shared.hr, marginVertical: 13}}/>
                    </View>
                    <View style={styles.settings.serverLabelContainer}>
                        <View style={styles.settings.slideLabelContainer}>
                            <Text style={styles.settings.serverLabel}> {`${strings['Invoice expire']} `}</Text>
                            <Text style={styles.settings.serverValue}> {`${invoiceExpire} `}</Text>
                        </View>
                        <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
                            <Slider
                                style={{ width: dWidth * 0.7 }}
                                key={'Invoice expire'}
                                step={1}
                                minimumValue={1}
                                maximumValue={24}
                                thumbTintColor={primaryColorLight}
                                value={invoiceExpire}
                                onSlidingComplete={this.onChangeInvoiceExpire}
                            />
                        </View>
                        <View style={styles.shared.line}/>
                    </View>
                    <View style={styles.settings.serverLabelContainer}>
                        <View style={styles.settings.slideLabelContainer}>
                            <Text style={styles.settings.serverLabel}> {`${strings['Poll timeout']} `}</Text>
                            <Text style={styles.settings.serverValue}> {`${pollTimeout} `}</Text>
                        </View>
                        <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
                            <Slider
                                style={{ width: dWidth * 0.7 }}
                                key={'Poll timeout'}
                                minimumValue={2}
                                maximumValue={120}
                                thumbTintColor={primaryColorLight}
                                value={pollTimeout}
                                onSlidingComplete={this.onChangePollTimeout}
                            />
                        </View>
                        <View style={styles.shared.line}/>
                    </View>
                    <View style={styles.settings.serverContainer}>
                        <View style={styles.settings.serverLabelContainer}>
                            <Text style={styles.settings.serverLabel}> {strings['Server']}</Text>
                        </View>

                        <View style={{paddingVertical: 8}}>
                            {/*это будет айтем списка*/}
                            <FlatList
                                data={servers.list}
                                renderItem={this.renderItem}
                                keyExtractor={this.keyExtractor}
                            />
                        </View>
                        <View style={styles.shared.line}/>
                        <TouchableOpacity onPress={this.add} style={styles.settings.serverAddContainer}>
                            <Image style={styles.settings.serverAddImage} source={ImageResources.icon_plus_server}/>
                            <Text style={styles.settings.serverAddLabel}>{`${strings['Add Server']} `}</Text>
                        </TouchableOpacity>
                        
                        <Text style={style.version}>{settings.version}.{settings.build}</Text>
                    </View>
                </ScrollView>
                <AwesomeAlert
                    {...this.state.alert}
                    titleStyle={this.state.alert.showProgress?styles.alert.text:styles.alert.title}
                    messageStyle={styles.alert.text}
                    overlayStyle={styles.alert.overlayStyle}
                    cancelButtonColor={secondaryColor}
                    confirmButtonColor={primaryColorLight}
                />
            </View>
        );
    }
}

const style = {
    container: {
        backgroundColor: '#fff',
        flex: 1,
    },
    cont: {
        margin: marginHorizontal,
        paddingBottom: fontSize16,
    },
    text: {
        fontFamily: FontNames.ProstoLight,
        fontSize: fontSize20,
        color: textPlaceholderColor,
        marginBottom: 5,
    },
    textTitle: {
        fontFamily: FontNames.ProstoLight,
        fontSize: fontSizeTitle,
        color: primaryColor3,
        marginBottom: 5,
    },
    version: {
        fontSize: fontSize5,
        textAlign: TextAlign.right
    }, 
};

Settings.propTYpes = {
    wallet: PropTypes.object,
    settings: PropTypes.object,
    servers: PropTypes.object,
    baseUrl: PropTypes.string,
    removeServer: PropTypes.func,
    currentServer: PropTypes.func,
    pingServer: PropTypes.func,
    pingReset: PropTypes.func,
    ping: PropTypes.object,
    getServers: PropTypes.func,
    getDefaultServerID: PropTypes.func,
    lang: PropTypes.string,
    recovery: PropTypes.object,
    setParam: PropTypes.func,
    getParam: PropTypes.func,
    pollTimeout: PropTypes.number,
    invoiceExpire: PropTypes.number,
    startSchedulePoll: PropTypes.func,
    stopSchedulePoll: PropTypes.func,
};

const mapStateToProps = createStructuredSelector({
    wallet: makeSelectWallet(),
    settings: makeSelectSettings(),
    baseUrl: makeSelectUrlEraChain(),
    servers: makeSelectServers(),
    ping: makeSelectPing(),
    defaultServerID: makeSelectDefaultServerID(),
    lang: makeSelectLang(),
    recovery: makeSelectRestore(),
    pollTimeout: makeSelectPollTimeout(),
    invoiceExpire: makeSelectInvoiceExpire(),
});

function mapDispatchToProps(dispatch) {
    return {
        removeServer: (id) => dispatch(serversActions.actionRemoveServer(id)),
        currentServer: (id) => dispatch(serversActions.actionCurrentServer(id)),
        pingServer: (url, source) => dispatch(serversActions.actionPingServer(url, source)),
        pingReset: () => dispatch(serversActions.actionPingServerReset()),
        getServers: () => dispatch(serversActions.actionListServers()),
        getDefaultServerID: () => dispatch(dbActions.actionGetParam('DefaultServer')),
        recoveryInvoices: (fromTimestamp) => dispatch(invoiceActions.actionRestoreInvoices(fromTimestamp)),
        setParam: (param) => dispatch(dbActions.actionSetParam(param)), 
        getParam: (name) => dispatch(dbActions.actionGetParam(name)),
        startSchedulePoll: () => dispatch(pollActions.actionSchedulePoll()),
        stopSchedulePoll: () => dispatch(pollActions.actionSchedulePollStop()),
    };
}

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(Settings);