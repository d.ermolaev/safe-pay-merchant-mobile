import React from 'react';
import PropTypes from 'prop-types';
import { Clipboard, Image, Platform, Text, TextInput, View, ActivityIndicator, Keyboard } from 'react-native';
import { marginHorizontal, textBorderColor, primaryColorLight, secondaryColor, styles, textPlaceholderColor, peachColor, primaryColor3, fontSizeInput } from '../../config/styles';
import {
    ImageResources,
    NavigationBar,
    BackHandled,
    FooterBtn,
    LineSeparator,
} from '../../components';
import { strings } from '../../config/localize';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import * as navigationActions from '../../actions/navigationActions';
import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect';
import { 
    makeSelectRequisite,
    makeSelectDefaultRequisiteID 
} from '../../selectors/dbSelectors';
import * as dbActions from '../../actions/dbActions';
import Toast from 'react-native-simple-toast';
import AwesomeAlert from 'react-native-awesome-alerts';
import { initAlert } from '../Common';
import { 
    validateRecipient,
    validateBank,
    validateCAcct,
    validateAcct,  
    validateBIK,
    validateINN,
    getRecipient,
    getBank,
    getCAcct,
    getAcct,
    getBIK,
    getINN,
    getKPP
} from '../../core/util/validate';

const amountKeyboardType = Platform.OS === 'android' ? Platform.Version > 19 ? 'numeric' : 'phone-pad' : 'numeric';

export class BankDetails extends BackHandled {
    //noinspection JSUnusedGlobalSymbols
    static navigationOptions = {
        header: false,
    };

    warning = (title, text) => {
        this.setState({
            alert: {
                title: `${title} `,
                message: `${text} `, 
                show: true,
                showProgress: false,
                closeOnTouchOutside: true,
                closeOnHardwareBackPress: true,
                showCancelButton: true,
                showConfirmButton: true,
                cancelText: `${strings['ButtonCancel']} `,
                confirmText: `${strings['ButtonOk']} `,
                onCancelPressed: this.hideAlert,
                onConfirmPressed: this.save,
            }
        });
    };

    hideAlert = () => {
        setTimeout(() => {
            this.props.telegramReset();
            this.setState({
                alert: {
                    ...this.state.alert,
                    show: false,
                },
                lockButton: false,
            });
        }, 250);
    };

    menu = () => {
        navigationActions.toggle();
    };

    copy = (text) => {

        Clipboard.setString(text);
        Toast.show(strings['Copied to clipboard']);
        
    };

    scrollView;

    setScrollViewRef = (item) => {
        //console.log(item);
        this.scrollView = item;
    };

    onChangedAccount = (text) => {
        this.setState({ acct: getAcct(text), edited: true });
    }

    onChangedRecipient = (text) => {
        this.setState({ recipient: getRecipient(text), edited: true });
    }

    onChangedBank = (text) => {
        this.setState({ bank: getBank(text), edited: true });
    }

    onChangedCAccount = (text) => {
        this.setState({ cacct: getCAcct(text), edited: true });
    }

    onChangedBIK = (text) => {
        this.setState({ bik: getBIK(text), edited: true });
    }

    onChangedINN = (text) => {
        this.setState({ inn: getINN(text), edited: true });
    }

    onChangedKPP = (text) => {
        this.setState({ kpp: getKPP(text), edited: true });
    }

    validColor = (valid) => {
        if (valid) {
            return primaryColor3;
        } else {
            return peachColor;
        }
    }

    save = () => {
        const {
            recipient,
            acct,
            cacct,
            bank, 
            bik,
            inn,
            kpp,
        } = this.state;
        const row = {
            id: this.props.id,
            recipient,
            acct,
            cacct,
            bank, 
            bik,
            inn,
            kpp
        };
        if (row.id > 0) {
            this.props.update(row);
        } else {
            this.props.add(row);
        }
        Keyboard.dismiss();
    }

    constructor(props) {
        super(props);
        this.focusListener = null;
        this.state = {
            ...this.props.requisite,
            alert: {
                ...initAlert,
            },
            edited: false,
        };
    }

    componentDidMount() {
        this.props.getDefaultID();
    }

    componentDidUpdate(prevProps) {
        if (prevProps.id !== this.props.id) {
            this.props.getRequisite({ id: this.props.id });
        }
        if (prevProps.requisite !== this.props.requisite) {
            this.setState({ ...this.props.requisite, edited: false });
        }
    }

    renderMultiLine = (text) => {
        const list = text.split(' ');
        return (
            <View style={{...styles.myWallet.textsubSubContainer}}>
                {list.map((word, index) => {
                    return <Text key={index} style={styles.sent.breakWord}>{word}</Text>;
                })}
            </View>
        );
    }

    render() {
        const {
            recipient,
            acct,
            cacct,
            bank, 
            bik,
            inn,
            kpp,
            edited
        } = this.state;

        const { loading } = this.props.requisite;

        const validAccount = validateAcct(acct);
        const validCAcct = validateCAcct(cacct);
        const validRecipient = validateRecipient(recipient);
        const validBank = validateBank(bank);
        const validBIK = validateBIK(bik);
        const validINN = validateINN(inn);
        const validKPP = true;
        
        const isConfirm = edited && !loading && validAccount && validCAcct && validBank && validRecipient && validBIK && validINN && validKPP;
        //console.log({ isConfirm, lockButton, sum, value, isQRCode, valid });

        const labelStyle = {...styles.sent.titleMessage, color: textPlaceholderColor};
        //const amountStyle = {...styles.sent.textInput, textAlign: 'right', color: primaryColor3};

        return (
            <View style={styles.container}>
                <Image resizeMode={'stretch'} source={ImageResources.background} style={styles.shared.topBackgroundImage}/>
                <NavigationBar
                    title={strings['Bank details']}
                    leftButton={ImageResources.icon_menu}
                    onLeftButtonPress={this.menu}
                />
                <View style={[styles.sent.subContainer, { marginBottom: 60 }]}>
                    <KeyboardAwareScrollView
                        contentContainerStyle={{paddingTop: 16, paddingBottom: 60}}
                        viewIsInsideTabBar={true}
                        showsVerticalScrollIndicator={false}
                        enableAutoAutomaticScroll={true}
                        ref={this.setScrollViewRef}>
                        <View style={{paddingHorizontal: marginHorizontal}}>
                            <View style={{flex: 1 }}>
                                <View style={styles.sent.rowRequisite}>
                                    <View style={styles.sent.labelContainer}>
                                        <Text style={labelStyle}>{`${strings['Recipient']}: `}</Text>
                                    </View>
                                    <View style={styles.sent.valueContainer}>
                                        <TextInput
                                            placeholder={`${strings['Recipient']} `}
                                            style={{ color: this.validColor(validRecipient), textAlign: 'right', paddingBottom: 0, paddingTop: 0, fontSize: fontSizeInput }}
                                            multiline={true}
                                            numberOfLines={2}
                                            underlineColorAndroid={'transparent'}
                                            placeholderTextColor={'#A1ACBA'}
                                            value={recipient} 
                                            onChangeText={this.onChangedRecipient}
                                            blurOnSubmit={true}
                                        />
                                    </View>
                                </View>
                                <LineSeparator />
                                <View style={styles.sent.rowRequisite}>
                                    <View style={styles.sent.labelContainer}>
                                        <Text style={labelStyle}>{`${strings['Account']}: `}</Text>
                                    </View>
                                    <View style={styles.sent.valueContainer}>
                                        <TextInput
                                            placeholder={`${strings['Account']} `}
                                            style={{ color: this.validColor(validAccount), textAlign: 'right', paddingBottom: 0, paddingTop: 0, fontSize: fontSizeInput }}
                                            multiline={false}
                                            numberOfLines={1}
                                            underlineColorAndroid={'transparent'}
                                            placeholderTextColor={'#A1ACBA'}
                                            value={acct} 
                                            onChangeText={this.onChangedAccount}
                                            blurOnSubmit={true}
                                            keyboardType={amountKeyboardType}
                                        />
                                    </View>
                                </View>
                                <LineSeparator />
                                <View style={styles.sent.rowRequisite}>
                                    <View style={styles.sent.labelContainer}>
                                        <Text style={labelStyle}>{`${strings['Bank']}: `}</Text>
                                    </View>
                                    <View style={styles.sent.valueContainer}>
                                        <TextInput
                                            placeholder={`${strings['Bank']} `}
                                            style={{ color: this.validColor(validBank), textAlign: 'right', paddingBottom: 0, paddingTop: 0, fontSize: fontSizeInput }}
                                            multiline={true}
                                            numberOfLines={2}
                                            underlineColorAndroid={'transparent'}
                                            placeholderTextColor={'#A1ACBA'}
                                            value={bank} 
                                            onChangeText={this.onChangedBank}
                                            blurOnSubmit={true}
                                        />
                                    </View>
                                </View>
                                <LineSeparator />
                                <View style={styles.sent.rowRequisite}>
                                    <View style={styles.sent.labelContainer}>
                                        <Text style={labelStyle}>{`${strings['Corresponding account']}: `}</Text>
                                    </View>
                                    <View style={styles.sent.valueContainer}>
                                        <TextInput
                                            placeholder={`${strings['Corresponding account']} `}
                                            style={{ color: this.validColor(validCAcct), textAlign: 'right', paddingBottom: 0, paddingTop: 0, fontSize: fontSizeInput }}
                                            multiline={false}
                                            numberOfLines={1}
                                            underlineColorAndroid={'transparent'}
                                            placeholderTextColor={'#A1ACBA'}
                                            value={cacct} 
                                            onChangeText={this.onChangedCAccount}
                                            blurOnSubmit={true}
                                            keyboardType={amountKeyboardType}
                                        />
                                    </View>
                                </View>
                                <LineSeparator />
                                <View style={styles.sent.rowRequisite}>
                                    <View style={styles.sent.labelContainer}>
                                        <Text style={labelStyle}>{`${strings['BIK']}: `}</Text>
                                    </View>
                                    <View style={styles.sent.valueContainer}>
                                        <TextInput
                                            placeholder={`${strings['BIK']} `}
                                            style={{ color: this.validColor(validBIK), textAlign: 'right', paddingBottom: 0, paddingTop: 0, fontSize: fontSizeInput }}
                                            multiline={false}
                                            numberOfLines={1}
                                            underlineColorAndroid={'transparent'}
                                            placeholderTextColor={'#A1ACBA'}
                                            value={bik} 
                                            onChangeText={this.onChangedBIK}
                                            blurOnSubmit={true}
                                            keyboardType={amountKeyboardType}
                                        />
                                    </View>
                                </View>
                                <LineSeparator />
                                <View style={styles.sent.rowRequisite}>
                                    <View style={styles.sent.labelContainer}>
                                        <Text style={labelStyle}>{`${strings['KPP']}: `}</Text>
                                    </View>
                                    <View style={styles.sent.valueContainer}>
                                        <TextInput
                                            placeholder={`${strings['KPP']} `}
                                            style={{ color: this.validColor(validKPP), textAlign: 'right', paddingBottom: 0, paddingTop: 0, fontSize: fontSizeInput }}
                                            multiline={false}
                                            numberOfLines={1}
                                            underlineColorAndroid={'transparent'}
                                            placeholderTextColor={'#A1ACBA'}
                                            value={kpp} 
                                            onChangeText={this.onChangedKPP}
                                            blurOnSubmit={true}
                                            keyboardType={amountKeyboardType}
                                        />
                                    </View>
                                </View>
                                <LineSeparator />
                                <View style={styles.sent.rowRequisite}>
                                    <View style={styles.sent.labelContainer}>
                                        <Text style={labelStyle}>{`${strings['INN']}: `}</Text>
                                    </View>
                                    <View style={styles.sent.valueContainer}>
                                        <TextInput
                                            placeholder={`${strings['INN']} `}
                                            style={{ color: this.validColor(validINN), textAlign: 'right', paddingBottom: 0, paddingTop: 0, fontSize: fontSizeInput }}
                                            multiline={false}
                                            numberOfLines={1}
                                            underlineColorAndroid={'transparent'}
                                            placeholderTextColor={'#A1ACBA'}
                                            value={inn} 
                                            onChangeText={this.onChangedINN}
                                            blurOnSubmit={true}
                                            keyboardType={amountKeyboardType}
                                        />
                                    </View>
                                </View>
                                <LineSeparator />
                            </View>
                            {loading ?
                                <ActivityIndicator animating={true}
                                    color={textBorderColor}
                                    size={'small'}/> : null
                            }
                        </View>
                    </KeyboardAwareScrollView>
                </View>
                <FooterBtn onPress={this.save} disabled={!isConfirm}>
                    <Image style={styles.myWallet.buttonImage180}
                        source={ImageResources.icon_sent}/>
                    <Text style={styles.myWallet.buttonText}>{`${strings['Save']} `}</Text>
                </FooterBtn>
                <AwesomeAlert
                    {...this.state.alert}
                    titleStyle={styles.alert.title}
                    messageStyle={styles.alert.text}
                    overlayStyle={styles.alert.overlayStyle}
                    cancelButtonColor={secondaryColor}
                    confirmButtonColor={primaryColorLight}
                />
            </View>
        );
    }

    onFocus = () => {
        setTimeout(() => this.scrollView.scrollToEnd(true), 250);
    }
}

BankDetails.propTypes = {
    id: PropTypes.number,
    requisite: PropTypes.object,
    getDefaultID: PropTypes.func,
    add: PropTypes.func,
    update: PropTypes.func,
    getRequisite: PropTypes.func,
};

const mapStateToProps = createStructuredSelector({
    id: makeSelectDefaultRequisiteID(),
    requisite: makeSelectRequisite(),
});

function mapDispatchToProps(dispatch) {
    return {
        getDefaultID: () => dispatch(dbActions.actionGetParam('DefaultRequisiteID')),
        getRequisite: (row) => dispatch(dbActions.actionGetRequisite(row)),
        add: (row) => dispatch(dbActions.actionAddRequisite(row)),
        update: (row) => dispatch(dbActions.actionUpdateRequisite(row)),
    };
}

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(BankDetails);
