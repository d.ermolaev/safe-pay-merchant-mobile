export const initAlert = {
    show: false,
    showProgress: false,
    title: '',
    message: '',
    closeOnTouchOutside: false,
    closeOnHardwareBackPress: false,
    showCancelButton: false,
    showConfirmButton: false,
    cancelText: '',
    confirmText: '',
};