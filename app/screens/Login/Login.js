import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Keyboard, View } from 'react-native';
import { styles } from '../../config/styles';
import { 
    LargeLogo,
    Loading,
    PinInput,
} from '../../components';
import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect';
import * as navigationActions from '../../actions/navigationActions';
import * as walletActions from '../../actions/walletActions';
import * as configActions from '../../actions/configActions';
import * as dbActions from '../../actions/dbActions';
import { 
    makeSelectAuth
} from '../../selectors/dbSelectors';
import {
    makeSelectWallet
} from '../../selectors/walletSelectors';
import { strings } from '../../config/localize';
//import LocalStore from '../../store/LocalStore';
import TouchID from 'react-native-touch-id';
import Toast from 'react-native-simple-toast';

//import { Base58 } from '../../core/crypt/Base58';
//import { testCrypt } from '../../core/EraChainCrypto';

const maxLengthPin = 4;

const optionalConfigObject = {
    title: strings['TouchID title'], // Android
    imageColor: '#e00606', // Android
    imageErrorColor: '#ff0000', // Android
    sensorDescription: strings['Sensor description'], // Android
    sensorErrorDescription: strings['Sensor error description'], // Android
    cancelText: strings['ButtonCancel'], // Android
    fallbackLabel: strings['Show Passcode'], // iOS (if empty, then label is hidden)
    unifiedErrors: false, // use unified error messages (default false)
    passcodeFallback: false, // iOS - allows the device to fall back to using the passcode, if faceid/touch is not available. this does not mean that if touchid/faceid fails the first few times it will revert to passcode, rather that if the former are not enrolled, then it will use the passcode.
};

export class Login extends Component {
    //noinspection JSUnusedGlobalSymbols
    static navigationOptions = {
        header: false,
        headerLeft: null,
    };

    supportedTouchID = () => {
        TouchID.isSupported()
            .then(this.authenticate)
            .catch(error => {
                //console.log(error);
                this.touchIDStarted = 1;
                //this.offTouchID();
                throw new Error(error);
            });
    }

    loading = (val) => {
        this.setState({ loading: val });
    }

    offTouchID = () => {
        this.setState({ closeTouchID: true });
    }

    authenticate = () => {
        
        if (this.touchIDStarted === 0) {
            //console.log({ state: this.state, msg: 'start authenticate()'});
            this.touchIDStarted++;
            this.loading(true);
            return TouchID.authenticate(strings['TouchID prompt'], optionalConfigObject)
                .then(() => {
                    this.touchIDStarted--;
                    //console.log({ pin: this.props.auth.pin });
                    this.offTouchID();
                    this.props.queryWallet(this.props.auth.pin);                    
                })
                .catch(() => {
                    //console.log(error);
                    //this.setState({ closeTouchID: true });
                    this.loading(false);
                    this.offTouchID();
                    
                });
        }
    }

    changePin = (pin) => {
        this.setState({pin});
        if (pin.length === maxLengthPin) {
            this.setState({ loading: true });
            setTimeout(() => {
                this.props.queryWallet(pin);
                Keyboard.dismiss();
            }, 200);   
        }
    }

    constructor(props) {
        super(props);
        this.touchIDStarted = 0;
        this.state = {
            pin: '',
            loading: false,
            closeTouchID: false,
            alert: {
                show: false,
                title: '',
                text: '',
            }
        };
    }

    async componentDidMount() {
        this.props.clearWallet();
    }

    componentDidUpdate(prevProps) {
        const { wallet } = prevProps;

        if (!wallet.keyPair && this.props.wallet.keyPair && !this.props.wallet.error) {
            //console.log('navigateToHome()');
            this.loading(false);
            navigationActions.navigateToHome();
        }
        if (!wallet.error && this.props.wallet.error) {
            this.setState({ pin: '', loading: false });
            Toast.show(strings['error_19']);
            this.props.clearWallet();
        }
    }

    render() {
        //console.log(this.state);  
        //console.log(this.props.lastBlock);      
        const { pin, loading, closeTouchID } = this.state;
        if (!closeTouchID) {
            this.supportedTouchID();   
        }
        return (
            <View style={styles.container}>
                <Loading state={loading}/>

                <View style={styles.login.containerTop}>
                    <LargeLogo/>
                </View>

                <View style={styles.login.containerBottom}>
                    <PinInput placeholder={strings['input pin']} onPinChaged={this.changePin} pin={pin}/>
                </View>
            </View>
        );
    }
}

Login.propTypes = {
    restore: PropTypes.func,
    queryWallet: PropTypes.func,
    clearWallet: PropTypes.func,
    auth: PropTypes.object,
    wallet: PropTypes.object,
};

const mapStateToProps = createStructuredSelector({
    auth: makeSelectAuth(),
    wallet: makeSelectWallet(),
});

function mapDispatchToProps(dispatch) {
    return {
        restore: (data) => dispatch(configActions.actionRestoreRedux(data)),
        queryWallet: (pin) => dispatch(dbActions.actionWalletQuery(pin)),
        clearWallet: () => dispatch(walletActions.actionWalletClear()),
    };
}

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(Login);

