import React, { Component } from 'react';
import { FlatList, Linking, View } from 'react-native';
import { styles } from '../../config/styles';
import { 
    ImageResources, 
    DrawerMenuItem,
    MenuHeader 
} from '../../components';
import { strings } from '../../config/localize';
import * as navigationActions from '../../actions/navigationActions';

class Menu extends Component {
    static navigationOptions = {
        header: false,
        gesturesEnabled: true,
    };

    constructor(props) {
        super(props);
        this.menuItems = [];
    }

    renderItem = (v) => {
        const { item } = v;
        return (
            <DrawerMenuItem
                onPress={this.onDrawerPress(item)}
                id={item.id}
                route={item.route}
                icon={item.icon}
                title={item.title}
            />
        );
    };

    keyExtractor = (item) => {
        return item.id;
    };

    onDrawerPress = (item) => {
        return () => {
            navigationActions.hideMenu();
            if (item.id === 'License') {
                const url = strings['LicenseURL'];
                Linking.openURL(url); 
            } else {
                //console.log({ item });
                navigationActions.reset(item.route);
            }
        };
    };

    componentDidMount() {
        //stores.accountStore.loadProfile();
    }

    render() {
        const footer = this.footer();
        this.menuItems = this.getMenu();
        const menuItems = this.filterMenu();
        return (
            <View style={styles.menu.container}>
                <View style={styles.menu.itemsContainer}>
                    <FlatList
                        bounces={false}
                        ListHeaderComponent={MenuHeader}
                        data={menuItems}
                        renderItem={this.renderItem}
                        keyExtractor={this.keyExtractor}
                        showsVerticalScrollIndicator={false}
                    />
                </View>
                {footer}
            </View>
        );
    }

    footer() {
        return (
            <View style={styles.menu.settingsContainer}>
                <View style={styles.menu.line}/>
                <DrawerMenuItem
                    onPress={this.onDrawerPress({
                        id: 'Settings',
                        route: 'Settings',
                        title: '',
                        icon: ImageResources.icon_settings
                    })}
                    id={'Settings'}
                    route={'Settings'}
                    icon={ImageResources.icon_settings}
                    title={strings['Settings']}
                />
            </View>);
    }

    filterMenu = () => {
        //return this.menuItems.filter(m => m.isAuthorized === undefined || m.isAuthorized === (stores.accountStore.profile && stores.accountStore.profile.isAuthorized));
        return this.menuItems.filter(m => m.isAuthorized === undefined || m.isAuthorized === true);
    };

    getMenu = () => {
        return [
            {
                id: 'MyWallet',
                route: 'MyWallet',
                icon: ImageResources.icon_receive,
                title: `${strings['My wallet']} `
            },
            {
                id: 'Requisite',
                route: 'Requisite',
                icon: ImageResources.icon_wallet,
                title: `${strings['Bank details']} `
            },
            {
                id: 'Patterns',
                route: 'Patterns',
                icon: ImageResources.icon_identification,
                title: strings['List patterns'],
            },
            {
                id: 'License',
                route: '',
                icon: ImageResources.icon_agreement,
                title: strings['ShowLicense'],
            },
            {
                id: 'Database',
                route: 'Database',
                icon: ImageResources.icon_refresh,
                title: strings['Menu store'],
            }
        ];
    };
}

export default Menu; 
