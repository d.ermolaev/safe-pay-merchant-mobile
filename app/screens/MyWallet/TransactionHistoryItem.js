import React from 'react';
import PropTypes from 'prop-types';
import { Image, Text, TouchableOpacity, View } from 'react-native';
import { styles } from '../../config/styles';
import { 
    ImageResources, 
    BackHandled
} from '../../components';
import { strings } from '../../config/localize';
import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect';
import { 
    makeSelectCurrency
} from '../../selectors/configSelectors';
import * as navigationActions from '../../actions/navigationActions';


class TransactionHistoryItem extends BackHandled {

    onPress = () => {
        //console.log('onPress');
        if (this.props.confirmed >= 0) {
            navigationActions.navigate('InvoiceView', { ...this.props.invoice, code: this.props.currency.code } );
        }
    };

    render() {
        const { numOrder } = this.props.invoice;
        const img = this.props.confirmed > 0 || this.props.confirmed === -1 ? null :
            <Image source={ImageResources.icon_exclamation_point} style={styles.historyItem.image2}/>;
            
        let confirmationStyle = styles.historyItem.unconfirmation;
        let confirmation = strings['Invoice unconfirmed'];  
        switch (this.props.confirmed) {
        case -1:
            confirmationStyle = styles.historyItem.expired;
            confirmation = strings['Invoice expired'];
            break;
        case 1:
        case 2:    
            confirmationStyle = styles.historyItem.confirmation;
            confirmation = strings['Invoice confirmation'];
        }
        //console.log(confirmation);

        return (
            <TouchableOpacity onPress={this.onPress} activeOpacity={0.9} style={styles.historyItem.container}>
                <View style={styles.historyItem.dateSumContainer}>
                    <Text style={styles.historyItem.date}>{`${strings['No']} ${numOrder}   ${this.props.dateTime} `}</Text>
                    <View style={{ flexDirection: 'row' }}>
                        <Text style={styles.historyItem.sum}>{`${this.props.sum} `}</Text>
                        <Text style={styles.historyItem.curr}>{`${this.props.currency.code} `}</Text>
                    </View>
                </View>
                <View style={styles.historyItem.confirmationContainer}>
                    <Text style={confirmationStyle}>{`${confirmation} `}</Text>
                    {img}
                </View>
                <Text style={styles.historyItem.header}>{`${this.props.header} `}</Text>
                <View style={styles.historyItem.bottomContainer}>
                    <View style={styles.historyItem.bottomLeftContainer}>
                        <Text style={styles.historyItem.accountLabel}>{`${strings['account number']} `}</Text>
                        <Text style={styles.historyItem.account}>{`${this.props.account} `}</Text>
                    </View>
                    {/*<Image style={styles.historyItem.image}
                        source={this.props.positive ? ImageResources.icon_receive : ImageResources.icon_send}/>*/}
                </View>
            </TouchableOpacity>
        );
    }
}

TransactionHistoryItem.propTypes = {
    sum: PropTypes.string,
    dateTime: PropTypes.string,
    header: PropTypes.string,
    positive: PropTypes.bool,
    account: PropTypes.string,
    index: PropTypes.number,
    confirmed: PropTypes.number,
    invoice: PropTypes.object,
    currency: PropTypes.object,
};

const mapStateToProps = createStructuredSelector({
    currency: makeSelectCurrency(),
});

export default connect(
    mapStateToProps,
    null
)(TransactionHistoryItem);