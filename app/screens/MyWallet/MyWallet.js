import React from 'react';
import PropTypes from 'prop-types';
import { primaryColorLight, styles, textBorderColor, fontSize14, fontSize7, marginHorizontal } from '../../config/styles';
import { 
    NavigationBar,
    ImageResources,
    BackHandled,
    LineSeparator,
    WalletComponent,
} from '../../components';
import { Btn } from '../../components/Btn/Btn';

import TransactionHistoryItem from './TransactionHistoryItem';
import { strings } from '../../config/localize';
//import { WalletComponent } from '../../common/WalletComponent';

//import { ServerStatusWidget } from '../../common/ServerStatusWidget';
import { ActivityIndicator, FlatList, Image, View } from 'react-native';
import { JustifyContent, Positions } from '../../config/RNStyles';
import * as navigationActions from '../../actions/navigationActions';
import * as dbActions from '../../actions/dbActions';
import * as pollActions from '../../actions/pollActions';
import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect';
import { 
    makeSelectAll
} from '../../selectors/configSelectors';
import { makeSelectPoll } from '../../selectors/chainSelectors';
import { makeSelectHistory } from '../../selectors/dbSelectors';
import { makeSelectWallet } from '../../selectors/walletSelectors';
import moment from 'moment/min/moment-with-locales';

class MyWallet extends BackHandled {

    static navigationOptions = {
        header: false,
        headerLeft: null,
    };

    constructor(props) {
        super(props);
        this.state = {
            page: 1,
        };
        this.focusListener = null;
    }

    keyExtractor = (item, index) => item.order || index.toString();

    renderItem = () => {
        return null;
    }

    renderItem = ({ item, index }) => {
        //console.log(item);
        return (
            <TransactionHistoryItem
                index={index}
                positive={true}
                sum={item.sum.toFixed(2).toString()}
                header={item.description}
                dateTime={moment.unix(item.dt / 1000).format('YYYY-MM-DD HH:mm')}
                account={item.recipient}
                confirmed={item.status}
                invoice={item}
            />
        );
    };

    refresh = () => {
        //console.log('refresh');
        //console.log({ pages: this.props.invoices.pages, page: this.state.page });
        this.props.getInvoicesPages();
        this.props.getInvoices(1);
        this.setState({ page: 2 });
    };

    menu = () => {
        //stores.navigationStore.drawerOpen();
        navigationActions.toggle();
    };

    goToSent = async () => {
        this.props.startPoll();
    };

    goToReceive = async () => {
        navigationActions.navigate('Receive');
    };

    onEndReached = () => {
        const { page } = this.state;
        //console.log({ pages: this.props.invoices.pages, page: this.state.page });
        if (page <= this.props.invoices.pages) {
            this.props.getInvoices(page);
            this.setState({ page: page + 1 });
        }
    };

    componentDidMount() {
        this.focusListener = this.props.navigation.addListener('willFocus', this.refresh);
    }

    componentWillUnmount() {
        if (this.focusListener) {
            this.focusListener.remove();
        }
    }

    render() {
        const { loading } = this.props.invoices;
        //console.log({leftButton:ImageResources.icon_menu});
        //console.log({ pages: this.props.invoices.pages, page: this.state.page });
        return (
            <View style={styles.container}>
                <Image resizeMode={'stretch'} source={ImageResources.background}
                    style={styles.shared.topBackgroundImage}/>
                <NavigationBar
                    title={strings['My wallet']}
                    leftButton={ImageResources.icon_menu}
                    onLeftButtonPress={this.menu}
                    rightWidget={null}
                />
                <View style={styles.myWallet.container}>

                    <FlatList
                        ListHeaderComponent={WalletComponent}
                        refreshing={false}
                        data={this.props.invoices.data}
                        keyExtractor={this.keyExtractor}
                        renderItem={this.renderItem}
                        showsVerticalScrollIndicator={false}
                        ItemSeparatorComponent={LineSeparator}
                        onEndReached={this.onEndReached}
                        onEndReachedThreshold={0.5}
                    />
                    {/*stores.walletStore.loading*/}
                    { loading ?
                        <View style={{height: 40, justifyContent: 'center'}}><ActivityIndicator animating={true}
                            color={textBorderColor}
                            size={'small'}/></View> : null}

                </View>

                <View style={_styles.container}>
                    <Btn title={strings['New order']} fill={true} onPress={this.goToReceive}
                        fillColor={primaryColorLight}
                        customBtnStyles={{ flex: 1, marginLeft: fontSize14, marginRight: fontSize7 }}/>
                    <Btn title={strings['Check status']} fill={true} onPress={this.goToSent} 
                        customBtnStyles={{ flex: 1, marginRight: fontSize14, marginLeft: fontSize7 }}
                        disabled={this.props.poll.executing}
                    />
                </View>
                {/*<FooterBtn onPress={this.goToSent}>*/}
                {/*<Image style={styles.myWallet.buttonImage} source={ImageResources.icon_sent}/>*/}
                {/*<Text style={styles.myWallet.buttonText}>{f`Sent`}</Text>*/}
                {/*</FooterBtn>*/}
            </View>
        );
    }
}

const _styles = {
    container: {
        position: Positions.absolute,
        bottom: 0,
        left: 0,
        right: 0,
        height: 116,
        justifyContent: JustifyContent.center,
        flex: 1,
        flexDirection: 'row',
        alignItems: 'center',
        paddingHorizontal: marginHorizontal,
    },
};

MyWallet.propTypes = {
    data: PropTypes.object,
    invoices: PropTypes.object,
    getInvoices: PropTypes.func,
    getInvoicesPages: PropTypes.func,
    startPoll: PropTypes.func,
    poll: PropTypes.object,
    wallet: PropTypes.object,
};

const mapStateToProps = createStructuredSelector({
    data: makeSelectAll(),
    invoices: makeSelectHistory(),
    poll: makeSelectPoll(),
    wallet: makeSelectWallet(),
});

function mapDispatchToProps(dispatch) {
    return {
        getInvoices: (page) => dispatch(dbActions.actionInvoices(page)),
        getInvoicesPages: () => dispatch(dbActions.actionInvoicesPages()),
        startPoll: () => dispatch(pollActions.actionStartPoll()),
    };
}

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(MyWallet);
