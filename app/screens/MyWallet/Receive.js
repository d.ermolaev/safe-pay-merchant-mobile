import React from 'react';
import PropTypes from 'prop-types';
import { Clipboard, Image, Platform, Text, TextInput, TouchableOpacity, View, ActivityIndicator } from 'react-native';
import { marginHorizontal, primaryColorLight, secondaryColor, dWidth, styles, textPlaceholderColor, textBorderColor, peachColor, primaryColor3, fontSizeCurr, fontSizeInput } from '../../config/styles';
import {
    ImageResources,
    NavigationBar,
    BackHandled,
    FooterBtn,
    LineSeparator,
    BottomMenu
} from '../../components';
import { strings } from '../../config/localize';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import QRCode from 'react-native-qrcode-svg';
import { initialState } from '../../reducers/invoiceReducer';
import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect';
import { makeSelectWallet } from '../../selectors/walletSelectors';
import { 
    currencyOf,
    makeSelectDefaultAddress,
    makeSelectNetworkPort,
    makeSelectInvoiceExpire,
    makeSelectCurrencies, 
    makeSelectFormatQRGost,
    makeSelectDefaultPublicKey,
} from '../../selectors/configSelectors';
import { makeSelectTelegram } from '../../selectors/telegramSelectors';
import { makeSelectCRUD, makeSelectUnique, makeSelectMaxOrder } from '../../selectors/dbSelectors';
import { makeSelectShowQRCode } from '../../selectors/invoiceSelectors';
import { 
    makeSelectPatterns,
    makeSelectDefaultTitle,
    makeSelectDefaultDesc,
    makeSelectRequisite,
    makeSelectDefaultRequisiteID 
} from '../../selectors/dbSelectors';
import moment from 'moment/min/moment-with-locales';
import uuid from 'react-native-uuid';
import PrivateKeyAccount from '../../core/account/PrivateKeyAccount';
import Account from '../../core/account/Account';
import { Base58 } from '../../core/crypt/Base58';
import R_Send from '../../core/transaction/R_Send';
import * as telegramActions from '../../actions/telegramActions';
import * as dbActions from '../../actions/dbActions';
import * as invoiceActions from '../../actions/invoiceActions';
import { convertPattern } from '../../reducers/invoiceReducer';
import Toast from 'react-native-simple-toast';
import AwesomeAlert from 'react-native-awesome-alerts';
import { initAlert } from '../Common';
import { validateEmptyPhone, numberPhone } from '../../core/util/validate';
import { encryptMessage } from '../../core/EraChainCrypto';
import { Bytes } from '../../core/Bytes';
import { checkError } from '../../core/request/checkError';

const amountKeyboardType = Platform.OS === 'android' ? Platform.Version > 19 ? 'numeric' : 'phone-pad' : 'numeric';
const phoneKeyboardType = 'phone-pad';

export class Receive extends BackHandled {
    //noinspection JSUnusedGlobalSymbols
    static navigationOptions = {
        header: false,
    };

    warning = (title, text) => {
        this.setState({
            alert: {
                title: `${title} `,
                message: `${text} `, 
                show: true,
                showProgress: false,
                confirmText: `${strings['ButtonOk']} `,
                closeOnTouchOutside: true,
                closeOnHardwareBackPress: true,
                showCancelButton: false,
                showConfirmButton: true,
                onConfirmPressed: () => {
                    this.hideAlert();
                },
            }
        });
    };

    hideAlert = () => {
        setTimeout(() => {
            this.props.telegramReset();
            this.setState({
                alert: {
                    ...this.state.alert,
                    show: false,
                },
                lockButton: false,
            });
        }, 250);
    };

    onBack = () => {
        this.back();
    };

    copy = (text) => {
        Clipboard.setString(text);
        Toast.show(strings['Copied to clipboard']);
    };

    convertDefauls = (invoice) => {
        const { defaultTitle, defaultDesc } = invoice;
        const title =  convertPattern(invoice, defaultTitle);
        const description = convertPattern(invoice, defaultDesc);
        this.setState({title, description});
    }

    reset = () => {
        const date = moment().valueOf();
        const expire = moment().add(this.props.expireSec, 'seconds').valueOf();
        const qrcode = uuid.v1();
        const user = '';
        const detail = '-';
        /*
        const invoice = {
            ...this.state,
            date,
            expire,
            qrcode,
            user,
            detail
        };
        */
        //this.convertDefauls(invoice);
        this.setState({ 
            date,
            expire,
            qrcode,
            user,
            detail
        });
    }

    existsRequsites = () => {
        const { recipient, bank, acct, cacct, bik, inn } = this.props.requsite;
        return recipient && bank && acct && cacct && bik && inn;
    }

    generateQRString = () => {
        const { headQRGost, requsite, num } = this.props;
        const { recipient, bank, acct, cacct, bik, inn } = requsite;
        const { sum, qrcode, title, user } = this.state;
        const strsum = Number(sum).toFixed(2).toString().replace(/\./, '');
        const Name = recipient && recipient.length > 0 ? `|Name=${recipient}`:'';
        const PersonalAcc = acct && acct.length > 0 ? `|PersonalAcc=${acct}`:'';
        const BankName = bank && bank.length > 0 ? `|BankName=${bank}`:'';
        const CorrespAcc = cacct && cacct.length > 0 ? `|CorrespAcc=${cacct}`:'';
        const BIC = bik && bik.length > 0 ? `|BIC=${bik}`:'';
        const PayeeINN = inn && inn.length > 0 ? `|PayeeINN=${inn}`:'';
        const Purpose = title && title.length > 0 ? `|Purpose=${title}`:'';
        const Phone = user && user.length > 0?`|Phone=${user}`:'';
        const qrstring = `${headQRGost}2${Name}${PersonalAcc}${BankName}${CorrespAcc}${BIC}${PayeeINN}${Purpose}|Sum=${strsum}|DocNo=${num.value}|UIN=${qrcode}${Phone}`;
        return qrstring;
    }

    encrypt = async (msg) => {
        const { wallet, defaultPublicKey } = this.props;
        const publicKey = await Base58.decode(defaultPublicKey);
        return await encryptMessage(msg, publicKey, wallet.keyPair.secretKey);
    }

    send = async () => {
        this.setState({ lockButton: true });
        try {
            const { keyPair } = this.props.wallet;
            //console.log({ seed, keyPair, address, sendAddress: this.props.sendAddress });
            const {
                date,
                user,
                curr,
                sum,
                title,
                description,
                detail,
                confirmations,
                confirmSignature,
                qrcode,
            } = this.state;

            const uidUser = user?numberPhone(user):qrcode;

            const message = {
                date: moment().valueOf(),
                order: this.props.num.value,
                expire: moment().add(this.props.expireSec, 'seconds').valueOf(),
                user: uidUser,
                curr,
                sum,
                title,
                description,
                detail,
            };
            
            const qr = this.generateQRString();
            this.setState({ qrcode: qr });
            //console.log({ qr });
            const creator = new PrivateKeyAccount(keyPair);
            const recipient = new Account(this.props.sendAddress);

            const feePow = 0;
            const reference = 1;
            const key = 0;
            const isText = new Int8Array([1]);
            
            const timestamp = moment().valueOf();

            const amount = null;

            let isEncripted = new Int8Array([1]);
            let messageBytes = await this.encrypt(JSON.stringify(message));
            if (messageBytes === false) {
                const msg = JSON.stringify(message);
                messageBytes = await Bytes.stringToByteArray(msg);
                isEncripted = new Int8Array([0]);
            }
            
            const head = uidUser;

            //console.log({ head });
            const port = this.props.port;
            //console.log({ port: this.props.port });
            const tx = new R_Send(creator, feePow, recipient, key, amount, head, messageBytes, isText, isEncripted, timestamp, reference);
            await tx.sign(creator, false, port);
            const transaction = await Base58.encode(await tx.toBytes(true, null));

            const invoice = { ...message, num: this.props.num.value, user, address: this.props.wallet.address, qrcode: qr, recipient: this.props.sendAddress, status: 0, lastdt: date, attempts: 0, confirmations, confirmSignature };
            this.props.telegramRequest(transaction, invoice);

        } catch (e) {
            this.setState({ lockButton: false });
            this.warning(strings['Warning'], checkError(e));
            //console.log(e);
        }
    };

    scrollView;
    orderTextInput;
    phoneTextInput;
    sumTextInput;

    setScrollViewRef = (item) => {
        //console.log(item);
        this.scrollView = item;
    };

    editNum = () => {
        if (this.orderTextInput && !this.orderTextInput.isFocused()) {
            this.orderTextInput.focus();
        }
    }

    editPhone = () => {
        if (this.phoneTextInput && !this.phoneTextInput.isFocused()) {
            this.phoneTextInput.focus();
        }
    }

    numColor = (valid) => {
        if (valid) {
            return primaryColor3;
        } else {
            return peachColor;
        }
    }

    phoneColor = (valid) => {
        if (valid) {
            return primaryColor3;
        } else {
            return peachColor;
        }
    }

    onNumChanged = (text) => {
        let num = text.replace(/[^\d+.,]/g, '').replace(/[.,]+/g, '').replace(' ', '');
        num = num.replace('[\\w]+', '');
        if (/\.$/.test(text) && num.indexOf('.') !== num.length - 1) {
            num = num.replace(/\.$/, '');
        }
        this.props.checkNumUnique(num);
    };

    onAmountChanged = (text) => {
        let amount = text.replace(/[^\d+.,]/g, '').replace(/[.,]+/g, '.').replace(' ', '');
        amount = amount.replace('[\\w]+', '');
        if (/\.$/.test(text) && amount.indexOf('.') !== amount.length - 1) {
            amount = amount.replace(/\.$/, '');
        }
        const parts = amount.split('.');
        if (parts.length < 3) {
            if (parts.length === 2) {
                if (parts[1].length < 9) {
                    this.setState({ sum: amount });
                }
            } else {
                this.setState({ sum: amount });
            }
        }
    };

    onChangedPhone = (text) => {
        this.setState({ user: text });
    }

    showPatterns = () => {
        const { isQRCode } = this.props;
        if (!isQRCode) {
            this.setState({ showPatterns: true });
        }
    }

    showPatterns2 = () => {
        const { isQRCode } = this.props;
        if (!isQRCode) {
            this.setState({ showPatterns2: true });
        }
    }

    closeMenu = () => {
        this.setState({ showPatterns: false, showPatterns2: false, });
    }

    onSelectPattern = (item) => {
        //console.log(item);
        this.setState({ defaultTitle: item.pattern });
    }

    onSelectPattern2 = (item) => {
        //console.log(item);
        this.setState({ defaultDesc: item.pattern });
    }

    constructor(props) {
        super(props);
        this.focusListener = null;
        const invoice = initialState.get('invoice').toJS();
        this.state = {
            ...invoice,
            defaultTitle: '',
            defaultDesc: '',
            lockButton: false,
            showPatterns: false,
            showPatterns2: false,
            alert: {
                ...initAlert,
            },
        };
    }

    componentWillMount() {
        this.props.showQRCode(false);
        this.props.getMaxOrder();
        this.props.getDefaultTitle();
        this.props.getDefaultDesc();
        this.props.getPatterns();
        this.props.getDefaultRequisiteID();
    }

    componentDidMount() {
        this.reset();
    }
    
    componentDidUpdate(prevProps, prevState) {
        //console.log({ props: this.props, this.props });
        if (prevProps.requisiteID !== this.props.requisiteID) {
            this.props.getRequisite({ id: this.props.requisiteID });
        }
        const { error } = this.props.telegram;
        if (!prevProps.telegram.error && error) {
            //this.warning(strings['Warning'], checkError()strings['error_17']);
            this.warning(strings['Warning'], checkError(error));
        }

        const { date, sum, user, defaultTitle, defaultDesc } = prevState;
        const { value } = prevProps.num;
        //console.log({ defaultTitle: this.props.defaultTitle, defaultDesc: this.props.defaultDesc });
        //console.log({ date, sum, user, value, defaultTitle, defaultDesc });
        if (
            (date !== this.state.date) ||
            (sum !== this.state.sum) ||
            (user !== this.state.user) ||
            (value !== this.props.num.value) ||
            (defaultTitle !== this.state.defaultTitle) ||
            (defaultDesc !== this.state.defaultDesc)
        ) {
            //console.log({ ...this.state, num: this.props.num.value });
            this.convertDefauls({ ...this.state, num: this.props.num.value });
        }

        if (
            (this.state.defaultTitle === '' ) && 
            (this.state.defaultTitle !== this.props.defaultTitle)
        ) {
            this.setState({ defaultTitle: this.props.defaultTitle });
        }
        if (
            (this.state.defaultTitle === '' ) &&
            (this.state.defaultDesc !== this.props.defaultDesc) 
        ) {
            this.setState({ defaultDesc: this.props.defaultDesc });
        }

        if (!prevProps.isQRCode && this.props.isQRCode) {
            this.onFocus();
        }

        return true;
    }

    renderMultiLine = (text) => {
        const list = text.split(' ');
        return (
            <View style={{...styles.myWallet.textsubSubContainer}}>
                {list.map((word, index) => {
                    return <Text key={index} style={styles.sent.breakWord}>{word}</Text>;
                })}
            </View>
        );
    }

    renderQR = (currency) => {
          
        const { sum, qrcode } = this.state;
        //console.log(qrcode);
        const { isQRCode } = this.props;
        if (isQRCode && qrcode) {
            return (
                <View>
                    <View style={{
                        ...styles.register.menuContainer, ...{
                            paddingVertical: 32,
                            paddingHorizontal: 32,
                            marginHorizontal: undefined
                        }
                    }}>
                        <QRCode
                            value={qrcode}
                            size={dWidth * 0.8}
                            logo={ImageResources.icon_qrlogo}
                            logoSize={dWidth * 0.15}
                            ecl={'H'}
                            logoBackgroundColor={'white'}
                        />
                    </View>
                    <View style={{ flexDirection: 'row', justifyContent: 'center' }}>
                        <Text style={{ ...styles.historyItem.sum, lineHeight: undefined, fontSize: fontSizeCurr, color: primaryColor3 }}>{`${sum} `}</Text>
                        <Text style={{ ...styles.historyItem.curr, lineHeight: undefined, fontSize: fontSizeCurr, color: primaryColor3 }}>{`${currency.code} `}</Text>
                    </View>
                </View>
            );
        } else if (this.props.telegram.fetching || this.props.crud.executing) {
            return (
                <View style={{height: 40, justifyContent: 'center'}}><ActivityIndicator animating={true}
                    color={textBorderColor}
                    size={'small'}/>
                </View>
            );
        } else {
            return null;
        }   
    }

    render() {
        const {
            user,
            date,
            expire,
            sum,
            title,
            lockButton,
            description,
            curr,
        } = this.state;
        
        const currency = currencyOf(curr, this.props.currencies);
        //console.log({ title, description });
        const { value, valid } = this.props.num;
        const { isQRCode, patterns } = this.props;
        const validPhone = validateEmptyPhone(user);
        const isConfirm = !lockButton && sum > 0 && value.length > 0 && !isQRCode && valid > 0 && validPhone;
        //console.log({ isConfirm, lockButton, sum, value, isQRCode, valid });

        const labelStyle = {...styles.sent.titleMessage, color: textPlaceholderColor};
        const amountStyle = {...styles.sent.textInput, textAlign: 'right', color: primaryColor3};
        const patternActions = patterns.data;

        return (
            <View style={styles.container}>
                <Image resizeMode={'stretch'} source={ImageResources.background} style={styles.shared.topBackgroundImage}/>
                <NavigationBar
                    title={strings['New order']}
                    leftButton={ImageResources.icon_back}
                    onLeftButtonPress={this.onBack}
                />
                <View style={[styles.sent.subContainer, { marginBottom: 60 }]}>
                    <KeyboardAwareScrollView
                        contentContainerStyle={{paddingTop: 16, paddingBottom: 60}}
                        viewIsInsideTabBar={true}
                        showsVerticalScrollIndicator={false}
                        enableAutoAutomaticScroll={true}
                        ref={this.setScrollViewRef}>
                        <View style={{paddingHorizontal: marginHorizontal}}>
                            <View style={{flex: 1 }}>
                                <View style={styles.sent.rowRequisite}>
                                    <View style={styles.sent.labelContainer}>
                                        <Text style={labelStyle}>{`${strings['From']}: `}</Text>
                                    </View>
                                    <View style={styles.sent.valueContainer}>
                                        <Text style={styles.sent.invoiceRequisite}  numberOfLines={1}>{`${this.props.wallet.address} `}</Text>
                                        <TouchableOpacity onPress={() => this.copy(this.props.wallet.address)} style={{...styles.register.buttonNonAbsolute}}>
                                            <Image style={styles.sent.image1} source={ImageResources.icon_copy_gray}/>
                                        </TouchableOpacity>
                                    </View>
                                </View>
                                <View style={styles.sent.rowRequisite}>
                                    <View style={styles.sent.labelContainer}>
                                        <Text style={labelStyle}>{`${strings['To']}: `}</Text>
                                    </View>
                                    <View style={styles.sent.valueContainer}>
                                        <Text style={styles.sent.invoiceRequisite} numberOfLines={1}>{`${this.props.sendAddress} `}</Text>
                                        <TouchableOpacity onPress={() => this.copy(this.props.sendAddress)} style={{...styles.register.buttonNonAbsolute}}>
                                            <Image style={styles.sent.image1} source={ImageResources.icon_copy_gray}/>
                                        </TouchableOpacity>
                                    </View>
                                </View>
                                <LineSeparator />
                                <View style={styles.sent.rowRequisite}>
                                    <View style={styles.sent.labelContainer}>
                                        <Text style={labelStyle}>{`${strings['Order']} #: `}</Text>
                                    </View>
                                    <View style={styles.sent.valueContainer}>
                                        <TextInput
                                            id={'order'}
                                            ref={(input) => { this.orderTextInput = input; }}
                                            placeholder={`${strings['Order']} `}
                                            style={{ textAlign: 'right', color: this.numColor(valid > 0), paddingBottom: 0, paddingTop: 0, fontSize: fontSizeInput }}
                                            multiline={false}
                                            numberOfLines={1}
                                            underlineColorAndroid={'transparent'}
                                            placeholderTextColor={'#A1ACBA'}
                                            value={value} 
                                            onChangeText={this.onNumChanged}
                                            editable = {!isQRCode}
                                            keyboardType={amountKeyboardType}
                                            blurOnSubmit={true}
                                        />
                                        {isQRCode?null:<TouchableOpacity activeOpacity={0.9} onPress={this.editNum}>
                                            <Image style={{width: 28, height: 30}} source={ImageResources.icon_user_edit}/>
                                        </TouchableOpacity>}
                                    </View>
                                </View>
                                <LineSeparator />
                                <View style={styles.sent.rowRequisite}>
                                    <View style={styles.sent.labelContainer}>
                                        <Text style={labelStyle}>{`${strings['Date']}:`}</Text>
                                    </View>
                                    <View style={styles.sent.valueContainer}>
                                        <Text style={styles.sent.invoiceRequisite}>{`${moment(date).format('YYYY/MM/DD HH:mm')}  `}</Text>
                                    </View>
                                </View>
                                <View style={styles.sent.rowRequisite}>
                                    <View style={styles.sent.labelContainer}>
                                        <Text style={labelStyle}>{`${strings['Expire']}: `}</Text>
                                    </View>
                                    <View style={styles.sent.valueContainer}>
                                        <Text style={styles.sent.invoiceRequisite}>{`${moment(expire).format('YYYY/MM/DD HH:mm')}  `}</Text>
                                    </View>
                                </View>
                                <View style={styles.sent.rowRequisite}>
                                    <View style={styles.sent.labelContainer}>
                                        <Text style={labelStyle}>{`${strings['Label title']}: `}</Text>
                                    </View>
                                    <TouchableOpacity onPress={this.showPatterns} style={{ flex: 1, flexDirection: 'row', alignItems: 'center', justifyContent: 'flex-end', marginLeft: 4, marginTop: 0 }}>
                                        {this.renderMultiLine(title)}
                                        <Image style={styles.myWallet.dropDownIcon} source={ImageResources.icon_list}/>
                                    </TouchableOpacity>
                                    <BottomMenu
                                        visible={this.state.showPatterns}
                                        actions={patternActions}
                                        onSelect={this.onSelectPattern}
                                        onClose={this.closeMenu}
                                    />    
                                </View>
                                <View style={styles.sent.rowRequisite}>
                                    <View style={styles.sent.labelContainer}>
                                        <Text style={labelStyle}>{`${strings['Label description']}: `}</Text>
                                    </View>
                                    <TouchableOpacity onPress={this.showPatterns2} style={{ flex: 1, flexDirection: 'row', alignItems: 'center', justifyContent: 'flex-end', marginLeft: 4, marginTop: 0}}>
                                        {this.renderMultiLine(description)}
                                        <Image style={styles.myWallet.dropDownIcon} source={ImageResources.icon_list}/>
                                    </TouchableOpacity>
                                    <BottomMenu
                                        visible={this.state.showPatterns2}
                                        actions={patternActions}
                                        onSelect={this.onSelectPattern2}
                                        onClose={this.closeMenu}
                                    />    
                                </View>
                                <View style={styles.sent.rowRequisite}>
                                    <View style={{ alignSelf: 'center' }}>
                                        <Text style={labelStyle}>{`${strings['Phone']}: `}</Text>
                                    </View>
                                    <View style={styles.sent.valueContainer}>
                                        <TextInput
                                            id={'phone'}
                                            ref={(input) => { this.phoneTextInput = input; }}
                                            placeholder={`${strings['Phone']} `}
                                            style={{ flex: 1, textAlign: 'right', color: this.phoneColor(validPhone), paddingBottom: 0, paddingTop: 0, fontSize: fontSizeInput }}
                                            multiline={false}
                                            numberOfLines={1}
                                            underlineColorAndroid={'transparent'}
                                            placeholderTextColor={'#A1ACBA'}
                                            value={user} 
                                            onChangeText={this.onChangedPhone}
                                            editable = {!isQRCode}
                                            keyboardType={phoneKeyboardType}
                                            blurOnSubmit={true}
                                        />
                                        {isQRCode?null:<TouchableOpacity activeOpacity={0.9} onPress={this.editPhone}>
                                            <Image style={{width: 28, height: 30}} source={ImageResources.icon_user_edit}/>
                                        </TouchableOpacity>}
                                    </View>
                                </View>
                            </View>
                            <LineSeparator />
                            
                            {this.renderQR(currency)}
                            {isQRCode?null:
                                <View style={[styles.sent.textContainer, { flexDirection: 'row', paddingTop: 16 }]}>
                                    <View style={{ justifyContent: 'center' }}>
                                        <Text style={{ ...styles.historyItem.currInput, fontSize: fontSizeCurr, color: primaryColor3 }}>{currency.code}</Text>
                                    </View>
                                    <TextInput
                                        id={'amount'}
                                        ref={(input) => { this.sumTextInput = input; }}
                                        placeholder={strings['Amount']}
                                        multiline={false}
                                        numberOfLines={1}
                                        style={[amountStyle, { flex: 1 } ]}
                                        underlineColorAndroid={'transparent'}
                                        placeholderTextColor={'#A1ACBA'}
                                        keyboardType={amountKeyboardType}
                                        value={sum === 0?'':sum.toString()}
                                        onChangeText={this.onAmountChanged}
                                        onFocus={this.onFocus}
                                        blurOnSubmit={true}
                                    />
                                </View>
                            }
                            
                        </View>
                    </KeyboardAwareScrollView>
                </View>
                {isConfirm ? <FooterBtn onPress={this.send}>
                    <Image style={styles.myWallet.buttonImage180}
                        source={ImageResources.icon_sent}/>
                    <Text style={styles.myWallet.buttonText}>{`${strings['Confirm']} `}</Text>
                </FooterBtn>:null}
                <AwesomeAlert
                    {...this.state.alert}
                    titleStyle={styles.alert.title}
                    messageStyle={styles.alert.text}
                    overlayStyle={styles.alert.overlayStyle}
                    cancelButtonColor={secondaryColor}
                    confirmButtonColor={primaryColorLight}
                />
            </View>
        );
    }

    onFocus = () => {
        setTimeout(() => this.scrollView.scrollToEnd(true), 250);
    }
}

Receive.propTypes = {
    wallet: PropTypes.object,
    requsite: PropTypes.object,
    sendAddress: PropTypes.string,
    telegram: PropTypes.object,
    telegramRequest: PropTypes.func,
    telegramReset: PropTypes.func,
    port: PropTypes.number,
    crud: PropTypes.object,
    num: PropTypes.object,
    numMax: PropTypes.object,
    checkNumUnique: PropTypes.func,
    getMaxOrder: PropTypes.func,
    showQRCode: PropTypes.func,
    isQRCode: PropTypes.bool,
    expireSec: PropTypes.number,
    getPatterns: PropTypes.func,
    patterns: PropTypes.object,
    defaultTitle: PropTypes.string,
    defaultDesc: PropTypes.string,
    getDefaultTitle: PropTypes.func,
    getDefaultDesc: PropTypes.func,
    currencies: PropTypes.array,
    headQRGost: PropTypes.string,
    getDefaultRequisiteID: PropTypes.func,
    getRequisite: PropTypes.func,
    requisiteID: PropTypes.number,
    defaultPublicKey: PropTypes.string,
};

const mapStateToProps = createStructuredSelector({
    wallet: makeSelectWallet(),
    requsite: makeSelectRequisite(),
    sendAddress: makeSelectDefaultAddress(),
    telegram: makeSelectTelegram(),
    port: makeSelectNetworkPort(),
    crud: makeSelectCRUD(),
    num: makeSelectUnique(),
    numMax: makeSelectMaxOrder(),
    isQRCode: makeSelectShowQRCode(),
    expireSec: makeSelectInvoiceExpire(),
    patterns: makeSelectPatterns(),
    defaultTitle: makeSelectDefaultTitle(),
    defaultDesc: makeSelectDefaultDesc(),
    currencies: makeSelectCurrencies(),
    headQRGost: makeSelectFormatQRGost(),
    requisiteID: makeSelectDefaultRequisiteID(),
    defaultPublicKey: makeSelectDefaultPublicKey(),
});

function mapDispatchToProps(dispatch) {
    return {
        telegramRequest: (transaction, invoice) => dispatch(telegramActions.actionTelegram(transaction, invoice)),
        telegramReset: () => dispatch(telegramActions.actionTelegramReset()),
        checkNumUnique: (num) => dispatch(dbActions.actionUnique(num)),
        getMaxOrder: () => dispatch(dbActions.actionMaxOrder()),
        showQRCode: (show) => dispatch(invoiceActions.actionQRCode(show)),
        getPatterns: () => dispatch(dbActions.actionPatternsQuery()),
        getDefaultTitle: () => dispatch(dbActions.actionGetParam('DefaultTitle')),
        getDefaultDesc: () => dispatch(dbActions.actionGetParam('DefaultDesc')),
        getDefaultRequisiteID: () => dispatch(dbActions.actionGetParam('DefaultRequisiteID')),
        getRequisite: (row) => dispatch(dbActions.actionGetRequisite(row)),
    };
}

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(Receive);
