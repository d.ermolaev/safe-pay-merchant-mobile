import React from 'react';
import { TouchableOpacity, Clipboard, Image, Text, View, Platform } from 'react-native';
import { marginHorizontal, dWidth, styles, textPlaceholderColor, primaryColor3, peachColor, fontSizeCurr, fontSize12, fontSize30 } from '../../config/styles';
import {
    ImageResources,
    NavigationBar,
    BackHandled,
    LineSeparator,
} from '../../components';
import { strings } from '../../config/localize';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import QRCode from 'react-native-qrcode-svg';
import { connect } from 'react-redux';
import moment from 'moment/min/moment-with-locales';
import Toast from 'react-native-simple-toast';
import { phonecall } from 'react-native-communications';
import Icon from 'react-native-vector-icons/dist/FontAwesome';
import { validatePhone } from '../../core/util/validate';


export class InvoiceView extends BackHandled {
    //noinspection JSUnusedGlobalSymbols
    static navigationOptions = {
        header: false,
    };

    numColor = (valid) => {
        if (valid) {
            return primaryColor3;
        } else {
            return peachColor;
        }
    }

    copy = (text) => {

        Clipboard.setString(text);
        Toast.show(strings['Copied to clipboard']);
        
    };

    onBack = () => {
        this.back();
    }

    scrollView;

    setScrollViewRef = (item) => {
        this.scrollView = item;
    };

    call = () => {
        const { user } = this.state;
        //console.log(user);
        if (validatePhone(user)) {
            //console.log(user);
            let phone = user;
            if (phone.indexOf('+') === -1) {
                if (phone.indexOf('8') !== 0) {
                    phone = `+${phone}`;
                }
            }
            phonecall(phone, true);
        }
    }

    phoneColor = () => {
        const { user } = this.state;
        if (validatePhone(user)) {
            return '#00aa00';
        } else {
            return '#aaa';
        } 
    }

    constructor(props) {
        super(props);
        const invoice = props.navigation.state.params;
        this.state = {
            ...invoice,
            lockButton: false,
        };
    }

    renderMultiLine = (text) => {
        const list = text.split(' ');
        return (
            <View style={{...styles.myWallet.textsubSubContainer}}>
                {list.map((word, index) => {
                    return <Text key={index} style={styles.sent.breakWord}>{word}</Text>;
                })}
            </View>
        );
    }

    renderQR = () => {

        const { qrcode } = this.state;
        if (qrcode && qrcode !== 'unspecified') {
            return (
                <View style={{
                    ...styles.register.menuContainer, ...{
                        paddingVertical: 32,
                        paddingHorizontal: 32,
                        marginHorizontal: undefined,
                    }
                }}>
                    <QRCode
                        value={qrcode}
                        size={dWidth * 0.8}
                        logo={ImageResources.icon_qrlogo}
                        logoSize={dWidth * 0.15}
                        logoBackgroundColor={'white'}
                    />
                </View>
            ); 
        } else {
            return null;
        }
        
    }

    render() {
        const {
            address,
            user,
            recipient,
            numOrder,
            date,
            expire,
            title,
            description,
            sum,
            code
        } = this.state;

        const labelStyle = {...styles.sent.titleMessage, color: textPlaceholderColor};

        return (
            <View style={styles.container}>
                <Image resizeMode={'stretch'} source={ImageResources.background} style={styles.shared.topBackgroundImage}/>
                <NavigationBar
                    title={strings['Invoice']}
                    leftButton={ImageResources.icon_back}
                    onLeftButtonPress={this.onBack}
                />
                <View style={[styles.sent.subContainer, { marginBottom: 60 }]}>
                    <KeyboardAwareScrollView
                        contentContainerStyle={{paddingTop: 16, paddingBottom: 60}}
                        viewIsInsideTabBar={true}
                        showsVerticalScrollIndicator={false}
                        enableAutoAutomaticScroll={true}
                        ref={this.setScrollViewRef}>
                        <View style={{paddingHorizontal: marginHorizontal}}>
                            <View>
                                <View style={styles.sent.rowRequisite}>
                                    <View style={styles.sent.labelContainer}>
                                        <Text style={labelStyle}>{`${strings['From']}: `}</Text>
                                    </View>
                                    <View style={styles.sent.valueContainer}>
                                        <Text style={styles.sent.invoiceRequisite} numberOfLines={1}>{`${address} `}</Text>
                                        <TouchableOpacity onPress={() => this.copy(address)} style={{...styles.register.buttonNonAbsolute}}>
                                            <Image style={styles.sent.image1} source={ImageResources.icon_copy_gray}/>
                                        </TouchableOpacity>
                                    </View>
                                </View>
                                <View style={styles.sent.rowRequisite}>
                                    <View style={styles.sent.labelContainer}>
                                        <Text style={labelStyle}>{`${strings['To']}: `}</Text>
                                    </View>
                                    <View style={styles.sent.valueContainer}>
                                        <Text style={styles.sent.invoiceRequisite} numberOfLines={1}>{`${recipient} `}</Text>
                                        <TouchableOpacity onPress={() => this.copy(recipient)} style={{...styles.register.buttonNonAbsolute}}>
                                            <Image style={styles.sent.image1} source={ImageResources.icon_copy_gray}/>
                                        </TouchableOpacity>
                                    </View>
                                </View>
                                <LineSeparator />
                                <View style={styles.sent.rowRequisite}>
                                    <View style={styles.sent.labelContainer}>
                                        <Text style={labelStyle}>{`${strings['Order']} #: `}</Text>
                                    </View>
                                    <View style={styles.sent.valueContainer}>
                                        <Text style={styles.sent.invoiceRequisite} numberOfLines={1}>{`${numOrder}  `}</Text>
                                    </View>
                                </View>
                                <LineSeparator />
                                <View style={styles.sent.rowRequisite}>
                                    <View style={styles.sent.labelContainer}>
                                        <Text style={labelStyle}>{`${strings['Date']}: `}</Text>
                                    </View>
                                    <View style={styles.sent.valueContainer}>
                                        <Text style={styles.sent.invoiceRequisite}>{`${moment(date).format('YYYY/MM/DD HH:mm')}  `}</Text>
                                    </View>
                                </View>
                                <View style={styles.sent.rowRequisite}>
                                    <View style={styles.sent.labelContainer}>
                                        <Text style={labelStyle}>{`${strings['Expire']}: `}</Text>
                                    </View>
                                    <View style={styles.sent.valueContainer}>
                                        <Text style={styles.sent.invoiceRequisite}>{`${moment(expire).format('YYYY/MM/DD HH:mm')}  `}</Text>
                                    </View>
                                </View>
                                <View style={styles.sent.rowRequisite}>
                                    <View style={styles.sent.labelContainer}>
                                        <Text style={labelStyle}>{`${strings['Label title']}: `}</Text>
                                    </View>
                                    {this.renderMultiLine(title)}
                                </View>
                                <View style={styles.sent.rowRequisite}>
                                    <View style={styles.sent.labelContainer}>
                                        <Text style={labelStyle}>{`${strings['Label description']}: `}</Text>
                                    </View>
                                    {this.renderMultiLine(description)}
                                </View>
                                { user ?
                                    <View style={styles.sent.rowRequisite}>
                                        <View style={styles.sent.labelContainer}>
                                            <Text style={labelStyle}>{`${strings['Phone']}: `}</Text>
                                        </View>
                                        <View style={styles.sent.valueContainer}>
                                            <Text style={styles.sent.invoiceRequisite}>{`${user}  `}</Text>
                                            <TouchableOpacity style={{ justifyContent: 'center', alignItems: 'center' }} activeOpacity={0.9} onPress={this.call}>
                                                <Icon 
                                                    style={{ marginLeft: fontSize12, color: this.phoneColor(), ...Platform.select({ ios: { paddingBottom: 4 }, android: {} }) }} 
                                                    name={'phone'} 
                                                    size={fontSize30} 
                                                />
                                            </TouchableOpacity>
                                        </View>
                                    </View> : null
                                }
                            </View>
                            <LineSeparator />
                            
                            {this.renderQR()}
                            
                            <View style={{ flex: 1, flexDirection: 'row', justifyContent: 'center' }}>
                                <Text style={{ ...styles.historyItem.sum, lineHeight: undefined, fontSize: fontSizeCurr, color: primaryColor3 }}>{`${sum.toFixed(2)} `}</Text>
                                <Text style={{ ...styles.historyItem.curr, lineHeight: undefined, fontSize: fontSizeCurr, color: primaryColor3 }}>{`${code} `}</Text>
                            </View>
                            
                        </View>
                    </KeyboardAwareScrollView>
                </View>
            </View>
        );
    }

    onFocus() {
        setTimeout(() => this.scrollView.scrollToEnd(true), 250);
    }
}

export default connect(
    null,
    null
)(InvoiceView);
