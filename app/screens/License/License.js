import React from 'react';
import { Image, Text, TouchableOpacity, View } from 'react-native';
import { WebView } from 'react-native-webview';
import { styles } from '../../config/styles';
import { 
    BackHandled,
    ImageResources,
    NavigationBar
} from '../../components';
import { strings } from '../../config/localize';
import * as navigationActions from '../../actions/navigationActions';
import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect';
import { makeSelectLang } from '../../selectors/configSelectors';

class License extends BackHandled {

    //noinspection JSUnusedGlobalSymbols
    static navigationOptions = {
        header: false,
        headerLeft: null,
    };

    accept = () => {
        this.setState({accepted: !this.state.accepted});
    };
    
    navigateToFirstLogin = () => {
        navigationActions.navigate('FirstLogin');
    };

    getLicense = () => {
        if (this.props.lang === 'ru') {
            this.setState({license: require('../../resources/license/ru.json')});
        } else {
            this.setState({license: require('../../resources/license/en.json')});
        }
    };

    componentWillMount() {
        this.setState({accepted: false});
        this.getLicense();
    }

    render() {
        const source = {html: this.state.license ?  this.state.license.html : undefined};

        const containerStyle = {
            ...styles.login.containerBottomLicence,
            justifyContent: 'space-around',
        };
        const square = this.state.accepted ? (
            <View style={styles.register.square}/>) : null;
        const nextButton = this.state.accepted ? (
            <View style={styles.login.bottomContainer}>
                <TouchableOpacity onPress={this.navigateToFirstLogin} style={styles.register.buttonContinue}
                    activeOpacity={0.9}>
                    <Text style={styles.register.buttonText}>{`${strings['Next']} `}</Text>
                </TouchableOpacity>
            </View>) :
            <View style={styles.login.bottomContainer}>
                <View style={{...styles.register.buttonContinue, ...{backgroundColor: '#D1D5DB'}}}>
                    <Text style={styles.register.buttonNextDisabledText}>{`${strings['Next']} `}</Text>
                </View>
            </View>;

        return (
            <View style={styles.container}>
                <Image source={ImageResources.background} style={styles.myContacts.topBackgroundImage}
                    resizeMode='cover'/>
                <NavigationBar
                    title={strings['License']}
                    leftButton={ImageResources.icon_back}
                    onLeftButtonPress={this.back}
                />
                <View style={containerStyle}>
                    <Text style={styles.register.liceseText}>{strings['License Text']}</Text>
                    <View style={styles.login.web}>
                        <WebView source={source} />
                    </View>
                    <TouchableOpacity onPress={this.accept} style={styles.register.aceptContainer}>
                        <View style={styles.register.aceptSquareBorder}>{square}</View>
                        <Text style={styles.register.aceptText}>{`${strings['I Accept']} `}</Text>
                    </TouchableOpacity>

                </View>
                {nextButton}
            </View>
        );
    }
}

const mapStateToProps = createStructuredSelector({
    lang: makeSelectLang(),
});

export default connect(
    mapStateToProps,
    null
)(License);

