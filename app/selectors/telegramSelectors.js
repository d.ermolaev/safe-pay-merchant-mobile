import { createSelector } from 'reselect';
import { initialState } from '../reducers/dbReducer';

/**
 * Direct selector to the userProfile state domain
 */

const selectTelegram = state =>
    state.getIn(['telegram', 'telegram'], initialState.get('telegram'));

const makeSelectTelegram = () =>
    createSelector(selectTelegram, substate => {
        return substate.toJS();
    });

export { 
    selectTelegram,
    makeSelectTelegram
};