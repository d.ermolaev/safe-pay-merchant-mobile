import { createSelector } from 'reselect';
import { initialState } from '../reducers/invoiceReducer';

/**
 * Direct selector to the userProfile state domain
 */

const selectInvoice = state =>
    state.getIn(['invoice', 'invoice'], initialState.get('invoice'));

const makeSelectInvoice = () =>
    createSelector(selectInvoice, substate => {
        return substate.toJS();
    });

const selectShowQRCode = state =>
    state.getIn(['invoice', 'gui', 'showQRCode'], initialState.getIn(['gui', 'showQRCode']));

const makeSelectShowQRCode = () =>
    createSelector(selectShowQRCode, substate => substate);

const selectRestore = state =>
    state.getIn(['invoice', 'restore'], initialState.get('restore'));

const makeSelectRestore = () =>
    createSelector(selectRestore, substate => {
        return substate.toJS();
    });

export { 
    selectInvoice, 
    makeSelectInvoice,
    selectShowQRCode,
    makeSelectShowQRCode,
    selectRestore,
    makeSelectRestore
};
