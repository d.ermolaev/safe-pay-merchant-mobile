import { createSelector } from 'reselect';
import { initialState } from '../reducers/blockchainReducer';

/**
 * Direct selector to the userProfile state domain
 */

const selectHightBlock = state =>
    state.getIn(['blockchain', 'common'], initialState.get('common'));

const makeSelectHightBlock = () =>
    createSelector(selectHightBlock, substate => substate.toJS());

const selectLastBlock = state =>
    state.getIn(['blockchain', 'common', 'lastBlock'], initialState.getIn(['common', 'lastBlock']));

const makeSelectLastBlock = () =>
    createSelector(selectLastBlock, substate => substate);

const selectTypeTransaction = state =>
    state.getIn(['blockchain', 'common', 'type'], initialState.getIn(['common', 'type']));

const makeSelectTypeTransaction = () =>
    createSelector(selectTypeTransaction, substate => substate);

const selectUnconfirmed = state =>
    state.getIn(['blockchain', 'unconfirmed'], initialState.get('unconfirmed'));

const makeSelectUnconfirmed = () =>
    createSelector(selectUnconfirmed, substate => substate.toJS());

const selectIncoming = state =>
    state.getIn(['blockchain', 'incoming'], initialState.get('incoming'));

const makeSelectIncoming = () =>
    createSelector(selectIncoming, substate => substate.toJS());

const selectPoll = state =>
    state.getIn(['blockchain', 'poll'], initialState.get('poll'));

const makeSelectPoll = () =>
    createSelector(selectPoll, substate => substate.toJS());

const selectSchedule = state =>
    state.getIn(['blockchain', 'schedulePoll'], initialState.get('schedulePoll'));

const makeSelectSchedule = () =>
    createSelector(selectSchedule, substate => substate.toJS());

export { 
    selectHightBlock,
    makeSelectHightBlock,
    selectLastBlock,
    makeSelectLastBlock,
    selectTypeTransaction,
    makeSelectTypeTransaction,
    selectUnconfirmed,
    makeSelectUnconfirmed,
    selectIncoming,
    makeSelectIncoming,
    selectPoll,
    makeSelectPoll,
    selectSchedule,
    makeSelectSchedule
};