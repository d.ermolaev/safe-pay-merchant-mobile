import * as selectors from '../dbSelectors';
import { fromJS } from 'immutable';
import { initialState } from '../../reducers/dbReducer';

const state = fromJS({
    db: initialState.toJS(),
});

describe('dbSelectors', () => {

    describe('selectDB', () => {
        it('should return db as object', () => {
            const substate = selectors.selectDB(state);
            expect(substate.toJS()).toEqual(initialState.get('db').toJS());
        });
    });

    describe('selectBackup', () => {
        it('should return backup as object', () => {
            const substate = selectors.selectBackup(state);
            expect(substate.toJS()).toEqual(initialState.get('backup').toJS());
        });
    });

    describe('selectRestore', () => {
        it('should return restore as object', () => {
            const substate = selectors.selectRestore(state);
            expect(substate.toJS()).toEqual(initialState.get('restore').toJS());
        });
    });

    describe('selectCRUD', () => {
        it('should return CRUD as object', () => {
            const substate = selectors.selectCRUD(state);
            expect(substate.toJS()).toEqual(initialState.get('crud').toJS());
        });
    });

    describe('selectUnique', () => {
        it('should return unique as object', () => {
            const substate = selectors.selectUnique(state);
            expect(substate.toJS()).toEqual(initialState.get('unique').toJS());
        });
    });

    describe('selectUnique', () => {
        it('should return unique as object', () => {
            const substate = selectors.selectUnique(state);
            expect(substate.toJS()).toEqual(initialState.get('unique').toJS());
        });
    });

    describe('selectMaxOrder', () => {
        it('should return maxOrder as object', () => {
            const substate = selectors.selectMaxOrder(state);
            expect(substate.toJS()).toEqual(initialState.get('maxOrder').toJS());
        });
    });

    describe('selectHistory', () => {
        it('should return history as object', () => {
            const substate = selectors.selectHistory(state);
            expect(substate.toJS()).toEqual(initialState.get('history').toJS());
        });
    });    

    describe('selectAuth', () => {
        it('should return auth as object', () => {
            const substate = selectors.selectAuth(state);
            expect(substate.toJS()).toEqual(initialState.get('auth').toJS());
        });
    });

    describe('selectPatterns', () => {
        it('should return patterns as object', () => {
            const substate = selectors.selectPatterns(state);
            expect(substate.toJS()).toEqual(initialState.get('patterns').toJS());
        });
    });

    describe('selectDefaultPattern', () => {
        it('should return defaultPatterns as object', () => {
            const substate = selectors.selectDefaultPattern(state);
            expect(substate).toEqual(null);
        });
    });

    describe('selectDefaultTitleID', () => {
        it('should return DefaultTitleID as number', () => {
            const substate = selectors.selectDefaultTitleID(state);
            expect(substate).toEqual(0);
        });
    });

    describe('selectDefaultDescID', () => {
        it('should return DefaultDescID as number', () => {
            const substate = selectors.selectDefaultDescID(state);
            expect(substate).toEqual(0);
        });
    });

    describe('selectDefaultTitle', () => {
        it('should return DefaultTitle as string', () => {
            const substate = selectors.selectDefaultTitle(state);
            expect(substate).toEqual('');
        });
    });

    describe('selectDefaultDesc', () => {
        it('should return DefaultDesc as string', () => {
            const substate = selectors.selectDefaultDesc(state);
            expect(substate).toEqual('');
        });
    });

    describe('selectDefaultServerID', () => {
        it('should return DefaultServerID as number', () => {
            const substate = selectors.selectDefaultServerID(state);
            expect(substate).toEqual(0);
        });
    });

    describe('selectPollTimeoutDB', () => {
        it('should return PollTimeoutDB as number', () => {
            const substate = selectors.selectPollTimeoutDB(state);
            expect(substate).toEqual(0);
        });
    });

    describe('selectInvoiceExpireDB', () => {
        it('should return InvoiceExpireDB as number', () => {
            const substate = selectors.selectInvoiceExpireDB(state);
            expect(substate).toEqual(0);
        });
    });

    describe('selectBackupNameDB', () => {
        it('should return BackupNameDB as string', () => {
            const substate = selectors.selectBackupNameDB(state);
            expect(substate).toEqual('');
        });
    });

    describe('selectDefaultRequisiteID', () => {
        it('should return DefaultRequisiteID as number', () => {
            const substate = selectors.selectDefaultRequisiteID(state);
            expect(substate).toEqual(0);
        });
    });

    describe('selectRequisite', () => {
        it('should return requisite as object', () => {
            const substate = selectors.selectRequisite(state);
            expect(substate.toJS()).toEqual(initialState.get('requisite').toJS());
        });
    }); 

});