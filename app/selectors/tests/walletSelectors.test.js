import * as selectors from '../walletSelectors';
import { fromJS } from 'immutable';
import { fixtures } from '../../core/crypt/tests/fixtures';
import { initialState } from '../../reducers/walletReducer';

const substate = initialState
    .setIn(['wallet', 'pin'], fixtures.pin)
    .setIn(['wallet', 'seed'], fixtures.seed)
    .setIn(['wallet', 'keyPair'], fromJS(fixtures.keyPair))
    .setIn(['wallet', 'address'], fixtures.address);

const state = fromJS({
    wallet: substate.toJS(),
});

describe('walletSelectors', () => {

    describe('selectPin', () => {
        it('should return PIN as string', () => {
            const substate = selectors.selectPin(state);
            expect(substate).toMatch(fixtures.pin);
        });
    });

    describe('selectSeed', () => {
        it('should return seed as string', () => {
            const substate = selectors.selectSeed(state);
            expect(substate).toMatch(fixtures.seed);
        });
    });

    describe('selectKeyPair', () => {
        it('should return keyPair as object', () => {
            const substate = selectors.selectKeyPair(state);
            expect(substate).toEqual(fixtures.keyPair);
        });
    });

    describe('selectAddress', () => {
        it('should return address as string', () => {
            const substate = selectors.selectAddress(state);
            expect(substate).toMatch(fixtures.address);
        });
    });

});