import * as selectors from '../chainSelectors';
import { fromJS } from 'immutable';
import { initialState } from '../../reducers/blockchainReducer';


const state = fromJS({
    blockchain: initialState.toJS(),
});

describe('chainSelectors', () => {

    describe('selectHightBlock', () => {
        it('should return HightBlock as object', () => {
            const substate = selectors.selectHightBlock(state);
            expect(substate.toJS()).toEqual(initialState.get('common').toJS());
        });
    });

    describe('selectLastBlock', () => {
        it('should return LastBlock as number', () => {
            const substate = selectors.selectLastBlock(state);
            expect(substate).toEqual(initialState.getIn(['common', 'lastBlock']));
        });
    });

    describe('selectTypeTransaction', () => {
        it('should return TypeTransaction as number', () => {
            const substate = selectors.selectTypeTransaction(state);
            expect(substate).toEqual(initialState.getIn(['common', 'type']));
        });
    });

    describe('selectUnconfirmed', () => {
        it('should return Unconfirmed as object', () => {
            const substate = selectors.selectUnconfirmed(state);
            expect(substate.toJS()).toEqual(initialState.get('unconfirmed').toJS());
        });
    });

    describe('selectIncoming', () => {
        it('should return Incoming as object', () => {
            const substate = selectors.selectIncoming(state);
            expect(substate.toJS()).toEqual(initialState.get('incoming').toJS());
        });
    });

    describe('selectPoll', () => {
        it('should return Poll as object', () => {
            const substate = selectors.selectPoll(state);
            expect(substate.toJS()).toEqual(initialState.get('poll').toJS());
        });
    });

    describe('selectSchedule', () => {
        it('should return Schedule as object', () => {
            const substate = selectors.selectSchedule(state);
            expect(substate.toJS()).toEqual(initialState.get('schedulePoll').toJS());
        });
    });

});