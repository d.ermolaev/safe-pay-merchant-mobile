import * as selectors from '../serverSelectors';
import { fromJS } from 'immutable';
import { initialState } from '../../reducers/serversReducer';

const state = fromJS({
    servers: initialState.toJS(),
});

describe('serverSelectors', () => {

    describe('selectServers', () => {
        it('should return Servers as object', () => {
            const substate = selectors.selectServers(state);
            expect(substate).toEqual({
                batchloading: false, 
                error: null, 
                list: [
                    {
                        checked: true, 
                        connectionString: 'http://89.235.184.229:9067/api', 
                        id: 0, 
                        name: 'Default Erachain'
                    }
                ], 
                loading: false
            });
        });
    });

    describe('selectPing', () => {
        it('should return Ping as object', () => {
            const substate = selectors.selectPing(state);
            expect(substate.toJS()).toEqual(initialState.get('ping').toJS());
        });
    });

});