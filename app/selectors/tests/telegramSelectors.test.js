import * as selectors from '../telegramSelectors';
import { fromJS } from 'immutable';
import { initialState } from '../../reducers/telegramReducer';

const state = fromJS({
    telegram: initialState.toJS(),
});

describe('telegramSelectors', () => {

    describe('selectTelegram', () => {
        it('should return Telegram as object', () => {
            const substate = selectors.selectTelegram(state);
            expect(substate.toJS()).toEqual(initialState.get('telegram').toJS());
        });
    });

});