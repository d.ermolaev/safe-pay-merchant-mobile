import * as selectors from '../invoiceSelectors';
import { fromJS } from 'immutable';
import { initialState } from '../../reducers/invoiceReducer';


const state = fromJS({
    invoice: initialState.toJS(),
});

describe('invoiceSelectors', () => {

    describe('selectInvoice', () => {
        it('should return invoice as object', () => {
            const substate = selectors.selectInvoice(state);
            expect(substate.toJS()).toEqual(initialState.get('invoice').toJS());
        });
    });

    describe('selectShowQRCode', () => {
        it('should return ShowQRCode as boolean', () => {
            const substate = selectors.selectShowQRCode(state);
            expect(substate).toEqual(initialState.getIn(['gui', 'showQRCode']));
        });
    });

    describe('selectRestore', () => {
        it('should return invoiceRestore as object', () => {
            const substate = selectors.selectRestore(state);
            expect(substate.toJS()).toEqual(initialState.get('restore').toJS());
        });
    });

});