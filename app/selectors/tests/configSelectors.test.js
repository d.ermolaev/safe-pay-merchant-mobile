import * as selectors from '../configSelectors';
import { fromJS } from 'immutable';
import { initialState } from '../../reducers/configReducer';
import url from 'url';

const state = fromJS({
    config: initialState.toJS(),
});

describe('configSelectors', () => {

    describe('selectBaseUrl', () => {
        it('should return BaseUrl as string', () => {
            const substate = selectors.selectBaseUrl(state);
            expect(substate).toEqual(initialState.getIn(['settings', 'baseUrl']));
        });
    });

    describe('selectUrlEraChain', () => {
        it('should return UrlEraChain as string', () => {
            const substate = selectors.selectUrlEraChain(state);
            expect(substate).toEqual(initialState.getIn(['settings', 'baseUrl']));
        });
    });

    describe('selectHost', () => {
        it('should return Host as string', () => {
            const substate = selectors.selectHost(state);
            let urlObject = url.parse(substate);
            expect(substate).toEqual(`${urlObject.protocol}//${urlObject.host}`);
        });
    });

    describe('selectLang', () => {
        it('should return lang as string', () => {
            const substate = selectors.selectLang(state);
            expect(substate).toEqual(initialState.getIn(['common', 'lang']));
        });
    });

    describe('selectSettings', () => {
        it('should return Settings as object', () => {
            const substate = selectors.selectSettings(state);
            expect(substate.toJS()).toEqual(initialState.get('settings').toJS());
        });
    });

    describe('selectDefaultAddress', () => {
        it('should return lang as string', () => {
            const substate = selectors.selectDefaultAddress(state);
            expect(substate).toEqual(initialState.getIn(['settings', 'defaultSendAddress']));
        });
    });

    describe('selectNetworkPort', () => {
        it('should return NetworkPort as number', () => {
            const substate = selectors.selectNetworkPort(state);
            expect(substate).toEqual(initialState.getIn(['settings', 'networkPort']));
        });
    });

    describe('selectConfirmMode', () => {
        it('should return ConfirmMode as number', () => {
            const substate = selectors.selectConfirmMode(state);
            expect(substate).toEqual(initialState.getIn(['settings', 'confirmMode']));
        });
    });

    describe('selectBlocksPerRequest', () => {
        it('should return BlocksPerRequest as number', () => {
            const substate = selectors.selectBlocksPerRequest(state);
            expect(substate).toEqual(initialState.getIn(['settings', 'blocksPerRequest']));
        });
    });

    describe('selectBlocksTimeout', () => {
        it('should return BlocksTimeout as number', () => {
            const substate = selectors.selectBlocksTimeout(state);
            expect(substate).toEqual(initialState.getIn(['settings', 'blocksTimeout']));
        });
    });

    describe('selectInvoiceExpire', () => {
        it('should return InvoiceExpire as number', () => {
            const substate = selectors.selectInvoiceExpire(state);
            expect(substate).toEqual(initialState.getIn(['settings', 'invoiceExpire']));
        });
    });

    describe('selectPollTimeout', () => {
        it('should return PollTimeout as number', () => {
            const substate = selectors.selectPollTimeout(state);
            expect(substate).toEqual(initialState.getIn(['settings', 'pollTimeout']));
        });
    });

    describe('selectCurrencies', () => {
        it('should return Currencies as object', () => {
            const substate = selectors.selectCurrencies(state);
            expect(substate.toJS()).toEqual(initialState.get('currencyes').toJS());
        });
    });

    describe('selectCurrency', () => {
        it('should return Currency as object', () => {
            const substate = selectors.selectCurrency(state, { invoice: { curr: 643 } });
            expect(substate).toEqual({
                name: '',
                code: '',
                curr: 643
            });
        });
    });

    describe('selectGenesisTimestamp', () => {
        it('should return GenesisTimestamp as number', () => {
            const substate = selectors.selectGenesisTimestamp(state);
            expect(substate).toEqual(initialState.getIn(['settings', 'genesisTimestamp']));
        });
    });

    describe('selectBlockTimestamp', () => {
        it('should return BlockTimestamp as number', () => {
            const substate = selectors.selectBlockTimestamp(state);
            expect(substate).toEqual(initialState.getIn(['settings', 'blockTimestamp']));
        });
    });

    describe('selectBackupName', () => {
        it('should return BackupName as string', () => {
            const substate = selectors.selectBackupName(state);
            expect(substate).toEqual(initialState.getIn(['settings', 'backupFileName']));
        });
    });

    describe('selectFormatQRGost', () => {
        it('should return selectFormatQRGost as string', () => {
            const substate = selectors.selectFormatQRGost(state);
            expect(substate).toEqual(initialState.getIn(['settings', 'formatQRGost']));
        });
    });

});