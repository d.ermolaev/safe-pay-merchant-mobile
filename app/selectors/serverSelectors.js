import { createSelector } from 'reselect';
import { initialState } from '../reducers/serversReducer';
import { selectBaseUrl } from './configSelectors';
import { selectDefaultServerID } from './dbSelectors';

/**
 * Direct selector to the userProfile state domain
 */

const selectServers = state => {
    let substate = state.getIn(['servers', 'servers'], initialState.get('servers')).toJS();
    const baseUrl = selectBaseUrl(state);
    const id = selectDefaultServerID(state);    //console.log({ id, substate });
    let servers = [{ id: 0, name: 'Default Erachain', connectionString: baseUrl, checked: id === 0 }];
    const serverList = substate.list.map((item) => {
        return { ...item, checked: item.id === id };
    });
    //console.log({ serverList });
    return { ...substate, list: servers.concat(serverList) };
};    

const makeSelectServers = () =>
    createSelector(selectServers, substate => substate);

const selectPing = state =>
    state.getIn(['servers', 'ping'], initialState.get('ping'));

const makeSelectPing = () =>
    createSelector(selectPing, substate => {
        return substate.toJS();
    });

export { 
    selectServers, 
    makeSelectServers,
    selectPing,
    makeSelectPing
};
