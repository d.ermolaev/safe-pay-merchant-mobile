import { createSelector } from 'reselect';
import { initialState } from '../reducers/dbReducer';
import { fromJS } from 'immutable';

/**
 * Direct selector to the userProfile state domain
 */

const selectDB = state =>
    state.getIn(['db', 'db'], initialState.get('db'));

const makeSelectDB = () =>
    createSelector(selectDB, substate => {
        return substate.toJS();
    });

const selectBackup = state =>
    state.getIn(['db', 'backup'], initialState.get('backup'));

const makeSelectBackup = () =>
    createSelector(selectBackup, substate => {
        return substate.toJS();
    });

const selectRestore = state =>
    state.getIn(['db', 'restore'], initialState.get('restore'));

const makeSelectRestore = () =>
    createSelector(selectRestore, substate => {
        return substate.toJS();
    });

const selectCRUD = state =>
    state.getIn(['db', 'crud'], initialState.get('crud'));

const makeSelectCRUD = () =>
    createSelector(selectCRUD, substate => {
        return substate.toJS();
    });

const selectUnique = state =>
    state.getIn(['db', 'unique'], initialState.get('unique'));

const makeSelectUnique = () =>
    createSelector(selectUnique, substate => {
        return substate.toJS();
    });

const selectMaxOrder = state =>
    state.getIn(['db', 'maxOrder'], initialState.get('maxOrder'));

const makeSelectMaxOrder = () =>
    createSelector(selectMaxOrder, substate => {
        return substate.toJS();
    });

const selectHistory = state =>
    state.getIn(['db', 'history'], initialState.get('history'));

const makeSelectHistory = () =>
    createSelector(selectHistory, substate => {
        return substate.toJS();
    });

const selectAuth = state =>
    state.getIn(['db', 'auth'], initialState.get('auth'));

const makeSelectAuth = () =>
    createSelector(selectAuth, substate => {
        return substate.toJS();
    });

const selectPatterns = state =>
    state.getIn(['db', 'patterns'], initialState.get('patterns'));

const makeSelectPatterns = () =>
    createSelector(selectPatterns, substate => {
        return substate.toJS();
    });

const selectDefaultPattern = state => {
    const substate = state.getIn(['db', 'patterns', 'data'], initialState.getIn(['patterns', 'data']));
    try {
        const patterns = substate.toJS();
        for (let i = 0; i < patterns.length; i++) {
            if (patterns[i].inwork === 1) {
                return fromJS(patterns[i]);
            }
        }
        return null;
    } catch (e) {
        console.log(e);
        return null;
    }
};

const makeSelectDefaultPattern = () =>
    createSelector(selectDefaultPattern, substate => {
        if (substate) {
            return substate.toJS();
        }
        return null;
    });

const selectDefaultTitleID = state => {
    const substate = state.getIn(['db', 'params', 'values', 'DefaultTitle'], null);
    try {
        if (!substate) throw new Error('DefaultTitle = 0');
        return Number(substate);
    } catch (e) {
        return 0;
    }
};

const makeSelectDefaultTitleID = () =>
    createSelector(selectDefaultTitleID, substate => {
        return substate;
    });

const selectDefaultTitle = state => {
    const id = selectDefaultTitleID(state);
    const { data } = selectPatterns(state).toJS();
    const result = data.filter(item => item.id === id);
    if (result.length > 0) {
        return result[0].pattern;
    } else {
        return '';
    }
};

const makeSelectDefaultTitle = () =>
    createSelector(selectDefaultTitle, substate => {
        return substate;
    });

const selectDefaultDescID = state => {
    const substate = state.getIn(['db', 'params', 'values', 'DefaultDesc'], null);
    try {
        if (!substate) throw new Error('DefaultDesc = 0');
        return Number(substate);
    } catch (e) {
        return 0;
    }
};

const makeSelectDefaultDescID = () =>
    createSelector(selectDefaultDescID, substate => {
        return substate;
    });

const selectDefaultDesc = state => {
    const id = selectDefaultDescID(state);
    const { data } = selectPatterns(state).toJS();
    const result = data.filter(item => item.id === id);
    if (result.length > 0) {
        return result[0].pattern;
    } else {
        return '';
    }
};

const makeSelectDefaultDesc = () =>
    createSelector(selectDefaultDesc, substate => {
        return substate;
    });

const selectDefaultServerID = state => {
    const substate = state.getIn(['db', 'params', 'values', 'DefaultServer'], null);
    try {
        if (!substate) throw new Error('DefaultServer = 0');
        return Number(substate);
    } catch (e) {
        return 0;
    }
};

const makeSelectDefaultServerID = () =>
    createSelector(selectDefaultServerID, substate => {
        return substate;
    });

const selectPollTimeoutDB = state => {
    const substate = state.getIn(['db', 'params', 'values', 'PollTimeout'], null);
    try {
        if (!substate) throw new Error('Param PollTimeout not found');
        return Number(substate);
    } catch (e) {
        return 0;
    }
};

const makePollTimeoutDB = () =>
    createSelector(selectPollTimeoutDB, substate => {
        return substate;
    });

const selectInvoiceExpireDB = state => {
    const substate = state.getIn(['db', 'params', 'values', 'InvoiceExpire'], null);
    try {
        if (!substate) throw new Error('Param InvoiceExpire not found');
        return Number(substate);
    } catch (e) {
        return 0;
    }
};

const makeInvoiceExpireDB = () =>
    createSelector(selectInvoiceExpireDB, substate => {
        return substate;
    });

const selectBackupNameDB = state => {
    const substate = state.getIn(['db', 'params', 'values', 'BackupName'], null);
    try {
        if (!substate) throw new Error('Param BackupName not found');
        return substate;
    } catch (e) {
        return '';
    }
};

const makeSelectBackupNameDB = () =>
    createSelector(selectBackupNameDB, substate => {
        return substate;
    });

const selectRequisite = state =>
    state.getIn(['db', 'requisite'], initialState.get('requisite'));

const makeSelectRequisite = () =>
    createSelector(selectRequisite, substate => {
        return substate.toJS();
    });

const selectDefaultRequisiteID = state => {
    const substate = state.getIn(['db', 'params', 'values', 'DefaultRequisiteID'], null);
    try {
        if (!substate) throw new Error('Param DefaultRequisiteID not found');
        return Number(substate);
    } catch (e) {
        return 0;
    }
};

const makeSelectDefaultRequisiteID = () =>
    createSelector(selectDefaultRequisiteID, substate => {
        return substate;
    });

export { 
    selectDB, 
    makeSelectDB,
    selectCRUD,
    makeSelectCRUD,
    selectUnique,
    makeSelectUnique,
    selectMaxOrder,
    makeSelectMaxOrder,
    selectHistory,
    makeSelectHistory,
    selectAuth,
    makeSelectAuth,
    selectPatterns,
    makeSelectPatterns,
    selectDefaultPattern,
    makeSelectDefaultPattern,
    selectDefaultTitleID,
    makeSelectDefaultTitleID,
    selectDefaultDescID,
    makeSelectDefaultDescID,
    selectDefaultTitle,
    makeSelectDefaultTitle,
    selectDefaultDesc,
    makeSelectDefaultDesc,
    selectDefaultServerID,
    makeSelectDefaultServerID,
    selectPollTimeoutDB,
    makePollTimeoutDB,
    selectInvoiceExpireDB,
    makeInvoiceExpireDB,
    selectBackup,
    makeSelectBackup,
    selectRestore,
    makeSelectRestore,
    selectBackupNameDB,
    makeSelectBackupNameDB,
    selectRequisite,
    makeSelectRequisite,
    selectDefaultRequisiteID,
    makeSelectDefaultRequisiteID,
};
