import { createSelector } from 'reselect';
import { initialState } from '../reducers/configReducer';
import { selectServers } from './serverSelectors';
import {
    selectInvoiceExpireDB,
    selectPollTimeoutDB,
    selectBackupNameDB,
} from './dbSelectors';
import url from 'url';

/**
 * Direct selector to the userProfile state domain
 */

const currencyOf = (curr, currencyes) => {
    const found = currencyes.filter(item => item.curr === curr);
    if (found.length > 0) {
        return found[0];
    } else {
        return {
            name: '',
            code: '',
            curr
        };
    }
};

const selectAll = state => state;

const makeSelectAll = () =>
    createSelector(selectAll, substate => substate.toJS());

const selectBaseUrl = state => 
    state.getIn(['config', 'settings', 'baseUrl'], initialState.getIn(['settings', 'baseUrl']));

const selectUrlEraChain = state => {
    const baseUrl = selectBaseUrl(state);
    const servers = selectServers(state);
    //console.log(servers);
    for (let i = 0; i < servers.list.length; i++)
    {
        if (servers.list[i].checked) {
            const server = servers.list[i];
            //console.log(server.connectionString);
            return server.connectionString;
        }
    }
    //console.log(baseUrl);
    return baseUrl;
};

const selectHost = state => {
    const baseUrl = selectUrlEraChain(state);
    let urlObject = url.parse(baseUrl);
    return `${urlObject.protocol}//${urlObject.host}`;
};
    
const makeSelectUrlEraChain = () =>
    createSelector(selectUrlEraChain, substate => substate);

const selectLang = state =>
    state.getIn(['config', 'common', 'lang'], initialState.getIn(['common', 'lang']));

const makeSelectLang = () =>
    createSelector(selectLang, substate => substate);

const selectSettings = state =>
    state.getIn(['config', 'settings'], initialState.get('settings'));

const makeSelectSettings = () =>
    createSelector(selectSettings, substate => substate.toJS());

const selectDefaultAddress = state => {
    const substate = state.getIn(['config', 'settings', 'defaultSendAddress'], initialState.getIn(['settings', 'defaultSendAddress']));
    return substate;
};
    
const makeSelectDefaultAddress = () => 
    createSelector(selectDefaultAddress, substate => { 
        return substate;
    });

const selectNetworkPort = state => {
    const port = state.getIn(['config', 'settings', 'networkPort'], initialState.getIn(['settings', 'networkPort']));
    const servers = selectServers(state);
    for (let i = 0; i < servers.list.length; i++)
    {
        if (servers.list[i].checked) {
            const server = servers.list[i];
            //console.log(server.connectionString);
            return server.port ? server.port : port;
        }
    }
    return port;
};  
    
const makeSelectNetworkPort = () => 
    createSelector(selectNetworkPort, substate => substate);

const selectConfirmMode = state => 
    state.getIn(['config', 'settings', 'confirmMode'], initialState.getIn(['settings', 'confirmMode']));
    
const makeSelectConfirmMode = () => 
    createSelector(selectConfirmMode, substate => substate);

const selectBlocksPerRequest = state => 
    state.getIn(['config', 'settings', 'blocksPerRequest'], initialState.getIn(['settings', 'blocksPerRequest']));
    
const makeSelectBlocksPerRequest = () => 
    createSelector(selectBlocksPerRequest, substate => substate);

const selectBlocksTimeout = state => 
    state.getIn(['config', 'settings', 'blocksTimeout'], initialState.getIn(['settings', 'blocksTimeout']));
    
const makeSelectBlocksTimeout = () => 
    createSelector(selectBlocksTimeout, substate => substate);

const selectInvoiceExpire = state => {
    const invoiceExpire1 = state.getIn(['config', 'settings', 'invoiceExpire'], initialState.getIn(['settings', 'invoiceExpire']));
    const invoiceExpire2 = selectInvoiceExpireDB(state);
    
    return invoiceExpire2 > 0 ? invoiceExpire2 : invoiceExpire1;
};  
    
const makeSelectInvoiceExpire = () => 
    createSelector(selectInvoiceExpire, substate => substate);

const selectPollTimeout = state => {
    const pollTimeout1 = state.getIn(['config', 'settings', 'pollTimeout'], initialState.getIn(['settings', 'pollTimeout']));
    const pollTimeout2 = selectPollTimeoutDB(state);
    //console.log({pollTimeout1, pollTimeout2});
    return pollTimeout2 > 0 ? pollTimeout2 : pollTimeout1;
};  
    
const makeSelectPollTimeout = () => 
    createSelector(selectPollTimeout, substate => substate);

const selectCurrencies = state =>
    state.getIn(['config', 'currencyes'], initialState.get('currencyes'));

const makeSelectCurrencies = () =>
    createSelector(selectCurrencies, substate => substate.toJS());

const selectCurrency = (state, props) => {
    const currencyes = selectCurrencies(state);
    const { curr } = props.invoice;
    return currencyOf(curr, currencyes.toJS());
};

const makeSelectCurrency = () =>
    createSelector(selectCurrency, substate => substate);
/*
const selectOneMetric = (state, props) => {
  const oneChart = state.get('oneChart', initialState);
  if ({}.hasOwnProperty.call(oneChart, props.idMetric)) {
    return oneChart[props.idMetric];
  }
  return oneChart;
};
*/

const selectGenesisTimestamp = state => 
    state.getIn(['config', 'settings', 'genesisTimestamp'], initialState.getIn(['settings', 'genesisTimestamp']));

const selectBlockTimestamp = state => 
    state.getIn(['config', 'settings', 'blockTimestamp'], initialState.getIn(['settings', 'blockTimestamp']));

const selectBackupName = state => {
    const backupName1 = state.getIn(['config', 'settings', 'backupFileName'], initialState.getIn(['settings', 'backupFileName']));
    const backupName2 = selectBackupNameDB(state);
    return backupName2.length > 0 ? backupName2 : backupName1;
};  
    
const makeSelectBackupName = () => 
    createSelector(selectBackupName, substate => substate);

const selectFormatQRGost = state => {
    const substate = state.getIn(['config', 'settings', 'formatQRGost'], initialState.getIn(['settings', 'formatQRGost']));
    return substate;
};
    
const makeSelectFormatQRGost = () => 
    createSelector(selectFormatQRGost, substate => { 
        return substate;
    });

const selectDefaultPublicKey = state => 
    state.getIn(['config', 'settings', 'defaultPublicKey'], initialState.getIn(['settings', 'defaultPublicKey']));
    
const makeSelectDefaultPublicKey = () => 
    createSelector(selectDefaultPublicKey, substate => substate);

export { 
    currencyOf,
    selectHost,
    selectBaseUrl,
    makeSelectAll,
    selectUrlEraChain, 
    makeSelectUrlEraChain,
    selectLang,
    makeSelectLang,
    selectSettings,
    makeSelectSettings,
    selectDefaultAddress,
    makeSelectDefaultAddress,
    selectNetworkPort,
    makeSelectNetworkPort,
    selectConfirmMode,
    makeSelectConfirmMode,
    selectBlocksPerRequest,
    makeSelectBlocksPerRequest,
    selectBlocksTimeout,
    makeSelectBlocksTimeout,
    selectInvoiceExpire,
    makeSelectInvoiceExpire,
    selectPollTimeout,
    makeSelectPollTimeout,
    selectCurrencies,
    makeSelectCurrencies,
    selectCurrency,
    makeSelectCurrency,
    selectGenesisTimestamp,
    selectBlockTimestamp,
    selectBackupName,
    makeSelectBackupName,
    selectFormatQRGost,
    makeSelectFormatQRGost,
    selectDefaultPublicKey,
    makeSelectDefaultPublicKey
};
