import { createSelector } from 'reselect';
import { initialState } from '../reducers/walletReducer';

/**
 * Direct selector to the userProfile state domain
 */

const toArray = (obj) => {
    if ((obj === undefined) || (obj === null)) {
        return obj; 
    }
    if (typeof(obj) === 'object') {
        return Object.values(obj);
    } else {    
        return obj;
    } 
};

const convertWallet = (wallet) => {
    return {
        ...wallet,
        seed: toArray(wallet.seed),
        keyPair: wallet.keyPair ? {
            secretKey: toArray(wallet.keyPair.secretKey),
            publicKey: toArray(wallet.keyPair.publicKey),
        } : null,
    };
};

const selectPin = state =>
    state.getIn(['wallet', 'wallet', 'pin'], initialState.getIn(['wallet', 'pin']));

const makeSelectPin = () =>
    createSelector(selectPin, substate => substate);

const selectSeed = state =>
    state.getIn(['wallet', 'wallet', 'seed'], initialState.getIn(['wallet', 'seed']));

const makeSelectSeed = () =>
    createSelector(selectSeed, substate => toArray(substate));

const selectKeyPair = state =>
    state.getIn(['wallet', 'wallet', 'keyPair'], initialState.getIn(['wallet', 'keyPair']));

const makeSelectKeyPair = () =>
    createSelector(selectKeyPair, substate => {
        if (!substate) return null;
        let keyPair = substate.toJS();
        keyPair.secretKey = toArray(keyPair.secretKey);
        keyPair.publicKey = toArray(keyPair.publicKey);
        return keyPair;
    });

const selectAddress = state =>
    state.getIn(['wallet', 'wallet', 'address'], initialState.getIn(['wallet', 'address']));

const makeSelectAddress = () =>
    createSelector(selectAddress, substate => substate);

const selectWallet = state => {
    return state.getIn(['wallet', 'wallet'], initialState.get('wallet'));
};

const makeSelectWallet = () => 
    createSelector(selectWallet, substate => {
        let wallet = substate.toJS();
        return convertWallet(wallet);
    });

export { 
    selectPin,
    makeSelectPin,
    selectKeyPair,
    makeSelectKeyPair,
    selectSeed, 
    makeSelectSeed,
    selectAddress,
    makeSelectAddress,
    selectWallet,
    makeSelectWallet,
    convertWallet
};
