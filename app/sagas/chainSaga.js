import { put, call, select, race, take } from 'redux-saga/effects';
import * as types from '../actions/types';
import * as blockchainActions from '../actions/blockchainActions';
import * as dbActions from '../actions/dbActions';
import { 
    lastBlockRequest,
    unconfirmedRequest,
    incomingRequest, 
} from '../core/request';
import * as selectors from '../selectors/configSelectors';
import * as chainSelectors from '../selectors/chainSelectors';

function fetchLastBlock({ baseUrl }) {
    return lastBlockRequest({ baseUrl });
}

function fetchUnconfirmed({ baseUrl, address, type, count, timestamp }) {
    return unconfirmedRequest({ baseUrl, address, type, count, timestamp });
}

export function fetchIncoming({ baseUrl, address, type, fromBlock }) {
    return incomingRequest({ baseUrl, address, type, fromBlock });
}

export function* heightBlockSaga() {
    try {
        const baseUrl = yield select(selectors.selectUrlEraChain);
        const response = yield call(fetchLastBlock, { baseUrl });
        const height = Number.parseInt(response.data);
        yield put(dbActions.actionHeightSave(height));
        const { error } = yield race({
            error: take(types.HEIGHT_SAVE_ERR),
            saved: take(types.HEIGHT_SAVE_OK),
        });
        if (error) {
            throw new Error(error);
        }
        yield put(blockchainActions.actionGetHeightChainOK(height));
    } catch (error) {
        yield put(blockchainActions.actionGetHeightChainErr(error));
    }
}

export function* unconfirmedSaga(action) {
    try {
        const baseUrl = yield select(selectors.selectHost);
        const count = yield select(selectors.selectBlocksPerRequest);
        const response = yield call(fetchUnconfirmed, { ...action.payload, baseUrl, count });
        //console.log(response);
        yield put(blockchainActions.actionGetUnconfirmedOk(response));
    } catch (error) {
        yield put(blockchainActions.actionGetUnconfirmedErr(error));
    }
}

export function* incomingSaga(action) {
    try {
        const baseUrl = yield select(selectors.selectHost);
        const fromBlock = yield select(chainSelectors.selectLastBlock);
        const response = yield call(fetchIncoming, { ...action.payload, baseUrl, fromBlock });
        //console.log(response);
        yield put(blockchainActions.actionGetIncomingOk(response));
    } catch (error) {
        yield put(blockchainActions.actionGetIncomingErr(error));
    }
}
