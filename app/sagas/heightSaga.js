import { put, call, take, race } from 'redux-saga/effects';
import * as types from '../actions/types';
import * as dbActions from '../actions/dbActions';
import DB from '../store/DB';

export function* saveHeightSaga(action) {
    try {
        yield put(dbActions.actionGetParam('height'));
        const { error, ok } = yield race({
            error: take(types.GET_PARAM_ERR),
            ok: take(types.GET_PARAM_OK),
        });
        if (error) {
            yield call(DB.insertParam, { name: 'height', val: action.payload.toString() });
        } 
        if (ok) {
            yield call(DB.updateParam, { name: 'height', val: action.payload.toString() });
        }
        yield put(dbActions.actionHeightSaveOk());
    } catch (error) {
        yield put(dbActions.actionHeightSaveErr(error));
    }
}

export function* restoreHeightSaga() {
    try {
        const param = yield call(DB.queryParam, 'height');
        yield put(dbActions.actionHeightRestoreOk(Number.parseInt(param.val)));
    } catch (error) {
        yield put(dbActions.actionHeightRestoreErr(error));
    }
}
