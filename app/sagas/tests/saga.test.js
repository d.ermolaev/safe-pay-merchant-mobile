import SagaTester from 'redux-saga-tester';
import * as types from '../../actions/types';
import DB from '../../store/DB';
import sagas from '../';
import createSagaMiddleware from 'redux-saga';
import createReducer from '../../reducers'; 
import { fixtures } from '../../core/crypt/tests/fixtures';
import openDbSaga from '../openDbSaga';
import closeDbSaga from '../closeDbSaga';
import * as serversSaga from '../serversSaga';
import backup from '../backupSaga';
import restore from '../restoreSaga';
import insertSaga from '../insertInvoiceSaga';
import updateSaga from '../updateInvoiceSaga';
import findInvoice from '../findInvoiceSaga';
import * as invoiceSaga from '../invoiceSaga';
import * as heightSaga from '../heightSaga';
import pagesSaga from '../invoicePagesSaga';
import maxOrderSaga from '../maxOrderSaga';
import uniqueSaga from '../uniqueSaga';
import * as paramSaga from '../paramSaga';
import * as pinSaga from '../pinSaga';
import * as walletSaga from '../walletSaga';
import * as patternSaga from '../patternSaga';
import telegramSaga from '../telegramSaga';
import checkServerSaga from '../checkServerSaga';
import * as chainSaga from '../chainSaga';
import transactionSaga from '../transactionSaga';
import pollSaga from '../pollSaga';
import scheduleSaga from '../scheduleSaga';
import * as requisiteSaga from '../requisiteSaga';
import { reduxWallet, wallet } from './common';

//const options = { onError: (error) => console.log(error) };

describe('Sagas flow', () => {

    const sagaMiddleware = createSagaMiddleware();
    const sagaTester = new SagaTester({
        initialState: reduxWallet,
        reducers : createReducer(),
        middlewares : [sagaMiddleware],
    });
    sagaMiddleware.run(sagas);
    
    it('should open mock DB', async () => {
        await DB.open();
        expect(DB.db).toBeDefined();
    });

    it('should open DB', async () => {
        sagaTester.start(openDbSaga);
        setTimeout(() => {
            sagaTester.dispatch({ type: types.SCHEDULE_POLL_STOP });
        }, 250);
        await sagaTester.waitFor(types.CREATE_DB_SUCCESS);
        expect(sagaTester.numCalled(types.CREATE_DB_SUCCESS)).toBe(1);
    });

    it('should loadServers', async () => {
        sagaTester.start(serversSaga.loadServers);
        await sagaTester.waitFor(types.LOAD_SERVERS_OK);
        expect(sagaTester.numCalled(types.LOAD_SERVERS_OK)).toBeGreaterThan(1);
    });

    it('should insert invoice', async () => {
        const action = {
            payload: fixtures.dbInvoice,
        };
        sagaTester.start(insertSaga, action);
        await sagaTester.waitFor(types.INSERT_INVOICE_OK);
        expect(sagaTester.numCalled(types.INSERT_INVOICE_OK)).toBe(1);
    });

    it('should update invoice', async () => {
        const action = {
            payload: {
                ...fixtures.dbInvoice,
                status: 2,
            },
        };
        sagaTester.start(updateSaga, action);
        await sagaTester.waitFor(types.UPDATE_INVOICE_OK);
        expect(sagaTester.numCalled(types.UPDATE_INVOICE_OK)).toBe(1);
    });

    it('should find invoice', async () => {
        const { address, recipient, signature, sum, curr } = fixtures.dbInvoice;
        const action = {
            payload: { address, recipient, signature, sum, curr },
        };
        sagaTester.start(findInvoice, action);
        await sagaTester.waitFor(types.FIND_INVOICE_OK);
        expect(sagaTester.numCalled(types.FIND_INVOICE_OK)).toBe(1);
    });

    it('should save height', async () => {
        const action = {
            payload: 1,
        };
        sagaTester.start(heightSaga.saveHeightSaga, action);
        await sagaTester.waitFor(types.HEIGHT_SAVE_OK);
        expect(sagaTester.numCalled(types.HEIGHT_SAVE_OK)).toBe(1);
    });

    it('should restore height from DB', async () => {
        sagaTester.start(heightSaga.restoreHeightSaga);
        await sagaTester.waitFor(types.HEIGHT_RESTORE_OK);
        expect(sagaTester.numCalled(types.HEIGHT_RESTORE_OK)).toBe(1);
    });

    it('should get pages of invoices', async () => {
        sagaTester.start(pagesSaga);
        await sagaTester.waitFor(types.GET_INVOICES_PAGES_OK);
        expect(sagaTester.numCalled(types.GET_INVOICES_PAGES_OK)).toBe(1);
    });

    it('should get invoices', async () => {
        const action = {
            payload: 1,
        };
        sagaTester.start(invoiceSaga.invoiceSaga, action);
        await sagaTester.waitFor(types.GET_INVOICES_OK);
        expect(sagaTester.numCalled(types.GET_INVOICES_OK)).toBe(1);
    });

    it('should add requisite', async () => {
        const action = {
            payload: fixtures.requisite,
        };
        sagaTester.start(requisiteSaga.requisiteAddSaga, action);
        await sagaTester.waitFor(types.ADD_REQUISITE_OK);
        expect(sagaTester.numCalled(types.ADD_REQUISITE_OK)).toBe(1);
    });

    it('should update requisite', async () => {
        const action = {
            payload: fixtures.requisite,
        };
        sagaTester.start(requisiteSaga.requisiteUpdateSaga, action);
        await sagaTester.waitFor(types.UPDATE_REQUISITE_OK);
        expect(sagaTester.numCalled(types.UPDATE_REQUISITE_OK)).toBe(1);
    });

    it('should get requisite', async () => {
        const action = {
            payload: fixtures.requisite,
        };
        sagaTester.start(requisiteSaga.requisiteQuerySaga, action);
        await sagaTester.waitFor(types.GET_REQUISITE_OK);
        expect(sagaTester.numCalled(types.GET_REQUISITE_OK)).toBe(1);
    });

    it('should check expired', async () => {
        sagaTester.start(invoiceSaga.checkExpired);
        await sagaTester.waitFor(types.CHECK_EXPIRED_OK);
        expect(sagaTester.numCalled(types.CHECK_EXPIRED_OK)).toBe(1);
    });

    it('should get max order', async () => {
        sagaTester.start(maxOrderSaga);
        await sagaTester.waitFor(types.GET_MAX_NUM_ORDER_OK);
        expect(sagaTester.numCalled(types.GET_MAX_NUM_ORDER_OK)).toBe(1);
    });

    it('should get unique num', async () => {
        const action = {
            payload: '1',
        };
        sagaTester.start(uniqueSaga, action);
        await sagaTester.waitFor(types.CHECK_UNIQUE_ORDER_ERR);
        expect(sagaTester.numCalled(types.CHECK_UNIQUE_ORDER_ERR)).toBe(1);
    });

    it('should set param', async () => {
        const action = {
            payload: {
                name: 'TestValue',
                val: 'test',
            }
        };
        sagaTester.start(paramSaga.setParamSaga, action);
        await sagaTester.waitFor(types.SET_PARAM_OK);
        expect(sagaTester.numCalled(types.SET_PARAM_OK)).toBeGreaterThan(0);
    });

    it('should get param', async () => {
        const action = {
            payload: 'TestValue',
        };
        sagaTester.start(paramSaga.paramSaga, action);
        await sagaTester.waitFor(types.GET_PARAM_OK);
        expect(sagaTester.numCalled(types.GET_PARAM_OK)).toBe(1);
    });

    it('should insert pin', async () => {
        const action = {
            payload: fixtures.pin,
        };
        sagaTester.start(pinSaga.pinInsertSaga, action);
        await sagaTester.waitFor(types.PIN_INSERT_OK);
        expect(sagaTester.numCalled(types.PIN_INSERT_OK)).toBe(1);
    });

    it('should get pin', async () => {
        sagaTester.start(pinSaga.pinQuerySaga);
        await sagaTester.waitFor(types.PIN_QUERY_OK);
        expect(sagaTester.numCalled(types.PIN_QUERY_OK)).toBe(1);
    });

    it('should add server', async () => {
        const action = {
            payload: fixtures.server,
        };
        sagaTester.start(serversSaga.addServerSaga, action);
        await sagaTester.waitFor(types.ADD_SERVER_OK);
        expect(sagaTester.numCalled(types.ADD_SERVER_OK)).toBeGreaterThan(1);
    });

    it('should get servers', async () => {
        sagaTester.start(serversSaga.serversQuerySaga);
        await sagaTester.waitFor(types.GET_LIST_SERVERS_OK);
        expect(sagaTester.numCalled(types.GET_LIST_SERVERS_OK)).toBe(1);
    });

    it('should remove server', async () => {
        const action = {
            payload: fixtures.server,
        };
        sagaTester.start(serversSaga.removeServerSaga, action);
        await sagaTester.waitFor(types.REMOVE_SERVER_OK);
        expect(sagaTester.numCalled(types.REMOVE_SERVER_OK)).toBe(1);
    });

    it('should set address', async () => {
        const action = {
            payload: wallet.address,
        };
        sagaTester.start(walletSaga.setAddressSaga, action);
        await sagaTester.waitFor(types.SET_ADDRESS_OK);
        expect(sagaTester.numCalled(types.SET_ADDRESS_OK)).toBe(1);
    });

    it('should create wallet', async () => {
        sagaTester.start(walletSaga.createWalletSaga);
        await sagaTester.waitFor(types.CREATE_WALLET_OK);
        expect(sagaTester.numCalled(types.CREATE_WALLET_OK)).toBe(1);
    });

    it('should get wallet', async () => {
        const action = {
            payload: wallet.pin,
        };
        sagaTester.start(walletSaga.walletQuerySaga, action);
        await sagaTester.waitFor(types.WALLET_QUERY_OK);
        expect(sagaTester.numCalled(types.WALLET_QUERY_OK)).toBe(1);
    });

    it('should insert pattern', async () => {
        const action = {
            payload: fixtures.pattern,
        };
        sagaTester.start(patternSaga.patternInsertSaga, action);
        await sagaTester.waitFor(types.PATTERN_INSERT_OK);
        expect(sagaTester.numCalled(types.PATTERN_INSERT_OK)).toBe(1);
    });

    it('should update pattern', async () => {
        const action = {
            payload: fixtures.pattern,
        };
        sagaTester.start(patternSaga.patternUpdateSaga, action);
        await sagaTester.waitFor(types.PATTERN_UPDATE_OK);
        expect(sagaTester.numCalled(types.PATTERN_UPDATE_OK)).toBe(1);
    });

    it('should delete pattern', async () => {
        const action = {
            payload: fixtures.pattern,
        };
        sagaTester.start(patternSaga.patternDeleteSaga, action);
        await sagaTester.waitFor(types.PATTERN_DELETE_OK);
        expect(sagaTester.numCalled(types.PATTERN_DELETE_OK)).toBe(1);
    });

    it('should get patterns', async () => {
        sagaTester.start(patternSaga.patternsQuerySaga);
        await sagaTester.waitFor(types.PATTERNS_QUERY_OK);
        expect(sagaTester.numCalled(types.PATTERNS_QUERY_OK)).toBe(1);
    });

    it('should telegram send', async () => {
        const action = {
            payload: fixtures.transaction,
        };
        sagaTester.start(telegramSaga, action);
        await sagaTester.waitFor(types.TELEGRAM_OK);
        expect(sagaTester.numCalled(types.TELEGRAM_OK)).toBe(1);
    });

    it('should server online', async () => {
        const action = {
            payload: fixtures.server.connectionString,
        };
        sagaTester.start(checkServerSaga, action);
        await sagaTester.waitFor(types.PING_SERVER_SUCCESS);
        expect(sagaTester.numCalled(types.PING_SERVER_SUCCESS)).toBe(1);
    });

    it('should get height', async () => {
        sagaTester.start(chainSaga.heightBlockSaga);
        await sagaTester.waitFor(types.GET_HEIGHT_CHAIN_OK);
        expect(sagaTester.numCalled(types.GET_HEIGHT_CHAIN_OK)).toBe(1);
    });

    it('should get unconfirmed telegram', async () => {
        const action = {
            payload: {
                address: fixtures.address, 
                type: fixtures.type, 
                timestamp: fixtures.timestamp,
            },
        };
        sagaTester.start(chainSaga.unconfirmedSaga, action);
        await sagaTester.waitFor(types.GET_UNCONFIRMEDCOMES_OK);
        expect(sagaTester.numCalled(types.GET_UNCONFIRMEDCOMES_OK)).toBe(1);
    });

    it('should get incoming telegram', async () => {
        const action = {
            payload: {
                address: fixtures.address, 
                type: fixtures.type, 
            },
        };
        sagaTester.start(chainSaga.incomingSaga, action);
        await sagaTester.waitFor(types.GET_INCOMINGFROMBLOCK_OK);
        expect(sagaTester.numCalled(types.GET_INCOMINGFROMBLOCK_OK)).toBe(1);
    });

    it('should transaction success', async () => {
        const action = {
            payload: fixtures.telegram, 
            newStatus: 1, 
        };
        sagaTester.start(transactionSaga, action);
        await sagaTester.waitFor(types.CHECK_TRANSACTION_OK);
        expect(sagaTester.numCalled(types.CHECK_TRANSACTION_OK)).toBe(1);
    });

    it('should poll success', async () => {
        sagaTester.start(pollSaga);
        await sagaTester.waitFor(types.POLL_OK);
        expect(sagaTester.numCalled(types.POLL_OK)).toBe(1);
    });

    it('should schedule success', async () => {
        sagaTester.start(scheduleSaga);
        setTimeout(() => {
            sagaTester.dispatch({ type: types.SCHEDULE_POLL_STOP });
        }, 250);
        await sagaTester.waitFor(types.SCHEDULE_POLL_OK);
        expect(sagaTester.numCalled(types.SCHEDULE_POLL_OK)).toBeGreaterThan(1);
    });

    it('should backup data', async () => {
        const action = {
            payload: 'backupName',
        };
        sagaTester.start(backup, action);
        await sagaTester.waitFor(types.BACKUP_DB_OK);
        expect(sagaTester.numCalled(types.BACKUP_DB_OK)).toBe(1);
    });

    it('should restore data', async () => {
        const action = {
            payload: 'backupName',
        };
        sagaTester.start(restore, action);
        await sagaTester.waitFor(types.RESTORE_DB_OK);
        setTimeout(() => {
            sagaTester.dispatch({ type: types.SCHEDULE_POLL_STOP });
        }, 250);
        expect(sagaTester.numCalled(types.RESTORE_DB_OK)).toBe(1);
    });

    it('should close DB', async () => {
        sagaTester.start(closeDbSaga);
        await sagaTester.waitFor(types.CLOSE_DB_SUCCESS);
        expect(sagaTester.numCalled(types.CLOSE_DB_SUCCESS)).toBe(1);
    });

});