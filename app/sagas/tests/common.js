import { fromJS } from 'immutable';
import { initialState } from '../../reducers/walletReducer';
import { fixtures } from '../../core/crypt/tests/fixtures';
import * as schema from '../../store/Schema';

const keyPair = {
    publicKey: fixtures.publicKey,
    secretKey: fixtures.privateKey,
};

export const stateWallet = initialState
    .setIn(['wallet', 'pin'], fixtures.pin)
    .setIn(['wallet', 'seed'], fixtures.seed)
    .setIn(['wallet', 'keyPair'], keyPair)
    .setIn(['wallet', 'address'], fixtures.address);

export const reduxWallet = fromJS({
    wallet: stateWallet.toJS(),
});

export const wallet = stateWallet.get('wallet').toJS();

export const walletDB = schema.getWallet(wallet);