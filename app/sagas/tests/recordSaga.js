import { runSaga, channel as stdChannel } from 'redux-saga';
import { fromJS } from 'immutable';

export async function recordSaga(saga, initialAction, initialState = fromJS({}), actions = []) {
    const dispatched = [];
    const channel = stdChannel(); 
    const task = runSaga(
        {
            channel,
            dispatch: (action) => {
                console.log(action);
                dispatched.push(action);
            },
            getState: () => (initialState),
        },
        saga,
        initialAction
    );
    for (let i = 0; i < actions.length; i += 2) {
        const action = actions[i](actions[i+1]);
        channel.put(action);
    }
    await task.toPromise();
    return dispatched;
}