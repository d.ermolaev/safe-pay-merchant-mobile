import { put, call, select, race, take } from 'redux-saga/effects';
import * as types from '../actions/types';
import * as walletSelectors from '../selectors/walletSelectors';
import * as walletActions from '../actions/walletActions';
import * as dbActions from '../actions/dbActions';
import DB from '../store/DB';

export function* setAddressSaga(action) {
    try {
        yield put(dbActions.actionSetParam({ name: 'defaultAddress', val: action.payload }));
        const { err } = yield race({
            err: take(types.SET_PARAM_ERR),
            ok: take(types.SET_PARAM_OK),
        });
        if (err) {
            throw new Error(err);
        }
        yield put(walletActions.actionSetAddressOk(action.payload));
    } catch (error) {
        yield put(walletActions.actionSetAddressErr(error));
    }
}

export function* walletQuerySaga(action) {
    try {
        const pin = action.payload;
        const param = yield call(DB.queryParam, 'defaultAddress');
        //console.log(param);
        const dbWallet = yield call(DB.queryWallet, param.val, pin);
        const wallet = walletSelectors.convertWallet(dbWallet);
        yield put(dbActions.actionWalletQueryOk(wallet));
    } catch (error) {
        yield put(dbActions.actionWalletQueryErr(error));
    }
}

export function* createWalletSaga() {
    try {
        const wallet = yield select(walletSelectors.selectWallet);
        const walletJs = wallet.toJS();
        if (!walletJs.pin) {
            const user = yield call(DB.queryPin);
            walletJs.pin = user.pin;
            walletJs.seed = [0];
        }
        const exists = yield call(DB.existsWallet, walletJs.address);
        if (!exists) {
            yield call(DB.insertWallet, walletJs);
        } else {
            yield call(DB.updateParam, { name: 'defaultAddress', val: walletJs.address });
        }
        yield put(walletActions.actionCreateWalletOk());
    } catch (error) {
        yield put(walletActions.actionCreateWalletError(error));
    }
}