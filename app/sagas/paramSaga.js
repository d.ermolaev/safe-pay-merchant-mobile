import { put, call } from 'redux-saga/effects';
import * as dbActions from '../actions/dbActions';
import DB from '../store/DB';


export function* paramSaga(action) {
    try {
        const param = yield call(DB.queryParam, action.payload);
        yield put(dbActions.actionGetParamOk(param));
    } catch (error) {
        yield put(dbActions.actionGetParamErr(error, action.payload));
    }
}

export function* setParamSaga(action) {
    try {
        try {
            yield call(DB.updateParam, action.payload);
        } catch(e) {
            try {
                yield call(DB.insertParam, action.payload);
            } catch (e) {
                throw new Error(e);
            }
        }
        yield put(dbActions.actionSetParamOk(action.payload));
    } catch (error) {
        yield put(dbActions.actionSetParamErr(error));
    }
}