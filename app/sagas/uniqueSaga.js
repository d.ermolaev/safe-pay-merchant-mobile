import { put, call } from 'redux-saga/effects';
import * as dbActions from '../actions/dbActions';
import DB from '../store/DB';

export default function* worker(action) {
    if (action.payload.length === 0) {
        yield put(dbActions.actionUniqueOk(action.payload));
        return;
    }
    try {
        yield call(DB.uniqueNum, action.payload);
        yield put(dbActions.actionUniqueOk());
    } catch (error) {
        yield put(dbActions.actionUniqueErr(error));
    }
}