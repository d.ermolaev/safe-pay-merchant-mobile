import { put, take, race, select } from 'redux-saga/effects';
import * as types from '../actions/types';
import * as dbActions from '../actions/dbActions';
import * as transactionActions from '../actions/transactionActions';
import * as configSelectors from '../selectors/configSelectors';
import * as walletSelectors from '../selectors/walletSelectors';
import { decryptMessage } from '../core/EraChainCrypto';

export default function* worker({ payload, newStatus }) {
    let  telegram;
    const transaction = payload;
    try {

        const { creator, recipient, message, signature, timestamp, confirmations, encrypted } = transaction;
        const confirmMode = yield select(configSelectors.selectConfirmMode);
        if (confirmMode > confirmations) {
            yield put(transactionActions.actionCheckTransactionOk());
            return;
        }
        
        if (encrypted) {
            const wallet = yield select(walletSelectors.selectWallet);
            const msg = decryptMessage(message, wallet.keyPair.publicKey, wallet.keyPair.secretKey);
            if (msg) {
                telegram = JSON.parse(msg);
            } else {
                throw new Error('Error decryption message');
            }
        } else {
            telegram = JSON.parse(message);
        }

        const { orderSignature, sum, curr } = telegram;
        const payload = {
            address: recipient,
            recipient: creator,
            signature: orderSignature,
            sum,
            curr
        };

        yield put(transactionActions.actionFindInvoice(payload));
        const { found } = yield race({
            found: take(types.FIND_INVOICE_OK),
            error: take(types.FIND_INVOICE_ERR)
        });

        if (found) {
            //console.log(found.payload);
            const invoice = found.payload;
            yield put(dbActions.actionUpdateInvoice({
                ...invoice,
                order: invoice.qrcode,
                status: newStatus,
                lastdt: timestamp,
                confirmations,
                confirmSignature: signature,
            }));
            const { updated, error } = yield race({
                updated: take(types.UPDATE_INVOICE_OK),
                error: take(types.UPDATE_INVOICE_ERR)
            });
            if (error) {
                throw new Error(error);
            }
            if (updated) {
                //onsole.log(invoice);
            }
        }
        yield put(transactionActions.actionCheckTransactionOk());
    } catch (error) {
        yield put(transactionActions.actionCheckTransactionErr(error));
    }
}
