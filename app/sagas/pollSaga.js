import { put, take, call, select, race } from 'redux-saga/effects';
import * as types from '../actions/types';
import * as pollActions from '../actions/pollActions';
import * as dbActions from '../actions/dbActions';
import * as blockchainActions from '../actions/blockchainActions';
import * as transactionActions from '../actions/transactionActions';
import * as chainSelectors from '../selectors/chainSelectors';
import * as configSelectors from '../selectors/configSelectors';
import DB from '../store/DB';

function* getAddress(list) {
    for (let item in list) {
        yield list[item];
    }
}

function* checkingTransactions(list, newStatus) {
    for (let i in list) { 
        const transaction = list[i];
        yield put(transactionActions.actionCheckTransaction(transaction, newStatus));
        const { error } = yield race({
            error: take(types.CHECK_TRANSACTION_ERR),
            checking: take(types.CHECK_TRANSACTION_OK),
        });
        if (error) {
            throw new Error(error);
        }
    }
}

export default function* worker() {
    let maxStatus = 0;  // не подтвержденные
    let list, it, next;
    try {
        
        const type = yield select(chainSelectors.selectTypeTransaction);
        const confirmMode = yield select(configSelectors.selectConfirmMode);

        if (confirmMode === 0) {
            
            //  unconfirmed poll
            list = yield call(DB.queryAddresses, maxStatus);
            it = getAddress(list);
            next = it.next();
            while (!next.done) {
                const row = next.value; 

                yield put(blockchainActions.actionGetUnconfirmed({ address: row.address, type, timestamp: row.dt }));
                const { unconfirmed, error1 } = yield race({
                    unconfirmed: take(types.GET_UNCONFIRMEDCOMES_OK),
                    error1: take(types.GET_UNCONFIRMEDCOMES_ERR)
                });
                if (error1) {
                    throw new Error(error1);
                }
                if (unconfirmed) {
                    const { payload } = unconfirmed;
                    const list = payload;
                    const newStatus = 1;
                    if (list.length > 0) {
                        yield call(checkingTransactions, list, newStatus);
                    }
                }
                next = it.next();
            }
        }

        //incomingblock poll
        maxStatus = 1; // подтвержденные блоки транзакций
        list = yield call(DB.queryAddresses, maxStatus);
        it = getAddress(list);
        next = it.next();
        while (!next.done) {

            const row = next.value; 

            let isFinish = false;
            while (!isFinish) {
                yield put(blockchainActions.actionGetIncoming({ address: row.address, type }));
                const { incoming, error2 } = yield race({
                    incoming: take(types.GET_INCOMINGFROMBLOCK_OK),
                    error2: take(types.GET_INCOMINGFROMBLOCK_ERR)
                });
                if (error2) {
                    throw new Error(error2);
                }
                if (incoming) {
                    const { payload } = incoming;
                    const { txs } = payload;
                    const newStatus = 2;
                    if (txs.length > 0) {
                        yield call(checkingTransactions, txs, newStatus);
                    }
                    let height = 0;
                    if ({}.hasOwnProperty.call(payload, 'next')) {
                        height = payload.next;
                    } else if ({}.hasOwnProperty.call(payload, 'height')) {
                        height = payload.height;
                        isFinish = true;
                    } else {
                        isFinish = true;
                    }
                    if (height > 0) {
                        yield put(dbActions.actionHeightSave(height));
                        const { error } = yield race({
                            error: take(types.HEIGHT_SAVE_ERR),
                            saved: take(types.HEIGHT_SAVE_OK),
                        });
                        if (error) {
                            throw new Error(error);
                        }
                        yield put(blockchainActions.actionGetHeightChainOK(height));
                    }
                }
            }
            next = it.next();
        }
        yield put(pollActions.actionPollOk());
    } catch (error) {
        yield put(pollActions.actionPollErr(error));
    }
}

/*
fork

function* firstAsync(payload){
  try {
    const response = yield call(API.post, payload);
    yield put(finishFirst(response));
    return response
  } catch (error) {}
}

function *firstThenSecond(action) {
  const firstTask = yield fork(firstAsync, action)
  const payload = yield join(firstTask)
  yield call(secondAsync, payload);
}
*/