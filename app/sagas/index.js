/**
 *  Redux saga class init
 */
import { takeEvery, all } from 'redux-saga/effects';
import * as types from '../actions/types';
import telegramSaga from './telegramSaga';
import checkServerSaga from './checkServerSaga';
import openDbSaga from './openDbSaga';
import closeDbSaga from './closeDbSaga';
import insertInvoiceSaga from './insertInvoiceSaga';
import updateInvoiceSaga from './updateInvoiceSaga';
import uniqueSaga from './uniqueSaga';
import maxOrderSaga from './maxOrderSaga';
import {
    invoiceSaga,
    checkExpired,
    restoreInvoicesSaga
} from './invoiceSaga';
import invoicePagesSaga from './invoicePagesSaga';
import pollSaga from './pollSaga';
import {
    heightBlockSaga,
    unconfirmedSaga,
    incomingSaga
} from './chainSaga';
import findInvoiceSaga from './findInvoiceSaga';
import transactionSaga from './transactionSaga';
import scheduleSaga from './scheduleSaga';
import {
    setPinSaga,
    pinInsertSaga,
    pinUpdateSaga,
    pinQuerySaga
} from './pinSaga';
import {
    setAddressSaga,
    walletQuerySaga,
    createWalletSaga,
} from './walletSaga';
import {
    saveHeightSaga,
    restoreHeightSaga
} from './heightSaga';
import {
    patternInsertSaga,
    patternUpdateSaga,
    patternDeleteSaga,
    patternsQuerySaga,
} from './patternSaga';
import { paramSaga, setParamSaga } from './paramSaga';
import {
    addServerSaga,
    removeServerSaga,
    serversQuerySaga,
    setCurrentServerSaga,
    loadServers
} from './serversSaga';
import backupSaga from './backupSaga';
import restoreSaga from './restoreSaga';
import {
    requisiteAddSaga,
    requisiteUpdateSaga,
    requisiteQuerySaga
} from './requisiteSaga';

export default function* watch() {
    yield all([takeEvery(types.TELEGRAM_REQUEST, telegramSaga)]);
    yield all([takeEvery(types.PING_SERVER, checkServerSaga)]);
    yield all([takeEvery(types.OPEN_DB, openDbSaga)]);
    yield all([takeEvery(types.CLOSE_DB, closeDbSaga)]);
    yield all([takeEvery(types.INSERT_INVOICE, insertInvoiceSaga)]);
    yield all([takeEvery(types.UPDATE_INVOICE, updateInvoiceSaga)]);
    yield all([takeEvery(types.CHECK_UNIQUE_ORDER, uniqueSaga)]);
    yield all([takeEvery(types.GET_MAX_NUM_ORDER, maxOrderSaga)]);
    yield all([takeEvery(types.GET_INVOICES, invoiceSaga)]);
    yield all([takeEvery(types.GET_INVOICES_PAGES, invoicePagesSaga)]);
    yield all([takeEvery(types.GET_HEIGHT_CHAIN, heightBlockSaga)]);
    yield all([takeEvery(types.GET_UNCONFIRMEDCOMES, unconfirmedSaga)]);
    yield all([takeEvery(types.GET_INCOMINGFROMBLOCK, incomingSaga)]);
    yield all([takeEvery(types.START_POLL, pollSaga)]);
    yield all([takeEvery(types.FIND_INVOICE, findInvoiceSaga)]);
    yield all([takeEvery(types.CHECK_TRANSACTION, transactionSaga)]);
    yield all([takeEvery(types.SCHEDULE_POLL, scheduleSaga)]);
    yield all([takeEvery(types.PIN_INSERT, pinInsertSaga)]);
    yield all([takeEvery(types.PIN_UPDATE, pinUpdateSaga)]);
    yield all([takeEvery(types.PIN_QUERY, pinQuerySaga)]);
    yield all([takeEvery(types.SET_PIN_CODE, setPinSaga)]);
    yield all([takeEvery(types.SET_ADDRESS, setAddressSaga)]);
    yield all([takeEvery(types.WALLET_QUERY, walletQuerySaga)]);
    yield all([takeEvery(types.HEIGHT_SAVE, saveHeightSaga)]);
    yield all([takeEvery(types.HEIGHT_RESTORE, restoreHeightSaga)]);
    yield all([takeEvery(types.PATTERNS_QUERY, patternsQuerySaga)]);
    yield all([takeEvery(types.PATTERN_INSERT, patternInsertSaga)]);
    yield all([takeEvery(types.PATTERN_UPDATE, patternUpdateSaga)]);
    yield all([takeEvery(types.PATTERN_DELETE, patternDeleteSaga)]);
    yield all([takeEvery(types.GET_PARAM, paramSaga)]);
    yield all([takeEvery(types.SET_PARAM, setParamSaga)]);
    yield all([takeEvery(types.ADD_SERVER, addServerSaga)]);
    yield all([takeEvery(types.REMOVE_SERVER, removeServerSaga)]);
    yield all([takeEvery(types.GET_LIST_SERVERS, serversQuerySaga)]);
    yield all([takeEvery(types.SET_CURRENT_SERVER, setCurrentServerSaga)]);
    yield all([takeEvery(types.CHECK_EXPIRED, checkExpired)]);
    yield all([takeEvery(types.LOAD_SERVERS, loadServers)]);
    yield all([takeEvery(types.RESTORE_INVOICES, restoreInvoicesSaga)]);
    yield all([takeEvery(types.CREATE_WALLET, createWalletSaga)]);
    yield all([takeEvery(types.BACKUP_DB, backupSaga)]);
    yield all([takeEvery(types.RESTORE_DB, restoreSaga)]);
    yield all([takeEvery(types.ADD_REQUISITE, requisiteAddSaga)]);
    yield all([takeEvery(types.UPDATE_REQUISITE, requisiteUpdateSaga)]);
    yield all([takeEvery(types.GET_REQUISITE, requisiteQuerySaga)]);
}
