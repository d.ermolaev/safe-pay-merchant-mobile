import { put, call } from 'redux-saga/effects';
import * as transactionActions from '../actions/transactionActions';
import DB from '../store/DB';

export default function* worker(action) {
    try {
        const row = yield call(DB.findInvoice, action.payload);
        yield put(transactionActions.actionFindInvoiceOk(row));
    } catch (error) {
        yield put(transactionActions.actionFindInvoiceErr(error));
    }
}