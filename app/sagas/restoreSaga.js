import { put, call, race, take, select } from 'redux-saga/effects';
import * as types from '../actions/types';
import * as dbActions from '../actions/dbActions';
import DB from '../store/DB';
import LocalStore from '../store/LocalStore';
import { selectAddress } from '../selectors/walletSelectors';
import { strings } from '../config/localize';

export default function* worker(action) { 
    const filename = action.payload;
    try {
        
        const data = yield call(LocalStore.restore, filename);
        //console.log(data);
        const address = yield select(selectAddress);
        //console.log({ address, currentAddress: data.Wallets[0].address });
        let existsAddress = false;
        for (let i = 0; i < data.Wallets.length; i++) {
            if (address === data.Wallets[i].address) {
                existsAddress = true;
            }
        }
        if (!existsAddress) {
            throw new Error(strings['Invalid key']);
        }
        //yield put(dbActions.actionReduxReset());
        yield call(DB.restore, data);
        yield put(dbActions.actionOpenDB());
        const { error } = yield race({
            error: take(types.OPEN_DB_ERROR),
            ok: take(types.CREATE_DB_SUCCESS),
        });
        if (error) {
            throw new Error(error);
        }
        //restart to Login
        /*
        yield put(dbActions.actionGetParam('DefaultServer'));
        yield put(dbActions.actionGetParam('InvoiceExpire'));
        yield put(dbActions.actionGetParam('PollTimeout'));
        */
        yield put(dbActions.actionRestoreOk());
    } catch (error) {
        yield put(dbActions.actionRestoreErr(error));
    }
}
