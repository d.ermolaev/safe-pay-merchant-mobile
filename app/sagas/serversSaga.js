import { put, call, race, take } from 'redux-saga/effects';
import * as types from '../actions/types';
import * as dbActions from '../actions/dbActions';
import * as serversActions from '../actions/serversActions';
import DB from '../store/DB';

export function* setCurrentServerSaga(action) {
    try {
        yield put(dbActions.actionSetParam({ name: 'DefaultServer', val: action.payload.toString() }));
        const { error } = yield race({
            error: take(types.SET_PARAM_ERR),
            ok: take(types.SET_PARAM_OK),
        });
        if (error) {
            throw new Error(error);
        }
        yield put(serversActions.actionCurrentServerOk(action.payload));
    } catch (error) {
        yield put(serversActions.actionCurrentServerErr(error));
    }
}

export function* addServerSaga(action) {
    try {
        const server = yield call(DB.getServer, action.payload);
        yield put(serversActions.actionAddServerOk(server));
    } catch (error) {
        try {
            const id = yield call(DB.insertServer, action.payload);
            yield put(serversActions.actionAddServerOk({ ...action.payload, id }));
        } catch (error) {
            yield put(serversActions.actionAddServerErr(error));
        }
    }
}

export function* removeServerSaga(action) {
    try {
        yield call(DB.deleteServer, action.payload);
        yield put(serversActions.actionRemoveServerOk(action.payload));
    } catch (error) {
        yield put(serversActions.actionRemoveServerErr(error));
    }
}

export function* serversQuerySaga() {
    try {
        const servers = yield call(DB.queryServers);
        yield put(serversActions.actionListServersOk(servers));
    } catch (error) {
        yield put(serversActions.actionListServersErr(error));
    }
}

export function* loadServers() {
    try {
        const servers = {
            list: require('../resources/settings/servers.json'),
        };
        const { list } = servers;
        for (let i = 0; i < list.length; i++) {
            yield put(serversActions.actionAddServer(list[i]));
            //console.log(list[i]);
            const { error } = yield race({
                error: take(types.ADD_SERVER_ERR),
                ok: take(types.ADD_SERVER_OK),
            });
            if (error) {
                throw new Error(error);
            }
        }
        yield put(serversActions.actionLoadServersOk());
    } catch (error) {
        yield put(serversActions.actionLoadServersErr(error));
    }
} 