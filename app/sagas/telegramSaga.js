import { put, call, select, take, race } from 'redux-saga/effects';
import * as types from '../actions/types';
import * as telegramActions from '../actions/telegramActions';
import * as dbActions from '../actions/dbActions';
import { broadcastRequest } from '../core/request';
import * as selectors from '../selectors/configSelectors';

function fetch({ baseUrl, transaction }) {
    return broadcastRequest({ baseUrl, transaction });
}

export default function* worker(action) {
    try {
        const baseUrl = yield select(selectors.selectUrlEraChain);
        const response = yield call(fetch, { baseUrl, transaction: action.payload });
        try {
            const invoice = { ...action.invoice, signature: response.signature };
            yield put(dbActions.actionInsertInvoice(invoice));
            const { error } = yield race({
                error: take(types.INSERT_INVOICE_ERR),
                insert: take(types.INSERT_INVOICE_OK),
            });
            if (error) {
                throw new Error(error);
            }
        } catch (error) {
            throw new Error(error);
        }
        yield put(telegramActions.actionTelegramOk(response));
    } catch (error) {
        yield put(telegramActions.actionTelegramFailed(error));
    }
}
