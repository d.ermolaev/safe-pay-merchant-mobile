import { put, call } from 'redux-saga/effects';
import * as dbActions from '../actions/dbActions';
import DB from '../store/DB';

export default function* worker(action) {
    try {
        yield call(DB.updateInvoice, action.payload);
        yield put(dbActions.actionUpdateInvoiceOk(action.payload));
    } catch (error) {
        yield put(dbActions.actionUpdateInvoiceErr(error));
    }
}