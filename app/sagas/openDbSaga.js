import { put, call, race, take, delay, select } from 'redux-saga/effects';
import * as types from '../actions/types';
import * as dbActions from '../actions/dbActions';
import * as serversActions from '../actions/serversActions';
import * as pollActions from '../actions/pollActions';
import * as blockchainActions from '../actions/blockchainActions';
import { selectSchedule } from '../selectors/chainSelectors';
import DB from '../store/DB';

export default function* worker() {
    try {
        
        yield call(DB.open);
        yield put(dbActions.actionOpenDBSuccess());
        yield call(DB.create);
        yield call(DB.check);
        
        // проверка наличия дополнительных серверов
        const servers = yield call(DB.queryServers);
        if (servers.length === 0) {
            yield put(serversActions.actionLoadServers());
            const { error } = yield race({
                error: take(types.LOAD_SERVERS_ERR),
                ok: take(types.LOAD_SERVERS_OK),
            });
            if (error) {
                throw new Error(error);
            }
        }
        yield put(dbActions.actionPinQuery()); 
        yield race({
            ok: take(types.PIN_QUERY_OK),
            error: take(types.PIN_QUERY_ERR)
        });
        yield put(dbActions.actionHeightRestore());
        const { err } = yield race({
            err: take(types.HEIGHT_RESTORE_ERR),
            ok: take(types.HEIGHT_RESTORE_OK),
        });
        if (err) {
            yield put(blockchainActions.actionGetHeightChain());
        }
        yield delay(1000);
        yield put(dbActions.actionCreateDBSuccess());
        const schedule = yield select(selectSchedule);
        if (!schedule.executing) {
            yield put(pollActions.actionSchedulePoll());
        }
    } catch (error) {
        yield put(dbActions.actionOpenDBError(error));
    }
}
