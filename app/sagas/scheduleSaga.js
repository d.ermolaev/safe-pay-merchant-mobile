import { put, take, select, race, delay } from 'redux-saga/effects';
import * as types from '../actions/types';
import * as dbActions from '../actions/dbActions';
import * as pollActions from '../actions/pollActions';
import * as configSelectors from '../selectors/configSelectors';
import * as chainSelectors from '../selectors/chainSelectors';
import * as invoiceActions from '../actions/invoiceActions';

export default function* worker() {
    let schedule = true;
    try {

        yield put(dbActions.actionGetParam('PollTimeout'));
        const { err } = yield race({
            err: take(types.GET_PARAM_ERR),
            param: take(types.GET_PARAM_OK),
        });
        if (err) {
            //console.log(err);
        }

        const pollTimeout = yield select(configSelectors.selectPollTimeout);
        const blocksTimeout = yield select(configSelectors.selectBlocksTimeout);
        while (schedule) {

            const { stop } = yield race({
                stop: take(types.SCHEDULE_POLL_STOP),
                timeoutSchedule: delay(pollTimeout * 1000),
            });
            if (stop) {
                break;
            }

            const poll = yield select(chainSelectors.selectPoll);
            if (poll.executing) {
                continue;
            }

            const unconfirmed = yield select(chainSelectors.selectUnconfirmed);
            if (unconfirmed.loading) {
                continue;
            }

            const incoming = yield select(chainSelectors.selectIncoming);
            if (incoming.loading) {
                continue;
            }
            
            yield put(pollActions.actionStartPoll());
            const { timeoutPoll, errorPoll } = yield race({
                timeoutPoll: delay(blocksTimeout),
                errorPoll: take(types.POLL_ERR),
                poll: take(types.POLL_OK),
            });
            if (errorPoll) {
                console.log(errorPoll);
            }
            if (timeoutPoll) {
                console.log('Timeout when trying to poll');
            }

            // checking invoice expired 
            yield put(invoiceActions.actionCheckExpired());
            const { errCheck } = yield race({
                errCheck: take(types.CHECK_EXPIRED_ERR),
                ok: take(types.CHECK_EXPIRED_OK),
            });
            if (errCheck) {
                console.log(errCheck);
            }
        }
        yield put(pollActions.actionSchedulePollOk());
    } catch (error) {
        yield put(pollActions.actionSchedulePollErr(error));
    }
}
