import { put, call, race, delay } from 'redux-saga/effects';
import * as serversActions from 'app/actions/serversActions';
import { 
    baseRequest,
    checkProp
} from '../core/request';

function fetch(url) {
    return baseRequest({ method: 'GET', url });
}

export default function* worker(action) {
    const url = action.payload;
    try {
        const { request, timeout } = yield race({
            request: call(fetch, url),
            timeout: delay(10000, 'Request timed out')
        });
        if (request) {
            if (checkProp(request, 'respInfo') && checkProp(request.respInfo, 'status')) {
                if ((request.respInfo.status >= 200) && (request.respInfo.status < 300)) {
                    yield put(serversActions.actionPingServerSuccess(url));
                    return;
                }
            }
            if (checkProp(request, 'message')) {
                yield put(serversActions.actionPingServerError(request.message, url));
            } else {
                throw new Error('Unknown error');
            }
        }
        else {
            yield put(serversActions.actionPingServerError(timeout, url));
        }
    } catch (e) {
        console.log(e);
        yield put(serversActions.actionPingServerError(e, url));
    }
}
