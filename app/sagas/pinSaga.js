import { put, call } from 'redux-saga/effects';
import * as dbActions from '../actions/dbActions';
import DB from '../store/DB';
import { hashString } from '../core/EraChainCrypto';

export function* setPinSaga(action) {
    yield put(dbActions.actionPinInsert(action.payload));
}

export function* pinInsertSaga(action) {
    try {
        const hash = hashString(action.payload);
        yield call(DB.deletePin);
        yield call(DB.insertPin, action.payload, hash);
        yield put(dbActions.actionPinInsertOk(action.payload, hash));
    } catch (error) {
        yield put(dbActions.actionPinInsertErr(error));
    }
}

export function* pinUpdateSaga(action) {
    try {
        const hash = hashString(action.payload);
        yield call(DB.updatePin, action.payload, hash);
        yield put(dbActions.actionPinUpdateOk(action.payload, hash));
    } catch (error) {
        yield put(dbActions.actionPinUpdateErr(error));
    }
}

export function* pinQuerySaga() {
    try {
        const pin = yield call(DB.queryPin);
        //console.log({ user: pin });
        yield put(dbActions.actionPinQueryOk(pin.pin, pin.hash));
    } catch (error) {
        yield put(dbActions.actionPinQueryErr(error));
    }
}
