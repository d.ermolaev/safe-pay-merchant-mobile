import { put, call } from 'redux-saga/effects';
import * as dbActions from '../actions/dbActions';
import DB from '../store/DB';

export default function* worker() {
    try {
        const pages = yield call(DB.invoicePages);
        yield put(dbActions.actionInvoicesPagesOk(pages));
    } catch (error) {
        yield put(dbActions.actionInvoicesPagesErr(error));
    }
}