import { put, call, select, take, race } from 'redux-saga/effects';
import * as types from '../actions/types';
import * as dbActions from '../actions/dbActions';
import * as invoiceActions from '../actions/invoiceActions';
import * as blockchainActions from '../actions/blockchainActions';
import DB from '../store/DB';
import moment from 'moment';
import * as configSelectors from '../selectors/configSelectors';
import * as walletSelectors from '../selectors/walletSelectors';
import { fetchIncoming } from './chainSaga';
import { decryptMessage } from '../core/EraChainCrypto';

export function* invoiceSaga(action) {
    try {
        const data = yield call(DB.queryInvoices, action.payload);
        yield put(dbActions.actionInvoicesOk(data));
    } catch (error) {
        yield put(dbActions.actionInvoicesErr(error));
    }
}

export function* checkExpired() {
    try {
        const currentDate = moment().valueOf();
        yield call(DB.checkExpired, currentDate);
        yield put(invoiceActions.actionCheckExpiredOk());
    } catch (error) {
        yield put(invoiceActions.actionCheckExpiredErr(error));
    }
}

export function* restoreInvoicesSaga(action) {
    let  telegram, response;
    let total = 0;
    try {

        const fromTimestamp = action.payload;
        const genesisTimestamp = yield select(configSelectors.selectGenesisTimestamp);
        const blockTimestamp = yield select(configSelectors.selectBlockTimestamp);
        let fromBlock = Math.round((fromTimestamp - genesisTimestamp) / 1000 / blockTimestamp);
        //console.log({ genesisTimestamp, fromTimestamp, fromBlock });
        const baseUrl = yield select(configSelectors.selectHost);
        const address = yield select(walletSelectors.selectAddress);
        
        let isFinish = false;
        while (!isFinish) {

            try {
                response = yield call(fetchIncoming, { baseUrl, address, type: 0, fromBlock });
                yield put(blockchainActions.actionGetIncomingOk(response));
            } catch (error) {
                yield put(blockchainActions.actionGetIncomingErr(error));
                throw new Error(error);
            }

            const { txs } = response;
            if (txs.length > 0) {
                for (let i = 0; i < txs.length; i++) {
                    const transaction = txs[i];
                    const { creator, recipient, message, signature, timestamp, confirmations, encrypted } = transaction;
                    //console.log({ creator, recipient, message, signature, timestamp, confirmations, encrypted });
                    if (encrypted) {
                        const wallet = yield select(walletSelectors.selectWallet);
                        const msg = decryptMessage(message, wallet.keyPair.publicKey, wallet.keyPair.secretKey);
                        if (msg) {
                            telegram = JSON.parse(msg);
                        } else {
                            throw new Error('Error decryption message');
                        }
                    } else {
                        telegram = JSON.parse(message);
                    }

                    const { orderSignature, sum, curr } = telegram;
                    const payload = {
                        address: recipient,
                        recipient: creator,
                        signature: orderSignature,
                        sum,
                        curr
                    };
                    try {
                        const exists = yield call(DB.existsInvoiceAny, payload);
                        //console.log({ exists });
                        if (!exists) {
                            const invoice = {
                                address: recipient,
                                num: 0,
                                qrcode: 'unspecified',
                                signature: orderSignature,
                                user: '',
                                date: timestamp,
                                expire: timestamp,
                                curr,
                                sum,
                                title: '',
                                description: '',
                                detail: '-',
                                recipient: creator,
                                status: 2,
                                lastdt: timestamp,
                                attempts: 0,
                                confirmations,
                                confirmSignature: signature
                            };
                            yield put(dbActions.actionInsertInvoice(invoice));
                            const { insertError, inserted } = yield race({
                                insertError: take(types.INSERT_INVOICE_ERR),
                                inserted: take(types.INSERT_INVOICE_OK),
                            });
                            if (insertError) {
                                throw new Error(insertError);
                            }
                            if (inserted) {
                                total += 1;
                            }
                        } else {
                            if (exists.status !== 2) {
                                const params = {
                                    ...exists,
                                    status: 2,
                                    lastdt: timestamp,
                                    confirmations,
                                    confirmSignature: signature,
                                };
                                yield put(dbActions.actionUpdateInvoice(params));
                                const { updateError, updated } = yield race({
                                    updateError: take(types.UPDATE_INVOICE_ERR),
                                    updated: take(types.UPDATE_INVOICE_OK),
                                });
                                if (updateError) {
                                    throw new Error(updateError);
                                }
                                if (updated) {
                                    total += 1;
                                }
                            }
                        }
                    } catch (e) {
                        throw new Error(e);
                    }
                }        
            }
            if ({}.hasOwnProperty.call(response, 'next')) {
                fromBlock = response.next;
            } else if ({}.hasOwnProperty.call(response, 'height')) {
                isFinish = true;
            } else {
                isFinish = true;
                throw new Error('Undefined last block');
            }
        }
        //console.log(total);
        yield put(invoiceActions.actionRestoreInvoicesOk(total));
    } catch (error) {
        yield put(invoiceActions.actionRestoreInvoicesErr(error));
    }
}
