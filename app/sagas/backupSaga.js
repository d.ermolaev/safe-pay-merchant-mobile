import { put, call } from 'redux-saga/effects';
import * as dbActions from '../actions/dbActions';
import DB from '../store/DB';
import LocalStore from '../store/LocalStore';

export default function* worker(action) { 
    const filename = action.payload;
    try {
        const backup = yield call(DB.backup);
        const path = yield call(LocalStore.backup, filename, backup);
        //console.log(path);
        yield put(dbActions.actionBackupOk(path));
    } catch (error) {
        yield put(dbActions.actionBackupErr(error));
    }
}
