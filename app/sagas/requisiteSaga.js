import { put, call, race, take } from 'redux-saga/effects';
import * as types from '../actions/types';
import * as dbActions from '../actions/dbActions';
import DB from '../store/DB';


export function* requisiteAddSaga(action) {
    try {
        const id = yield call(DB.insertRequisite, action.payload);
        yield put(dbActions.actionSetParam({ name: 'DefaultRequisiteID', val: id.toString() }));
        const { error } = yield race({
            error: take(types.SET_PARAM_ERR),
            ok: take(types.SET_PARAM_OK),
        });
        if (error) {
            throw new Error(error);
        }
        yield put(dbActions.actionAddRequisiteOk(action.payload));
    } catch (error) {
        yield put(dbActions.actionAddRequisiteErr(error));
    }
}

export function* requisiteUpdateSaga(action) {
    try {
        yield call(DB.updateRequisite, action.payload);
        yield put(dbActions.actionUpdateRequisiteOk(action.payload));
    } catch (error) {
        yield put(dbActions.actionUpdateRequisiteErr(error));
    }
}

export function* requisiteQuerySaga(action) {
    try {
        const data = yield call(DB.getRequisite, action.payload);
        yield put(dbActions.actionGetRequisiteOk(data));
    } catch (error) {
        yield put(dbActions.actionGetRequisiteErr(error));
    }
}

