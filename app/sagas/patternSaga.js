import { put, call, race, take, select } from 'redux-saga/effects';
import * as types from '../actions/types';
import * as dbActions from '../actions/dbActions';
import DB from '../store/DB';
import {  
    selectDefaultTitleID,
    selectDefaultDescID
} from '../selectors/dbSelectors';

export function* patternInsertSaga(action) {
    try {

        const existsID = yield call(DB.existsPattern, action.payload);
        if (existsID > 0) {
            throw new Error('Pattern exists');
        }

        const id = yield call(DB.insertPattern, action.payload);
        const { defaultTitle, defaultDesc } = action.payload;
        if (defaultTitle) {
            yield put(dbActions.actionSetParam({ name: 'DefaultTitle', val: id.toString() }));
            yield race({
                error: take(types.SET_PARAM_ERR),
                ok: take(types.SET_PARAM_OK),
            });
        } 
        if (defaultDesc) {
            yield put(dbActions.actionSetParam({ name: 'DefaultDesc', val: id.toString() }));
            yield race({
                error: take(types.SET_PARAM_ERR),
                ok: take(types.SET_PARAM_OK),
            });
        }
        yield put(dbActions.actionPatternInsertOk({ ...action.payload, id }));
    } catch (error) {
        yield put(dbActions.actionPatternInsertErr(error));
    }
}

export function* patternUpdateSaga(action) {
    try {
        yield call(DB.updatePattern, action.payload);
        const { id, defaultTitle, defaultDesc } = action.payload;

        const idDefaultTitle = yield select(selectDefaultTitleID);
        let val = id.toString();
        let isSetTitle = defaultTitle;
        if (idDefaultTitle === id) {
            val = '0';
            isSetTitle = !defaultTitle;
        }
        if (isSetTitle) {
            yield put(dbActions.actionSetParam({ name: 'DefaultTitle', val }));
            yield race({
                error: take(types.SET_PARAM_ERR),
                ok: take(types.SET_PARAM_OK),
            });
        } 
        
        const idDefaultDesc = yield select(selectDefaultDescID);
        val = id.toString();
        let isSetDesc = defaultDesc;
        if (idDefaultDesc === id) {
            val = '0';
            isSetDesc = !defaultDesc;
        }
        if (isSetDesc) {
            yield put(dbActions.actionSetParam({ name: 'DefaultDesc', val }));
            yield race({
                error: take(types.SET_PARAM_ERR),
                ok: take(types.SET_PARAM_OK),
            });
        }
        yield put(dbActions.actionPatternUpdateOk(action.payload));
    } catch (error) {
        yield put(dbActions.actionPatternUpdateErr(error));
    }
}

export function* patternDeleteSaga(action) {
    try {
        yield call(DB.deletePattern, action.payload);
        yield put(dbActions.actionPatternDeleteOk(action.payload));
    } catch (error) {
        yield put(dbActions.actionPatternDeleteErr(error));
    }
}

export function* patternsQuerySaga() {
    try {
        const data = yield call(DB.queryPatterns);
        yield put(dbActions.actionPatternsQueryOk(data));
    } catch (error) {
        yield put(dbActions.actionPatternsQueryErr(error));
    }
}

