import { put, call } from 'redux-saga/effects';
import * as dbActions from '../actions/dbActions';
import DB from '../store/DB';

export default function* worker(action) {
    try {
        yield call(DB.insertInvoice, action.payload);
        //console.log(action.payload);
        yield put(dbActions.actionInsertInvoiceOk(action.payload));
    } catch (error) {
        yield put(dbActions.actionInsertInvoiceErr(error));
    }
}