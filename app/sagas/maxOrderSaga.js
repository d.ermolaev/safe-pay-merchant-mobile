import { put, call } from 'redux-saga/effects';
import * as dbActions from '../actions/dbActions';
import DB from '../store/DB';

export default function* worker() {
    try {
        const num = yield call(DB.maxNum);
        yield put(dbActions.actionMaxOrderOk(num + 1));
    } catch (error) {
        yield put(dbActions.actionMaxOrderErr(error));
    }
}