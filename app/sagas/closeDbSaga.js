import { put, call } from 'redux-saga/effects';
import * as dbActions from '../actions/dbActions';
import DB from '../store/DB';

export default function* worker() {
    try {
        yield call(DB.close);
        yield put(dbActions.actionCloseDBSuccess());
    } catch (error) {
        yield put(dbActions.actionCloseDBError(error));
    }
}