import { Dimensions } from 'react-native';

const dWidth = Dimensions.get('window').width;

export class Positions {
    static relative = 'relative';
    static absolute = 'absolute';
}

export class AlignItems {
    static flexStart = 'flex-start';
    static flexEnd = 'flex-end';
    static center = 'center';
    static stretch = 'stretch';
    static baseline = 'baseline';
}

export class JustifyContent {
    static flexStart = 'flex-start';
    static flexEnd = 'flex-end';
    static center = 'center';
    static spaceBetween = 'space-between';
    static spaceAround = 'space-around';
}

export class FlexDirection {
    static row = 'row';
    static column = 'column';
    static rowReverse = 'row-reverse';
    static columnReverse = 'column-reverse';
}

export class TextAlign {
    static center = 'center';
    static left = 'left';
    static right = 'right';
}

export class Overflow {
    static visible = 'visible';
    static hidden = 'hidden';
}

/*
type AlignItemsValues = "flex-start" | "flex-end" | "center" | "stretch" | "baseline";
type TextAlignValues = "auto" | "left" | "right" | "center";
type PositionValues = "absolute" | "relative";
type FlexDirectionValues = "row" | "column" | "row-reverse" | "column-reverse";
type JustifyContentValues = "flex-start" | "flex-end" | "center" | "space-between" | "space-around";
type OverflowValues = "visible" | "hidden";
*/

// imageSize: { width: number, height: number }
// return : { width: number, height: number }  
export const scaleToDevice = (imageSize) => {
    return {height: (dWidth * imageSize.height) / imageSize.width, width: dWidth};
};
