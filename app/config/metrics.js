/*
 * platform/application wide metrics for proper styling
*/
import { Platform } from 'react-native';
import { dWidth, dHeight } from './styles';

const metrics = {
    screenWidth: dWidth < dHeight ? dWidth : dHeight,
    screenHeight: dWidth < dHeight ? dHeight : dWidth,
    navBarHeight: Platform.OS === 'ios' ? 54 : 66
};

export default metrics;
