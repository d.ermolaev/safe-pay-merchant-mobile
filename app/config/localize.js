import LocalizedStrings from 'react-native-localization';

export const strings = new LocalizedStrings({
    en: {
        'LicenseSource': '../../resources/license/en.json',
        'LicenseURL': 'https://erachain.org/assets/ERA/Erachain_Licence_Agreement_last.pdf',
        'Account seed': 'Account seed',
        'Add person': 'Enter person',
        'SelectedWithBestHeight': 'Node with best height has detectd',
        'Fill from entered person': 'Fill from entered person',
        'Get public key': 'Get public key',
        'Get person key': 'Get person number',
        'Insert person key': 'Enter person\'s key',
        'Share account': 'Share account',
        'Camera is not avaliable': 'Camera is not avaliable',
        'Share public key': 'Share public key',
        'QR reading error': 'QR reading error',
        'ShowLicense': 'Show License',
        'ScanQR': 'Scan QR',
        'defaultTipNoConnection': 'There\'s no connection.',
        'Warning': 'Warning',
        'servNamepls': 'Input server\'s name',
        'NodeNotRespond': 'Server not responding',
        'error_13': 'Connection strinng error',
        'ThisConSteringAlreadyExist': 'This connection string already exist',
        'Addd': 'Add',
        'Asset not fount': 'Asset not fount',
        'NodesNotRespond': 'Nodes not respond',
        'Switch to': 'Switch to',
        'Checking': 'Checking',
        'Server\'s name': 'Server\'s name',
        'Connection string': 'Connection string',
        'Asset': 'Asset',
        'AssetNaming': 'Asset name',
        'CopyPublicKey': 'Copy public key',
        'ShowPublicKeyAsQR': 'Show public key as QR code',
        'SendLetter': 'Send letter',
        'PersonNotFound': 'Person not found',
        'NothingToCopy': 'Nothing to copy',
        'Naming': 'Naming',
        'Code': 'Code',
        'Description': 'Description',
        'divisible': 'divisible',
        'movable': 'movable',
        'Creator': 'Creator',
        'ShowProfile': 'Show profile',
        'Block': 'Block',
        'Signature': 'Signature',
        'Confirmations': 'Confirmations',
        'Date': 'Date',
        'Type': 'Type',
        'Receiver': 'Receiver',
        'decode': 'decode',
        'Fee': 'Fee',
        'AssetCard': 'Asset\'s card',
        'Language': 'Language',
        'Share my contact': 'Share',
        'I\'am in the Era System': 'I\'m in ERA',
        'My Profile': 'My Profile',
        'Invalid address': 'Invalid address',
        'Person not found': 'Person not found',
        'Need select image': 'You have to select image',
        'error_0': 'The field name is too long',
        'error_1': 'The field name must contain at least 12 characters',
        'error_2': 'Field description required',
        'error_3': 'You must select a date of birth',
        'error_4': 'Place of birth is required',
        'error_5': 'Enter the correct address of the person',
        'error_6': 'All fields are required',
        'error_7': 'Asset is not selected',
        'error_8': 'Could not retrieve information about the person',
        'error_9': 'Error parsing data.',
        'error_10': 'Header is too long',
        'error_11': 'Message is too long',
        'error_12': 'Amount field should contain only digits',
        'error_15': 'Write error because pin is wrong',
        'error_16': 'Duplicate connection string',
        'error_17': 'Error send telegram',
        'error_18': 'Error create local database',
        'error_19': 'Invalid PIN',
        'Header': 'Header',
        'confirmed person': 'confirmed person',
        'anonimous person': 'Anonymous user',
        'unconfirmed person': 'unconfirmed person',
        'connection lost': 'Offline',
        'connection ok': 'Online',
        'receiving profile': 'Receiving person',
        'uncomfirmed': 'not confirmed',
        'confirmations': 'confirmations',
        'License Text': 'License text',
        'person confirmation': 'Person\'s confirmation',
        'Confirm': 'Confirm',
        'Insert address person': 'Paste person\'s account public key',
        'Insert secret key': 'Insert secret key',
        'OR': 'OR',
        'Insert account seed': 'Insert account seed',
        'I Accept': 'I Accept',
        'Account': 'Account',
        'account number': 'account number:',
        'secret key generated': 'Secret key has generated.\nCopy it to clipboard and save in the safe place.',
        'Insert form buffer': 'Insert person from the Clipboard',
        'Accounts': 'Accounts',
        'press icon right': 'Press icon right to insert',
        'Refresh invoices': 'Refresh',
        'Check status': 'Check',
        'Restore old': 'Restore old',
        'Create new': 'Create new',
        'Person data': 'Person\'s data',
        'Persons dict': 'Persons directory',
        'Assets dict': 'Assets directory',
        'Sorry not found': 'Sorry, there is nothing found by your request',
        'Settings': 'Settings',
        'input pin': 'input pin',
        'Person profile': 'Person profile',
        'Favorite persons': 'Favorite persons',
        'Find person in era': 'Find person in Erachain',
        'Searching, wait': 'Wait, searching…',
        'message_1': 'The transaction was not accepted by the server.\nYour balance won\'t be changed.',
        'message_1.1': 'The transaction was not accepted by the server.',
        'message_2': 'The transaction was accepted by the server.\nYour balance will change after\n confirmation.',
        'message_2.1': 'The transaction was accepted by the server.',
        'message_3': 'Favorite assets not found. Search assets in Erachain',
        'message_4': 'Insert secret key to the next field',
        'message_5': 'ERA Safe Pay did not found secret key on your device',
        'message_6': 'Favorite persons not found. Search in Erachain',
        'message_7': 'Favorite persons not found. Search in Erachain',
        'Create Wallet': 'Create Wallet',
        'Find in Fav': 'Find in favorites',
        'Amount': 'Sum',
        'Decline': 'Decline',
        'Verify': 'Verify',
        'Active Name': 'Asset\'s name',
        'Comment': 'Comment',
        'Send Active': 'Send asset',
        'Copied from clipboard': 'Copied from clipboard',
        'Copied to clipboard': 'Copied to clipboard',
        'Copy to clipboard': 'Copy to clipboard',
        'no records': 'There is no records',
        'no fav assets': 'Favorite assets list is empty',
        'AddToFavorite': 'Add to favorites',
        'Add Asset To Favorite': 'Add to favorite',
        'RemoveFromFavorite': 'Remove from favorites',
        'Remove Asset From Favorite': 'Remove from favorite',
        'Recipient': 'Recipient',
        'Keys': 'Keys',
        'Public key': 'Public key',
        'Public bank key': 'Public key for the bank',
        'Private key': 'Private key',
        'Server': 'Server',
        'Add Server': 'Add Server',
        'not enoght': 'Not enough',
        'confirm pls': 'Confirm transaction',
        'Yours address': 'Your address',
        'Yours contacts': 'Your contacts',
        'Send active': 'Send Asset',
        'My wallet': 'My invoices',
        'Copy account': 'Copy account',
        'Yours correct address': 'Your Erachain address:',
        'Copy open key': 'Copy open key',
        'Open Key': 'Public key',
        'Transactions history': 'Transactions history',
        'Register Person': 'Register Person',
        'Transaction': 'Transaction',
        'User address': 'User address',
        'Send transfer': 'Send transfer',
        'Message': 'Message',
        'Size': 'Size',
        'will be fee': 'fee will be withheld',
        'Add': 'Add',
        'Info': 'Info',
        'Search': 'Search',
        'Import': 'Import',
        'Additional form': 'Additional form',
        'birthplace': 'birthplace',
        'Full name': 'Full name',
        'description': 'description',
        'birth date': 'birth date',
        'Continue': 'Continue',
        'License': 'License',
        'Blue': 'Blue',
        'Green': 'Green',
        'Brown': 'Brown',
        'lightBlue': 'Light blue',
        'I\'m in Era: ': 'I\'m in ERA: ',
        'Favorite assets': 'Favorite assets',
        'Find in Era': 'Find Assets in Erachain',
        'g': '',
        'Gray': 'Gray',
        'Blond': 'Blond',
        'Olive': 'Olive',
        'Yantar': 'Amber',
        'Brunette': 'Brunette',
        'Brown-haired': 'Brown-haired',
        'Red-haired': 'Ginger',
        'oldGray': 'Gray-haired',
        'Rusiy': 'Blond',
        'Male': 'Male',
        'Female': 'Female',
        'Yelow': 'Yellow',
        'Afro': 'Black',
        'Search persons in Erachain': 'Search persons in Erachain',
        'Favorite is empty': 'Favorite list is empty',
        'eye color': 'Eye color',
        'hair color': 'Hair color',
        'sex': 'Sex',
        'Height': 'Height',
        'Remove from contacts list': 'Remove from contacts list',
        'Select place': 'Select place',
        'Next': 'Next',
        'Favorite Persons': 'Favorite Persons',
        'Favorite Actives': 'Favorite Assets',
        'Share data': 'Share data',
        'Favorite': 'Favorite',
        'Preson is not Confirmed': 'Person does not confirmed',
        'Transaction Confirmation': 'Transaction Confirmation',
        'Close': 'Close',
        'Declare': 'Declare',
        'Select from gallery': 'Select from gallery',
        'Make new photo': 'Take a new picture',
        'Remove photo': 'Remove photo',
        'WorkWithPersons': 'Deal with persons',
        'User data': 'User data',
        'create pin': 'Create pin',
        'confirm pin': 'Confirm pin',
        'pins do not match': 'Pins do not match',
        'Back': 'Back',
        'Add photo': 'Add photo',
        'Register': 'Register',
        'Wallet': 'Wallet',
        'Receive': 'Receive',
        'NoAccounts': 'You have no avaliable accounts',
        'ReceiveAssets': 'Receive assets',
        'error_14': 'There\'s no valid address',
        'AccountlessPerson': 'Person has no available accounts',
        'Invoice': 'Invoice ',   
        'Expire': 'Expire',
        'Order': 'Order',
        'Purpose': 'Purpose',
        'New order': 'New Invoice',
        'Transaction not confirmed': 'Transaction not confirmed',
        'Invoice confirmation': 'Verified', 
        'Invoice unconfirmed': 'Sent, waiting for payment', 
        'Invoice expired': 'Expired', 
        'String pattern': 'String pattern',  
        'List patterns': 'Patterns',
        'Pattern name': 'Name',
        'Pattern': 'Pattern',
        'Save': 'Save',
        'Pattern guide': 'Pattern guide',
        'Add Pattern': 'Add Pattern',
        'Title warning': 'Warning!',
        'Remove pattern': 'Remove pattern?',
        'ButtonCancel': 'Cancel',
        'ButtonOk': 'OK',
        'Purpose of payment': 'Purpose of payment',
        'Label title': 'Title',
        'Label description': 'Purpose',
        'Default title': 'Default title',
        'Default desc': 'Default purpose',
        'Pattern including VAT': 'Including VAT 18%',
        'Pattern without VAT': 'Without VAT',
        'Pattern tip': 'Pattern tip',
        'No': 'No',
        'From': 'От',
        'To': 'Кому',
        'Invoices recovery': 'Recovery invoices from EraChain',
        'Recovery': 'Recovery',
        'Recovery from': 'Recovery from date',
        'Wait recovery': 'Recovery ...',
        'Recovery error': 'Error of recovery',
        'Recovery success': 'Restored',
        'Invoice expire': 'Invoice expire, hours',
        'Poll timeout': 'Poll timeout of status, min.',
        'Menu store': 'Database',
        'Backup file name': 'File name',
        'Backup': 'Backup to file',
        'Restore': 'Restore from file', 
        'PIN': 'PIN', 
        'Backup error': 'Backup error',
        'Restore error': 'Restore error',
        'Backup progress': 'Backup ...',
        'Restore progress': 'Restore ...',
        'Backup complete': 'Backup complete',
        'Phone': 'Phone',
        'iCloud failed': 'iCloud service not available',
        'Invalid key': 'Invalid secret key',
        'Remarks for restore': 'For recovery, the private key from the setting will be used',
        'TouchID title': 'Authentication required', 
        'TouchID prompt': 'To enter Safe Pay',  
        'Sensor description': 'Touch sensor',
        'Sensor error description': 'Failed',
        'Show Passcode': 'Enter PIN',
        'Corresponding account': 'Corr. account',
        'Bank': 'Bank of recipient',
        'BIK': 'BIC',
        'INN': 'TIN',
        'KPP': 'IEC',
        'Bank details': 'Details',
        'No requisites': 'Fill in the payment details',
        'Import secret': 'Import private key',
    },
    ru: {
        'LicenseSource': '../../resources/license/ru.json',
        'LicenseURL': 'https://erachain.org/assets/ERA/Erachain_Licence_Agreement_2_01_ru.pdf',
        'Account seed': 'Сид счета',
        'Add person': 'Внести персону',
        'SelectedWithBestHeight': 'Найдена нода с большей высотой',
        'Fill from entered person': 'Заполнить из последнего внесения персоны',
        'Get public key': 'Получить ключ',
        'Get person key': 'Найти номер персоны',
        'Insert person key': 'Введите код персоны',
        'defaultTipNoConnection': 'Невозможно отобразить подсказку. Нет соединения.',
        'Asset not fount': 'Актив не найден в избранных',
        'QR reading error': 'Ошибка чтения QR',
        'Warning': 'Внимание',
        'servNamepls': 'Введите имя сервера',
        'NodeNotRespond': 'Сервер не отвечает',
        'error_13': 'Ошибка в строке подключения',
        'ThisConSteringAlreadyExist': 'Такая строка подключения уже существует',
        'Addd': 'Добавить',
        'NodesNotRespond': 'Ни один сервер не отвечает',
        'Switch to': 'Переключение на',
        'Checking': 'Проверка',
        'Server\'s name': 'Имя сервера',
        'Connection string': 'Строка подключения',
        'Asset': 'Актив',
        'AssetNaming': 'Наименование актива',
        'CopyPublicKey': 'Копировать публичный ключ',
        'ShowPublicKeyAsQR': 'Публичный ключ QR-код',
        'SendLetter': 'Отправить письмо',
        'PersonNotFound': 'Персона не найдена',
        'NothingToCopy': 'Нечего копировать',
        'Naming': 'Наименование',
        'Code': 'Код',
        'Description': 'Описание',
        'divisible': 'делимое',
        'movable': 'дивжимое',
        'Creator': 'Создатель',
        'ShowProfile': 'Посмотреть профиль пользователя',
        'Block': 'Блок',
        'Signature': 'Подпись',
        'Confirmations': 'Подтверждения',
        'Date': 'Дата',
        'Type': 'Тип',
        'Receiver': 'Получатель',
        'decode': 'расшифровать',
        'Fee': 'Комиссия',
        'AssetCard': 'Карточка актива',
        'Language': 'Язык',
        'Share my contact': 'Поделиться визиткой',
        'I\'am in the Era System': 'Я в системе ERA',
        'My Profile': 'Мой профиль',
        'Invalid address': 'Не корректный адрес',
        'Person not found': 'Персона не найдена',
        'Need select image': 'Необходимо выбрать изображение',
        'error_0': 'Поле ФИО слишком длинное',
        'error_1': 'Поле ФИО должно содержать не менее 12 символов',
        'error_2': 'Необходимо заполнить поле описание',
        'error_3': 'Необходимо выбрать дату рождения',
        'error_4': 'Необходимо указать место рождения',
        'error_5': 'Введите корректный адрес персоны',
        'error_6': 'Все поля обязательны для заполнения',
        'error_7': 'Актив не выбран',
        'error_8': 'Не удалось получить информацию о персоне',
        'error_9': 'Ошибка разбора данных.',
        'error_10': 'Заголовок слишком длинный',
        'error_11': 'Сообщение слишком длинное',
        'error_12': 'Поле Количество должно содержать только цифры',
        'error_15': 'Ошибка при сохранении из-за неверного PIN',
        'error_16': 'Дублирование сервера',
        'error_17': 'Ощибка при отправке телеграммы',
        'error_18': 'Ошибка создания локальной базы данных',
        'error_19': 'Неверный пин-код',
        'Header': 'Заголовок',
        'confirmed person': 'удостоверенная персона',
        'anonimous person': 'Анонимный пользователь',
        'unconfirmed person': 'не удостоверенная персона',
        'connection lost': 'Соединение потеряно',
        'connection ok': 'Соединение восстановлено',
        'receiving profile': 'Получение пользователя',
        'uncomfirmed': 'не подтверждено',
        'confirmations': 'подтверждений',
        'License Text': 'Текст лицензии',
        'person confirmation': 'Удостоверение персоны',
        'Confirm': 'Подтвердить',
        'Insert address person': 'Вставьте публичный ключ счета персоны',
        'Insert secret key': 'Вставьте секретный ключ',
        'OR': 'ИЛИ',
        'Insert account seed': 'Вставьте сид счета',
        'I Accept': 'Я принимаю',
        'Account': 'Счёт',
        'account number': 'номер счета:',
        'secret key generated': 'Сгенерирован секретный ключ.\nСкопируйте его в буфер и сохраните в безопасном, надежном месте',
        'Insert form buffer': 'Вставьте персону из буфера',
        'Accounts': 'Счета',
        'press icon right': 'Для вставки нажмите иконку справа',
        'Refresh invoices': 'Обновить',
        'Check status': 'Проверка',
        'Restore old': 'Восстановить старый',
        'Create new': 'Создать новый',
        'Person data': 'Данные персоны',
        'Persons dict': 'Справочник персон',
        'Assets dict': 'Справочник активов',
        'Sorry not found': 'Извините, по вашему запросу ничего не найдено.',
        'Settings': 'Настройки',
        'input pin': 'Введите пин-код',
        'Person profile': 'Профиль персоны',
        'Favorite persons': 'Избранные персоны',
        'Find person in era': 'Найти Персону в Erachain',
        'Searching, wait': 'Подождите, выполняется поиск…',
        'message_1': 'Транзакция не принята сервером.\nВаш баланс не изменится.',
        'message_1.1': 'Транзакция не принята сервером.',
        'message_2': 'Транзакция принята сервером.\nВаш баланс изменится после\n ее подтверждения.',
        'message_2.1': 'Транзакция принята сервером.',
        'message_3': 'Избранные активы отсутствуют, выберите активы из Erachain',
        'message_4': 'Вставьте секретный ключ в следующее поле',
        'message_5': 'ERA Safe Pay не обнаружил на устройстве секретный ключ',
        'message_6': 'Избранные персоны отсутствуют выберите персоны из Erachain',
        'message_7': 'Избранные персоны не найдены выполните поиск в Erachain',
        'Create Wallet': 'Создать кошелёк',
        'Find in Fav': 'Найти в избранных',
        'Amount': 'Сумма',
        'Decline': 'Отказаться',
        'Verify': 'Удостоверить',
        'Active Name': 'Наименование актива',
        'Comment': 'Комментарий',
        'Send Active': 'Отправить актив',
        'Copied from clipboard': 'Скопировано из буфера обмена',
        'Copied to clipboard': 'Скопировано в буфер обмена',
        'Copy to clipboard': 'Копировать в буфер',
        'no records': 'Записи отсутствуют',
        'no fav assets': 'Список избранных Активов пуст',
        'AddToFavorite': 'Добавить в избранные',
        'Add Asset To Favorite': 'Добавить в избранное',
        'RemoveFromFavorite': 'Удалить из избранных',
        'Remove Asset From Favorite': 'Удалить из избранного',
        'Recipient': 'Получатель',
        'Keys': 'Ключи',
        'Public key': 'Публичный ключ',
        'Public bank key': 'Публичный ключ для банка',
        'Private key': 'Секретный ключ',
        'Server': 'Сервер',
        'Add Server': 'Добавить сервер',
        'not enoght': 'Не достаточно',
        'confirm pls': 'Подтвердите выполнение транзакции перевода',
        'Yours address': 'Ваш адрес',
        'Yours contacts': 'Ваши контакты',
        'Send active': 'Отправить актив',
        'My wallet': 'Мои счета',
        'Copy account': 'Копировать счёт',
        'Yours correct address': 'Ваш Erachain адрес:',
        'Copy open key': 'Копировать публичный ключ',
        'Open Key': 'Публичный ключ',
        'Transactions history': 'История транзакций',
        'Register Person': 'Регистрация персоны',
        'Transaction': 'Транзакция',
        'User address': 'Адрес пользователя',
        'Send transfer': 'Отправить перевод',
        'Message': 'Сообщение',
        'Size': 'Размер',
        'will be fee': 'Будет удержан сбор',
        'Add': 'Отправить данные',
        'Info': 'Сведения',
        'Search': 'Поиск',
        'Import': 'Импорт',
        'Additional form': 'Дополнительная форма',
        'birthplace': 'место рождения',
        'Full name': 'ФИО',
        'description': 'описание',
        'birth date': 'дата рождения',
        'Continue': 'Продолжить',
        'License': 'Лицензия',
        'Blue': 'Синий',
        'Green': 'Зелёный',
        'Brown': 'Карий',
        'lightBlue': 'Голубой',
        'I\'m in Era: ': 'Я в ERA: ',
        'Favorite assets': 'Избранные активы',
        'Find in Era': 'Поиск актива в Erachain',
        'g': 'г.',
        'Gray': 'Серый',
        'Blond': 'Блондин',
        'Olive': 'Оливковый',
        'Yantar': 'Янтарный',
        'Brunette': 'Брюнет',
        'Brown-haired': 'Шатен',
        'Red-haired': 'Рыжий',
        'oldGray': 'Седой',
        'Rusiy': 'Русый',
        'Male': 'Мужской',
        'Female': 'Женский',
        'Yelow': 'Жёлтый',
        'Afro': 'Чёрный',
        'Search persons in Erachain': 'Поиск Персон в Erachain',
        'Favorite is empty': 'Список избранного пуст',
        'eye color': 'Цвет глаз',
        'hair color': 'Цвет волос',
        'sex': 'Пол',
        'Height': 'Рост',
        'Remove from contacts list': 'Удалить из контактов',
        'Select place': 'Выберите место',
        'Next': 'Далее',
        'Favorite Persons': 'Избранные Персоны',
        'Favorite Actives': 'Избранные Активы',
        'Share data': 'Поделиться данными',
        'Favorite': 'Избранное',
        'Preson is not Confirmed': 'Персона не удостоверена',
        'Transaction Confirmation': 'Подтверждение транзакции',
        'Close': 'Закрыть',
        'Declare': 'Удостоверение',
        'Select from gallery': 'Выбрать фото из галереи',
        'Make new photo': 'Сделать новое фото',
        'Remove photo': 'Удалить фото',
        'WorkWithPersons': 'Работа с персонами',
        'User data': 'Данные пользователя',
        'create pin': 'Создайте пин-код',
        'confirm pin': 'Повторите пин-код',
        'pins do not match': 'Пин-коды не совпадают',
        'Back': 'Назад',
        'Add photo': 'Добавить фото',
        'Register': 'Регистрация',
        'Wallet': 'Кошелек',
        'Receive': 'Получить',
        'NoAccounts': 'У вас нет доступных счетов',
        'ReceiveAssets': 'Получить актив',
        'error_14': 'Отсутствует адрес',
        'ScanQR': 'Сканировать QR',
        'ShowLicense': 'Лицензионное соглашение',
        'Share account': 'Отправить счёт',
        'Camera is not avaliable': 'Камера не доступна',
        'Share public key': 'Отправить публичный ключ',
        'Camera not avaliable': 'Камера недоступна',
        'AccountlessPerson': 'У персоны нет доступных счетов',
        'Invoice': 'Счет ',
        'Expire': 'Истекает',
        'Order': 'Номер',
        'Purpose': 'Назначение',
        'New order': 'Новый счет',
        'Transaction not confirmed': 'Транзакция не подтверждена',
        'Invoice confirmation': 'Подтвержден',
        'Invoice unconfirmed': 'Отправлен, ожидает оплаты',
        'Invoice expired': 'Expired', 
        'String pattern': 'Строковый шаблон',
        'List patterns': 'Шаблоны',
        'Pattern name': 'Имя',
        'Pattern': 'Шаблон',
        'Save': 'Сохранить',
        'Pattern guide': 'Pattern guide',
        'Add Pattern': 'Добавить шаблон',
        'Title warning': 'Внимание!',
        'Remove pattern': 'Удалить шаблон?',
        'ButtonCancel': 'Отмена',
        'ButtonOk': 'OK',
        'Purpose of payment': 'Назначение платежа',
        'Label title': 'Заголовок',
        'Label description': 'Назначение',
        'Default title': 'Заголовок по умолчанию',
        'Default desc': 'Назначение платежа по умолчанию',
        'Pattern including VAT': 'Включая НДС 18%',
        'Pattern without VAT': 'Без НДС',
        'Pattern tip': 'Подсказка по шаблонам',
        'No': '№',
        'From': 'From',
        'To': 'To',
        'Invoices recovery': 'Восстановление счетов из EraChain',
        'Recovery': 'Восстановление',
        'Recovery from': 'Восстановить с даты',
        'Wait recovery': 'Восстановление ...',
        'Recovery error': 'Ошибка восстановления',
        'Recovery success': 'Восстановлено платежей',
        'Invoice expire': 'Время жизни платежа, час.',
        'Poll timeout': 'Таймаут опроса статуса, мин.',
        'Menu store': 'База данных',
        'Backup file name': 'Имя файла',
        'Backup': 'Сохранить в файл',
        'Restore': 'Восстановить из файла', 
        'PIN': 'PIN-код', 
        'Backup error': 'Ошибка сохранения',
        'Restore error': 'Ошибка восстановления',
        'Backup progress': 'Сохранение ...',
        'Restore progress': 'Восстановление ...',
        'Backup complete': 'Сохранено',
        'Phone': 'Телефон',
        'iCloud failed': 'iCloud сервис не доступен',
        'Invalid key': 'Неверный секретный ключ',
        'Remarks for restore': 'Для восстановления будет использован приватный ключ из настроек',
        'TouchID title': 'Требуется аутентификация', 
        'TouchID prompt': 'Для входа в Safe Pay',  
        'Sensor description': 'Сенсорный датчик',
        'Sensor error description': 'Неудачный',   
        'Show Passcode': 'Ввести ПИН',   
        'Corresponding account': 'Корр.счет',
        'Bank': 'Банк получателя',
        'BIK': 'БИК',
        'INN': 'ИНН',
        'KPP': 'КПП', 
        'Bank details': 'Реквизиты',
        'No requisites': 'Заполните платежные ревизиты',
        'Import secret': 'Импорт секретного ключа',
    },
});
