/* tslint:disable:no-object-literal-type-assertion no-magic-numbers*/

import { Dimensions, Platform, StyleSheet } from 'react-native';
import { isIphoneX } from 'react-native-iphone-x-helper';
import { AlignItems, FlexDirection, JustifyContent, Positions, TextAlign } from './RNStyles';
import { FontNames } from './FontNames';

export const textBorderColor = '#39414d'; 
export const textPlaceholderColor = '#39414d'; //728196
export const primaryColor = '#576373'; // #576373 //сильно-серый
export const primaryColorLight = '#C89600'; //#EC036E  //красный
export const secondaryColor = '#6E5EA3'; //#375C98 синий
export const primaryColor2 = '#39414d'; //3B5998 // темно-серый
export const primaryColor3 = '#2C315D'; //3F67A2 синий
export const disableColor = '#AAAAAA';
export const shadowColor = '#EAE7E7';   
export const whiteColor = '#FFFFFF';
export const lineLightColor = '#F6F6F6';
export const lineColor = '#E6EBF2';
export const menuItemBackgroudSelected = '#EFF2F5';
export const backgroundGray = '#EEF1F5';
export const transparentColor = 'transparent';
export const slideMenuTextColor = '#666666';
export const sliderTransparentBackground = '#00000058';
export const peachColor = '#F58460';
export const logoColor1 = '#C89600';

const { width, height } = Dimensions.get('window');

/*
{window: {…}, version: 26}version: 26window: {width: 107, height: 190, scale: 1, fontScale: 1}__proto__: Object
fc580b53-bab8-45c3-8…-a06c533f495e:96418 
 */

const deviceHeight = isIphoneX() ? height - 78 : height;

const deviceWidth = width;

/*
export function RFValue(fontSize) {
    // guideline height for standard 5" device screen
    const standardScreenHeight = 680;
    const heightPercent = (fontSize * deviceHeight) / standardScreenHeight;
    return Math.round(heightPercent) + 1;
}
*/

export function RF(percent) {
    const heightPercent = (percent * deviceHeight) / 100;
    return Math.round(heightPercent);
}

export const marginVerticat = RF(5); 
export const marginHorizontal = RF(2); 

export const fontSizeLabel = RF(1.9); //12
export const fontSizeLabel2 = RF(2.2); //14
export const fontSizeValue = RF(1.9); //12
export const fontSizeInput = 14;
export const fontSizeText = RF(1.9); 
export const fontSizeCurr = RF(2.2);
export const fontSizeButton = RF(2.2);
export const fontSizeMenu = RF(2.2);
export const fontSizePin = RF(6.4); //40
export const fontSizeTitle = RF(2.6); //16

export const fontSize2 = RF(0.2);
export const fontSize4 = RF(0.5);
export const fontSize5 = RF(0.6);
export const fontSize7 = RF(0.8);
export const fontSize8 = RF(1);
export const fontSize10 = RF(1.5);
export const fontSize11 = RF(1.8);
export const fontSize12 = 12;               //for TextInput
export const fontSize14 = 14;               //for TextInput
export const fontSize15 = RF(2.5);
export const fontSize16 = 16;               //for TextInput
export const fontSize18 = RF(3.0);
export const fontSize19 = RF(3.1);
export const fontSize20 = RF(3.3);
export const fontSize21 = RF(3.4);
export const fontSize22 = RF(3.5);
export const fontSize23 = RF(3.7);
export const fontSize24 = RF(3.9);
export const fontSize26 = RF(4.2);
export const fontSize28 = RF(4.6);
export const fontSize30 = RF(4.8);
export const fontSize47 = RF(7.5);
export const fontSize50 = RF(8);

//export const fontSize12 = RF(1.9)
//export const fontSize14 = RF(2.2)
//export const fontSize16 = RF(2.6)


//const dimensions = Dimensions.get('window');
export const dWidth = deviceWidth;
export const dHeight = deviceHeight;
export const sWidth = deviceWidth;
export const sHeight = deviceHeight;

/*
console.log({ 
    version: Platform.Version, 
    window: Dimensions.get('window'),
    screen: Dimensions.get('screen'),
    dWidth, 
    dHeight
});
*/


const dPaddingHorizontal = 24;
const dInfoItemHeight = 100;
const dNavigationBarHeight = 42;
const dMenuItemHeight = 46;
const dMenuIconLefpPadding = 24;
const minLabelWidth = Math.round(dWidth / 4); 

/*
console.log({
    dWidth,
    dHeight,
    fontSize2,
    fontSize4,
    fontSize5,
    fontSize8,
    fontSize10,
    fontSize11,
    fontSize12,
    fontSize14,
    fontSize15,
    fontSize16,
    fontSize18,
    fontSize19,
    fontSize20,
    fontSize21,
    fontSize22,
    fontSize23,
    fontSize26,
    fontSize28,
    fontSize30,
});
*/

export const debugBorders = __DEV__ ? {
    borderWidth: 1,
    borderColor: 'red'
} : {};

const avatarLargeSize = 70;
export const styles = {
    debugBorders: {...debugBorders},

    splash: {width: dWidth, height: dHeight, flex: 1, resizeMode: 'cover'},

    container: {flex: 1, position: Positions.relative, backgroundColor: whiteColor},
    container2: {flex: 1, backgroundColor: whiteColor},

    qrRreader: {
        width: dWidth - dWidth * 0.2,
        height: dWidth - dWidth * 0.2,
    },

    assets: {
        fullScreenButton: {
            position: Positions.absolute,
            bottom: 8,
            right: 8,
            width: 22,
            height: 22,
            tintColor: textBorderColor
        }
    },
    alert: {
        title: {
            fontFamily: FontNames.ProstoLight,
        },
        text: {
            fontFamily: FontNames.ProstoLight,

        },
        overlayStyle: {
            width: dWidth,
            height: dHeight,
            position: 'absolute',
            backgroundColor: 'rgba(52,52,52,0.5)',
        },
        container: {
            flex: 1,
            width: dWidth,
            height: dHeight,
            alignItems: 'center',
            justifyContent: 'center',
            position: 'absolute',
        },
        content: {
            maxWidth: dWidth * 0.8,
        },
    },
    settings: {
        serverContainer: {
            flex: 1,
        },
        alert: {
            backgroundColor: transparentColor,
            fontFamily: FontNames.ProstoRegular,
            fontSize: fontSize16,
            color: textBorderColor,
            paddingVertical: 2,
            lineHeight: fontSize24,
            marginVertical: 16,
        },
        slideLabelContainer: {
            flexDirection: 'row', 
            justifyContent: 'space-between', 
        },
        serverLabelContainer: {marginBottom: 16},
        serverLabel: {
            flex: 4,
            color: secondaryColor,
            backgroundColor: transparentColor,
            fontSize: fontSizeTitle,
            fontFamily: FontNames.ProstoLight,
        },
        serverValue: {
            flex: 1,
            textAlign: 'right',
            color: primaryColor,
            backgroundColor: transparentColor,
            fontSize: fontSizeValue,
            fontFamily: FontNames.ProstoLight,
        },
        serverItemContainer: {
            marginVertical: 8,
            flexDirection: FlexDirection.row,
            alignItems: AlignItems.center,
            justifyContent: JustifyContent.spaceBetween,
        },
        serverItemSubContainer: {
            flexDirection: FlexDirection.row,
            alignItems: AlignItems.center,
            justifyContent: JustifyContent.flexStart,
            flexShrink: 3
        },
        dot: {
            backgroundColor: primaryColorLight,
            width: 5,
            height: 5,
            borderRadius: 2.5,
        },
        dot2: {
            backgroundColor: secondaryColor,
            width: 5,
            height: 5,
            borderRadius: 2.5,
        },
        nodot: {
            backgroundColor: 'transparent',
            width: 5,
            height: 5,
            borderRadius: 2.5,
        },
        serverName: {
            color: textPlaceholderColor,
            backgroundColor: transparentColor,
            fontSize: fontSize14,
            fontFamily: FontNames.ProstoLight,
            marginLeft: 8,
            flexShrink: 3
        },
        serverConnectionString: {
            color: textPlaceholderColor,
            backgroundColor: transparentColor,
            fontSize: fontSize10,
            fontFamily: FontNames.ProstoLight,
            marginLeft: 8,
            flexShrink: 3
        },
        serverDelete: {
            width: 24,
            height: 24,
        },
        serverAddImage: {
            width: 20,
            height: 20
        },
        serverAddContainer: {
            marginVertical: fontSize16,
            flexDirection: FlexDirection.row,
            alignItems: AlignItems.center,
            justifyContent: JustifyContent.flexStart
        },
        serverAddLabel: {
            color: secondaryColor,
            backgroundColor: transparentColor,
            fontSize: fontSizeButton,
            lineHeight: fontSize23,
            fontFamily: FontNames.ProstoLight,
            marginLeft: 8,
            ...Platform.select({
                ios: {
                    paddingTop: 4
                },
                android: {}
            })
        },

    },

    shared: {
        topBackgroundImage: {
            position: Positions.absolute,
            top: 0,
            left: -2,
            right: -2,
            height: 64,
            width: dWidth + 4
        },
        line: {
            backgroundColor: lineColor,
            height: 1,
            marginHorizontal: 0
        },
        hr: {
            borderBottomColor: lineColor,
            borderBottomWidth: StyleSheet.hairlineWidth,
        },
    },
    geo: {
        container: {
            flex: 1,
            position: Positions.relative,
            backgroundColor: whiteColor,
        },
        contentContainer: {
            flex: 1,
            paddingHorizontal: 16
        },
        searchContainer: {
            flexDirection: FlexDirection.row,
            alignItems: AlignItems.center,
            justifyContent: JustifyContent.flexStart,
            paddingVertical: 16,
            borderBottomColor: '#B6BFCB',
            borderBottomWidth: 1,
        },
        searchImage: {
            width: 19,
            height: 20,
            marginHorizontal: 8
        },
        searchText: {
            backgroundColor: transparentColor,
            fontFamily: FontNames.ProstoLight,
            fontSize: fontSize14,
            color: textPlaceholderColor,
            flex: 1,
            paddingLeft: 8,
            textAlignVertical: AlignItems.center,
            alignSelf: AlignItems.center
        },
        locationItemsListContainer: {
            paddingHorizontal: 8,
            paddingVertical: 8,
        },
        item: {
            container: {
                justifyContent: JustifyContent.spaceBetween,
            },
            textContainer: {
                paddingVertical: 8,
            },
            title: {
                backgroundColor: transparentColor,
                fontFamily: FontNames.ProstoRegular,
                fontSize: fontSize16,
                color: secondaryColor,
                paddingVertical: 2
            },
            subtitle: {
                backgroundColor: transparentColor,
                fontFamily: FontNames.ProstoRegular,
                fontSize: fontSize16,
                color: textBorderColor,
                paddingVertical: 2
            },
        }

    },
    transactionConfirm: {
        middleContainer: {
            backgroundColor: whiteColor,
            flex: 1,

        },
        messageContainer: {
            paddingVertical: 16,
            alignItems: AlignItems.center,
            justifyContent: JustifyContent.flexStart,

        },
        message: {
            textAlign: AlignItems.center,
            fontSize: fontSize16,
            lineHeight: fontSize18,
            color: textPlaceholderColor,
            backgroundColor: transparentColor,
            fontFamily: FontNames.ProstoRegular,
        },
        headerInfoContainer: {
            alignItems: AlignItems.flexStart,
            justifyContent: JustifyContent.flexStart,
            paddingVertical: 8
        },
        receiver: {
            fontSize: fontSize20,
            lineHeight: fontSize26,
            color: secondaryColor,
            backgroundColor: transparentColor,
            fontFamily: FontNames.ProstoRegular

        },
        account: {
            fontSize: fontSize16,
            lineHeight: fontSize21,
            color: textPlaceholderColor,
            backgroundColor: transparentColor,
            fontFamily: FontNames.ProstoRegular,
            marginTop: 16
        },
        bottomInfoContainerTextStyle: {
            alignItems: AlignItems.flexStart,
            justifyContent: JustifyContent.flexStart,
        },
        textDesc: {
            fontSize: fontSize16,
            lineHeight: fontSize21,
            color: textPlaceholderColor,
            backgroundColor: transparentColor,
            fontFamily: FontNames.ProstoRegular,
            marginTop: 16
        },

        buttonContainer: {
            paddingVertical: 4,
            paddingHorizontal: 8

        },
        feeText: {
            fontSize: fontSize16,
            lineHeight: fontSize21,
            color: primaryColorLight,
            backgroundColor: transparentColor,
            fontFamily: FontNames.ProstoRegular,
            marginVertical: 16,
            textAlign: TextAlign.center,
        },
        warningText: {
            fontSize: fontSize16,
            lineHeight: fontSize21,
            color: peachColor,
            backgroundColor: transparentColor,
            fontFamily: FontNames.ProstoRegular,
            textAlign: AlignItems.center
        }
    },
    transactionDetail: {
        container: {flex: 1, backgroundColor: whiteColor, paddingVertical: 8, paddingHorizontal: 16},
        subContainer: {
            flexDirection: FlexDirection.row,
            alignItems: AlignItems.center,
            justifyContent: JustifyContent.flexStart,
            paddingVertical: 8
        },
        title: {
            color: secondaryColor,
            backgroundColor: transparentColor,
            fontSize: fontSize16,
            lineHeight: fontSize21,
            fontFamily: FontNames.ProstoLight
        },
        value: {
            flex: 1,
            color: textPlaceholderColor,
            backgroundColor: transparentColor,
            fontSize: fontSize16,
            lineHeight: fontSize21,
            fontFamily: FontNames.ProstoLight,
            marginLeft: 8
        },
    },

    login: {
        containerTop: {
            //position: Positions.relative,
            height: 120
            // paddingBottom: bottomTiltSize.height,
        },

        largeLogo: {
            flex: 1,
            backgroundColor: primaryColor3,
            alignItems: AlignItems.center,
            justifyContent: JustifyContent.center,
        },
        bottomContainer: {
            // position: Positions.absolute,
            // bottom: 0,
            // left: 0,
            // right: 0,
            height: 60,
            flexDirection: FlexDirection.row,
            alignItems: AlignItems.stretch,
            justifyContent: JustifyContent.center
        },
        web: {
            borderColor: textBorderColor,
            borderWidth: 1,
            borderRadius: 4,
            flex: 1,
            paddingHorizontal: 8,
            paddingVertical: 8,
        },
        largeLogoBackgroundImage: {
            flex: 1,
            width: dWidth,
            height: 200,
            alignItems: AlignItems.center,
            justifyContent: JustifyContent.center,
            // paddingHorizontal: 32,
            // paddingTop: 126,
            // paddingBottom: 158
        },
        logo: {
            width: dWidth - 64,
        },
        message: {
            color: textPlaceholderColor,
            fontSize: fontSizeTitle,
            fontFamily: FontNames.ProstoRegular,
            paddingHorizontal: 36,
            textAlign: 'center',
            lineHeight: fontSize21,
            marginBottom: 33,
            letterSpacing: 0
        },
        bottomTilt: {
            position: Positions.absolute,
            bottom: -40,
            height: 75,
            width: dWidth * 1.2,
            backgroundColor: '#fff',
            transform: [
                {skewY: '-10deg'}
            ],
        },

        register: {
            btn: {position: Positions.absolute, right: 52, bottom: 25},
            icon: {},
        },

        containerBottom: {
            flex: 1,
            backgroundColor: '#fff',
            flexDirection: FlexDirection.column,
            justifyContent: JustifyContent.flexStart,
            paddingHorizontal: 32,
            paddingTop: 32

        },
        containerBottomLicence: {
            flex: 1,
            backgroundColor: '#fff',
            flexDirection: FlexDirection.column,
            justifyContent: JustifyContent.flexStart,
            paddingHorizontal: 16,
            paddingTop: 16

        },
        containerBottomComplete: {
            flex: 1,
            backgroundColor: '#fff',
            flexDirection: FlexDirection.column,
            justifyContent: JustifyContent.flexStart,
        },

        containerForm: {
            alignItems: AlignItems.center,
            position: Positions.relative,
            justifyContent: JustifyContent.center,
            flexDirection: FlexDirection.row,
            marginBottom: 25
        },

        containerBtn: {
            flexDirection: FlexDirection.row,
            justifyContent: JustifyContent.center,
            width: 230,
        },

        customBtnStyles: {
            marginBottom: 8,
        },

        placeholder: {
            flex: 1,
            backgroundColor: 'transparent',
            position: Positions.absolute,
            fontFamily: FontNames.ProstoLight,
            fontSize: fontSizeTitle,
            color: primaryColor,
        },

        inputShow: {
            position: Positions.absolute,
            flexDirection: FlexDirection.row,
            justifyContent: JustifyContent.spaceBetween,
            width: 230,
        },

        digit: {
            width: 40,
            height: 50,
            borderBottomColor: textPlaceholderColor,
            borderBottomWidth: 1,
            alignItems: AlignItems.center,
            justifyContent: JustifyContent.center,
        },

        input: {
            backgroundColor: 'transparent',
            opacity: 0.1,
            color: '#fff',
            borderColor: '#fff',
            fontSize: fontSize5,
            width: 230,
            height: 50,
            borderWidth: 1
        },

        digitValue: {
            backgroundColor: 'transparent',
            fontSize: fontSizePin,
            fontFamily: FontNames.ProstoLight
        }
    },

    register: {
        top: {height: 120},
        bottom: {flex: 1, backgroundColor: '#fff', paddingHorizontal: 32},
        icon_pin: {
            tintColor: primaryColor2,
            height: 19,
        },
        userImageSelect: {
            marginBottom: 17,
            marginTop: 24
        },
        exContainer: {
            position: Positions.absolute,
            top: 16,
            right: 16
        },
        exPoint: {
            width: 24,
            height: 24,
        },
        square: {width: 18, height: 18, backgroundColor: secondaryColor, borderRadius: 4},
        squareSmall: {width: 12, height: 12, backgroundColor: secondaryColor, borderRadius: 4},
        aceptContainer: {
            flexDirection: FlexDirection.row,
            alignItems: AlignItems.center,
            justifyContent: JustifyContent.flexStart,
            marginRight: 16
        },
        button1: {
        },
        aceptSquareBorder: {
            width: 24,
            height: 24,
            marginVertical: 16,
            borderColor: textBorderColor,
            borderWidth: 1,
            alignItems: AlignItems.center,
            justifyContent: JustifyContent.center,
            borderRadius: 4
        },
        aceptSquareBorderSmall: {
            width: 16,
            height: 16,
            marginVertical: 16,
            borderColor: textBorderColor,
            borderWidth: 1,
            alignItems: AlignItems.center,
            justifyContent: JustifyContent.center,
            borderRadius: 4
        },
        liceseText: {
            color: secondaryColor,
            fontFamily: FontNames.ProstoRegular,
            fontSize: fontSizeLabel2,
            marginBottom: 16,
            textAlignVertical: 'center',
            lineHeight: fontSize20
        },
        aceptText: {
            color: textPlaceholderColor,
            backgroundColor: transparentColor,
            fontFamily: FontNames.ProstoRegular,
            fontSize: fontSizeLabel2,
            marginLeft: 8
        },
        buttonContinue: {
            backgroundColor: primaryColor3,
            flexDirection: FlexDirection.row,
            flex: 1,
            alignItems: AlignItems.center,
            justifyContent: JustifyContent.center,
        },
        completeBigMessage: {
            fontSize: fontSize14,
            color: textBorderColor,
            backgroundColor: transparentColor,
            lineHeight: Platform.OS === 'ios' ? fontSize16 : fontSize24,
            fontFamily: FontNames.ProstoRegular,
            textAlign: 'center',
            paddingVertical: 24
        },
        bottomContainer: {
            position: Positions.absolute,
            bottom: 0,
            left: 0,
            right: 0,
            height: 70,
            flexDirection: FlexDirection.row,
            alignItems: AlignItems.stretch,
            justifyContent: JustifyContent.center
        },
        buttonText: {
            color: whiteColor,
            fontFamily: FontNames.ProstoRegular,
            fontSize: fontSize18,
            marginHorizontal: 10,
            backgroundColor: transparentColor,
            lineHeight: fontSize21
        },
        buttonNextDisabledText: {
            color: textPlaceholderColor,
            fontFamily: FontNames.ProstoRegular,
            fontSize: fontSize18,
            marginHorizontal: 10,
            backgroundColor: transparentColor,
            lineHeight: fontSize21
        },
        copyButtonText: {
            flex: 1,
            textAlign: 'center',
            textAlignVertical: 'center',
            color: textPlaceholderColor
        },
        message: {
            color: textPlaceholderColor,
            fontSize: fontSizeTitle,
            lineHeight: fontSize18,
            fontFamily: FontNames.ProstoRegular,
            textAlign: 'center',
            width: 200,
            alignSelf: 'center'
        },
        formRow: {marginVertical: 7},
        formRowRow: {
            flex: 1,
            alignItems: AlignItems.stretch,
            flexDirection: FlexDirection.row,
            marginHorizontal: -9
        },
        formRowCell: {flex: 1, marginHorizontal: 9},
        border: {
            borderColor: textBorderColor,
            borderWidth: 1,
            borderRadius: 4,
            paddingLeft: 0,
            paddingRight: 8,
            alignItems: AlignItems.stretch,
            justifyContent: JustifyContent.spaceBetween,
        },
        text: {
            backgroundColor: 'transparent',
            fontSize: fontSizeValue,
            fontFamily: FontNames.ProstoLight,
            color: textBorderColor,
        },
        textEdit: {
            backgroundColor: 'transparent',
            fontSize: fontSizeInput,
            fontFamily: FontNames.ProstoLight,
            color: textBorderColor,
            paddingHorizontal: 16,
            justifyContent: JustifyContent.center,
            alignContent: AlignItems.flexStart,
            textAlignVertical: AlignItems.center,
            ...Platform.select({
                ios: {
                    paddingBottom: 2,
                },
                android: {
                    paddingBottom: 0,
                }
            }),
            paddingTop: 0,

        },
        textInput: {
            backgroundColor: 'transparent',
            borderColor: textBorderColor,
            borderWidth: 1,
            borderRadius: 4,
            fontSize: fontSizeInput,
            fontFamily: FontNames.ProstoLight,
            paddingRight: 46,
            paddingLeft: 16,
            textAlignVertical: AlignItems.center,
            color: textBorderColor,
            ...Platform.select({
                ios: {
                    paddingTop: 11,
                    paddingBottom: 14,
                },
                android: {
                    paddingTop: 10,
                    paddingBottom: 11,
                }
            }),
        },

        textInputDescription: {
            backgroundColor: 'transparent',
            borderColor: textBorderColor,
            borderWidth: 1,
            borderRadius: 4,
            height: 94,
            lineHeight: fontSize21,
            fontSize: fontSize16,
            fontFamily: FontNames.ProstoLight,
            paddingHorizontal: 16,
            alignContent: AlignItems.flexStart,
            textAlignVertical: 'top',
            color: textBorderColor
        },
        textInputLocation: {
            backgroundColor: 'transparent',
            paddingLeft: 34
        },
        wholeContainer: {
            flex: 1,
            flexDirection: 'column',
            justifyContent: 'center',
            backgroundColor: sliderTransparentBackground
        },
        menuBackground: {
            backgroundColor: sliderTransparentBackground,
            flex: 1,
            height: dHeight,
            position: Positions.absolute,
            left: 0,
            right: 0,
            bottom: 0,
            top: 0
        },
        menuContainer: {
            borderRadius: 4,
            alignItems: 'center',
            flexDirection: 'column',
            justifyContent: 'center',
            backgroundColor: whiteColor,
            marginHorizontal: 30,
        },
        menuScrollView: {},
        menuCancelContainer: {
            position: Positions.absolute,
            left: 16,
            right: 16,
            bottom: 36,
            backgroundColor: whiteColor,
            borderRadius: 4
        },
        menuItemContainer: {
            flexDirection: FlexDirection.row,
            paddingVertical: fontSize10,
            alignItems: AlignItems.center,
            justifyContent: JustifyContent.flexStart,
        },
        menuText: {
            flex: 1,
            backgroundColor: 'transparent',
            marginLeft: 24,
            marginRight: 24,
            paddingRight: 8,
            color: slideMenuTextColor,
            fontFamily: FontNames.ProstoRegular,
            fontSize: fontSize14,
        },
        menuIcon: {},
        button: {
            alignItems: 'center',
            justifyContent: 'center'
        },
        buttonNonAbsolute: {
            height: 24,
            width: 24,
            alignItems: 'center',
            justifyContent: 'center',
            marginHorizontal: 2
        },
        image: {
            width: 18,
            height: 22
        },

        dropDown: {
            container: {
                flex: 1,
                flexDirection: FlexDirection.row,
                justifyContent: JustifyContent.spaceBetween,
                alignItems: AlignItems.center
            },
            icon: {
                width: 15,
                height: 8
            } 
        },

        more: {
            btn: {
                marginVertical: 7,
                paddingHorizontal: 9,
                flexDirection: FlexDirection.row,
                alignItems: AlignItems.center,
                justifyContent: JustifyContent.flexEnd
            },
            icon: {marginRight: 9, marginLeft: 12},
            iconRight: {marginRight: 9, transform: [{rotate: '-90deg'}], marginLeft: 12},
            text: {
                backgroundColor: 'transparent',
                textAlign: TextAlign.right,
                fontFamily: FontNames.ProstoRegular,
                fontSize: fontSize16,
                color: secondaryColor
            },
            textOpen: {
                backgroundColor: 'transparent',
                textAlign: TextAlign.center,
                fontFamily: FontNames.ProstoRegular,
                fontSize: fontSize18,
                color: secondaryColor
            },
        },

        height: {
            container: {
                position: Positions.relative,
                alignItems: AlignItems.flexStart,
                justifyContent: JustifyContent.center
            },
            label: {
                fontFamily: FontNames.ProstoLight,
                fontSize: fontSize16,
                color: textPlaceholderColor,
                backgroundColor: 'transparent',
            },
            value: {
                backgroundColor: 'transparent',
                position: Positions.absolute,
                fontFamily: FontNames.ProstoRegular,
                fontSize: fontSize12,
                color: textPlaceholderColor
            },
        },
    },

    myWallet: {
        container: {
            flex: 1,
            backgroundColor: whiteColor,
            marginBottom: 116,
            paddingHorizontal: marginHorizontal,
        },
        topContainer: {
            flexDirection: FlexDirection.row,
            width: dWidth,
            height: dInfoItemHeight,
            paddingVertical: dPaddingHorizontal,
            alignItems: AlignItems.center,
            justifyContent: JustifyContent.spaceBetween
        },
        textSubContainer: {
            flexDirection: FlexDirection.row,
            alignItems: AlignItems.center,
            justifyContent: JustifyContent.spaceBetween,
            height: 42,
            borderRadius: 4,
            borderWidth: 1,
            borderColor: textBorderColor,
            marginTop: 8,
            paddingHorizontal: 8
        },
        textsubSubContainer: {
            flex: 1,
            flexDirection: FlexDirection.row,
            alignItems: AlignItems.center,
            justifyContent: JustifyContent.flexEnd,
            flexWrap: 'wrap'
        },
        noHistoryText: {
            backgroundColor: 'transparent',
            color: primaryColor2,
            fontFamily: FontNames.ProstoLight,
            fontSize: fontSize12,
        },
        currencyNameBottom: {
            backgroundColor: 'transparent',
            color: primaryColor2,
            fontFamily: FontNames.ProstoRegular,
            fontSize: fontSize20,
        },
        dropDownIcon: {
            width: 12,
            height: 7,
            tintColor: secondaryColor,
            marginLeft: fontSize4,
        } ,
        currencyValueBottom: {
            backgroundColor: transparentColor,
            color: primaryColor2,
            fontFamily: FontNames.ProstoLight,
            fontSize: fontSize20,
            marginLeft: 8
        },
        bottomContainer: {
            position: Positions.absolute,
            bottom: 0,
            left: 0,
            right: 0,
            height: 60,
            flexDirection: FlexDirection.row,
            alignItems: AlignItems.stretch,
            justifyContent: JustifyContent.center
        },
        buttonLeft: {
            backgroundColor: primaryColor3,
            flexDirection: FlexDirection.row,
            flex: 1,
            alignItems: AlignItems.center,
            justifyContent: JustifyContent.center,
        },
        buttonRight: {
            backgroundColor: primaryColor2,
            flexDirection: FlexDirection.row,
            flex: 1,
            alignItems: AlignItems.center,
            justifyContent: JustifyContent.center

        },
        buttonText: {
            color: whiteColor,
            fontFamily: FontNames.ProstoRegular,
            fontSize: fontSize18,
            marginHorizontal: 10,
            lineHeight: fontSize23,
            ...Platform.select({
                ios: {
                    paddingTop: 4,
                },
                android: {

                }
            })
        },
        buttonImage: {
            width: 30,
            height: 30,
        },
        buttonImage180: {
            width: 30,
            height: 30,
            transform: [{rotate: '180deg'}]
        },
        balanceContainer: {
            flexDirection: FlexDirection.row,
            justifyContent: JustifyContent.spaceBetween,
            alignItems: AlignItems.flexStart,
            paddingTop: 13
        },
        refreshImage: {
            tintColor: whiteColor,
            width: 30,
            height: 30,

        },
        refreshButton: {
            backgroundColor: primaryColor3,
            width: 42,
            height: 42,
            borderRadius: 4,
            justifyContent: JustifyContent.center,
            alignItems: AlignItems.center,
            marginTop: 8
        },

    },

    myContacts: {
        container: {
            backgroundColor: whiteColor,
            flex: 1
        },
        topBackgroundImage: {
            position: Positions.absolute,
            top: 0,
            left: 0,
            right: 0,
            height: 178,
            width: dWidth
        },
        contentContainer: {
            paddingHorizontal: 16,
            flex: 1
        },
        buttonsContainer: {
            position: Positions.absolute,
            left: 36,
            right: 35,
            bottom: 16
        },
        formRow: {marginVertical: 4},

    },

    userPlus: {
        container: {
            backgroundColor: whiteColor,
            flex: 1
        },
        subContainer: {
            paddingHorizontal: 16,
        },
        textInput: {
            backgroundColor: whiteColor,
            borderRadius: 4,
            color: textPlaceholderColor,
            lineHeight: fontSize20,
            fontSize: fontSize16,
            fontFamily: FontNames.ProstoLight,
            paddingHorizontal: 10,
            paddingVertical: 5,
            paddingRight: 50,
            height: 139,
            justifyContent: JustifyContent.flexStart,
            alignContent: AlignItems.flexStart,
            textAlignVertical: 'top',
            borderWidth: 1,
            borderColor: textBorderColor
        },
        button: {
            position: Positions.absolute,
            top: 8,
            right: 8,
            zIndex: 10,
            width: 28,
            height: 30
        },
        image: {
            width: 28,
            height: 30
        },
    },

    sent: {
        container: {
            backgroundColor: whiteColor,
            flex: 1
        },
        dropDown: {
            backgroundColor: whiteColor,
            flexDirection: FlexDirection.row,
            alignItems: AlignItems.center,
            justifyContent: JustifyContent.spaceBetween,
            paddingVertical: 8,
            paddingHorizontal: 8,
            height: 48,
            borderColor: textBorderColor,
            borderWidth: 1,
            borderRadius: 4,
            marginBottom: 8
        },
        invisiableButton: {
            position: Positions.absolute,
            top: 0,
            bottom: 0,
            left: 40,
            right: 0,
        },
        dropDownText: {

            color: textBorderColor,
            lineHeight: fontSize20,
            fontSize: fontSize16,
            fontFamily: FontNames.ProstoRegular,
            justifyContent: JustifyContent.flexStart,
            alignContent: AlignItems.flexStart,
        },
        subContainer: {
            flex: 1,
        },
        imageButton: {
            width: 18,
            height: 18
        },
        textContainer: {
            paddingVertical: 8
        },
        buttonsContainer: {
            flexDirection: FlexDirection.row,
            alignItems: AlignItems.flexStart,
            justifyContent: JustifyContent.spaceBetween
        },
        buttonsSubContainer: {
            flexDirection: FlexDirection.row,
            alignItems: AlignItems.center,
            justifyContent: JustifyContent.flexStart
        },
        textInput: {
            backgroundColor: whiteColor,
            color: textBorderColor,
            fontSize: fontSizeInput,
            fontFamily: FontNames.ProstoRegular,
            paddingLeft: 8,
            paddingRight: 16,
            paddingBottom: 0,
            paddingTop: 0,
            height: 47,
            justifyContent: JustifyContent.flexStart,
            alignContent: AlignItems.flexStart,
            borderColor: textBorderColor,
            borderWidth: 1,
            borderRadius: 4,
            textAlignVertical: AlignItems.center
        },
        textButton: {
            backgroundColor: transparentColor,
            color: textPlaceholderColor,
            lineHeight: fontSize14,
            fontSize: fontSize14,
            fontFamily: FontNames.ProstoRegular,
            marginLeft: 8
        },
        titleMessage: {
            backgroundColor: 'transparent',
            color: primaryColor2,
            fontFamily: FontNames.ProstoLight,
            fontSize: fontSizeLabel,
        },
        valueMessage: {
            backgroundColor: 'transparent',
            color: primaryColor3,
            fontFamily: FontNames.ProstoLight,
            fontSize: fontSize18,
        },
        rowRequisite: { 
            flex: 1, 
            flexDirection: 'row', 
            justifyContent: 'space-between',
            paddingVertical: fontSize7, 
        },
        invoiceRequisite: {
            flexGrow: 1,
            textAlign: 'right',            
            backgroundColor: 'transparent',
            color: primaryColor3,
            fontFamily: FontNames.ProstoLight,
            fontSize: fontSizeValue,
            paddingRight: 8,
        },
        breakWord: {          
            backgroundColor: 'transparent',
            color: primaryColor3,
            fontFamily: FontNames.ProstoLight,
            fontSize: fontSizeValue,
            paddingLeft: fontSize4,
        },
        textCommentInput: {
            paddingTop: 15,
            height: 103,
            textAlignVertical: 'top'
        },
        button: {
            position: Positions.absolute,
            top: 16,
            right: 8,
            bottom: 16
        },
        button1: {
            position: Positions.absolute,
            top: 8,
            right: 0,
            bottom: 8,
            paddingTop: 8,
            paddingRight: 8,
            paddingBottom: 8,

        },
        image: {
            width: 18,
            height: 22,
        },
        image1: {
            width: 18,
            height: 22,
        },
        image2: {
            width: 22,
            height: 22,
        },
        titleContainer: {
            paddingVertical: 12,
            alignItems: AlignItems.flexStart,
            justifyContent: JustifyContent.spaceAround,
        },
        title: {
            backgroundColor: 'transparent',
            color: secondaryColor,
            fontFamily: FontNames.ProstoLight,
            fontSize: fontSize16,
        },
        userContainer: {
            flexDirection: FlexDirection.row,
            alignItems: AlignItems.center,
            justifyContent: JustifyContent.spaceBetween
        },
        iconCheck: {
            tintColor: primaryColorLight,
        },
        copyButton: {
            width: 30,
            height: 30,
        },
        labelContainer: {
            alignSelf: 'center', 
            minWidth: minLabelWidth,
        },
        valueContainer: {
            flex: 1,
            flexDirection: 'row', 
            justifyContent: 'flex-end', 
            alignItems: 'center',
        }
    },

    search: {
        container: {
            backgroundColor: whiteColor,
            flex: 1,
        },

        searchContainer: {
            height: 50,
            paddingHorizontal: 2,
            paddingVertical: 8,
            borderBottomColor: '#B6BFCB',
            borderBottomWidth: 1,
            justifyContent: JustifyContent.center,
        },
        searchTextInput: {
            borderRadius: 5,
            lineHeight: fontSize20,
            fontSize: fontSize16,
            fontFamily: FontNames.ProstoLight,
            paddingHorizontal: 10,
            paddingLeft: 40,
            backgroundColor: whiteColor,
            paddingVertical: 0,
            color: textPlaceholderColor,
            height: 28
        },
        usersListContainer: {
            flex: 1,
        },
        searchIcon: {
            position: Positions.absolute,
            left: 4,
            top: 12,
            bottom: 12,
            width: 24,
            height: 24
        }

    },

    identification: {
        container: {
            backgroundColor: whiteColor,
            flex: 1,
        },
        top: {
            height: dHeight / 2
        },
        bottom: {
            paddingHorizontal: 24,
            justifyContent: JustifyContent.spaceAround,
            flex: 1
        },
        statusIdentificationHead: {
            backgroundColor: 'transparent',
            color: whiteColor,
            fontFamily: FontNames.ProstoLight,
            fontSize: fontSize16,
            letterSpacing: -0.32,
            position: Positions.absolute,
            bottom: 16,
            left: 0,
            right: 0,
            textAlign: AlignItems.center
        },
        textContainer: {
            paddingHorizontal: 38,
            paddingTop: 24
        },
        textSubcontainer: {
            paddingVertical: 16,
            flexDirection: FlexDirection.row,
        },
        statusIdentificationBottomText: {
            backgroundColor: 'transparent',
            color: secondaryColor,
            fontFamily: FontNames.ProstoRegular,
            fontSize: fontSize20,
        },
        buttonContainer: {
            paddingHorizontal: 8
        },
        buttonsLinksContainer: {
            paddingVertical: 24,
            flexDirection: FlexDirection.row,
            justifyContent: JustifyContent.spaceBetween,
            alignItems: AlignItems.center
        },
        cancelText: {
            backgroundColor: 'transparent',
            color: textPlaceholderColor,
            fontFamily: FontNames.ProstoRegular,
            fontSize: fontSize16,
        },
        licenseText: {
            backgroundColor: 'transparent',
            color: primaryColorLight,
            fontFamily: FontNames.ProstoRegular,
            fontSize: fontSize16,
        },
        licenseLineText: {
            backgroundColor: primaryColorLight,
            height: 1
        },
        btnText: {
            backgroundColor: 'transparent',
            fontFamily: FontNames.ProstoRegular,
            fontSize: fontSize16,
            color: whiteColor,
            marginLeft: 14
        },
        iconOk: {
            tintColor: primaryColor2,
            marginLeft: 16,
            width: 25,
            height: 25
        },

    },

    contacts: {
        container: {
            backgroundColor: whiteColor,
            flex: 1,

        },

    },

    userProfile: {
        contentContainer: {
            paddingBottom: 60
        },
        buttonsContainer: {
            paddingHorizontal: 35,
            paddingBottom: 8
        },
    },

    userListItem: {
        container: {
            flexDirection: FlexDirection.row,
            alignItems: AlignItems.center,
            justifyContent: JustifyContent.flexStart,
            paddingVertical: 8
        },
        textContainer: {
            flex: 1,
            marginLeft: 24,
            alignItems: AlignItems.flexStart,
            justifyContent: JustifyContent.spaceAround
        },
        image: {
            width: 50,
            height: 50,
            borderRadius: 25,
        },
        spinner: {
            width: 50,
            height: 50,
            borderRadius: 25,
            backgroundColor: textBorderColor
        },
        name: {
            backgroundColor: 'transparent',
            color: primaryColorLight,
            fontFamily: FontNames.ProstoRegular,
            fontSize: fontSize16,
        },
        id: {
            backgroundColor: 'transparent',
            color: textBorderColor,
            fontFamily: FontNames.ProstoRegular,
            fontSize: fontSize12,
        },

    },

    registerTabs: {
        container: {
            position: Positions.absolute,
            bottom: 0,
            width: dWidth,
            flexDirection: FlexDirection.row,
        },
        tab: {
            paddingVertical: 16,
            alignItems: 'center',
            opacity: 0.62,
            flex: 1
        },
        tabActive: {
            opacity: 1,
        },
        text: {
            backgroundColor: 'transparent',
            color: '#fff',
            fontFamily: FontNames.ProstoRegular,
            fontSize: fontSize16,
        },
    },

    backBtn: {
        container: {
            width: 48,
            height: 48,
            alignItems: AlignItems.center,
            justifyContent: JustifyContent.center,
            top: 25,
            left: fontSize12,
            zIndex: 1,
        },
        icon: {width: 22, height: 22},
    },

    backgroundImage: {
        flex: 1,
        alignSelf: AlignItems.stretch,
        width: null,
        height: null,
        ...debugBorders,
    },

    userImage: {
        container: {
            alignItems: AlignItems.center,
        },
        image: {
            borderColor: secondaryColor,
            borderWidth: 2,
            width: 90,
            height: 90,
            borderRadius: 45
        },
    },
    dots: {
        container: {
            flexDirection: FlexDirection.row,
            alignItems: AlignItems.flexEnd,
            justifyContent: JustifyContent.flexStart,
            paddingHorizontal: 3,
            paddingBottom: 5

        },
        dot: {
            backgroundColor: textBorderColor,
            width: 4,
            height: 4,
            marginHorizontal: 3,
            borderRadius: 2
        },
    },

    userImageSelect: {
        container: {alignItems: AlignItems.center, justifyContent: JustifyContent.flexStart},
        text: {
            backgroundColor: 'transparent',
            lineHeight: fontSize19,
            fontSize: fontSize16,
            fontFamily: FontNames.ProstoRegular,
            color: secondaryColor,
            marginTop: 13
        },
    },

    userDataPanel: {
        container: {
            paddingVertical: dPaddingHorizontal,
            height: 76,
            flexDirection: FlexDirection.row,
            justifyContent: JustifyContent.spaceBetween,
            alignItems: AlignItems.center,
            paddingHorizontal: 24,
        },
        title: {
            backgroundColor: 'transparent',
            color: primaryColor2,
            fontFamily: FontNames.ProstoLight,
            fontSize: fontSize20,
        },
        dataRow: {
            flexDirection: FlexDirection.row,
            alignItems: AlignItems.center,
            justifyContent: JustifyContent.flexStart,
            paddingVertical: 4,
            paddingRight: 24
        },
        image: {
            width: 24,
            height: 24,

        },
        text: {
            backgroundColor: 'transparent',
            fontFamily: FontNames.ProstoLight,
            fontSize: fontSize16,
            color: textBorderColor,
            marginLeft: 16,
            maxWidth: dWidth * 0.8
        },
        account: {
            fontFamily: FontNames.ProstoLight,
            fontSize: fontSize16,
            color: textPlaceholderColor,
            lineHeight: fontSize20
        },
    },

    navigationBar: {
        container: {
            backgroundColor: transparentColor,
            height: dNavigationBarHeight,
            alignItems: AlignItems.stretch,
            justifyContent: JustifyContent.center,
            marginTop: 22,
            paddingBottom: fontSize10,
            width: dWidth
        },
        tip: {
            fontSize: fontSize16,
            fontFamily: FontNames.ProstoRegular,
            color: textBorderColor,
            paddingVertical: 16,
            paddingHorizontal: 16,
            textAlign: 'center',
            textAlignVertical: 'center'
        },
        iconButton: {
            width: 44,
            height: 44,
            resizeMode: 'center',
        },
        navigationHeader: {
            width: dWidth,
            flexDirection: FlexDirection.row,
            alignItems: AlignItems.center,
            justifyContent: JustifyContent.spaceBetween,
        },
        title: {
            backgroundColor: 'transparent',
            fontSize: fontSize18,
            fontFamily: FontNames.ProstoRegular,
            color: whiteColor,
            flex: 2,
            textAlign: 'center'
        }
    },

    menu: {
        spinner: {
            width: 120,
            height: 120,
            borderRadius: 60,
            borderWidth: 2,
            borderColor: whiteColor,
            backgroundColor: textBorderColor
        },
        item: {
            container: {
                height: dMenuItemHeight,
            },

            leftLine: {
                width: 2,
                backgroundColor: secondaryColor,
                position: Positions.absolute,
                top: 0,
                bottom: 0,
                left: 0
            },
            button: {
                height: dMenuItemHeight,
                alignItems: AlignItems.center,
                justifyContent: JustifyContent.flexStart,
                flexDirection: FlexDirection.row,
            },
            icon: {
                marginLeft: dMenuIconLefpPadding,
            },
            title: {
                backgroundColor: 'transparent',
                fontSize: fontSizeMenu,
                fontFamily: FontNames.ProstoLight,
                color: textPlaceholderColor,
                maxWidth: dWidth * .6,
                flexShrink: 1,
                flex: 1,
                paddingLeft: fontSize7,
                ...Platform.select({
                    ios: {
                        paddingTop: 4
                    },
                    android: {}
                })
            }
        },
        container: {
            flex: 1,
            position: Positions.relative,
            backgroundColor: whiteColor,
        },
        textContainer: {
            marginTop: 40,
            alignItems: AlignItems.center,
            justifyContent: JustifyContent.flexStart
        },
        backgroundImage: {
            width: dWidth,
            height: 170,
            marginTop: -20,
        },
        backgroundEmpty: {
            flex: 1,
            backgroundColor: whiteColor,
            alignItems: AlignItems.center,
            justifyContent: JustifyContent.center,
            paddingTop: 20,
        },
        userView: {
            position: Positions.absolute,
            top: 0,
            right: 0,
            left: 0,
            bottom: 0,
            alignItems: AlignItems.center,
            justifyContent: JustifyContent.flexStart,
            paddingTop: 90

        },
        userImage: {
            width: 120,
            height: 120,
            borderRadius: 60,
            borderWidth: 4,
            borderColor: logoColor1,
        },
        userImageLoading: {
            width: 120,
            height: 120,
            borderRadius: 60,
            borderWidth: 4,
            borderColor: whiteColor,
            backgroundColor: textBorderColor
        },
        userName: {
            backgroundColor: 'transparent',
            fontSize: fontSize18,
            fontFamily: FontNames.ProstoRegular,
            color: primaryColor2,
            marginTop: 4,
            textAlign: AlignItems.center,
            paddingHorizontal: 16
        },
        userKey: {
            backgroundColor: 'transparent',
            fontSize: fontSize14,
            fontFamily: FontNames.ProstoRegular,
            color: primaryColorLight,
        },
        userStatus: {
            backgroundColor: 'transparent',
            fontSize: fontSize14,
            fontFamily: FontNames.ProstoRegular,
            color: primaryColorLight,
            marginTop: 4
        },
        userEditButton: {
            position: Positions.absolute,
            top: 8,
            right: 8,
        },
        itemsContainer: {
            backgroundColor: whiteColor,
            flex: 1,
        },
        settingsContainer: {
        },
        settingsLineTop: {},
        settingsIcon: {
            marginLeft: dMenuIconLefpPadding,
        },
        settingsText: {
            marginLeft: dMenuIconLefpPadding,
        },
        line: {
            position: Positions.absolute,
            left: 0,
            right: 0,
            top: 0,
            height: 1,
            backgroundColor: lineLightColor
        }

    },

    infoItem: {
        container: {
            paddingVertical: dPaddingHorizontal,
            alignItems: AlignItems.flexStart,
            justifyContent: JustifyContent.spaceAround,
        },
        title: {
            backgroundColor: 'transparent',
            color: primaryColor2,
            fontFamily: FontNames.ProstoLight,
            fontSize: fontSizeTitle,
            alignSelf: 'stretch',
        },
        value: {
            backgroundColor: 'transparent',
            color: textPlaceholderColor,
            fontFamily: FontNames.ProstoLight,
            fontSize: fontSizeValue,
            flex: 10,
        },
        valueContainer: {
            flexDirection: FlexDirection.row,
            alignItems: AlignItems.center,
            justifyContent: JustifyContent.spaceBetween,
            alignSelf: AlignItems.stretch
        },
        button: {
            width: 30,
            height: 30,
            flex: 1
        },
    },

    contactInfo: {
        container: {
            flexDirection: FlexDirection.row,
            alignItems: AlignItems.center,
            justifyContent: JustifyContent.flexStart,
            paddingHorizontal: 24,
            paddingBottom: 16,
            paddingTop: 16,
        },
        avatar: {
            width: avatarLargeSize,
            height: avatarLargeSize,
            borderRadius: 35,
            borderWidth: 1,
            borderColor: whiteColor,
        },
        spinner: {
            width: 70,
            height: 70,
            borderRadius: 35,
            borderWidth: 1,
            borderColor: whiteColor,
            backgroundColor: textBorderColor
        },
        infoPanel: {
            paddingLeft: 24,
            alignItems: AlignItems.flexStart,
            justifyContent: JustifyContent.center
        },
        nameText: {
            backgroundColor: 'transparent',
            color: whiteColor,
            fontFamily: FontNames.ProstoRegular,
            fontSize: fontSize18,
            width: dWidth - avatarLargeSize - 24 * 2,
            paddingRight: 24
        },
        key: {
            backgroundColor: 'transparent',
            color: whiteColor,
            fontFamily: FontNames.ProstoLight,
            fontSize: fontSize12,
        },
        line: {
            height: 1,
            width: 133,
            backgroundColor: textBorderColor,
            marginVertical: 4
        },
        infoBottomContainer: {
            flexDirection: FlexDirection.row,
            alignItems: AlignItems.center,
            justifyContent: JustifyContent.flexStart
        },
        iconPin: {
            height: 14,
            width: 9.8,
            tintColor: whiteColor,
            marginLeft: 8,
            marginRight: 4
        },
        bottomText: {
            color: whiteColor,
            fontFamily: FontNames.ProstoLight,
            fontSize: fontSize14,
            maxWidth: 100,
            backgroundColor: 'transparent',
        },

    },

    historyItem: {
        container: {
            paddingVertical: 16
        },
        dateSumContainer: {
            flexDirection: FlexDirection.row,
            alignItems: AlignItems.center,
            justifyContent: JustifyContent.spaceBetween,
        },
        image: {
            width: 24,
            height: 24
        },
        image2: {
            width: 24,
            height: 24,
            marginLeft: 8,
        },
        confirmationContainer: {
            flexDirection: FlexDirection.row,
            alignItems: AlignItems.center,
            justifyContent: JustifyContent.flexStart,
        },
        date: {
            flex: 1,
            backgroundColor: 'transparent',
            color: secondaryColor,
            fontFamily: FontNames.ProstoRegular,
            fontSize: fontSizeText,
        },
        confirmation: {
            backgroundColor: 'transparent',
            color: primaryColorLight,
            fontFamily: FontNames.ProstoRegular,
            fontSize: fontSizeText,
            marginTop: 8
        },
        unconfirmation: {
            backgroundColor: 'transparent',
            color: peachColor,
            fontFamily: FontNames.ProstoRegular,
            fontSize: fontSizeText,
            marginTop: 8
        },
        expired: {
            backgroundColor: 'transparent',
            color: primaryColor,
            fontFamily: FontNames.ProstoRegular,
            fontSize: fontSizeText,
            marginTop: 8
        },
        header: {
            backgroundColor: 'transparent',
            color: textPlaceholderColor,
            fontFamily: FontNames.ProstoRegular,
            fontSize: fontSizeText,
            marginTop: 8
        },
        sum: {
            backgroundColor: 'transparent',
            color: primaryColor2,
            fontFamily: FontNames.ProstoRegular,
            fontSize: fontSizeText,
            alignSelf: AlignItems.flexEnd,
        },
        curr: {
            marginLeft: 8,
            backgroundColor: 'transparent',
            color: primaryColor2,
            fontFamily: FontNames.ProstoRegular,
            fontSize: fontSizeText,
            alignSelf: AlignItems.flexEnd,
        },
        currInput: {
            marginRight: fontSize14,
            backgroundColor: 'transparent',
            color: primaryColor2,
            fontFamily: FontNames.ProstoRegular,
            fontSize: fontSizeCurr,
            alignSelf: AlignItems.flexEnd,
        },
        bottomContainer: {
            flexDirection: FlexDirection.row,
            justifyContent: JustifyContent.spaceBetween,
            alignItems: AlignItems.flexEnd,
        },
        bottomLeftContainer: {
            flexDirection: FlexDirection.row,
            justifyContent: JustifyContent.flexStart
        },
        accountLabel: {
            backgroundColor: 'transparent',
            color: primaryColorLight,
            fontFamily: FontNames.ProstoRegular,
            fontSize: fontSizeText,
        },
        account: {
            backgroundColor: 'transparent',
            color: textPlaceholderColor,
            fontFamily: FontNames.ProstoRegular,
            fontSize: fontSizeText,
            marginLeft: 8
        },
    }
};