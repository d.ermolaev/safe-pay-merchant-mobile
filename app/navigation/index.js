import React, { Component } from 'react';
import PropTypes from 'prop-types';
import NavigationService from './NavigationService';
import RootNavigationStack from './RootNavigationStack';

class AppNavigator extends Component {
    
    // gets the current screen from navigation state

    loadState = () => {
        const { startScreen } = this.props;
        return {
            index: 0,
            routes: [
                {key: startScreen, routeName: startScreen},
            ],
        };
    }

    render() {
        return (
            <RootNavigationStack
                { ...this.props }
                ref={navigatorRef => {
                    NavigationService.setTopLevelNavigator(navigatorRef);
                }}
                loadNavigationState={this.loadState}
            />
        );
    }
}

AppNavigator.propTypes = {
    startScreen: PropTypes.string,
};

export default AppNavigator;