import { NavigationActions, StackActions } from 'react-navigation';
import { DrawerActions } from 'react-navigation-drawer';

let _navigator;
let _activeRoute = '';

function setActiveRoute(route) {
    //console.log(`setActiveRoute: ${route}`);
    _activeRoute = route;
}

function getActiveRoute() {
    //console.log(`getActiveRoute: ${_activeRoute}`);
    return _activeRoute;
}

function setTopLevelNavigator(navigatorRef) {
    if (navigatorRef) {
        _navigator = navigatorRef;
    }
}

function navigate(routeName, params) {
    _navigator.dispatch(
        NavigationActions.navigate({
            routeName,
            params,
        })
    );
}

function resetToHome(routeName, params) {
    const resetAction = StackActions.reset({
        index: 0,
        actions: [NavigationActions.navigate({ routeName, params })],
    });
    _navigator.dispatch(resetAction);
}

function reset(routeName, params) {
    const resetAction = NavigationActions.navigate({
        index: 0,
        routeName,
        params,
    });
    _navigator.dispatch(resetAction);
}

function toggle() {
    _navigator.dispatch(
        DrawerActions.toggleDrawer()
    );
}

function showMenu() {
    _navigator.dispatch(
        DrawerActions.openDrawer()
    );
}

function hideMenu() {
    _navigator.dispatch(
        DrawerActions.closeDrawer()
    );
}

function goBack(key) {
    const backAction = NavigationActions.back({
        key: key
    });
    _navigator.dispatch(
        backAction
    );
}

// add other navigation functions that you need and export them

export default {
    navigate,
    reset,
    resetToHome,
    goBack,
    toggle,
    showMenu,
    hideMenu,
    setTopLevelNavigator,
    getActiveRoute,
    setActiveRoute,
};
