import { Component } from 'react';
import { createDrawerNavigator, createStackNavigator } from 'react-navigation';
import { 
    MyWallet,
    Receive, 
    InvoiceView
} from '../screens/MyWallet';
import { Settings, AddServer } from '../screens/Settings';
import { Patterns, EditPattern } from '../screens/Pattern';
import { Database } from '../screens/Database';
import { BankDetails } from '../screens/BankDetails';
import { Menu } from '../screens/Menu';
import { sWidth } from '../config/styles';

const requisiteRoutesConfigs = {
    Requisite: {screen: BankDetails},
};

const RequisiteStackNavigator = createStackNavigator(requisiteRoutesConfigs, {
    headerMode: 'none'
});

const walletRoutesConfigs = {
    MyWallet: {screen: MyWallet},
    Receive: {screen: Receive},
    InvoiceView: {screen: InvoiceView},
};

const WalletStackNavigator = createStackNavigator(walletRoutesConfigs, {
    headerMode: 'none'
});

const SettingsRoutesConfigs = {
    Settings: {screen: Settings},
    AddServer: {screen: AddServer},
};

const SettingsStackNavigator = createStackNavigator(SettingsRoutesConfigs, {
    headerMode: 'none'
});

const PatternRoutesConfigs = {
    Patterns: {screen: Patterns},
    EditPattern: {screen: EditPattern},
};

const PatternStackNavigator = createStackNavigator(PatternRoutesConfigs, {
    headerMode: 'none'
});

export class Main extends Component {

    static navigationOptions = {
        header: false,
        headerLeft: null,
    };

    static screen = createDrawerNavigator(
        {
            Wallet: {screen: WalletStackNavigator},
            Requisite: {screen: RequisiteStackNavigator},
            SettingsStack: {screen: SettingsStackNavigator},
            PatternStack: {screen: PatternStackNavigator},
            Database: {screen: Database},
        }, {
            contentComponent: Menu,
            drawerWidth: sWidth * 0.8,
            drawerPosition: 'left',
            headerMode: 'none',
            drawerLockMode: 'locked-closed',
        }
    );
}
