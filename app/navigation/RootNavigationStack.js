
import { createStackNavigator, createAppContainer } from 'react-navigation';
import {Import} from '../screens/Import';
import {ImportPin} from '../screens/ImportPin';
import {CreateWallet} from '../screens/CreateWallet';
import {License} from '../screens/License';
import {FirstLogin} from '../screens/FirstLogin';
import {Complete} from '../screens/Complete';
import {Login} from '../screens/Login';
import { Main } from './MainNavigationStack';

const rootNavigator = createStackNavigator(
    {
        CreateWallet: {screen: CreateWallet},
        Import: {screen: Import},
        ImportPin: {screen: ImportPin},
        License: {screen: License},
        FirstLogin: {screen: FirstLogin},
        Complete: {screen: Complete},
        Login: {screen: Login},

        Main: Main,
    },
    {
        headerMode: 'none'
    }
);

export default createAppContainer(rootNavigator);