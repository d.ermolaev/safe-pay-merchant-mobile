import * as types from './types';

export function actionListServers() {
    return {
        type: types.GET_LIST_SERVERS,
    };
}

export function actionListServersOk(data) {
    return {
        type: types.GET_LIST_SERVERS_OK,
        payload: data,
    };
}

export function actionListServersErr(error) {
    return {
        type: types.GET_LIST_SERVERS_ERR,
        payload: error,
    };
}

export function actionCurrentServer(id) {
    return {
        type: types.SET_CURRENT_SERVER,
        payload: id,
    };
}

export function actionCurrentServerOk(id) {
    return {
        type: types.SET_CURRENT_SERVER_OK,
        payload: id,
    };
}

export function actionCurrentServerErr(error) {
    return {
        type: types.SET_CURRENT_SERVER_ERR,
        payload: error,
    };
}

export function actionRemoveServer(id) {
    return {
        type: types.REMOVE_SERVER,
        payload: id,
    };
}

export function actionRemoveServerOk(id) {
    return {
        type: types.REMOVE_SERVER_OK,
        payload: id,
    };
}

export function actionRemoveServerErr(error) {
    return {
        type: types.REMOVE_SERVER_ERR,
        payload: error,
    };
}

export function actionAddServer(server) {
    return {
        type: types.ADD_SERVER,
        payload: server,
    };
}

export function actionAddServerOk(server) {
    return {
        type: types.ADD_SERVER_OK,
        payload: server,
    };
}

export function actionAddServerErr(error) {
    return {
        type: types.ADD_SERVER_ERR,
        payload: error,
    };
}

export function actionPingServerReset() {
    return {
        type: types.PING_SERVER_RESET,
    };
}

export function actionPingServer(url, source) {
    return {
        type: types.PING_SERVER,
        payload: url, 
        source
    };
}

export function actionPingServerSuccess(url) {
    return {
        type: types.PING_SERVER_SUCCESS,
        payload: url,
    };
}

export function actionPingServerError(error, url) {
    return {
        type: types.PING_SERVER_ERROR,
        payload: error,
        url
    };
}

export function actionLoadServers() {
    return {
        type: types.LOAD_SERVERS,
    };
}

export function actionLoadServersOk() {
    return {
        type: types.LOAD_SERVERS_OK,
    };
}

export function actionLoadServersErr(error) {
    return {
        type: types.LOAD_SERVERS_ERR,
        payload: error,
    };
}