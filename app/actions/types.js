// Navigation



// DB
export const REDUX_DB_RESET = 'REDUX_DB_RESET';

export const OPEN_DB = 'OPEN_DB';
export const OPEN_DB_SUCCESS = 'OPEN_DB_SUCCESS';
export const OPEN_DB_ERROR = 'OPEN_DB_ERROR';

export const DB_EXISTS = 'DB_EXISTS';
export const CREATE_DB_SUCCESS = 'CREATE_DB_SUCCESS';
export const CREATE_DB_ERROR = 'CREATE_DB_ERROR';

export const CLOSE_DB = 'CLOSE_DB';
export const CLOSE_DB_SUCCESS = 'CLOSE_DB_SUCCESS';
export const CLOSE_DB_ERROR = 'CLOSE_DB_ERROR';

export const BACKUP_DB = 'BACKUP_DB';
export const BACKUP_DB_OK = 'BACKUP_DB_OK';
export const BACKUP_DB_ERR = 'BACKUP_DB_ERR';
export const BACKUP_RESET = 'BACKUP_RESET';

export const RESTORE_DB = 'RESTORE_DB';
export const RESTORE_DB_OK = 'RESTORE_DB_OK';
export const RESTORE_DB_ERR = 'RESTORE_DB_ERR';
export const RESTORE_RESET = 'RESTORE_RESET';

export const PIN_INSERT = 'PIN_INSERT';
export const PIN_INSERT_OK = 'PIN_INSERT_OK';
export const PIN_INSERT_ERR = 'PIN_INSERT_ERR';

export const PIN_UPDATE = 'PIN_UPDATE';
export const PIN_UPDATE_OK = 'PIN_UPDATE_OK';
export const PIN_UPDATE_ERR = 'PIN_UPDATE_ERR';

export const PIN_QUERY = 'PIN_QUERY';
export const PIN_QUERY_OK = 'PIN_QUERY_OK';
export const PIN_QUERY_ERR = 'PIN_QUERY_ERR';

export const WALLET_QUERY = 'WALLET_QUERY';
export const WALLET_QUERY_OK = 'WALLET_QUERY_OK';
export const WALLET_QUERY_ERR = 'WALLET_QUERY_ERR';

export const HEIGHT_SAVE = 'HEIGHT_SAVE';
export const HEIGHT_SAVE_OK = 'HEIGHT_SAVE_OK';
export const HEIGHT_SAVE_ERR = 'HEIGHT_SAVE_ERR';

export const HEIGHT_RESTORE = 'HEIGHT_RESTORE';
export const HEIGHT_RESTORE_OK = 'HEIGHT_RESTORE_OK';
export const HEIGHT_RESTORE_ERR = 'HEIGHT_RESTORE_ERR';

export const PATTERN_INSERT = 'PATTERN_INSERT';
export const PATTERN_INSERT_OK = 'PATTERN_INSERT_OK';
export const PATTERN_INSERT_ERR = 'PATTERN_INSERT_ERR';

export const PATTERN_UPDATE = 'PATTERN_UPDATE';
export const PATTERN_UPDATE_OK = 'PATTERN_UPDATE_OK';
export const PATTERN_UPDATE_ERR = 'PATTERN_UPDATE_ERR';

export const PATTERN_DELETE = 'PATTERN_DELETE';
export const PATTERN_DELETE_OK = 'PATTERN_DELETE_OK';
export const PATTERN_DELETE_ERR = 'PATTERN_DELETE_ERR';

export const PATTERNS_QUERY = 'PATTERNS_QUERY';
export const PATTERNS_QUERY_OK = 'PATTERNS_QUERY_OK';
export const PATTERNS_QUERY_ERR = 'PATTERNS_QUERY_ERR';

// transaction
export const TELEGRAM_REQUEST = 'TELEGRAM_REQUEST';
export const TELEGRAM_OK = 'TELEGRAM_OK';
export const TELEGRAM_FAILED = 'TELEGRAM_FAILED';
export const TELEGRAM_RESET = 'TELEGRAM_RESET';

export const RESTORE_REDUX = 'RESTORE_REDUX';

export const SET_LANGUAGE = 'SET_LANGUAGE';

export const LOAD_SETTINGS = 'LOAD_SETTINGS';
export const LOAD_CURRENCYES = 'LOAD_CURRENCYES';

export const CREATE_WALLET = 'CREATE_WALLET';
export const CREATE_WALLET_OK = 'CREATE_WALLET_OK';
export const CREATE_WALLET_ERROR = 'CREATE_WALLET_ERROR';

export const WALLET_CLEAR = 'WALLET_CLEAR';

export const SET_PIN_CODE = 'SET_PIN_CODE';

export const SET_KEYPAIR = 'SET_KEYPAIR';

export const SET_SEED = 'SET_SEED';

export const SET_ADDRESS = 'SET_ADDRESS';
export const SET_ADDRESS_OK = 'SET_ADDRESS_OK';
export const SET_ADDRESS_ERR = 'SET_ADDRESS_ERR';

export const RESTORE_INVOICES = 'RESTORE_INVOICES';
export const RESTORE_INVOICES_OK = 'RESTORE_INVOICES_OK';
export const RESTORE_INVOICES_ERR = 'RESTORE_INVOICES_ERR';

// SERVERS
export const GET_LIST_SERVERS = 'GET_LIST_SERVERS';
export const GET_LIST_SERVERS_OK = 'GET_LIST_SERVERS_OK';
export const GET_LIST_SERVERS_ERR = 'GET_LIST_SERVERS_ERR';

export const SET_CURRENT_SERVER = 'SET_CURRENT_SERVER';
export const SET_CURRENT_SERVER_OK = 'SET_CURRENT_SERVER_OK';
export const SET_CURRENT_SERVER_ERR = 'SET_CURRENT_SERVER_ERR';

export const ADD_SERVER = 'ADD_SERVER';
export const ADD_SERVER_OK = 'ADD_SERVER_OK';
export const ADD_SERVER_ERR = 'ADD_SERVER_ERR';

export const REMOVE_SERVER = 'REMOVE_SERVER';
export const REMOVE_SERVER_OK = 'REMOVE_SERVER_OK';
export const REMOVE_SERVER_ERR = 'REMOVE_SERVER_ERR';

export const PING_SERVER = 'PING_SERVER';
export const PING_SERVER_SUCCESS = 'PING_SERVER_SUCCESS';
export const PING_SERVER_ERROR = 'PING_SERVER_ERROR';
export const PING_SERVER_RESET = 'PING_SERVER_RESET';

export const LOAD_SERVERS = 'LOAD_SERVERS';
export const LOAD_SERVERS_OK = 'LOAD_SERVERS_OK';
export const LOAD_SERVERS_ERR = 'LOAD_SERVERS_ERR';

// INVOICE
export const GUI_INVOICE_QRCODE = 'GUI_INVOICE_QRCODE';

export const INSERT_INVOICE = 'INSERT_INVOICE';
export const INSERT_INVOICE_OK = 'INSERT_INVOICE_OK';
export const INSERT_INVOICE_ERR = 'INSERT_INVOICE_ERR';

export const UPDATE_INVOICE = 'UPDATE_INVOICE';
export const UPDATE_INVOICE_OK = 'UPDATE_INVOICE_OK';
export const UPDATE_INVOICE_ERR = 'UPDATE_INVOICE_ERR';

export const CHECK_UNIQUE_ORDER = 'CHECK_UNIQUE_ORDER';
export const CHECK_UNIQUE_ORDER_OK = 'CHECK_UNIQUE_ORDER_OK';
export const CHECK_UNIQUE_ORDER_ERR = 'CHECK_UNIQUE_ORDER_ERR';

export const GET_MAX_NUM_ORDER = 'GET_MAX_NUM_ORDER';
export const GET_MAX_NUM_ORDER_OK = 'GET_MAX_NUM_ORDER_OK';
export const GET_MAX_NUM_ORDER_ERR = 'GET_MAX_NUM_ORDER_ERR';

export const GET_INVOICES = 'GET_INVOICES';
export const GET_INVOICES_OK = 'GET_INVOICES_OK';
export const GET_INVOICES_ERR = 'GET_INVOICES_ERR';

export const GET_INVOICES_PAGES = 'GET_INVOICES_PAGES';
export const GET_INVOICES_PAGES_OK = 'GET_INVOICES_PAGES_OK';
export const GET_INVOICES_PAGES_ERR = 'GET_INVOICES_PAGES_ERR';

export const CHECK_EXPIRED = 'CHECK_EXPIRED';
export const CHECK_EXPIRED_OK = 'CHECK_EXPIRED_OK';
export const CHECK_EXPIRED_ERR = 'CHECK_EXPIRED_ERR';


//ERACHAIN
export const GET_HEIGHT_CHAIN = 'GET_HEIGHT_CHAIN';
export const GET_HEIGHT_CHAIN_OK = 'GET_HEIGHT_CHAIN_OK';
export const GET_HEIGHT_CHAIN_ERR = 'GET_HEIGHT_CHAIN_ERR';

export const GET_UNCONFIRMEDCOMES = 'GET_UNCONFIRMEDCOMES';
export const GET_UNCONFIRMEDCOMES_OK = 'GET_UNCONFIRMEDCOMES_OK';
export const GET_UNCONFIRMEDCOMES_ERR = 'GET_UNCONFIRMEDCOMES_ERR';

export const GET_INCOMINGFROMBLOCK = 'GET_INCOMINGFROMBLOCK';
export const GET_INCOMINGFROMBLOCK_OK = 'GET_INCOMINGFROMBLOCK_OK';
export const GET_INCOMINGFROMBLOCK_ERR = 'GET_INCOMINGFROMBLOCK_ERR';

export const START_POLL = 'START_POLL';
export const POLL_OK = 'POLL_OK';
export const POLL_ERR = 'POLL_ERR';

export const SCHEDULE_POLL = 'SCHEDULE_POLL';
export const SCHEDULE_POLL_STOP = 'SCHEDULE_POLL_STOP';
export const SCHEDULE_POLL_OK = 'SCHEDULE_POLL_OK';
export const SCHEDULE_POLL_ERR = 'SCHEDULE_POLL_ERR';

export const CHECK_TRANSACTION = 'CHECK_TRANSACTION';
export const CHECK_TRANSACTION_OK = 'CHECK_TRANSACTION_OK';
export const CHECK_TRANSACTION_ERR = 'CHECK_TRANSACTION_ERR';

export const FIND_INVOICE = 'FIND_INVOICE';
export const FIND_INVOICE_OK = 'FIND_INVOICE_OK';
export const FIND_INVOICE_ERR = 'FIND_INVOICE_ERR';

export const GET_PARAM = 'GET_PARAM';
export const GET_PARAM_OK = 'GET_PARAM_OK';
export const GET_PARAM_ERR = 'GET_PARAM_ERR';

export const SET_PARAM = 'SET_PARAM';
export const SET_PARAM_OK = 'SET_PARAM_OK';
export const SET_PARAM_ERR = 'SET_PARAM_ERR';

// Requisite

export const ADD_REQUISITE = 'ADD_REQUISITE';
export const ADD_REQUISITE_OK = 'ADD_REQUISITE_OK';
export const ADD_REQUISITE_ERR = 'ADD_REQUISITE_ERR';

export const UPDATE_REQUISITE = 'UPDATE_REQUISITE';
export const UPDATE_REQUISITE_OK = 'UPDATE_REQUISITE_OK';
export const UPDATE_REQUISITE_ERR = 'UPDATE_REQUISITE_ERR';

export const GET_REQUISITE = 'GET_REQUISITE';
export const GET_REQUISITE_OK = 'GET_REQUISITE_OK';
export const GET_REQUISITE_ERR = 'GET_REQUISITE_ERR';