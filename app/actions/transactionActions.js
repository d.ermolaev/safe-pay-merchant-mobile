import * as types from './types';

export function actionCheckTransaction(transaction, newStatus) {
    return {
        type: types.CHECK_TRANSACTION,
        payload: transaction,
        newStatus
    };
}

export function actionCheckTransactionOk() {
    return {
        type: types.CHECK_TRANSACTION_OK,
    };
}

export function actionCheckTransactionErr(error) {
    return {
        type: types.CHECK_TRANSACTION_ERR,
        payload: error,
    };
}

export function actionFindInvoice({ address, recipient, signature, sum, curr }) {
    return {
        type: types.FIND_INVOICE,
        payload: { address, recipient, signature, sum, curr },
    };
}

export function actionFindInvoiceOk(row) {
    return {
        type: types.FIND_INVOICE_OK,
        payload: row,
    };
}

export function actionFindInvoiceErr(error) {
    return {
        type: types.FIND_INVOICE_ERR,
        payload: error,
    };
}