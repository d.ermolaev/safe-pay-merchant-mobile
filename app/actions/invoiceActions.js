import * as types from './types';

export function actionQRCode(show) {
    return {
        type: types.GUI_INVOICE_QRCODE,
        payload: show,
    };
}

export function actionCheckExpired() {
    return {
        type: types.CHECK_EXPIRED,
    };
}

export function actionCheckExpiredOk() {
    return {
        type: types.CHECK_EXPIRED_OK,
    };
}

export function actionCheckExpiredErr(error) {
    return {
        type: types.CHECK_EXPIRED_ERR,
        payload: error,
    };
}

export function actionRestoreInvoices(fromTimestamp) {
    return {
        type: types.RESTORE_INVOICES,
        payload: fromTimestamp,
    };
}

export function actionRestoreInvoicesOk(count) {
    return {
        type: types.RESTORE_INVOICES_OK,
        payload: count,
    };
}

export function actionRestoreInvoicesErr(error) {
    return {
        type: types.RESTORE_INVOICES_ERR,
        payload: error,
    };
}