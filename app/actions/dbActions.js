import * as types from './types';

export function actionReduxReset() {
    return {
        type: types.REDUX_DB_RESET,
    };
}

export function actionOpenDB() {
    return {
        type: types.OPEN_DB,
    };
}

export function actionOpenDBSuccess() {
    return {
        type: types.OPEN_DB_SUCCESS,
    };
}

export function actionOpenDBError(error) {
    return {
        type: types.OPEN_DB_ERROR,
        payload: error,
    };
}

export function actionDBExists() {
    return {
        type: types.DB_EXISTS,
    };
}

export function actionCreateDBSuccess() {
    return {
        type: types.CREATE_DB_SUCCESS,
    };
}

export function actionCreateDBError(error) {
    return {
        type: types.CREATE_DB_ERROR,
        payload: error,
    };
}

export function actionCloseDB() {
    return {
        type: types.CLOSE_DB,
    };
}

export function actionCloseDBSuccess() {
    return {
        type: types.CLOSE_DB_SUCCESS,
    };
}

export function actionCloseDBError(error) {
    return {
        type: types.CLOSE_DB_ERROR,
        payload: error,
    };
}

export function actionInsertInvoice(params) {
    return {
        type: types.INSERT_INVOICE,
        payload: params,
    };
}

export function actionInsertInvoiceOk(invoice) {
    return {
        type: types.INSERT_INVOICE_OK,
        payload: invoice,
    };
}

export function actionInsertInvoiceErr(error) {
    return {
        type: types.INSERT_INVOICE_ERR,
        payload: error,
    };
}

export function actionUpdateInvoice(params) {
    return {
        type: types.UPDATE_INVOICE,
        payload: params,
    };
}

export function actionUpdateInvoiceOk(id) {
    return {
        type: types.UPDATE_INVOICE_OK,
        payload: id,
    };
}

export function actionUpdateInvoiceErr(error) {
    return {
        type: types.UPDATE_INVOICE_ERR,
        payload: error,
    };
}

export function actionUnique(text) {
    return {
        type: types.CHECK_UNIQUE_ORDER,
        payload: text,
    };
}

export function actionUniqueOk() {
    return {
        type: types.CHECK_UNIQUE_ORDER_OK,
    };
}

export function actionUniqueErr() {
    return {
        type: types.CHECK_UNIQUE_ORDER_ERR,
    };
}

export function actionMaxOrder() {
    return {
        type: types.GET_MAX_NUM_ORDER,
    };
}

export function actionMaxOrderOk(num) {
    return {
        type: types.GET_MAX_NUM_ORDER_OK,
        payload: num,
    };
}

export function actionMaxOrderErr(error) {
    return {
        type: types.GET_MAX_NUM_ORDER_ERR,
        payload: error,
    };
}

export function actionInvoices(page) {
    return {
        type: types.GET_INVOICES,
        payload: page,
    };
}

export function actionInvoicesOk(data) {
    return {
        type: types.GET_INVOICES_OK,
        payload: data,
    };
}

export function actionInvoicesErr(error) {
    return {
        type: types.GET_INVOICES_ERR,
        payload: error,
    };
}

export function actionInvoicesPages() {
    return {
        type: types.GET_INVOICES_PAGES,
    };
}

export function actionInvoicesPagesOk(pages) {
    return {
        type: types.GET_INVOICES_PAGES_OK,
        payload: pages,
    };
}

export function actionInvoicesPagesErr(error) {
    return {
        type: types.GET_INVOICES_PAGES_ERR,
        payload: error,
    };
}

export function actionPinInsert(hash) {
    return {
        type: types.PIN_INSERT,
        payload: hash
    };
}

export function actionPinInsertOk(pin, hash) {
    return {
        type: types.PIN_INSERT_OK,
        payload: hash,
        pin
    };
}

export function actionPinInsertErr(error) {
    return {
        type: types.PIN_INSERT_ERR,
        payload: error,
    };
}

export function actionPinUpdate(hash) {
    return {
        type: types.PIN_UPDATE,
        payload: hash,
    };
}

export function actionPinUpdateOk(pin, hash) {
    return {
        type: types.PIN_UPDATE_OK,
        payload: hash,
        pin
    };
}

export function actionPinUpdateErr(error) {
    return {
        type: types.PIN_UPDATE_ERR,
        payload: error,
    };
}

export function actionPinQuery() {
    return {
        type: types.PIN_QUERY,
    };
}

export function actionPinQueryOk(pin, hash) {
    return {
        type: types.PIN_QUERY_OK,
        payload: hash,
        pin
    };
}

export function actionPinQueryErr(error) {
    return {
        type: types.PIN_QUERY_ERR,
        payload: error,
    };
}

export function actionWalletQuery(pin) {
    return {
        type: types.WALLET_QUERY,
        payload: pin,
    };
}

export function actionWalletQueryOk(wallet) {
    return {
        type: types.WALLET_QUERY_OK,
        payload: wallet,
    };
}

export function actionWalletQueryErr(error) {
    return {
        type: types.WALLET_QUERY_ERR,
        payload: error,
    };
}

export function actionHeightSave(height) {
    return {
        type: types.HEIGHT_SAVE,
        payload: height,
    };
}

export function actionHeightSaveOk() {
    return {
        type: types.HEIGHT_SAVE_OK,
    };
}

export function actionHeightSaveErr(error) {
    return {
        type: types.HEIGHT_SAVE_ERR,
        payload: error,
    };
}

export function actionHeightRestore() {
    return {
        type: types.HEIGHT_RESTORE,
    };
}

export function actionHeightRestoreOk(height) {
    return {
        type: types.HEIGHT_RESTORE_OK,
        payload: height,
    };
}

export function actionHeightRestoreErr(error) {
    return {
        type: types.HEIGHT_RESTORE_ERR,
        payload: error,
    };
}

export function actionPatternInsert(pattern) {
    return {
        type: types.PATTERN_INSERT,
        payload: pattern,
    };
}

export function actionPatternInsertOk(pattern) {
    return {
        type: types.PATTERN_INSERT_OK,
        payload: pattern,
    };
}

export function actionPatternInsertErr(error) {
    return {
        type: types.PATTERN_INSERT_ERR,
        payload: error,
    };
}

export function actionPatternUpdate(pattern) {
    return {
        type: types.PATTERN_UPDATE,
        payload: pattern,
    };
}

export function actionPatternUpdateOk(pattern) {
    return {
        type: types.PATTERN_UPDATE_OK,
        payload: pattern,
    };
}

export function actionPatternUpdateErr(error) {
    return {
        type: types.PATTERN_UPDATE_ERR,
        payload: error,
    };
}

export function actionPatternDelete(pattern) {
    return {
        type: types.PATTERN_DELETE,
        payload: pattern,
    };
}

export function actionPatternDeleteOk(pattern) {
    return {
        type: types.PATTERN_DELETE_OK,
        payload: pattern,
    };
}

export function actionPatternDeleteErr(error) {
    return {
        type: types.PATTERN_DELETE_ERR,
        payload: error,
    };
}

export function actionPatternsQuery() {
    return {
        type: types.PATTERNS_QUERY,
    };
}

export function actionPatternsQueryOk(data) {
    return {
        type: types.PATTERNS_QUERY_OK,
        payload: data,
    };
}

export function actionPatternsQueryErr(error) {
    return {
        type: types.PATTERNS_QUERY_ERR,
        payload: error,
    };
}

export function actionGetParam(name) {
    return {
        type: types.GET_PARAM,
        payload: name,
    };
}

export function actionGetParamOk(value) {
    return {
        type: types.GET_PARAM_OK,
        payload: value,
    };
}

export function actionGetParamErr(error, name) {
    return {
        type: types.GET_PARAM_ERR,
        payload: error,
        name
    };
}

export function actionSetParam(param) {
    return {
        type: types.SET_PARAM,
        payload: param,
    };
}

export function actionSetParamOk(param) {
    return {
        type: types.SET_PARAM_OK,
        payload: param,
    };
}

export function actionSetParamErr(error) {
    return {
        type: types.SET_PARAM_ERR,
        payload: error,
    };
}

export function actionBackupReset() {
    return {
        type: types.BACKUP_RESET,
    };
}

export function actionBackup(filename) {
    return {
        type: types.BACKUP_DB,
        payload: filename,
    };
}

export function actionBackupOk(path) {
    return {
        type: types.BACKUP_DB_OK,
        payload: path,
    };
}

export function actionBackupErr(error) {
    return {
        type: types.BACKUP_DB_ERR,
        payload: error,
    };
}

export function actionRestoreReset() {
    return {
        type: types.RESTORE_RESET,
    };
}

export function actionRestore(filename) {
    return {
        type: types.RESTORE_DB,
        payload: filename,
    };
}

export function actionRestoreOk() {
    return {
        type: types.RESTORE_DB_OK,
    };
}

export function actionRestoreErr(error) {
    return {
        type: types.RESTORE_DB_ERR,
        payload: error,
    };
}

export function actionAddRequisite(row) {
    return {
        type: types.ADD_REQUISITE,
        payload: row,
    };
}

export function actionAddRequisiteOk(row) {
    return {
        type: types.ADD_REQUISITE_OK,
        payload: row,
    };
}

export function actionAddRequisiteErr(error) {
    return {
        type: types.ADD_REQUISITE_ERR,
        payload: error,
    };
}

export function actionUpdateRequisite(row) {
    return {
        type: types.UPDATE_REQUISITE,
        payload: row,
    };
}

export function actionUpdateRequisiteOk(row) {
    return {
        type: types.UPDATE_REQUISITE_OK,
        payload: row,
    };
}

export function actionUpdateRequisiteErr(error) {
    return {
        type: types.UPDATE_REQUISITE_ERR,
        payload: error,
    };
}

export function actionGetRequisite(row) {
    return {
        type: types.GET_REQUISITE,
        payload: row,
    };
}

export function actionGetRequisiteOk(row) {
    return {
        type: types.GET_REQUISITE_OK,
        payload: row,
    };
}

export function actionGetRequisiteErr(error) {
    return {
        type: types.GET_REQUISITE_ERR,
        payload: error,
    };
}