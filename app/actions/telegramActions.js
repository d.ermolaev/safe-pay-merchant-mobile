/*
 * Reducer actions related with login
 */
import * as types from './types';

export function actionTelegram(transaction, invoice) {
    return {
        type: types.TELEGRAM_REQUEST,
        payload: transaction,
        invoice,
    };
}

export function actionTelegramFailed(error) {
    return {
        type: types.TELEGRAM_FAILED,
        payload: error,
    };
}

export function actionTelegramOk(response) {
    return {
        type: types.TELEGRAM_OK,
        payload: response,
    };
}

export function actionTelegramReset() {
    return {
        type: types.TELEGRAM_RESET,
    };
}
