import * as types from './types';

export function actionStartPoll() {
    return {
        type: types.START_POLL,
    };
}

export function actionPollOk() {
    return {
        type: types.POLL_OK,
    };
}

export function actionPollErr(error) {
    return {
        type: types.POLL_ERR,
        payload: error,
    };
}

export function actionSchedulePoll() {
    return {
        type: types.SCHEDULE_POLL,
    };
}

export function actionSchedulePollStop() {
    return {
        type: types.SCHEDULE_POLL_STOP,
    };
}

export function actionSchedulePollOk() {
    return {
        type: types.SCHEDULE_POLL_OK,
    };
}

export function actionSchedulePollErr(error) {
    return {
        type: types.SCHEDULE_POLL_ERR,
        payload: error,
    };
}