// export action creators
import * as loginActions from './loginActions';
import * as navigationActions from './navigationActions';
import * as telegramActions from './telegramActions';
import * as configActions from './configActions';
import * as walletActions from './walletActions';
import * as serversActions from './serversActions';
import * as invoiceActions from './invoiceActions';
import * as dbActions from './dbActions';
import * as blockchainActions from './blockchainActions';
import * as pollActions from './pollActions';
import * as transactionActions from './transactionActions';

export const ActionCreators = Object.assign(
    {},
    configActions,
    loginActions,
    navigationActions,
    telegramActions,
    walletActions,
    serversActions,
    invoiceActions,
    dbActions,
    blockchainActions,
    pollActions,
    transactionActions
);
