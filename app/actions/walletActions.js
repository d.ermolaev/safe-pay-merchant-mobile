import * as types from './types';


export function actionSetPinCode(pin) {
    return {
        type: types.SET_PIN_CODE,
        payload: pin,
    };
}

export function actionSetKeyPair(keyPair) {
    return {
        type: types.SET_KEYPAIR,
        payload: keyPair,
    };
}

export function actionSetSeed(seed) {
    return {
        type: types.SET_SEED,
        payload: seed,
    };
}

export function actionSetAddress(address) {
    return {
        type: types.SET_ADDRESS,
        payload: address,
    };
}

export function actionSetAddressOk(address) {
    return {
        type: types.SET_ADDRESS_OK,
        payload: address,
    };
}

export function actionSetAddressErr(error) {
    return {
        type: types.SET_ADDRESS_ERR,
        payload: error,
    };
}

export function actionCreateWallet() {
    return {
        type: types.CREATE_WALLET,
    };
}

export function actionCreateWalletOk() {
    return {
        type: types.CREATE_WALLET_OK,
    };
}

export function actionCreateWalletError(error) {
    return {
        type: types.CREATE_WALLET_ERROR,
        payload: error,
    };
}

export function actionWalletClear() {
    return {
        type: types.WALLET_CLEAR,
    };
}