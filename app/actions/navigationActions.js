/*
 * Reducer actions related with navigation
 */
import NavigationService from 'app/navigation/NavigationService';

export function navigateToHome() {
    NavigationService.resetToHome('Main');
}

export function reset(key, params) {
    NavigationService.reset(key, params);
}

export function navigate(key, params) {
    NavigationService.navigate(key, params);
}

export function back() {
    NavigationService.goBack();
}

export function toggle() {
    NavigationService.toggle();
}

export function hideMenu() {
    NavigationService.hideMenu();
}

export function showMenu() {
    NavigationService.showMenu();
}

export function setActiveRoute(routeName) {
    NavigationService.setActiveRoute(routeName);
}

export function getActiveRoute() {
    return NavigationService.getActiveRoute();
}