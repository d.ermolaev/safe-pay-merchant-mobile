import * as types from './types';

export function actionLoadSettings(settings) {
    return {
        type: types.LOAD_SETTINGS,
        payload: settings,
    };
}

export function actionLoadCurrencies(currencies) {
    return {
        type: types.LOAD_CURRENCYES,
        payload: currencies,
    };
}

export function actionRestoreRedux(all) {
    return {
        type: types.RESTORE_REDUX,
        payload: all,
    };
}

export function actionSetLanguage(lang) {
    return {
        type: types.SET_LANGUAGE,
        payload: lang,
    };
}