import * as types from './types';

export function actionGetHeightChain() {
    return {
        type: types.GET_HEIGHT_CHAIN,
    };
}

export function actionGetHeightChainOK(numBlock) {
    return {
        type: types.GET_HEIGHT_CHAIN_OK,
        payload: numBlock,
    };
}

export function actionGetHeightChainErr(error) {
    return {
        type: types.GET_HEIGHT_CHAIN_ERR,
        payload: error,
    };
}

export function actionGetUnconfirmed({ address, type, timestamp }) {
    return {
        type: types.GET_UNCONFIRMEDCOMES,
        payload: { address, type, timestamp },
    };
}

export function actionGetUnconfirmedOk(response) {
    return {
        type: types.GET_UNCONFIRMEDCOMES_OK,
        payload: response,
    };
}

export function actionGetUnconfirmedErr(error) {
    return {
        type: types.GET_UNCONFIRMEDCOMES_ERR,
        payload: error,
    };
}

export function actionGetIncoming({ address, type }) {
    return {
        type: types.GET_INCOMINGFROMBLOCK,
        payload: { address, type },
    };
}

export function actionGetIncomingOk(response) {
    return {
        type: types.GET_INCOMINGFROMBLOCK_OK,
        payload: response,
    };
}

export function actionGetIncomingErr(error) {
    return {
        type: types.GET_INCOMINGFROMBLOCK_ERR,
        payload: error,
    };
}
