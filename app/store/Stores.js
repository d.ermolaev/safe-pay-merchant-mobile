import {navigationStore} from './NavigationStore';

class Stores {
    navigationStore = navigationStore;
}

const stores = new Stores();

export default stores;
