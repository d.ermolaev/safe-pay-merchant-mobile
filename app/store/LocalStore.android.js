//import { AES } from 'crypto-js';
import * as CryptoJS from 'crypto-js';
import RNFetchBlob from 'react-native-fetch-blob';
import { Bytes } from '../core/Bytes';

//const path = RNFetchBlob.fs.dirs.DownloadDir;

//console.log(RNFetchBlob.fs);

export default class LocalStore {

    static backup(filename, data) {
        //console.log(data);
        return new Promise((resolve, reject) => {
            try {
                //const aes = AES.encrypt(, pin).toString();
                Bytes.stringToByteArray(JSON.stringify(data))
                    .then(message => {
                        const bytes = Array.from(message);
                        const fn = `${RNFetchBlob.fs.dirs.DownloadDir}/${filename}.dat`;
                        RNFetchBlob.fs.writeFile(fn, bytes, 'ascii')
                            .then(() => {
                                resolve(fn);
                            })
                            .catch(e => reject(e));
                    })
                    .catch(e => reject(e));
            } catch (e) {
                reject(e);
            }
        });
    }

    static restore(filename) {
        return new Promise(async (resolve, reject) => {
            const fn = `${RNFetchBlob.fs.dirs.DownloadDir}/${filename}.dat`;
            try {
                //console.log(fn);
                const data = await RNFetchBlob.fs.readFile(fn, 'ascii');
                const message = await Bytes.stringFromByteArray(data);
                //const enc = AES.decrypt(message, pin);
                const json = message.toString(CryptoJS.enc.Utf8);
                //console.log(JSON.parse(json));
                resolve(JSON.parse(json));
            } catch (e) {
                reject(e);
            }
        });
    }

}

