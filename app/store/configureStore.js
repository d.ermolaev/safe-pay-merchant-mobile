import { createStore, compose, applyMiddleware } from 'redux';
import { fromJS } from 'immutable';
import { createLogger } from 'redux-logger';
import createSagaMiddleware from 'redux-saga';
// enable Reactotron
//import Reactotron from '../../ReactotronConfig';

import createReducer from 'app/reducers'; // where reducers is a object of reducers
import sagas from 'app/sagas';

// enable Reactotron
//const sagaMonitor = Reactotron.createSagaMonitor();

const middleware = [];

// enable Reactotron
//const sagaMiddleware = createSagaMiddleware({sagaMonitor});
const sagaMiddleware = createSagaMiddleware();

middleware.push(sagaMiddleware);

if (__DEV__) {
    middleware.push(createLogger());
}
const initialState = fromJS({});

const reducers = createReducer();
const enhancers = [applyMiddleware(...middleware)];

// enable Reactotron
//const store = createStore(reducers, initialState, compose(...enhancers, Reactotron.createEnhancer()));
const store = createStore(reducers, initialState, compose(...enhancers ));

const configureStore = () => {
    return store;
};

sagaMiddleware.run(sagas);

export default configureStore;


