//import { AES } from 'crypto-js';
import { NativeModules } from 'react-native';
import * as CryptoJS from 'crypto-js';
import RNFetchBlob from 'react-native-fetch-blob';
import { Bytes } from '../core/Bytes';
import { checkError } from '../core/request/checkError';


const Documents = NativeModules.ICloudDocuments;

const path = RNFetchBlob.fs.dirs.DocumentDir;

//console.log(Documents);
Documents.getICloudToken((err, token)=>{
    if (err) {
        LocalStore.tokenICloud = null;
        LocalStore.error = checkError(err);
    }
    if (token) {
        LocalStore.tokenICloud = token;
    }
});

export default class LocalStore {

    static tokenICloud = null;
    static error = null;

    static getICloudToken = () => {
        Documents.getICloudToken((err, token)=>{
            if (err) {
                LocalStore.tokenICloud = null;
                LocalStore.error = err;
            }
            if (token) {
                LocalStore.tokenICloud = token;
            }
        });
    }

    static backup(filename, data) {
        //console.log(data);
        return new Promise((resolve, reject) => {
            try {
                //const aes = AES.encrypt(, pin).toString();
                Bytes.stringToByteArray(JSON.stringify(data))
                    .then(message => {
                        const bytes = Array.from(message);
                        const fn = `${path}/${filename}.dat`;
                        RNFetchBlob.fs.writeFile(fn, bytes, 'ascii')
                            .then(() => {
                                Documents.replaceFileToICloud(fn, (err, resultURL)=>{
                                    if (err) {
                                        //console.log(err);
                                        reject(err);
                                        return;
                                    } 
                                    //console.log(resultURL);
                                    if (resultURL.isUploaded) {
                                        //console.log(resultURL);
                                        resolve(resultURL.path);
                                    }
                                });
                            })
                            .catch(e => reject(e));
                    })
                    .catch(e => reject(e));
            } catch (e) {
                reject(e);
            }
        });
    }

    static restore(filename) {
        return new Promise(async (resolve, reject) => {
            Documents.iCloudDocumentPath((err, iCloudPath) => {
                if (err) {
                    reject(err);
                }
                if (iCloudPath) {
                    const fn = `${iCloudPath}/${filename}.dat`;
                    Documents.replaceFileFromICloud(fn, async (err, resultURL) => {
                        if (err) {
                            //console.log(err);
                            reject(err);
                        }
                        try {
                            //console.log(fn);
                            ///private/var/mobile/Library/Mobile Documents/iCloud~ru~safepay/Documents/Qwerty.dat
                            ///private/var/mobile/Library/Mobile Documents/iCloud~ru~safepay/Documents/Qwerty.dat
                            ///private/var/mobile/Library/Mobile Documents/iCloud~ru~safepay/Documents/safepay.dat
                            const localPath = resultURL.replace('file://', '');
                            //console.log(localPath);                            
                            const data = await RNFetchBlob.fs.readFile(localPath, 'ascii');
                            const message = await Bytes.stringFromByteArray(data);
                            //const enc = AES.decrypt(message, pin);
                            const json = message.toString(CryptoJS.enc.Utf8);
                            //console.log(JSON.parse(json));
                            resolve(JSON.parse(json));
                        } catch (e) {
                            reject(e);
                        }
                    });
                }
            });
        });
    }

}

