import Realm from 'realm';
import * as schema from './Schema';
import { AES } from 'crypto-js';
import * as CryptoJS from 'crypto-js';

const pageSize = 5;

export default class DB {

    static db;

    static open = () => {
        return new Promise((resolve, reject) => {
            //console.log(Object.values(schema.Meta));
            Realm.open({schema: Object.values(schema.Meta)})
                .then(realm => {
                    //console.log(realm);
                    DB.db = realm;
                    //console.log('REALM path:', realm.path);
                    resolve();
                })
                .catch(e => {
                    reject(e);
                });
        });
    };

    static close = () => {
        return new Promise((resolve) => {
            resolve();
        });
    };

    static create = () => {
        return new Promise((resolve) => {
            resolve();
        });
    };

    static check = () => {
        return new Promise((resolve, reject) => {
            if (DB.db) {
                resolve();
            } else {
                reject(new Error('Local DB not opened'));
            }
        });
    };

    static queryInvoices = (page) => {
        return new Promise((resolve, reject) => {
            try {
                const list = [];
                const invoices = DB.db.objects('Invoices').sorted('lastdt', true);
                const start = (page - 1) * pageSize;
                const end = start + pageSize;
                for (let object of invoices.slice(start,end).values()) {
                    const row = schema.fromRealm('Invoices', object);
                    //console.log(row);
                    list.push(row);
                }
                resolve(list);
            } catch (e) {
                reject(e);
            }
        });
    };

    static invoicePages = () => {
        return new Promise((resolve, reject) => {
            try {
                const invoices = DB.db.objects('Invoices').sorted('lastdt', true);
                let pages = invoices.length / pageSize;
                const remain = invoices.length % pageSize;
                if (remain > 0) {
                    pages += 1;
                }
                resolve(pages);
            } catch (e) {
                reject(e);
            }
        });
    };

    static insertInvoice = (params) => {
        return new Promise((resolve, reject) => {
            try {
                DB.db.write(() => {
                    // handle the id
                    let id = DB.db.objects('Invoices').max('id');
                    let invoice = schema.getInvoice(params);
                    invoice.id = id === undefined ? 1 : ++id;
                    DB.db.create('Invoices', invoice);
                    resolve(schema.fromRealm('Invoices', invoice));
                });
            } catch (e) {
                reject(e);
            }
        });
    };

    static updateInvoice = (params) => {
        return new Promise((resolve, reject) => {
            try {
                DB.db.write(() => {
                    // handle the id                    
                    let invoices = DB.db.objects('Invoices').filtered('id = $0', params.id);
                    invoices[0].status = params.status;
                    invoices[0].lastdt = params.lastdt;
                    invoices[0].confirmations = params.confirmations;
                    invoices[0].confirmSignature = params.confirmSignature;
                    invoices[0].attempts += 1;
                    resolve(schema.fromRealm('Invoices', invoices[0]));
                });
            } catch (e) {
                reject(e);
            }
        });
    };

    static uniqueNum = (numStr) => {
        return new Promise((resolve, reject) => {
            try {
                DB.db.write(() => {                   
                    let invoices = DB.db.objects('Invoices').filtered('numOrder = $0', Number(numStr));
                    if (invoices.length !== 0) {
                        reject('Not unique');
                    }
                    resolve();
                });
            } catch (e) {
                reject(e);
            }
        });
    }

    static maxNum = () => {
        return new Promise((resolve, reject) => {
            try {
                DB.db.write(() => {                   
                    let num = DB.db.objects('Invoices').max('numOrder');
                    if (num === undefined) {
                        num = 0;
                    }
                    resolve(num);
                });
            } catch (e) {
                reject(e);
            }
        });
    }

    static queryAddresses = (maxStatus) => {
        return new Promise((resolve, reject) => {
            try {
                //console.log(DB.db.objects('Invoices').filtered('status <= $0', maxStatus));
                const invoices = DB.db.objects('Invoices').filtered('status <= $0', maxStatus).sorted('dt');
                let addresses = [];
                let results = [];
                for (let i = 0; i < invoices.length; i++) {
                    const invoice = invoices[i];
                    //console.log({ invoice });
                    if (!addresses.includes(invoice.address)) {
                        results.push({ address: invoice.address, dt: invoice.dt });
                    }
                }
                resolve(results);
            } catch (e) {
                reject(e);
            }
        });
    };

    static findInvoice = ({ address, recipient, signature, sum, curr }) => {
        return new Promise((resolve, reject) => {
            try {
                const invoices = DB.db.objects('Invoices').filtered('address = $0 AND recipient = $1 AND signature = $2 AND sum = $3 AND curr = $4 AND status < 2', address, recipient, signature, sum, curr);
                if (invoices.length > 0) {
                    resolve(schema.fromRealm('Invoices', invoices[0]));
                } else {
                    throw new Error('Not found');
                }
            } catch (e) {
                reject(e);
            }
        });
    };

    static existsInvoice = ({ address, recipient, signature, sum, curr }) => {
        return new Promise((resolve, reject) => {
            try {
                const invoices = DB.db.objects('Invoices').filtered('address = $0 AND recipient = $1 AND signature = $2 AND sum = $3 AND curr = $4 AND status < 2', address, recipient, signature, sum, curr);
                if (invoices.length > 0) {
                    resolve(schema.fromRealm('Invoices', invoices[0]));
                } else {
                    resolve(false);
                }
            } catch (e) {
                reject(e);
            }
        });
    };

    static existsInvoiceAny = ({ address, recipient, signature, sum, curr }) => {
        return new Promise((resolve, reject) => {
            try {
                const invoices = DB.db.objects('Invoices').filtered('address = $0 AND recipient = $1 AND signature = $2 AND sum = $3 AND curr = $4', address, recipient, signature, sum, curr);
                if (invoices.length > 0) {
                    resolve(schema.fromRealm('Invoices', invoices[0]));
                } else {
                    resolve(false);
                }
            } catch (e) {
                reject(e);
            }
        });
    };

    // currentDate : number moment().valueOf() // milliseconds
    static checkExpired = (currentDate) => {
        return new Promise((resolve, reject) => {
            try {
                DB.db.write(() => {
                    // handle the id                    
                    let invoices = DB.db.objects('Invoices').filtered('status =  0 AND expire < $0', currentDate);
                    //console.log({ invoices, currentDate });
                    for (let i = 0; i < invoices.length; i++) {
                        invoices[i].status = -1;
                    }
                    resolve();
                });
            } catch (e) {
                reject(e);
            }
        });
    };

    //BEGIN==================================User==============================

    // pin : hash string
    static insertPin = (pin, hash) => {
        return new Promise((resolve, reject) => {
            try {
                DB.db.write(() => {
                    const users = DB.db.objects('User');
                    if (users.length > 0) {
                        DB.db.delete(users[0]);
                    }
                    const user = {
                        id: 1,
                        pin: pin,
                        hash: hash,
                    };
                    //console.log({ user });
                    DB.db.create('User', user);
                    resolve();
                });
            } catch (e) {
                reject(e);
            }
        });
    };

    // oldPin : hash string
    static updatePin = (oldPin, hash) => {
        return new Promise((resolve, reject) => {
            try {
                const users = DB.db.objects('User');
                if (users.length === 0) {
                    throw new Error('Not found');
                }
                DB.db.write(() => {
                    DB.db.delete(users[0]);
                    const user = {
                        id: 1,
                        pin: oldPin,
                        hash: hash,
                    };
                    DB.db.create('User', user);
                    resolve();
                });
            } catch (e) {
                reject(e);
            }
        });
    };

    static queryPin = () => {
        return new Promise((resolve, reject) => {
            try {
                const users = DB.db.objects('User');
                if (users.length > 0) {
                    //console.log({ user:schema.fromRealm('User', users[0]) });
                    resolve(schema.fromRealm('User', users[0]));
                } else {
                    throw new Error('Pin not found');
                }
            } catch (e) {
                reject(e);
            }
        });
    }

    static deletePin = () => {
        return new Promise((resolve, reject) => {
            try {
                const users = DB.db.objects('User');
                if (users.length === 0) {
                    resolve();
                    return;
                } 
                DB.db.write(() => {
                    DB.db.delete(users[0]);
                    resolve();
                });
            } catch (e) {
                reject(e);
            }
        });
    }

    //BEGIN==================================Wallets==============================

    // wallet : { seed, address, keyPair }
    static insertWallet = (wallet) => {
        return new Promise((resolve, reject) => {
            try {
                DB.db.write(() => {
                    // handle the id
                    let id = DB.db.objects('Wallets').max('id');
                    let decrypt = schema.getWallet(wallet);
                    decrypt.id = id === undefined ? 1 : ++id;
                    DB.db.create('Wallets', decrypt);
                    resolve();
                });
            } catch (e) {
                reject(e);
            }
        });
    };

    // address: string
    static queryWallet = (address, pin) => {
        return new Promise((resolve, reject) => {
            try {
                const wallets = DB.db.objects('Wallets').filtered('address = $0', address);
                
                if (wallets.length === 0) {
                    throw new Error('Not found');
                }
                const row = wallets[0];
                const encSeed = AES.decrypt(row.seed, pin);
                const encKeys = AES.decrypt(row.keyPair, pin);
                //console.log({ row, encSeed, encKeys });
                const wallet = {
                    ...row,
                    seed: JSON.parse(encSeed.toString(CryptoJS.enc.Utf8)),
                    keyPair: JSON.parse(encKeys.toString(CryptoJS.enc.Utf8)),
                };
                resolve(wallet);
            } catch (e) {
                reject(e);
            }
        });
    }

    // address: string
    static existsWallet = (address) => {
        return new Promise((resolve, reject) => {
            try {
                const wallets = DB.db.objects('Wallets').filtered('address = $0', address);
                resolve(wallets.length !== 0);
            } catch (e) {
                reject(e);
            }
        });
    }
    
    // address: string
    static deleteWallet = (address) => {
        return new Promise((resolve, reject) => {
            try {
                const wallets = DB.db.objects('Wallets').filtered('address = $0', address);
                if (wallets.length === 0) {
                    resolve();
                }
                DB.db.write(() => {
                    DB.db.delete(wallets[0]);
                });
            } catch (e) {
                reject(e);
            }
        });
    }

    //END==================================Wallets==============================

    //BEGIN================================Params===============================

    // name : string
    // val : string
    static insertParam = ({ name, val }) => {
        return new Promise((resolve, reject) => {
            try {
                DB.db.write(() => {
                    // handle the id
                    let id = DB.db.objects('Params').max('id');
                    const params = {
                        id: id === undefined ? 1 : ++id,
                        name,
                        val: typeof(val) !== 'string' ? val.toString() : val
                    };
                    DB.db.create('Params', params);
                    resolve();
                });
            } catch (e) {
                reject(e);
            }
        });
    };

    // name : string
    // val : string
    static updateParam = ({ name, val }) => {
        return new Promise((resolve, reject) => {
            try {
                DB.db.write(() => {                   
                    let params = DB.db.objects('Params').filtered('name = $0', name);
                    params[0].val = typeof(val) !== 'string' ? val.toString() : val;
                    //console.log(params[0]);
                    resolve();
                });
            } catch (e) {
                reject(e);
            }
        });
    };

    // name: string
    static queryParam = (name) => {
        return new Promise((resolve, reject) => {
            try {
                DB.db.write(() => {                   
                    let params = DB.db.objects('Params').filtered('name = $0', name);
                    //console.log(params);
                    if (params.length === 0) {
                        throw new Error('Not found');
                    }
                    resolve(schema.fromRealm('Params', params[0]));
                });
            } catch (e) {
                //console.log(e);
                reject(e);
            }
        });
    }

    //END==================================Params===============================

    //BEGIN================================Patterns===============================

    // name : string
    // pattern : string
    static insertPattern = ({ name, pattern }) => {
        return new Promise((resolve, reject) => {
            try {
                DB.db.write(() => {
                    // handle the id
                    let id = DB.db.objects('Patterns').max('id');
                    const newPattern = {
                        id: id === undefined ? 1 : ++id,
                        name,
                        pattern
                    }; 
                    //console.log(newPattern);
                    DB.db.create('Patterns', newPattern);
                    resolve(newPattern.id);
                });
            } catch (e) {
                reject(e);
            }
        });
    };

    // id: number 
    static deletePattern = ({ id }) => {
        return new Promise((resolve, reject) => {
            try {
                const pattern = DB.db.objectForPrimaryKey('Patterns', id);
                //console.log(pattern);
                if (pattern !== undefined) {
                    DB.db.write(() => {
                        DB.db.delete(pattern);
                    });
                }
                resolve();
            } catch (e) {
                reject(e);
            }
        });
    }

    // id: number 
    static updatePattern = ({ id, name, pattern }) => {
        return new Promise((resolve, reject) => {
            try {
                DB.db.write(() => {
                    // handle the id
                    let patterns = DB.db.objects('Patterns').filtered('id = $0', id);
                    if (patterns.length > 0) {
                        patterns[0].name = name;
                        patterns[0].pattern = pattern;
                    } else {
                        throw new Error('Pattern not found');
                    }
                    resolve();
                });
            } catch (e) {
                reject(e);
            }
        });
    }
    
    static queryPatterns = () => {
        return new Promise((resolve, reject) => {
            try {
                const list = [];
                const patterns = DB.db.objects('Patterns');
                for (let object of patterns.values()) {
                    const pattern = schema.fromRealm('Patterns', object);
                    list.push(pattern);
                }
                resolve(list);
            } catch (e) {
                reject(e);
            }
        });
    }

    static existsPattern = ({ name, pattern }) => {
        return new Promise((resolve, reject) => {
            try {
                const patterns = DB.db.objects('Patterns').filtered('name = $0 AND pattern = $1', name, pattern);
                if (patterns.length === 1) {
                    const row = patterns[0];
                    resolve(row.id);
                } else {
                    resolve(0);
                }
            } catch (e) {
                reject(e);
            }
        });
    }

    //END==================================Patterns===============================

    //BEGIN================================Servers===============================

    // name : string
    // pattern : string
    static insertServer = ({ name, connectionString, port }) => {
        return new Promise((resolve, reject) => {
            try {
                DB.db.write(() => {
                    // handle the id
                    let id = DB.db.objects('Servers').max('id');
                    const server = {
                        id: id === undefined ? 1 : ++id,
                        name,
                        connectionString,
                        port
                    }; 
                    DB.db.create('Servers', server);
                    resolve(server.id);
                });
            } catch (e) {
                reject(e);
            }
        });
    };

    // id: number 
    static deleteServer = (id) => {
        return new Promise((resolve, reject) => {
            try {
                const server = DB.db.objectForPrimaryKey('Servers', id);
                if (server !== undefined) {
                    DB.db.write(() => {
                        DB.db.delete(server);
                    });
                }
                resolve();
            } catch (e) {
                reject(e);
            }
        });
    }

    // connectionString: string
    static getServer = ({ connectionString }) => {
        return new Promise((resolve, reject) => {
            try {
                const servers = DB.db.objects('Servers').filtered('connectionString = $0', connectionString);
                if (servers.length === 0) {
                    throw new Error('Not found');
                }
                resolve(schema.fromRealm('Servers', servers[0]));
            } catch (e) {
                reject(e);
            }
        });
    }
    
    static queryServers = () => {
        return new Promise((resolve, reject) => {
            try {
                const list = [];
                const servers = DB.db.objects('Servers');
                for (let object of servers.values()) {
                    const server = schema.fromRealm('Servers', object);
                    list.push(server);
                }
                resolve(list);
            } catch (e) {
                reject(e);
            }
        });
    }

    //END==================================Servers===============================

    //BEGIN================================Requisites===============================

    static insertRequisite = ({ recipient, bank, acct, cacct, bik, inn, kpp }) => {
        return new Promise((resolve, reject) => {
            try {
                DB.db.write(() => {
                    // handle the id
                    let id = DB.db.objects('Requisites').max('id');
                    const obj = {
                        id: id === undefined ? 1 : ++id,
                        recipient, 
                        bank, 
                        acct, 
                        cacct, 
                        bik, 
                        inn, 
                        kpp
                    }; 
                    DB.db.create('Requisites', obj);
                    resolve(obj.id);
                });
            } catch (e) {
                reject(e);
            }
        });
    };

    static updateRequisite = ({ id, recipient, bank, acct, cacct, bik, inn, kpp }) => {
        return new Promise((resolve, reject) => {
            try {
                DB.db.write(() => {
                    const obj = DB.db.objectForPrimaryKey('Requisites', id);
                    if (obj) {
                        obj.recipient = recipient;
                        obj.bank = bank;
                        obj.acct = acct;
                        obj.cacct = cacct;
                        obj.bik = bik;
                        obj.inn = inn;
                        obj.kpp = kpp;
                    } else {
                        throw new Error('Requisite not found');
                    }
                    resolve();
                });
            } catch (e) {
                reject(e);
            }
        });
    };

    // id: number 
    static deleteRequisite = (id) => {
        return new Promise((resolve, reject) => {
            try {
                const obj = DB.db.objectForPrimaryKey('Requisites', id);
                if (obj !== undefined) {
                    DB.db.write(() => {
                        DB.db.delete(obj);
                    });
                }
                resolve();
            } catch (e) {
                reject(e);
            }
        });
    }

    // connectionString: string
    static getRequisite = ({ id }) => {
        return new Promise((resolve, reject) => {
            try {
                const obj = DB.db.objectForPrimaryKey('Requisites', id);
                if (!obj) {
                    throw new Error('Not found');
                }
                resolve(schema.fromRealm('Requisites', obj));
            } catch (e) {
                reject(e);
            }
        });
    }

    //END==================================Requisites===============================

    //BEGIN==================================Backup==============================

    static query = (table) => {
        return new Promise((resolve, reject) => {
            try {
                const list = [];
                const rows = DB.db.objects(table);
                for (let object of rows.values()) {
                    const row = schema.fromRealm(table, object);
                    list.push(row);
                }
                resolve(list);
            } catch (e) {
                reject(e);
            }
        });
    };

    static delete = (table) => {
        return new Promise((resolve, reject) => {
            try {
                const row = DB.db.objectForPrimaryKey(table, table.id);
                if (row !== undefined) {
                    DB.db.write(() => {
                        DB.db.delete(row);
                    });
                }
                resolve();
            } catch (e) {
                reject(e);
            }
        });
    };

    /* */
    static tables = () => {
        return new Promise((resolve) => {
            resolve(schema.tables());
        });
    };

    static backup = () => {
        return new Promise((resolve, reject) => {
            DB.tables()
                .then(async (tables) => {
                    const backup = {};
                    for (let i = 0; i < tables.length; i++) {
                        try {
                            const rows = await DB.query(tables[i]);
                            backup[tables[i]] = rows;
                        } catch (e) {
                            reject(e);
                        }
                    }
                    resolve(backup);
                })
                .catch(e => reject(e));
        });
    };    

    static restore = (data) => {
        return new Promise((resolve, reject) => {
            const deleteAll = new Promise((resolve) => {
                DB.db.write(() => {
                    DB.db.deleteAll();
                    resolve();
                });
            });
            deleteAll
                .then(() => {
                    DB.tables()
                        .then(async (tables) => {
                            DB.db.write(async () => {
                                for (let i = 0; i < tables.length; i++) {
                                    const table = tables[i];
                                    try {
                                        const rows = data[table]; 
                                        for (let j = 0; j < rows.length; j++) {
                                            //console.log(table, rows[j]);
                                            DB.db.create(table, rows[j]);
                                        }
                                    } catch (e) {
                                        reject(e);
                                    }
                                }
                                resolve();
                            });

                        })
                        .catch(e => reject(e));
                })
                .catch(e => reject(e));
            
        });
    }; 


    //END==================================Backup===============================

}