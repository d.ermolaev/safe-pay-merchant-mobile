import { AES } from 'crypto-js';

export const fromRealm = (table, realmObject) => {
    let properties = Properties(table);
    let object = {};
    for (let i = 0; i < Object.keys(properties).length; i++) {
        const key = Object.keys(properties)[i];
        if ({}.hasOwnProperty.call(realmObject, key)) {
            object[key] = realmObject[key];
        }    
    }
    return object;
};

export const Meta = {
    User: {
        name: 'User',
        primaryKey: 'id',
        properties: {
            id: 'int',
            pin: 'string',
            hash: 'string',
        }
    },
    Params: {
        name: 'Params',
        primaryKey: 'id',
        properties: {
            id: 'int',
            name: { type: 'string', indexed: true },
            val: { type: 'string', optional: true },
        }
    },
    Servers: {
        name: 'Servers',
        primaryKey: 'id',
        properties: {
            id: 'int',
            name: 'string',
            connectionString: 'string',
            port: 'int',
        }
    },
    Patterns: {
        name: 'Patterns',
        primaryKey: 'id',
        properties: {
            id: 'int',
            name: 'string',
            pattern: 'string'
        }
    },
    Wallets: {
        name: 'Wallets',
        primaryKey: 'id',
        properties: {
            id: 'int',
            seed: 'string',
            address: 'string',
            keyPair: 'string',
        }
    },
    Invoices: {
        name: 'Invoices',
        primaryKey: 'id',
        properties: {
            id: 'int',
            address: 'string',
            numOrder: { type: 'int', indexed: true },
            qrcode: 'string',
            signature: 'string',
            user: 'string',
            dt: 'int',
            expire: 'int',
            curr: 'int',
            sum: 'float',
            title: 'string',
            description: 'string',
            detail: 'string',
            recipient: 'string',
            status: 'int',
            lastdt: 'int',
            attempts: 'int',
            confirmations: 'int',
            confirmSignature: 'string',
        }
    },
    Requisites: {
        name: 'Requisites',
        primaryKey: 'id',
        properties: {
            id: 'int',
            recipient: 'string',
            bank: 'string',
            acct: 'string',
            cacct: 'string',
            bik: 'string',
            inn: 'string',
            kpp: 'string',
        }
    },
};

const Properties = (table) => {
    return Meta[table].properties;
};

export const tables = () => {
    return Object.values(Meta).map(meta => meta.name);
};

export const getWallet = (params) => {
    //console.log(params);
    return {
        id: 0,
        seed: AES.encrypt(JSON.stringify(params.seed), params.pin).toString(),
        address: params.address,
        keyPair: AES.encrypt(JSON.stringify(params.keyPair), params.pin).toString(),   
    };
};

export const getInvoice = (params) => {
    //console.log({params});
    return {
        id: 0,
        address: params.address,
        numOrder: Number(params.num),
        qrcode: params.qrcode,
        signature: params.signature,
        user: params.user,
        dt: params.date,
        expire: params.expire,
        curr: params.curr,
        sum: Number(params.sum),
        title: params.title,
        description: params.description,
        detail: params.detail,
        recipient: params.recipient,
        status: params.status,
        lastdt: params.lastdt,
        attempts: params.attempts,
        confirmations: params.confirmations,
        confirmSignature: params.confirmSignature,        
    };
};
