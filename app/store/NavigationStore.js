import { NavigationActions } from 'react-navigation';
import { AppNavigator } from '../navigation';
import { Keyboard } from 'react-native';
import * as _ from 'lodash';

export class NavigationStore {

    _activeRoute;

    menuId;

    navigationState = {
        index: 0,
        routes: [
            {
                key: 'Login', 
                routeName: 'Login'
            },
        ],
    };

    get activeRoute() {
        let routeName = 'MyWallet';
        try {
            //console.log('ST', this.navigationState);
            let state = this.navigationState;
            while (state.routes) {
                state = state.routes[state.index];
                const nState = state;
                if (nState.routeName) {
                    routeName = nState.routeName;
                }
            }
        } catch (e) {
            console.log(e);
        }

        return routeName;
    }

    dispatch = (action, stackNavState = true) => {
        if (action.routeName === 'DrawerOpen') {
            Keyboard.dismiss();
        }

        const previousNavState = stackNavState ? this.navigationState : undefined;
        const newState = AppNavigator.router.getStateForAction(action, previousNavState);
        if (newState && newState !== previousNavState) {
            this.navigationState = newState;

            return true;
        }

        return false;
    };

    _params;

    get params() {
        return this._params || {};
    }

    dispatchNavigation(routerAction, reset = false) {
        const previousNavState = reset ? null : this.navigationState;
        const newState = AppNavigator.router.getStateForAction(routerAction, previousNavState);
        //console.log('STATE', newState);
        // if (newState && this.navigationState.routes[0].routes[0].routes[0].routes) {
        //     newState.routes[0].routes[0].routes[0].routes = [newState.routes[0].routes[0].routes[0].routes[0]];
        // }

        if (newState && !_.isEqual(newState, previousNavState)) {
            this.navigationState = newState;

            return newState;
        }

        return null;
    }

    reset(routeName, params, navAction) {
        this.setActiveRoute(routeName);
        this._params = params;
        const resetAction = NavigationActions.navigate({routeName, params, action: navAction});
        const routerAction = NavigationActions.reset({index: 0, actions: [resetAction]});

        this.menuId = 'MyWallet';
        return this.dispatchNavigation(routerAction, true);
    }

    goBack(key) {
        Keyboard.dismiss();
        // const routes = this.getStackRoutes();
        // if (!!key) {
        //     routes.forEach((route, index) => {
        //         if (route.key === key) {
        //             this.activeRoute = routes[index - 1].routeName;
        //         }
        //     });
        // } else {
        //     this.activeRoute = routes[routes.length - 2].routeName;
        // }

        return this.dispatchNavigation(NavigationActions.back({key}));
    }

    navigate(routeName, params, action, reset) {
        Keyboard.dismiss();

        //console.log('NAV', this.navigationState.routes);
        if (this.activeRoute === routeName) {
            this.navigate('DrawerClose');
            return;
        }
        this.setActiveRoute(routeName);
        this._params = params;
        this.dispatchNavigation(NavigationActions.navigate({routeName, params, action}), reset);
    }


    setParams(params) {
        this._params = params;
        return this.dispatchNavigation(NavigationActions.setParams({params, key: this.navigationState.routes[0].key}));
    }

    // getStackRoutes(): any[] {
    //     return !!this.navigationState.routes[0].routes ? this.navigationState.routes[0].routes[0].routes[0].routes : this.navigationState.routes;
    // }

    drawerOpen() {
        Keyboard.dismiss();
        this.navigate('DrawerOpen');
    }

    drawerClose() {
        this.navigate('DrawerClose');
    }

    selectMenu(id) {
        this.menuId = id;
    }

    setActiveRoute(routeName) {
        switch (routeName) {
        case 'DrawerOpen':
        case 'DrawerClose':
            break;

        case 'Main':
            this._activeRoute = 'MyWallet';
            break;

        case 'Wallet':
            this._activeRoute = 'MyWallet';
            break;
        case 'Assets':
            this._activeRoute = 'AssetsLocal';
            break;
        case 'Persons':
            this._activeRoute = 'PersonsLocal';
            break;
        case 'Register':
            this._activeRoute = 'Registration';
            break;
        case 'Declare':
            this._activeRoute = 'Declaration';
            break;
        case 'Identification':
            this._activeRoute = 'Identificate';
            break;


        default:
            this._activeRoute = routeName;
            break;
        }
    }
}

export const navigationStore = new NavigationStore();
