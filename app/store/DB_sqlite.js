
import SQLite from 'react-native-sqlite-storage';
import { AES } from 'crypto-js';
import * as CryptoJS from 'crypto-js';

SQLite.DEBUG(true);
SQLite.enablePromise(true);

const database_name = 'qrsp.db';
const database_version = '1.0';
const database_displayname = 'SQLite QRSP Database';
const database_size = 200000;

const pageSize = 5;

export default class DB {

    static db;

    static open = () => {
        return new Promise((resolve, reject) => {
            SQLite.echoTest()
                .then(() => {
                    SQLite.openDatabase(database_name, database_version, database_displayname, database_size)
                        .then((db) => {
                            DB.db = db;
                            resolve();
                        })
                        .catch(e => reject(e));
                })
                .catch(e => {
                    reject(e);
                });
        });
    };

    static close = () => {
        return new Promise((resolve, reject) => {
            if (DB.db) {
                DB.db.close()
                    .then(() => resolve())
                    .catch(e => reject(e));
            } else {
                resolve();
            }
        });
    };

    static create = () => {
        return new Promise((resolve, reject) => {
            DB.db.transaction(DB.createSchema)
                .then(() => {
                    resolve();
                })
                .catch(e => {
                    reject(e);
                });
        });
    };

    static createSchema = (tx) => {

        const promise1 = new Promise((resolve, reject) => {
            tx.executeSql('CREATE TABLE IF NOT EXISTS Version( version_id INTEGER PRIMARY KEY NOT NULL); ')
                .then(() => resolve())
                .catch(e => reject(e));
        });

        const promise2 = new Promise((resolve, reject) => {
            tx.executeSql('CREATE TABLE IF NOT EXISTS User( id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,'
                + 'pin TEXT,'
                + 'hash TEXT'
                + ');')
                .then(() => resolve())
                .catch(e => reject(e));
        });

        const promise3 = new Promise((resolve, reject) => {
            tx.executeSql('CREATE TABLE IF NOT EXISTS Params( id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,'
                + 'name TEXT, '
                + 'val TEXT'
                + ');')
                .then(() => resolve())
                .catch(e => reject(e));
        });

        const promise4 = new Promise((resolve, reject) => {
            tx.executeSql('CREATE INDEX IF NOT EXISTS idxName ON Params (name);')
                .then(() => resolve())
                .catch(e => reject(e));
        });

        const promise5 = new Promise((resolve, reject) => {
            tx.executeSql('CREATE TABLE IF NOT EXISTS Servers( id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,'
                + 'name TEXT, '
                + 'connectionString TEXT, '
                + 'port INTEGER'
                + ');')
                .then(() => resolve())
                .catch(e => reject(e));
        });

        const promise6 = new Promise((resolve, reject) => {
            tx.executeSql('CREATE TABLE IF NOT EXISTS Patterns( id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,'
                + 'name TEXT, '
                + 'pattern TEXT'
                + ');')
                .then(() => resolve())
                .catch(e => reject(e));
        });

        const promise7 = new Promise((resolve, reject) => {
            tx.executeSql('CREATE TABLE IF NOT EXISTS Wallets( '
                + 'id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, '
                + 'seed TEXT, '
                + 'address TEXT, '
                + 'keyPair TEXT'
                + ');')
                .then(() => resolve())
                .catch(e => reject(e));
        });
        
        const promise8 = new Promise((resolve, reject) => {
            tx.executeSql('CREATE TABLE IF NOT EXISTS Invoices( '
                + 'id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, '
                + 'address TEXT, '
                + 'numOrder TEXT, '
                + 'qrcode TEXT, '
                + 'signature TEXT, '
                + 'user TEXT, '
                + 'dt INTEGER, '
                + 'expire INTEGER, '
                + 'curr INTEGER, '
                + 'sum REAL, '
                + 'title TEXT, '
                + 'description TEXT, '
                + 'detail TEXT, '
                + 'recipient TEXT, '
                + 'status INTEGER, '
                + 'lastdt INTEGER, '
                + 'attempts INTEGER,'
                + 'confirmations INTEGER,'
                + 'confirmSignature TEXT '
                + ');')
                .then(() => resolve())
                .catch(e => reject(e));
        });
        
        promise1
            .then(() => {
                return promise2;
            })
            .then(() => {
                return promise3;
            })
            .then(() => {
                return promise4;
            })
            .then(() => {
                return promise5;
            })
            .then(() => {
                return promise6;
            })
            .then(() => {
                return promise7;
            })
            .then(() => {
                return promise8;
            })
            .catch(e => {
                throw new Error(e);
            });
    };

    static check = () => {
        return new Promise((resolve, reject) => {
            DB.db.transaction((tx) => {
                tx.executeSql('SELECT 1 FROM Version LIMIT 1')
                    .then(() => resolve())
                    .catch(e => reject(e));
            })
                .then(() => resolve())
                .catch(e => reject(e));
        });
    };
    
    static queryInvoices = (page) => {
        return new Promise((resolve, reject) => {
            DB.db.transaction((tx) => {
                tx.executeSql('SELECT * FROM Invoices ORDER BY lastdt DESC LIMIT ? OFFSET ?', [ pageSize, (page - 1) * pageSize ])
                    .then(([, results]) => {
                        const len = results.rows.length;
                        let rows = [];
                        for (let i = 0; i < len; i++) {
                            const row = results.rows.item(i);
                            rows.push(row);
                        }
                        console.log(rows);
                        resolve(rows);
                    })
                    .catch(e => reject(e));
            })
                .then((rows) => resolve(rows))
                .catch(e => reject(e));
        });
    };

    static invoicePages = () => {
        return new Promise((resolve, reject) => {
            DB.db.transaction((tx) => {
                tx.executeSql('SELECT COUNT(*) / ? as pages, COUNT(*) % ? as remain FROM Invoices', [ pageSize, pageSize ])
                    .then(([, results]) => {
                        const len = results.rows.length;
                        let pages = 0;
                        for (let i = 0; i < len; i++) {
                            const row = results.rows.item(i);
                            pages = row.pages;
                            if (row.remain > 0) {
                                pages += 1;
                            }
                        }
                        //console.log(pages);
                        resolve(pages);
                    })
                    .catch(e => reject(e));
            })
                .then((pages) => resolve(pages))
                .catch(e => reject(e));
        });
    };

    // params : array
    static insertInvoice = (params) => {
        return new Promise((resolve, reject) => {
            DB.db.transaction((tx) => {
                tx.executeSql('INSERT INTO Invoices ('
                + 'address,'
                + 'numOrder,'
                + 'qrcode,'
                + 'signature,'
                + 'user,'
                + 'dt,' 
                + 'expire,' 
                + 'curr,' 
                + 'sum,' 
                + 'title,' 
                + 'description,'
                + 'detail,'
                + 'recipient,'
                + 'status,' 
                + 'lastdt,' 
                + 'attempts,'
                + 'confirmations,'
                + 'confirmSignature'
                + ') VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?);', 
                [
                    params.address,
                    params.num,
                    params.qrcode,
                    params.signature,
                    params.user,
                    params.date,
                    params.expire,
                    params.curr,
                    params.sum,
                    params.title,
                    params.description,
                    params.detail,
                    params.recipient,
                    params.status,
                    params.lastdt,
                    params.attempts,
                    params.confirmations,
                    params.confirmSignature
                ])
                    .then(() => resolve())
                    .catch(e => reject(e));
            })
                .then(() => resolve())
                .catch(e => reject(e));
        });
    };

    // params : array
    static updateInvoice = (params) => {
        return new Promise((resolve, reject) => {
            DB.db.transaction((tx) => {
                tx.executeSql('UPDATE Invoices SET '
                + 'status = ?, ' 
                + 'lastdt = ?, ' 
                + 'confirmations = ?, '
                + 'confirmSignature = ?, '
                + 'attempts = attempts + 1 '
                + 'WHERE id = ?'
                + ';', [ params.status, params.lastdt, params.confirmations, params.confirmSignature, params.id])
                    .then(() => resolve())
                    .catch(e => reject(e));
            })
                .then(() => resolve())
                .catch(e => reject(e));
        });
    };

    static uniqueNum = (numStr) => {
        return new Promise((resolve, reject) => {
            DB.db.transaction((tx) => {
                tx.executeSql('SELECT numOrder FROM Invoices WHERE numOrder = ?', [ numStr ])
                    .then(([, results]) => {
                        if (results.rows.length !== 0) {
                            reject('Not unique');
                        }
                        resolve();
                    })
                    .catch(e => {
                        reject(e);
                    });
            }) 
                .then(() => resolve())
                .catch(e => reject(e));
        });
    }

    static maxNum = () => {
        return new Promise((resolve, reject) => {
            DB.db.transaction((tx) => {
                tx.executeSql('SELECT MAX(CAST(numOrder AS INTEGER)) as numOrder FROM Invoices')
                    .then(([, results]) => {
                        let len = results.rows.length;
                        let num = 0;
                        for (let i = 0; i < len; i++) {
                            let row = results.rows.item(i);
                            num = row.numOrder;
                            break;
                        }
                        resolve(num);
                    })
                    .catch(e => reject(e));
            }) 
                .then(() => resolve())
                .catch(e => reject(e));
        });
    }

    static queryAddresses = (maxStatus) => {
        return new Promise((resolve, reject) => {
            DB.db.transaction((tx) => {
                tx.executeSql('SELECT address, MIN(dt) as dt FROM Invoices WHERE status <= ? GROUP BY address', [ maxStatus ])
                    .then(([, results]) => {
                        const len = results.rows.length;
                        let rows = [];
                        for (let i = 0; i < len; i++) {
                            const row = results.rows.item(i);
                            rows.push(row);
                        }
                        //console.log(rows);
                        resolve(rows);
                    })
                    .catch(e => reject(e));
            })
                .then((rows) => resolve(rows))
                .catch(e => reject(e));
        });
    };

    static findInvoice = ({ address, recipient, signature, sum, curr }) => {
        return new Promise((resolve, reject) => {
            DB.db.transaction((tx) => {
                tx.executeSql('SELECT * FROM Invoices WHERE address = ? AND recipient = ? AND signature = ? AND sum = ? AND curr = ? AND status < 2', [ address, recipient, signature, sum, curr ])
                    .then(([, results]) => {
                        const len = results.rows.length;
                        if (len > 0) {
                            //console.log(results.rows.item(0));
                            resolve(results.rows.item(0));
                        } else {
                            throw new Error('Not found');
                        }
                    })
                    .catch(e => reject(e));
            })
                .then((row) => resolve(row))
                .catch(e => reject(e));
        });
    };

    static existsInvoice = ({ address, recipient, signature, sum, curr }) => {
        return new Promise((resolve, reject) => {
            DB.db.transaction((tx) => {
                tx.executeSql('SELECT * FROM Invoices WHERE address = ? AND recipient = ? AND signature = ? AND sum = ? AND curr = ?', [ address, recipient, signature, sum, curr ])
                    .then(([, results]) => {
                        const len = results.rows.length;
                        resolve(len > 0);
                    })
                    .catch(e => reject(e));
            })
                .then((row) => resolve(row))
                .catch(e => reject(e));
        });
    };

    // currentDate : number moment().valueOf() // milliseconds
    static checkExpired = (currentDate) => {
        return new Promise((resolve, reject) => {
            DB.db.transaction((tx) => {
                tx.executeSql('UPDATE Invoices SET '
                + 'status = -1 ' 
                + 'WHERE status =  0 AND expire < ?'
                + ';', [ currentDate ])
                    .then(() => {
                        //console.log(results);
                        resolve();
                    })
                    .catch(e => reject(e));
            })
                .then(() => resolve())
                .catch(e => reject(e));
        });
    };

    //BEGIN==================================User==============================

    // pin : hash string
    static insertPin = (pin, hash) => {
        return new Promise((resolve, reject) => {
            DB.db.transaction((tx) => {
                tx.executeSql('INSERT INTO User ('
                + 'pin,'
                + 'hash'
                + ') VALUES (?);', 
                [
                    pin,
                    hash
                ])
                    .then(() => resolve())
                    .catch(e => reject(e));
            })
                .then(() => resolve())
                .catch(e => reject(e));
        });
    };

    // oldPin : hash string
    static updatePin = (oldPin, hash) => {
        return new Promise((resolve, reject) => {
            DB.db.transaction((tx) => {
                tx.executeSql('UPDATE User SET '
                + 'pin = ?,' 
                + 'hash = ? '
                + 'WHERE pin = ?'
                + ';', [ oldPin, hash ])
                    .then(() => resolve())
                    .catch(e => reject(e));
            })
                .then(() => resolve())
                .catch(e => reject(e));
        });
    };

    static queryPin = () => {
        return new Promise((resolve, reject) => {
            DB.db.transaction((tx) => {
                tx.executeSql('SELECT pin, hash FROM User ORDER BY id DESC LIMIT 1')
                    .then(([, results]) => {
                        let len = results.rows.length;
                        if (len === 0) {
                            throw new Error('Pin not found');
                        }
                        const row = results.rows.item(0);
                        resolve(row);
                    })
                    .catch((e) => reject(e));
            }) 
                .then((pin) => resolve(pin))
                .catch((e) => reject(e));
        });
    }

    static deletePin = () => {
        return new Promise((resolve, reject) => {
            DB.db.transaction((tx) => {
                tx.executeSql('DELETE FROM User')
                    .then(() => resolve())
                    .catch(e => reject(e));
            }) 
                .then(() => resolve())
                .catch(e => reject(e));
        });
    }

    //BEGIN==================================Wallets==============================

    // wallet : { seed, address, keyPair }
    static insertWallet = (wallet) => {
        return new Promise((resolve, reject) => {
            DB.db.transaction((tx) => {
                tx.executeSql('INSERT INTO Wallets ('
                + 'seed, '
                + 'address, '
                + 'keyPair'
                + ') VALUES (?, ?, ?);', 
                [
                    AES.encrypt(JSON.stringify(wallet.seed), wallet.pin).toString(),
                    wallet.address,
                    AES.encrypt(JSON.stringify(wallet.keyPair), wallet.pin).toString(),
                ])
                    .then(() => resolve())
                    .catch(e => reject(e));
            })
                .then(() => resolve())
                .catch(e => reject(e));
        });
    };

    // address: string
    static queryWallet = (address, pin) => {
        return new Promise((resolve, reject) => {
            DB.db.transaction((tx) => {
                tx.executeSql('SELECT * FROM Wallets WHERE address = ?', [ address ])
                    .then(([, results]) => {
                        let len = results.rows.length;
                        if (len === 0) {
                            throw new Error('Not found');
                        }
                        const row = results.rows.item(0);
                        const encSeed = AES.decrypt(row.seed, pin);
                        const encKeys = AES.decrypt(row.keyPair, pin);
                        const wallet = {
                            ...row,
                            seed: JSON.parse(encSeed.toString(CryptoJS.enc.Utf8)),
                            keyPair: JSON.parse(encKeys.toString(CryptoJS.enc.Utf8)),
                        };
                        console.log(wallet);
                        resolve(wallet);
                    })
                    .catch((e) => reject(e));
            }) 
                .then((row) => resolve(row))
                .catch((e) => reject(e));
        });
    }
    
    // address: string
    static deleteWallet = (address) => {
        return new Promise((resolve, reject) => {
            DB.db.transaction((tx) => {
                tx.executeSql('DELETE FROM Wallet WHERE address = ?', [ address ])
                    .then(() => resolve())
                    .catch(e => reject(e));
            }) 
                .then(() => resolve())
                .catch(e => reject(e));
        });
    }

    //END==================================Wallets==============================

    //BEGIN================================Params===============================

    // name : string
    // val : string
    static insertParam = ({ name, val }) => {
        return new Promise((resolve, reject) => {
            DB.db.transaction((tx) => {
                tx.executeSql('INSERT INTO Params ('
                + 'name, '
                + 'val '
                + ') VALUES (?, ?);', 
                [ name, val ])
                    .then(() => resolve())
                    .catch(e => reject(e));
            })
                .then(() => resolve())
                .catch(e => reject(e));
        });
    };

    // name : string
    // val : string
    static updateParam = ({ name, val }) => {
        return new Promise((resolve, reject) => {
            DB.db.transaction((tx) => {
                tx.executeSql('UPDATE Params SET '
                + 'val = ? '
                + 'WHERE name = ?;', 
                [ val, name ])
                    .then(() => resolve())
                    .catch(e => reject(e));
            })
                .then(() => resolve())
                .catch(e => reject(e));
        });
    };

    // name: string
    static queryParam = (name) => {
        return new Promise((resolve, reject) => {
            DB.db.transaction((tx) => {
                tx.executeSql('SELECT * FROM Params WHERE name = ? LIMIT 1', [ name ])
                    .then(([, results]) => {
                        let len = results.rows.length;
                        if (len === 0) {
                            throw new Error('Not found');
                        }
                        resolve(results.rows.item(0));
                    })
                    .catch((e) => reject(e));
            }) 
                .then((row) => resolve(row))
                .catch((e) => reject(e));
        });
    }

    //END==================================Params===============================

    //BEGIN================================Patterns===============================

    // name : string
    // pattern : string
    static insertPattern = ({ name, pattern }) => {
        return new Promise((resolve, reject) => {
            DB.db.transaction((tx) => {
                tx.executeSql('INSERT INTO Patterns ('
                + 'name, '
                + 'pattern '
                + ') VALUES (?, ?);', 
                [ name, pattern ])
                    .then(([, results]) => {
                        if (results.rowsAffected !== 1) {
                            throw new Error('Error insert pattern');
                        } 
                        resolve(results.insertId);
                    })
                    .catch(e => reject(e));
            })
                .then((id) => resolve(id))
                .catch(e => reject(e));
        });
    };

    // id: number 
    static deletePattern = ({ id }) => {
        return new Promise((resolve, reject) => {
            DB.db.transaction((tx) => {
                tx.executeSql('DELETE FROM Patterns WHERE id = ?', [ id ])
                    .then(() => resolve())
                    .catch(e => reject(e));
            }) 
                .then(() => resolve())
                .catch(e => reject(e));
        });
    }

    // id: number 
    static updatePattern = ({ id, name, pattern }) => {
        return new Promise((resolve, reject) => {
            DB.db.transaction((tx) => {
                tx.executeSql('UPDATE Patterns SET name = ?, pattern = ? WHERE id = ?', [ name, pattern, id ])
                    .then(() => resolve())
                    .catch(e => reject(e));
            }) 
                .then(() => resolve())
                .catch(e => reject(e));
        });
    }
    
    static queryPatterns = () => {
        return new Promise((resolve, reject) => {
            DB.db.transaction((tx) => {
                tx.executeSql('SELECT * FROM Patterns')
                    .then(([, results]) => {
                        let rows = [];
                        let len = results.rows.length;
                        for (let i = 0; i < len; i++) {
                            let row = results.rows.item(i);
                            rows.push(row);
                        }
                        resolve(rows);
                    })
                    .catch((e) => reject(e));
            }) 
                .then((rows) => resolve(rows))
                .catch((e) => reject(e));
        });
    }

    static existsPattern = ({ name, pattern }) => {
        return new Promise((resolve, reject) => {
            DB.db.transaction((tx) => {
                tx.executeSql('SELECT * FROM Patterns WHERE name = ? AND pattern = ? LIMIT 1', [ name, pattern ])
                    .then(([, results]) => {
                        const len = results.rows.length;
                        if (len === 1) {
                            const row = results.rows.item(0);
                            resolve(row.id);
                        } else {
                            resolve(0);
                        }
                    })
                    .catch((e) => reject(e));
            }) 
                .then((result) => resolve(result))
                .catch((e) => reject(e));
        });
    }

    //END==================================Patterns===============================

    //BEGIN================================Servers===============================

    // name : string
    // pattern : string
    static insertServer = ({ name, connectionString, port }) => {
        return new Promise((resolve, reject) => {
            DB.db.transaction((tx) => {
                tx.executeSql('INSERT INTO Servers ('
                + 'name, '
                + 'connectionString, '
                + 'port '
                + ') VALUES (?, ?, ?);', 
                [ name, connectionString, port ])
                    .then(([, results]) => {
                        if (results.rowsAffected !== 1) {
                            throw new Error('Error insert server');
                        } 
                        resolve(results.insertId);
                    })
                    .catch(e => reject(e));
            })
                .then((id) => resolve(id))
                .catch(e => reject(e));
        });
    };

    // id: number 
    static deleteServer = (id) => {
        return new Promise((resolve, reject) => {
            DB.db.transaction((tx) => {
                tx.executeSql('DELETE FROM Servers WHERE id = ?', [ id ])
                    .then(() => resolve())
                    .catch(e => reject(e));
            }) 
                .then(() => resolve())
                .catch(e => reject(e));
        });
    }

    // connectionString: string
    static getServer = ({ connectionString }) => {
        return new Promise((resolve, reject) => {
            DB.db.transaction((tx) => {
                tx.executeSql('SELECT * FROM Servers WHERE connectionString = ? LIMIT 1', [ connectionString ])
                    .then(([, results]) => {
                        let len = results.rows.length;
                        if (len === 0) {
                            throw new Error('Not found');
                        }
                        resolve(results.rows.item(0));
                    })
                    .catch((e) => reject(e));
            }) 
                .then((row) => resolve(row))
                .catch((e) => reject(e));
        });
    }
    
    static queryServers = () => {
        return new Promise((resolve, reject) => {
            DB.db.transaction((tx) => {
                tx.executeSql('SELECT * FROM Servers')
                    .then(([, results]) => {
                        let rows = [];
                        let len = results.rows.length;
                        for (let i = 0; i < len; i++) {
                            let row = results.rows.item(i);
                            rows.push(row);
                        }
                        resolve(rows);
                    })
                    .catch((e) => reject(e));
            }) 
                .then((rows) => resolve(rows))
                .catch((e) => reject(e));
        });
    }

    //END==================================Servers===============================

    //BEGIN==================================Backup==============================

    static query = (table) => {
        return new Promise((resolve, reject) => {
            DB.db.transaction((tx) => {
                const query = `SELECT * FROM ${table}`;
                tx.executeSql(query)
                    .then(([, results]) => {
                        const len = results.rows.length;
                        let rows = [];
                        for (let i = 0; i < len; i++) {
                            const row = results.rows.item(i);
                            rows.push(row);
                        }
                        resolve(rows);
                    })
                    .catch(e => reject(e));
            })
                .then((rows) => resolve(rows))
                .catch(e => reject(e));
        });
    };

    static insert = (table, row) => {
        return new Promise((resolve, reject) => {
            DB.db.transaction((tx) => {
                const names = Object.getOwnPropertyNames(row);
                const args = new Array(names.length);
                args.fill('?');
                const values = Object.values(row);
                const query = `INSERT INTO ${table} (${names.toString()}) VALUES (${args.toString()})`;
                console.log(query);
                tx.executeSql(query, values)
                    .then(([, results]) => {
                        if (results.rowsAffected !== 1) {
                            throw new Error('Error insert server');
                        } 
                        resolve(results.insertId);
                    })
                    .catch(e => reject(e));
            })
                .then((id) => resolve(id))
                .catch(e => reject(e));
        });
    };

    static delete = (table) => {
        return new Promise((resolve, reject) => {
            DB.db.transaction((tx) => {
                const query = `DELETE FROM ${table}`;
                tx.executeSql(query)
                    .then(() => resolve())
                    .catch(e => reject(e));
            })
                .then(() => resolve())
                .catch(e => reject(e));
        });
    };

    /* */
    static tables = () => {
        return new Promise((resolve, reject) => {
            DB.db.transaction((tx) => {
                tx.executeSql('SELECT name FROM sqlite_master WHERE type =\'table\' AND (name NOT LIKE \'sqlite_%\' AND name <> \'android_metadata\')')
                    .then(([, results]) => {
                        const len = results.rows.length;
                        let rows = [];
                        for (let i = 0; i < len; i++) {
                            const row = results.rows.item(i);
                            rows.push(row.name);
                        }
                        //console.log(rows);
                        resolve(rows);
                    })
                    .catch(e => reject(e));
            })
                .then((rows) => resolve(rows))
                .catch(e => reject(e));
        });
    };

    static backup = () => {
        return new Promise((resolve, reject) => {
            DB.tables()
                .then(async (tables) => {
                    const backup = {};
                    for (let i = 0; i < tables.length; i++) {
                        try {
                            const rows = await DB.query(tables[i]);
                            backup[tables[i]] = rows;
                        } catch (e) {
                            reject(e);
                        }
                    }
                    resolve(backup);
                })
                .catch(e => reject(e));
        });
    };    

    static restore = (data) => {
        return new Promise((resolve, reject) => {
            DB.tables()
                .then(async (tables) => {
                    for (let i = 0; i < tables.length; i++) {
                        const table = tables[i];
                        try {
                            await DB.delete(table);
                            const rows = data[table]; 
                            for (let j = 0; j < rows.length; j++) {
                                await DB.insert(table, rows[j]);
                            }
                        } catch (e) {
                            reject(e);
                        }
                    }
                    resolve();
                })
                .catch(e => reject(e));
        });
    }; 

    //END==================================Backup===============================

}

