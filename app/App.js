
import React, { Component } from 'react';
import { View } from 'react-native';
import PropTypes from 'prop-types';
import NetInfo from '@react-native-community/netinfo';
import AppNavigator from 'app/navigation';
import { primaryColorLight, secondaryColor, styles } from './config/styles';

import Toast from 'react-native-simple-toast';
import { 
    Splash
} from './components';
import AppSettings from './core/settings/AppSettings';
import { connect } from 'react-redux';
import * as navigationActions from './actions/navigationActions';
import * as configActions from './actions/configActions';
import * as dbActions from './actions/dbActions';
import { createStructuredSelector } from 'reselect';
import { makeSelectAll } from './selectors/configSelectors';
import { makeSelectPin } from './selectors/walletSelectors';
import { 
    makeSelectDB,
    makeSelectAuth,
} from './selectors/dbSelectors';
import { makeSelectSchedule } from './selectors/chainSelectors';
import { strings } from './config/localize';
import AwesomeAlert from 'react-native-awesome-alerts';
import { initAlert } from './screens/Common';

const getActiveRouteName = (navigationState) => {
    if (!navigationState) {
        return null;
    }
    const route = navigationState.routes[navigationState.index];
    // dive into nested navigators
    if (route.routes) {
        return getActiveRouteName(route);
    }
    return route.routeName;
};

class App extends Component {

    constructor(props) {
        super(props);
        this.state = {
            alert: {
                ...initAlert,
            },
        };
    }

    warning = async (title, text) => {
        this.setState({
            alert: {
                title: `${title} `,
                message: `${text} `, 
                show: true,
                showProgress: false,
                confirmText: `${strings['ButtonOk']} `,
                closeOnTouchOutside: true,
                closeOnHardwareBackPress: true,
                showCancelButton: false,
                showConfirmButton: true,
                onCancelPressed: () => {
                    this.hideAlert();
                },
                onConfirmPressed: () => {
                    this.hideAlert();
                },
            }
        });
    };

    hideAlert = () => {
        this.setState({
            alert: {
                ...this.state.alert,
                show: false,
            }
        });
    };

    unsubscribeNetInfo = null; 
    checkConnection = () => {
        this.unsubscribeNetInfo = NetInfo.addEventListener(state => {
            if (!state.isConnected) {
                Toast.show(strings['connection lost']);
            } 
        });
    };

    readCurrencyes = () => {
        const currencies = {
            list: require('./resources/settings/currency.json'),
        };
        this.props.loadCurrencies(currencies.list);
    }

    isPatternLoaded = false;
    loadPatterns = () => {
        if (!this.isPatternLoaded) {
            const patterns = {
                list: require('./resources/settings/patterns.json'),
            };
            const { list } = patterns;
            for (let i = 0; i < list.length; i++) {
                this.props.addPattern(list[i]);
            }
            this.isPatternLoaded = true;
        }
    }

    shortLang = (lang) => {
        const arr = lang.split('-');
        if (arr.length > 1 ) {
            return arr[0];
        } else {
            return lang;
        }
    }

    async componentDidMount() {
        //const data = await LocalStore.readFile('1234');
        //console.log(data);
        //this.props.restore(data);
        this.checkConnection();
        this.props.openDB();
        this.props.setLang(this.shortLang(this.props.lang));
        const appSettings = new AppSettings();
        const settings = appSettings.settings;
        this.props.loadSettings(settings);
        this.readCurrencyes();
        //console.log(settigns);
    }

    componentDidUpdate(prevProps) {
        if (!prevProps.db.error && this.props.db.error) {
            this.warning(strings['Warning'], strings['error_18']);
        }
        if (!prevProps.db.created && this.props.db.created) {
            this.loadPatterns();
        }
    }

    async componentWillUnmount() {
        this.props.closeDB();
        if (this.unsubscribeNetInfo) {
            this.unsubscribeNetInfo();
        }
    }

    render() {
        //console.log({ auth: this.props.auth });
        const { created } = this.props.db;
        if (!created) {
            return (
                <View style={{ flex: 1 }}> 
                    <Splash />
                    <AwesomeAlert
                        {...this.state.alert}
                        titleStyle={this.state.alert.showProgress?styles.alert.text:styles.alert.title}
                        messageStyle={styles.alert.text}
                        overlayStyle={styles.alert.overlayStyle}
                        cancelButtonColor={secondaryColor}
                        confirmButtonColor={primaryColorLight}
                    />
                </View>
            );
        }
        const startScreen = this.props.auth.pin ? 'Login' : 'CreateWallet';
        return (
            <AppNavigator 
                startScreen={startScreen} 
                onNavigationStateChange={(prevState, currentState) => {
                    const currentScreen = getActiveRouteName(currentState);
                    const prevScreen = getActiveRouteName(prevState);
                    if (prevScreen !== currentScreen) {
                        navigationActions.setActiveRoute(currentScreen);
                    }
                }}
            />
        );
    }
}

App.propTypes = {
    auth: PropTypes.object,
    lang: PropTypes.string, 
    startScreen: PropTypes.string,
    data: PropTypes.object,
    pin: PropTypes.string,
    setLang: PropTypes.func,
    restore: PropTypes.func,
    openDB: PropTypes.func,
    closeDB: PropTypes.func,
    loadSettings: PropTypes.func,
    loadCurrencies: PropTypes.func,
    db: PropTypes.object,
    children: PropTypes.array,
    schedule: PropTypes.object,
    addPattern: PropTypes.func,
    getServers: PropTypes.func,
    servers: PropTypes.object,
};

const mapStateToProps = createStructuredSelector({
    auth: makeSelectAuth(),
    data: makeSelectAll(),
    pin: makeSelectPin(),
    db: makeSelectDB(),
    schedule: makeSelectSchedule(),
});

function mapDispatchToProps(dispatch) {
    return {
        restore: (data) => dispatch(configActions.actionRestoreRedux(data)),
        setLang: (lang) => dispatch(configActions.actionSetLanguage(lang)),
        openDB: () => dispatch(dbActions.actionOpenDB()),
        closeDB: () => dispatch(dbActions.actionCloseDB()),
        loadSettings: (settings) => dispatch(configActions.actionLoadSettings(settings)),
        loadCurrencies: (currencies) => dispatch(configActions.actionLoadCurrencies(currencies)),
        addPattern: (pattern) => dispatch(dbActions.actionPatternInsert(pattern)),
    };
}

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(App);