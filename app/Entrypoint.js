/**
 * React Native App
 * Everthing starts from the entrypoint
 */
import React, { Component } from 'react';
import { View, Text, StatusBar } from 'react-native';
import { Provider } from 'react-redux';
import App from './App';
import configureStore from 'app/store/configureStore';
//import LocalStore from './store/LocalStore';
import { strings } from './config/localize';
import DeviceInfo from 'react-native-device-info';

const store = configureStore();

class Entrypoint extends Component {

    constructor(props, context) {
        super(props, context);
        this.state = {
            lang: 'en',
        };
    }

    async UNSAFE_componentWillMount() {
        // set language
        const lang = DeviceInfo.getDeviceLocale();
        strings.setLanguage(lang);
        this.setState({ lang });
        /*
        const startScreen = await LocalStore.walletExists() ? 'Login' : 'CreateWallet';
        this.setState({ mounted: true, startScreen });
        */
    }

    render() {
        const { lang } = this.state;
        return (
            <View style={{flex: 1}}>
                <StatusBar backgroundColor={'transparent'} barStyle={'light-content'} translucent={true}/>
                <Provider store={store}>
                    <App lang={lang} />
                </Provider>
                {__DEV__ ? 
                    <Text style={styles.msgStyle}>{'Connect ...'}</Text> : null}
            </View>
        );
    }
}

const styles = {
    msgStyle: {
        position: 'absolute',
        bottom: 2,
        right: 2,
        color: '#aaaaaaaa',
        backgroundColor: 'transparent',
        fontSize: 10,
    }
};

export default Entrypoint;