import * as types from '../actions/types';
import { fromJS } from 'immutable';

export const initialState = fromJS({
    settings: {
        googleApiUrl: 'https://maps.googleapis.com/maps/api',
        localizationUrl: 'http://era.dextechnology.com:8000/uploads/Localization',
        environment: 'temp',
        baseUrl: 'http://89.235.184.229:9067/api',
        blocksPerRequest: 30,
        googleMapApiKey: '',
        blocksTimeout: 10000,
        confirmMode: 0,
        version: '1',
        build: 1,
        hockeyAppApiToken: '',
        hockeyAppId: '',
        minImageSize: 15 * 1024,
        maxImageSize: 20 * 1024,
        jpegQuality: 80,
        licenseUrl: 'http://era.dextechnology.com:8000/uploads/License/Get',
        locationSelectorDebounce: 0,
        defaultPin: '',
        defaultSecretKey: '',
        networkPort: 9066,
        defaultSendAddress: '',
        defaultSendAmount: '',
        defaultSendMessage: '',
        defaultSendHead: '',
        tipsUrl: 'http://era.dextechnology.com:8000',
        invoiceExpire: 3600,
        genesisTimestamp: 1511164500000,
        blockTimestamp: 120,
        formatQRGost: 'ST0001', 
    },
    common: {
        lang: 'en',  
    },
    currencyes: [],
});

export default function configReducer (state = initialState, action) {
    switch (action.type) {
    case types.LOAD_SETTINGS: 
        return state.set('settings', fromJS(action.payload));
    case types.LOAD_CURRENCYES: 
        return state.set('currencyes', fromJS(action.payload));
    case types.RESTORE_REDUX:
        return state
            .mergeDeep({ common: fromJS(action.payload).getIn(['config', 'common'], initialState.get('common')) });
    case types.SET_LANGUAGE:
        return state.setIn(['common', 'lang'], action.payload);
    default:
        return state;
    }
}
