import * as types from '../actions/types';
import { fromJS } from 'immutable';
import { checkError } from '../core/request/checkError';

export const initialState = fromJS({
    telegram: {
        fetching: false,
        error: null,
        data: null,
    },
});

export default function testReducer (state = initialState, action) {
    switch (action.type) {
    case types.TELEGRAM_RESET:
        return state
            .set('telegram', initialState.get('telegram'));
    case types.TELEGRAM_REQUEST:
        //console.log(types.TELEGRAM_REQUEST);
        return state
            .setIn(['telegram', 'fetching'], true)
            .setIn(['telegram', 'error'], null)
            .setIn(['telegram', 'data'], null);
    case types.TELEGRAM_OK:
        return state
            .setIn(['telegram', 'fetching'], false)
            .setIn(['telegram', 'data'], fromJS(action.payload));
    case types.TELEGRAM_FAILED:
        //console.log(types.TELEGRAM_FAILED);
        return state
            .setIn(['telegram', 'fetching'], false)
            .setIn(['telegram', 'error'], checkError(action.payload));
    default:
        return state;
    }
}
