import * as types from '../actions/types';
import { fromJS, isImmutable } from 'immutable';
import { checkError } from '../core/request/checkError';

//http://207.154.242.242:9067/api
/*
            {name: 'Germany Erachain', connectionString: 'http://207.154.241.228:9047/api', port: 9046},
            {name: 'England Erachain', connectionString: 'http://165.227.231.217:9047/api', port: 9046},
            {name: 'USA Erachain', connectionString: 'http://159.203.121.64:9047/api', port: 9046},
            {name: 'For developers', connectionString: 'http://89.235.184.251:9067/api', port: 9066}
*/

export const initialState = fromJS({
    servers: {
        loading: false,
        error: null,
        list: [],
        batchloading: false,
    },
    ping: {
        url: null,
        fetching: false,
        error: null,
        source: 'screen'
    }
});

export default function walletReducer (state = initialState, action) {
    switch (action.type) {
    case types.RESTORE_REDUX:
        return state
            .mergeDeep({ servers: fromJS(action.payload).getIn(['servers', 'servers'], initialState.get('servers')) });
    case types.LOAD_SERVERS:
        return state
            .setIn(['servers', 'batchloading'], true);  
    case types.LOAD_SERVERS_OK:
        return state
            .setIn(['servers', 'error'], null)
            .setIn(['servers', 'batchloading'], false);  
    case types.LOAD_SERVERS_ERR:
        return state
            .setIn(['servers', 'error'], checkError(action.payload))
            .setIn(['servers', 'batchloading'], false);  
    case types.GET_LIST_SERVERS:
        return state
            .setIn(['servers', 'loading'], true)
            .setIn(['servers', 'error'], null);
    case types.GET_LIST_SERVERS_OK:
        return state
            .setIn(['servers', 'loading'], false)
            .setIn(['servers', 'list'], fromJS(action.payload));
    case types.GET_LIST_SERVERS_ERR:
        return state
            .setIn(['servers', 'loading'], false)
            .setIn(['servers', 'error'], checkError(action.payload));
    case types.REMOVE_SERVER:
    case types.ADD_SERVER:
        return state
            .setIn(['servers', 'loading'], true)
            .setIn(['servers', 'error'], null);
    case types.ADD_SERVER_OK:
        return state
            .setIn(['servers', 'loading'], false)
            .updateIn(['servers', 'list'], arr => arr.push(fromJS(action.payload)));
    case types.ADD_SERVER_ERR:
        return state
            .setIn(['servers', 'loading'], false)
            .setIn(['servers', 'error'], checkError(action.payload));
    case types.REMOVE_SERVER_OK:
        return state
            .setIn(['servers', 'loading'], false)
            .updateIn(['servers', 'list'], arr => arr.filter((item) => {
                const server = isImmutable(item) ? item.toJS() : item;
                return server.id !== action.payload;
            }));
    case types.REMOVE_SERVER_ERR:
        return state
            .setIn(['servers', 'loading'], false)
            .setIn(['servers', 'error'], checkError(action.payload));
    case types.PING_SERVER_RESET:
        return state
            .set('ping', initialState.get('ping'));
    case types.PING_SERVER:
        return state
            .setIn(['ping', 'source'], action.source)
            .setIn(['ping', 'fetching'], true)
            .setIn(['ping', 'url'], null)
            .setIn(['ping', 'error'], null);    
    case types.PING_SERVER_SUCCESS:
        return state
            .setIn(['ping', 'url'], action.payload)
            .setIn(['ping', 'fetching'], false);     
    case types.PING_SERVER_ERROR:
        return state
            .setIn(['ping', 'url'], action.url)
            .setIn(['ping', 'fetching'], false)
            .setIn(['ping', 'error'], checkError(action.payload));
    default:
        return state;
    }
}
