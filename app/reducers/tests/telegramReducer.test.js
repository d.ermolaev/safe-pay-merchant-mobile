import reducer, { initialState } from '../telegramReducer';
import { fromJS } from 'immutable';
import * as types from '../../actions/types'; 
import * as telegramActions from '../../actions/telegramActions';
import { fixtures } from '../../core/crypt/tests/fixtures';

describe('telegramReducer', () => {

    it('telegramReducer initial state', () =>
        expect(reducer(undefined, {})).toEqual(initialState)
    );

    it(`${types.TELEGRAM_RESET}`, () => {
        expect(reducer(undefined, telegramActions.actionTelegramReset()))
            .toEqual(
                initialState    
                    .set('telegram', initialState.get('telegram')) 
            );
    });

    it(`${types.TELEGRAM_REQUEST}`, () => {
        expect(reducer(undefined, telegramActions.actionTelegram(fixtures.transaction, fixtures.invoice)))
            .toEqual(
                initialState    
                    .setIn(['telegram', 'fetching'], true)
                    .setIn(['telegram', 'error'], null)
                    .setIn(['telegram', 'data'], null) 
            );
    });

    it(`${types.TELEGRAM_FAILED}`, () => {
        expect(reducer(undefined, telegramActions.actionTelegramFailed('error')))
            .toEqual(
                initialState    
                    .setIn(['telegram', 'fetching'], false)
                    .setIn(['telegram', 'error'], 'error')
            );
    });

    it(`${types.TELEGRAM_OK}`, () => {
        expect(reducer(undefined, telegramActions.actionTelegramOk({})))
            .toEqual(
                initialState    
                    .setIn(['telegram', 'fetching'], false)
                    .setIn(['telegram', 'data'], fromJS({}))
            );
    });

});