import reducer, { initialState } from '../configReducer';
import { fromJS } from 'immutable';
import * as types from '../../actions/types'; 
import * as configActions from '../../actions/configActions';
import AppSettings from '../../core/settings/AppSettings';

const appSettings = new AppSettings();
const settings = appSettings.settings;

const currencies = {
    list: require('../../resources/settings/currency.json'),
};

describe('configReducer', () => {

    it('configReducer initial state', () =>
        expect(reducer(undefined, {})).toEqual(initialState)
    );

    it(`${types.LOAD_SETTINGS}`, () => {
        expect(reducer(undefined, configActions.actionLoadSettings(settings)))
            .toEqual(
                initialState    
                    .set('settings', fromJS(settings)) 
            );
    });

    it(`${types.LOAD_CURRENCYES}`, () => {
        expect(reducer(undefined, configActions.actionLoadCurrencies(currencies.list)))
            .toEqual(
                initialState    
                    .set('currencyes', fromJS(currencies.list)) 
            );
    });

    it(`${types.SET_LANGUAGE}`, () => {
        expect(reducer(undefined, configActions.actionSetLanguage('ru')))
            .toEqual(
                initialState    
                    .setIn(['common', 'lang'], 'ru') 
            );
    });

});