import reducer, { initialState } from '../invoiceReducer';
import * as types from '../../actions/types'; 
import * as invoiceActions from '../../actions/invoiceActions';
import * as dbActions from '../../actions/dbActions';
import { fixtures } from '../../core/crypt/tests/fixtures';

describe('invoiceReducer', () => {

    it('invoiceReducer initial state', () =>
        expect(reducer(undefined, {})).toEqual(initialState)
    );

    it(`${types.GUI_INVOICE_QRCODE}`, () => {
        expect(reducer(undefined, invoiceActions.actionQRCode(true)))
            .toEqual(
                initialState    
                    .setIn(['gui', 'showQRCode'], true)  
            );
    });

    it(`${types.INSERT_INVOICE_OK}`, () => {
        expect(reducer(undefined, dbActions.actionInsertInvoiceOk({})))
            .toEqual(
                initialState    
                    .setIn(['gui', 'showQRCode'], true)  
            );
    });

    it(`${types.CHECK_EXPIRED}`, () => {
        expect(reducer(undefined, invoiceActions.actionCheckExpired()))
            .toEqual(
                initialState    
                    .setIn(['tasks', 'result'], false)
                    .setIn(['tasks', 'error'], null)
                    .setIn(['tasks', 'checking'], true)  
            );
    });

    it(`${types.CHECK_EXPIRED_OK}`, () => {
        expect(reducer(undefined, invoiceActions.actionCheckExpiredOk()))
            .toEqual(
                initialState    
                    .setIn(['tasks', 'result'], true)
                    .setIn(['tasks', 'checking'], false) 
            );
    });

    it(`${types.CHECK_EXPIRED_ERR}`, () => {
        expect(reducer(undefined, invoiceActions.actionCheckExpiredErr('error')))
            .toEqual(
                initialState    
                    .setIn(['tasks', 'error'], 'error')
                    .setIn(['tasks', 'checking'], false) 
            );
    });

    it(`${types.RESTORE_INVOICES}`, () => {
        expect(reducer(undefined, invoiceActions.actionRestoreInvoices(fixtures.timestamp)))
            .toEqual(
                initialState    
                    .setIn(['restore', 'result'], 0)
                    .setIn(['restore', 'error'], null)
                    .setIn(['restore', 'executing'], true)  
            );
    });

    it(`${types.RESTORE_INVOICES_OK}`, () => {
        expect(reducer(undefined, invoiceActions.actionRestoreInvoicesOk(1)))
            .toEqual(
                initialState    
                    .setIn(['restore', 'result'], 1)
                    .setIn(['restore', 'executing'], false) 
            );
    });

    it(`${types.RESTORE_INVOICES_ERR}`, () => {
        expect(reducer(undefined, invoiceActions.actionRestoreInvoicesErr('error')))
            .toEqual(
                initialState    
                    .setIn(['restore', 'error'], 'error')
                    .setIn(['restore', 'executing'], false) 
            );
    });

});