import reducer, { initialState } from '../dbReducer';
import { fromJS } from 'immutable';
import * as types from '../../actions/types'; 
import * as dbActions from '../../actions/dbActions';
import { fixtures } from '../../core/crypt/tests/fixtures';

describe('dbReducer', () => {

    it('dbReducer initial state', () =>
        expect(reducer(undefined, {})).toEqual(initialState)
    );

    it(`${types.OPEN_DB}`, () => {
        expect(reducer(undefined, dbActions.actionOpenDB()))
            .toEqual(
                initialState    
                    .setIn(['db', 'created'], false)
                    .setIn(['db', 'open'], false)
                    .setIn(['db', 'connecting'], true)  
            );
    });

    it(`${types.OPEN_DB_SUCCESS}`, () => {
        expect(reducer(undefined, dbActions.actionOpenDBSuccess()))
            .toEqual(
                initialState    
                    .setIn(['db', 'open'], true)
                    .setIn(['db', 'error'], null)
                    .setIn(['db', 'connecting'], false)  
            );
    });

    it(`${types.OPEN_DB_ERROR}`, () => {
        expect(reducer(undefined, dbActions.actionOpenDBError('error')))
            .toEqual(
                initialState    
                    .setIn(['db', 'open'], false)
                    .setIn(['db', 'error'], 'error')
                    .setIn(['db', 'connecting'], false)  
            );
    });

    it(`${types.DB_EXISTS}`, () => {
        expect(reducer(undefined, dbActions.actionDBExists()))
            .toEqual(
                initialState    
                    .setIn(['db', 'created'], true)  
            );
    });

    it(`${types.CREATE_DB_SUCCESS}`, () => {
        expect(reducer(undefined, dbActions.actionCreateDBSuccess()))
            .toEqual(
                initialState    
                    .setIn(['db', 'created'], true)  
            );
    });

    it(`${types.CREATE_DB_ERROR}`, () => {
        expect(reducer(undefined, dbActions.actionCreateDBError('error')))
            .toEqual(
                initialState    
                    .setIn(['db', 'error'], 'error')
                    .setIn(['db', 'created'], false)  
            );
    });

    it(`${types.CLOSE_DB}`, () => {
        expect(reducer(undefined, dbActions.actionCloseDB()))
            .toEqual(
                initialState    
                    .setIn(['db', 'disconnecting'], true)  
            );
    });

    it(`${types.CLOSE_DB_SUCCESS}`, () => {
        expect(reducer(undefined, dbActions.actionCloseDBSuccess()))
            .toEqual(
                initialState    
                    .setIn(['db', 'open'], false)
                    .setIn(['db', 'disconnecting'], false)  
            );
    });

    it(`${types.CLOSE_DB_ERROR}`, () => {
        expect(reducer(undefined, dbActions.actionCloseDBError('error')))
            .toEqual(
                initialState    
                    .setIn(['db', 'error'], 'error')
                    .setIn(['db', 'disconnecting'], false)  
            );
    });

    it('PARAMS', () => {

        expect(reducer(undefined, dbActions.actionGetParamErr('error')))
            .toEqual(
                initialState    
                    .setIn(['params', 'loading'], false)
                    .setIn(['params', 'values', undefined], null)
                    .setIn(['params', 'error'], 'error')  
            );
        
        expect(reducer(undefined, dbActions.actionSetParamErr('error')))
            .toEqual(
                initialState    
                    .setIn(['params', 'loading'], false)
                    .setIn(['params', 'error'], 'error')  
            );

        expect(reducer(undefined, dbActions.actionGetParam('name')))
            .toEqual(
                initialState    
                    .setIn(['params', 'loading'], true)
                    .setIn(['params', 'error'], null)  
            );

        expect(reducer(undefined, dbActions.actionSetParam({ name: 'name', val: 'value' })))
            .toEqual(
                initialState    
                    .setIn(['params', 'loading'], true)
                    .setIn(['params', 'error'], null)  
            );

        expect(reducer(undefined, dbActions.actionGetParamOk({ name: 'name', val: 'value' })))
            .toEqual(
                initialState    
                    .setIn(['params', 'loading'], false)
                    .setIn(['params', 'values', 'name'], 'value')  
            );

        expect(reducer(undefined, dbActions.actionSetParamOk({ name: 'name', val: 'value' })))
            .toEqual(
                initialState    
                    .setIn(['params', 'loading'], false)
                    .setIn(['params', 'values', 'name'], 'value')  
            );

    });

    it('PIN insert/update/query', () => {

        expect(reducer(undefined, dbActions.actionPinInsert(fixtures.sha256Hash)))
            .toEqual(
                initialState    
                    .setIn(['auth', 'loading'], true) 
            );

        expect(reducer(undefined, dbActions.actionPinUpdate(fixtures.sha256Hash)))
            .toEqual(
                initialState    
                    .setIn(['auth', 'loading'], true) 
            );   
        
        expect(reducer(undefined, dbActions.actionPinQuery()))
            .toEqual(
                initialState    
                    .setIn(['auth', 'loading'], true) 
            ); 

        expect(reducer(undefined, dbActions.actionPinInsertOk(fixtures.pin, fixtures.sha256Hash)))
            .toEqual(
                initialState    
                    .setIn(['auth', 'loading'], false)
                    .setIn(['auth', 'pin'], fixtures.pin)
                    .setIn(['auth', 'hash'], fixtures.sha256Hash) 
            );   
            
        expect(reducer(undefined, dbActions.actionPinUpdateOk(fixtures.pin, fixtures.sha256Hash)))
            .toEqual(
                initialState    
                    .setIn(['auth', 'loading'], false)
                    .setIn(['auth', 'pin'], fixtures.pin)
                    .setIn(['auth', 'hash'], fixtures.sha256Hash)  
            );

        expect(reducer(undefined, dbActions.actionPinQueryOk(fixtures.pin, fixtures.sha256Hash)))
            .toEqual(
                initialState    
                    .setIn(['auth', 'loading'], false)
                    .setIn(['auth', 'pin'], fixtures.pin)
                    .setIn(['auth', 'hash'], fixtures.sha256Hash) 
            );

        expect(reducer(undefined, dbActions.actionPinInsertErr('error')))
            .toEqual(
                initialState    
                    .setIn(['auth', 'loading'], false)
                    .setIn(['auth', 'error'], 'error') 
            );

        expect(reducer(undefined, dbActions.actionPinUpdateErr('error')))
            .toEqual(
                initialState    
                    .setIn(['auth', 'loading'], false)
                    .setIn(['auth', 'error'], 'error') 
            );

        expect(reducer(undefined, dbActions.actionPinQueryErr('error')))
            .toEqual(
                initialState    
                    .setIn(['auth', 'loading'], false)
                    .setIn(['auth', 'error'], 'error') 
            );

    });

    it('Invoice insert/update', () => {
        expect(reducer(undefined, dbActions.actionInsertInvoice({})))
            .toEqual(
                initialState    
                    .setIn(['crud', 'error'], null)
                    .setIn(['crud', 'id'], '')
                    .setIn(['crud', 'executing'], true)
                    .setIn(['crud', 'table'], 'invoices')
                    .setIn(['crud', 'operation'], 'insert')  
            );

        expect(reducer(undefined, dbActions.actionUpdateInvoice(fixtures.dbInvoice)))
            .toEqual(
                initialState    
                    .setIn(['crud', 'error'], null)
                    .setIn(['crud', 'id'], '')
                    .setIn(['crud', 'executing'], true)
                    .setIn(['crud', 'table'], 'invoices')
                    .setIn(['crud', 'operation'], 'update')  
            );
        
        expect(reducer(undefined, dbActions.actionInsertInvoiceOk(fixtures.dbInvoice)))
            .toEqual(
                initialState    
                    .setIn(['crud', 'id'], fixtures.dbInvoice.qrcode)
                    .setIn(['crud', 'executing'], false)  
            );

        expect(reducer(undefined, dbActions.actionUpdateInvoiceOk(fixtures.dbInvoice)))
            .toEqual(
                initialState    
                    .updateIn(['history','data'], arr => {
                        return arr.map(item => {
                            const invoice = item.toJS();
                            if (invoice.signature === fixtures.dbInvoice.signature) {
                                const updated = {
                                    status: fixtures.dbInvoice.status,
                                    attempts: fixtures.dbInvoice.attempts,
                                    lastdt: fixtures.dbInvoice.lastdt,
                                    confirmations: fixtures.dbInvoice.confirmations,
                                    confirmSignature: fixtures.dbInvoice.confirmSignature,
                                };
                                return fromJS({ ...invoice, ...updated });
                            }
                            return item;
                        });
                    })
                    .setIn(['crud', 'id'], fixtures.dbInvoice.qrcode)
                    .setIn(['crud', 'executing'], false)  
            );

        expect(reducer(undefined, dbActions.actionInsertInvoiceErr('error')))
            .toEqual(
                initialState    
                    .setIn(['crud', 'error'], 'error')
                    .setIn(['crud', 'executing'], false)  
            );
        
        expect(reducer(undefined, dbActions.actionUpdateInvoiceErr('error')))
            .toEqual(
                initialState    
                    .setIn(['crud', 'error'], 'error')
                    .setIn(['crud', 'executing'], false)  
            );
    });

    it('UNIQUE_ORDER', () => {
        expect(reducer(undefined, dbActions.actionUnique('1')))
            .toEqual(
                initialState    
                    .setIn(['unique', 'value'], '1')
                    .setIn(['unique', 'valid'], 0)  
            );
        expect(reducer(undefined, dbActions.actionUniqueOk()))
            .toEqual(
                initialState    
                    .setIn(['unique', 'valid'], 1) 
            );
        expect(reducer(undefined, dbActions.actionUniqueErr()))
            .toEqual(
                initialState    
                    .setIn(['unique', 'valid'], -1) 
            );
        expect(reducer(undefined, dbActions.actionMaxOrder()))
            .toEqual(
                initialState    
                    .setIn(['maxOrder', 'loading'], true)
                    .setIn(['maxOrder', 'error'], null) 
            );
        expect(reducer(undefined, dbActions.actionMaxOrderOk(1)))
            .toEqual(
                initialState    
                    .setIn(['maxOrder', 'loading'], false)
                    .setIn(['unique', 'value'], '1')
                    .setIn(['unique', 'valid'], 1) 
            );
        expect(reducer(undefined, dbActions.actionMaxOrderErr('error')))
            .toEqual(
                initialState    
                    .setIn(['unique', 'valid'], -1)
                    .setIn(['maxOrder', 'loading'], false)
                    .setIn(['maxOrder', 'error'], 'error') 
            );
    });

    it('GET INVOICES', () => {
        expect(reducer(undefined, dbActions.actionInvoices(1)))
            .toEqual(
                initialState    
                    .setIn(['history', 'loading'], true)
                    .setIn(['history', 'error'], null)
                    .setIn(['history', 'data'], fromJS([]))  
            );
        expect(reducer(undefined, dbActions.actionInvoicesOk([])))
            .toEqual(
                initialState    
                    .setIn(['history', 'loading'], false)
                    .updateIn(['history', 'data'], arr => arr.concat(fromJS([])))  
            );
        expect(reducer(undefined, dbActions.actionInvoicesErr('error')))
            .toEqual(
                initialState    
                    .setIn(['history', 'loading'], false)
                    .setIn(['history', 'error'], 'error')  
            );
    });

    it('GET PAGES OF INVOICES', () => {
        expect(reducer(undefined, dbActions.actionInvoicesPages()))
            .toEqual(
                initialState    
                    .setIn(['history', 'loading'], true)
                    .setIn(['history', 'error'], null)  
            );
        expect(reducer(undefined, dbActions.actionInvoicesPagesOk(1)))
            .toEqual(
                initialState    
                    .setIn(['history', 'loading'], false)
                    .setIn(['history', 'pages'], 1)  
            );
        expect(reducer(undefined, dbActions.actionInvoicesPagesErr('error')))
            .toEqual(
                initialState    
                    .setIn(['history', 'loading'], false)
                    .setIn(['history', 'error'], 'error')  
            );
    });

    it('PATTERNS', () => {
        expect(reducer(undefined, dbActions.actionPatternsQuery()))
            .toEqual(
                initialState    
                    .setIn(['patterns', 'loading'], true)
                    .setIn(['patterns', 'error'], null)  
            );
        expect(reducer(undefined, dbActions.actionPatternsQueryOk([])))
            .toEqual(
                initialState    
                    .setIn(['patterns', 'loading'], false)
                    .setIn(['patterns', 'data'], fromJS([]))  
            );
        expect(reducer(undefined, dbActions.actionPatternsQueryErr('error')))
            .toEqual(
                initialState    
                    .setIn(['patterns', 'loading'], false)
                    .setIn(['patterns', 'error'], 'error')  
            );
    });

    it('PATTERN insert/update/delete', () => {
        expect(reducer(undefined, dbActions.actionPatternInsert(fixtures.pattern)))
            .toEqual(
                initialState    
                    .setIn(['patterns', 'loading'], true)
                    .setIn(['patterns', 'error'], null)  
            );

        expect(reducer(undefined, dbActions.actionPatternInsertOk(fixtures.pattern)))
            .toEqual(
                initialState    
                    .setIn(['patterns', 'loading'], false)
                    .updateIn(['patterns','data'], arr => arr.push(fromJS(fixtures.pattern)))  
            );

        expect(reducer(undefined, dbActions.actionPatternInsertErr('error')))
            .toEqual(
                initialState    
                    .setIn(['patterns', 'loading'], false)
                    .setIn(['patterns', 'error'], 'error')  
            );

        expect(reducer(undefined, dbActions.actionPatternUpdate(fixtures.pattern)))
            .toEqual(
                initialState    
                    .setIn(['patterns', 'loading'], true)
                    .setIn(['patterns', 'error'], null)  
            );

        expect(reducer(undefined, dbActions.actionPatternUpdateOk(fixtures.pattern)))
            .toEqual(
                initialState    
                    .setIn(['patterns', 'loading'], false)
                    .updateIn(['patterns','data'], arr => {
                        return arr.map(item => {
                            const pattern = item.toJS();
                            if (pattern.id === fixtures.pattern.id) {
                                const updated = {
                                    name: fixtures.pattern.name,
                                    pattern: fixtures.pattern.pattern,
                                    inwork: fixtures.pattern.inwork,
                                };
                                return fromJS({ ...pattern, ...updated });
                            }
                            return item;
                        });
                    })  
            );

        expect(reducer(undefined, dbActions.actionPatternUpdateErr('error')))
            .toEqual(
                initialState    
                    .setIn(['patterns', 'loading'], false)
                    .setIn(['patterns', 'error'], 'error')  
            );

        expect(reducer(undefined, dbActions.actionPatternDelete(fixtures.pattern)))
            .toEqual(
                initialState    
                    .setIn(['patterns', 'loading'], true)
                    .setIn(['patterns', 'error'], null)  
            );

        expect(reducer(undefined, dbActions.actionPatternDeleteOk(fixtures.pattern)))
            .toEqual(
                initialState    
                    .setIn(['patterns', 'loading'], false)
                    .updateIn(['patterns','data'], arr => {
                        return arr.filter(item => {
                            const pattern = item.toJS();
                            return pattern.id !== fixtures.pattern.id;
                        });
                    })  
            );

        expect(reducer(undefined, dbActions.actionPatternDeleteErr('error')))
            .toEqual(
                initialState    
                    .setIn(['patterns', 'loading'], false)
                    .setIn(['patterns', 'error'], 'error')  
            );
        
    });


    it('REQUISITE insert/update/query', () => {
        expect(reducer(undefined, dbActions.actionAddRequisite(fixtures.requisite)))
            .toEqual(
                initialState    
                    .setIn(['requisite', 'loading'], true)
                    .setIn(['requisite', 'error'], null)  
            );
        
        expect(reducer(undefined, dbActions.actionUpdateRequisite(fixtures.requisite)))
            .toEqual(
                initialState    
                    .setIn(['requisite', 'loading'], true)
                    .setIn(['requisite', 'error'], null)  
            );

        expect(reducer(undefined, dbActions.actionGetRequisite(fixtures.requisite)))
            .toEqual(
                initialState    
                    .setIn(['requisite', 'loading'], true)
                    .setIn(['requisite', 'error'], null)  
            );

        expect(reducer(undefined, dbActions.actionAddRequisiteOk(fixtures.requisite)))
            .toEqual(
                initialState    
                    .setIn(['requisite', 'loading'], false)
                    .setIn(['requisite', 'recipient'], fixtures.requisite.recipient)
                    .setIn(['requisite', 'acct'], fixtures.requisite.acct)
                    .setIn(['requisite', 'bank'], fixtures.requisite.bank)
                    .setIn(['requisite', 'cacct'], fixtures.requisite.cacct)
                    .setIn(['requisite', 'bik'], fixtures.requisite.bik)
                    .setIn(['requisite', 'inn'], fixtures.requisite.inn)
                    .setIn(['requisite', 'kpp'], fixtures.requisite.kpp)  
            );

        expect(reducer(undefined, dbActions.actionUpdateRequisiteOk(fixtures.requisite)))
            .toEqual(
                initialState    
                    .setIn(['requisite', 'loading'], false)
                    .setIn(['requisite', 'recipient'], fixtures.requisite.recipient)
                    .setIn(['requisite', 'acct'], fixtures.requisite.acct)
                    .setIn(['requisite', 'bank'], fixtures.requisite.bank)
                    .setIn(['requisite', 'cacct'], fixtures.requisite.cacct)
                    .setIn(['requisite', 'bik'], fixtures.requisite.bik)
                    .setIn(['requisite', 'inn'], fixtures.requisite.inn)
                    .setIn(['requisite', 'kpp'], fixtures.requisite.kpp)  
            );

        expect(reducer(undefined, dbActions.actionGetRequisiteOk(fixtures.requisite)))
            .toEqual(
                initialState    
                    .setIn(['requisite', 'loading'], false)
                    .setIn(['requisite', 'recipient'], fixtures.requisite.recipient)
                    .setIn(['requisite', 'acct'], fixtures.requisite.acct)
                    .setIn(['requisite', 'bank'], fixtures.requisite.bank)
                    .setIn(['requisite', 'cacct'], fixtures.requisite.cacct)
                    .setIn(['requisite', 'bik'], fixtures.requisite.bik)
                    .setIn(['requisite', 'inn'], fixtures.requisite.inn)
                    .setIn(['requisite', 'kpp'], fixtures.requisite.kpp)  
            );

        expect(reducer(undefined, dbActions.actionAddRequisiteErr('error')))
            .toEqual(
                initialState    
                    .setIn(['requisite', 'loading'], false)
                    .setIn(['requisite', 'error'], 'error')  
            );
        
        expect(reducer(undefined, dbActions.actionUpdateRequisiteErr('error')))
            .toEqual(
                initialState    
                    .setIn(['requisite', 'loading'], false)
                    .setIn(['requisite', 'error'], 'error')  
            );

        expect(reducer(undefined, dbActions.actionGetRequisiteErr('error')))
            .toEqual(
                initialState    
                    .setIn(['requisite', 'loading'], false)
                    .setIn(['requisite', 'error'], 'error')  
            );

        
    });

});