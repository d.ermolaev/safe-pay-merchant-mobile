import reducer, { initialState } from '../blockchainReducer';
import { fromJS } from 'immutable';
import * as types from '../../actions/types'; 
import * as pollActions from '../../actions/pollActions';
import * as blockchainActions from '../../actions/blockchainActions';
import * as dbActions from '../../actions/dbActions';
import { fixtures } from '../../core/crypt/tests/fixtures';
import DB from '../../store/DB';

const params = {
    address: fixtures.address,
    type: 0,
    timestamp: fixtures.timestamp, 
};

describe('blockchainReducer', () => {

    it('blockchainReducer initial state', () =>
        expect(reducer(undefined, {})).toEqual(initialState)
    );

    it(`${types.SCHEDULE_POLL}`, () => {
        expect(reducer(undefined, pollActions.actionSchedulePoll()))
            .toEqual(
                initialState    
                    .setIn(['schedulePoll', 'error'], null)
                    .setIn(['schedulePoll', 'executing'], true)  
            );
    });

    it(`${types.SCHEDULE_POLL_ERR}`, () => {
        expect(reducer(undefined, pollActions.actionSchedulePollErr('error')))
            .toEqual(
                initialState    
                    .setIn(['schedulePoll', 'error'], 'error')
                    .setIn(['schedulePoll', 'executing'], false)  
            );
    });

    it(`${types.SCHEDULE_POLL_OK}`, () => {
        expect(reducer(undefined, pollActions.actionSchedulePollOk()))
            .toEqual(
                initialState    
                    .setIn(['schedulePoll', 'executing'], false)  
            );
    });

    it(`${types.START_POLL}`, () => {
        expect(reducer(undefined, pollActions.actionStartPoll()))
            .toEqual(
                initialState    
                    .setIn(['poll', 'error'], null)
                    .setIn(['poll', 'executing'], true)  
            );
    });

    it(`${types.POLL_ERR}`, () => {
        expect(reducer(undefined, pollActions.actionPollErr('error')))
            .toEqual(
                initialState    
                    .setIn(['poll', 'error'], 'error')
                    .setIn(['poll', 'executing'], false)  
            );
    });
    
    it(`${types.POLL_OK}`, () => {
        expect(reducer(undefined, pollActions.actionPollOk()))
            .toEqual(
                initialState    
                    .setIn(['poll', 'executing'], false)  
            );
    });

    it(`${types.GET_HEIGHT_CHAIN}`, () => {
        expect(reducer(undefined, blockchainActions.actionGetHeightChain()))
            .toEqual(
                initialState    
                    .setIn(['common', 'error'], null)
                    .setIn(['common', 'loading'], true)  
            );
    });

    it(`${types.GET_HEIGHT_CHAIN_ERR}`, () => {
        expect(reducer(undefined, blockchainActions.actionGetHeightChainErr('error')))
            .toEqual(
                initialState    
                    .setIn(['common', 'error'], 'error')
                    .setIn(['common', 'loading'], false)  
            );
    });

    it(`${types.GET_HEIGHT_CHAIN_OK}`, () => {
        expect(reducer(undefined, blockchainActions.actionGetHeightChainOK(555)))
            .toEqual(
                initialState    
                    .setIn(['common', 'lastBlock'], 555)
                    .setIn(['common', 'loading'], false)  
            );
    });

    it(`${types.GET_UNCONFIRMEDCOMES}`, () => {
        expect(reducer(undefined, blockchainActions.actionGetUnconfirmed(params)))
            .toEqual(
                initialState    
                    .setIn(['unconfirmed', 'data'], fromJS([]))
                    .setIn(['unconfirmed', 'loading'], true)
                    .setIn(['unconfirmed', 'error'], null)  
            );
    });

    it(`${types.GET_UNCONFIRMEDCOMES_OK}`, () => {
        expect(reducer(undefined, blockchainActions.actionGetUnconfirmedOk([])))
            .toEqual(
                initialState    
                    .setIn(['unconfirmed', 'data'], fromJS([]))
                    .setIn(['unconfirmed', 'loading'], false)  
            );
    });

    it(`${types.GET_UNCONFIRMEDCOMES_ERR}`, () => {
        expect(reducer(undefined, blockchainActions.actionGetUnconfirmedErr('error')))
            .toEqual(
                initialState    
                    .setIn(['unconfirmed', 'error'], 'error')
                    .setIn(['unconfirmed', 'loading'], false)  
            );
    });

    it(`${types.GET_INCOMINGFROMBLOCK}`, () => {
        expect(reducer(undefined, blockchainActions.actionGetIncoming(params)))
            .toEqual(
                initialState    
                    .setIn(['incoming', 'data'], fromJS([]))
                    .setIn(['incoming', 'loading'], true)  
            );
    });

    it(`${types.GET_INCOMINGFROMBLOCK_OK}`, () => {
        expect(reducer(undefined, blockchainActions.actionGetIncomingOk([])))
            .toEqual(
                initialState    
                    .setIn(['incoming', 'data'], fromJS([]))
                    .setIn(['incoming', 'loading'], false)  
            );
    });

    it(`${types.GET_INCOMINGFROMBLOCK_ERR}`, () => {
        expect(reducer(undefined, blockchainActions.actionGetIncomingErr('error')))
            .toEqual(
                initialState    
                    .setIn(['incoming', 'error'], 'error')
                    .setIn(['incoming', 'loading'], false)
            );
    });

    it(`${types.HEIGHT_SAVE}`, () => {
        expect(reducer(undefined, dbActions.actionHeightSave(555)))
            .toEqual(
                initialState    
                    .setIn(['common', 'loading'], true)
                    .setIn(['common', 'error'], null)  
            );
    });

    it(`${types.HEIGHT_SAVE_OK}`, () => {
        expect(reducer(undefined, dbActions.actionHeightSaveOk()))
            .toEqual(
                initialState    
                    .setIn(['common', 'loading'], false)  
            );
    });

    it(`${types.HEIGHT_SAVE_ERR}`, () => {
        expect(reducer(undefined, dbActions.actionHeightSaveErr('error')))
            .toEqual(
                initialState    
                    .setIn(['common', 'loading'], false)
                    .setIn(['common', 'error'], 'error') 
            );
    });

    it(`${types.HEIGHT_RESTORE}`, () => {
        expect(reducer(undefined, dbActions.actionHeightRestore()))
            .toEqual(
                initialState    
                    .setIn(['common', 'loading'], true)
                    .setIn(['common', 'error'], null)  
            );
    });

    it(`${types.HEIGHT_RESTORE_OK}`, () => {
        expect(reducer(undefined, dbActions.actionHeightRestoreOk(555)))
            .toEqual(
                initialState    
                    .setIn(['common', 'loading'], false)
                    .setIn(['common', 'lastBlock'], 555)  
            );
    });

    it(`${types.HEIGHT_RESTORE_ERR}`, () => {
        expect(reducer(undefined, dbActions.actionHeightRestoreErr('error')))
            .toEqual(
                initialState    
                    .setIn(['common', 'loading'], false)
                    .setIn(['common', 'error'], 'error') 
            );
    });

});