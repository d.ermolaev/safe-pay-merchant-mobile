import reducer, { initialState } from '../serversReducer';
import { fromJS } from 'immutable';
import * as types from '../../actions/types'; 
import * as serversActions from '../../actions/serversActions';
import { fixtures } from '../../core/crypt/tests/fixtures';

describe('serversReducer', () => {

    it('serversReducer initial state', () =>
        expect(reducer(undefined, {})).toEqual(initialState)
    );

    it(`${types.LOAD_SERVERS}`, () => {
        expect(reducer(undefined, serversActions.actionLoadServers()))
            .toEqual(
                initialState    
                    .setIn(['servers', 'batchloading'], true) 
            );
    });

    it(`${types.LOAD_SERVERS_OK}`, () => {
        expect(reducer(undefined, serversActions.actionLoadServersOk()))
            .toEqual(
                initialState    
                    .setIn(['servers', 'error'], null)
                    .setIn(['servers', 'batchloading'], false) 
            );
    });

    it(`${types.LOAD_SERVERS_ERR}`, () => {
        expect(reducer(undefined, serversActions.actionLoadServersErr('error')))
            .toEqual(
                initialState    
                    .setIn(['servers', 'error'], 'error')
                    .setIn(['servers', 'batchloading'], false) 
            );
    });

    it(`${types.GET_LIST_SERVERS}`, () => {
        expect(reducer(undefined, serversActions.actionListServers()))
            .toEqual(
                initialState    
                    .setIn(['servers', 'loading'], true)
                    .setIn(['servers', 'error'], null) 
            );
    });

    it(`${types.GET_LIST_SERVERS_OK}`, () => {
        expect(reducer(undefined, serversActions.actionListServersOk([])))
            .toEqual(
                initialState    
                    .setIn(['servers', 'loading'], false)
                    .setIn(['servers', 'list'], fromJS([])) 
            );
    });

    it(`${types.GET_LIST_SERVERS_ERR}`, () => {
        expect(reducer(undefined, serversActions.actionListServersErr('error')))
            .toEqual(
                initialState    
                    .setIn(['servers', 'loading'], false)
                    .setIn(['servers', 'error'], 'error') 
            );
    });

    it(`${types.REMOVE_SERVER}`, () => {
        expect(reducer(undefined, serversActions.actionRemoveServer(1)))
            .toEqual(
                initialState    
                    .setIn(['servers', 'loading'], true)
                    .setIn(['servers', 'error'], null)
            );
    });

    it(`${types.REMOVE_SERVER_ERR}`, () => {
        expect(reducer(undefined, serversActions.actionRemoveServerErr('error')))
            .toEqual(
                initialState    
                    .setIn(['servers', 'loading'], false)
                    .setIn(['servers', 'error'], 'error')
            );
    });

    it(`${types.REMOVE_SERVER_OK}`, () => {
        expect(reducer(undefined, serversActions.actionRemoveServerOk(1)))
            .toEqual(
                initialState    
                    .setIn(['servers', 'loading'], false)
                    .updateIn(['servers', 'list'], arr => arr.filter((item) => {
                        const server = item.toJS();
                        return server.id !== 1;
                    }))
            );
    });

    it(`${types.ADD_SERVER}`, () => {
        expect(reducer(undefined, serversActions.actionAddServer({})))
            .toEqual(
                initialState    
                    .setIn(['servers', 'loading'], true)
                    .setIn(['servers', 'error'], null)
            );
    });

    it(`${types.ADD_SERVER_ERR}`, () => {
        expect(reducer(undefined, serversActions.actionAddServerErr('error')))
            .toEqual(
                initialState    
                    .setIn(['servers', 'loading'], false)
                    .setIn(['servers', 'error'], 'error')
            );
    });

    it(`${types.ADD_SERVER_OK}`, () => {
        expect(reducer(undefined, serversActions.actionAddServerOk(fixtures.server)))
            .toEqual(
                initialState    
                    .setIn(['servers', 'loading'], false)
                    .updateIn(['servers', 'list'], arr => arr.push(fromJS(fixtures.server)))
            );
    });

    it(`${types.PING_SERVER_RESET}`, () => {
        expect(reducer(undefined, serversActions.actionPingServerReset()))
            .toEqual(
                initialState    
                    .set('ping', initialState.get('ping'))
            );
    });
    
    it(`${types.PING_SERVER}`, () => {
        expect(reducer(undefined, serversActions.actionPingServer(fixtures.server.connectionString, 'testing')))
            .toEqual(
                initialState    
                    .setIn(['ping', 'source'], 'testing')
                    .setIn(['ping', 'fetching'], true)
                    .setIn(['ping', 'url'], null)
                    .setIn(['ping', 'error'], null)
            );
    });

    it(`${types.PING_SERVER_ERROR}`, () => {
        expect(reducer(undefined, serversActions.actionPingServerError('error', fixtures.server.connectionString)))
            .toEqual(
                initialState    
                    .setIn(['ping', 'url'], fixtures.server.connectionString)
                    .setIn(['ping', 'fetching'], false)
                    .setIn(['ping', 'error'], 'error')
            );
    });

    it(`${types.PING_SERVER_SUCCESS}`, () => {
        expect(reducer(undefined, serversActions.actionPingServerSuccess(fixtures.server.connectionString)))
            .toEqual(
                initialState    
                    .setIn(['ping', 'url'], fixtures.server.connectionString)
                    .setIn(['ping', 'fetching'], false)
            );
    });

});