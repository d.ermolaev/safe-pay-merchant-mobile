import { convertPattern } from '../invoiceReducer';
import { fixtures } from '../../core/crypt/tests/fixtures';

it('Convert Patterns', () => {
    const pattern = '@date @num @sum @user';
    const invoice = {
        date: fixtures.timestamp,
        num: 1,
        sum: 100.99,
        user: '+7-999-9-999999',
    };
    const result = '2019-07-05 1 100.99 +7-999-9-999999';
    expect(result).toMatch(convertPattern(invoice, pattern));
});

