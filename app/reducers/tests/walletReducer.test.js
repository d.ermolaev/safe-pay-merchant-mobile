import reducer, { initialState } from '../walletReducer';
import { fromJS } from 'immutable';
import * as types from '../../actions/types'; 
import * as walletActions from '../../actions/walletActions';
import * as dbActions from '../../actions/dbActions';
import { fixtures } from '../../core/crypt/tests/fixtures';

const wallet = {
    address: fixtures.address,
    seed: fixtures.seedBase58,
    keyPair: {
        secretKey: fixtures.privateKey,
        publicKey: fixtures.publicKey,
    },
};

describe('walletReducer', () => {

    it('walletReducer initial state', () =>
        expect(reducer(undefined, {})).toEqual(initialState)
    );

    it(`${types.CREATE_WALLET}`, () => {
        expect(reducer(undefined, walletActions.actionCreateWallet()))
            .toEqual(
                initialState    
                    .setIn(['wallet', 'creating'], true)
                    .setIn(['wallet', 'error'], null)  
            );
    });

    it(`${types.CREATE_WALLET_ERROR}`, () => {
        expect(reducer(undefined, walletActions.actionCreateWalletError('error')))
            .toEqual(
                initialState    
                    .setIn(['wallet', 'creating'], false)
                    .setIn(['wallet', 'error'], 'error')  
            );
    });

    it(`${types.CREATE_WALLET_OK}`, () => {
        expect(reducer(undefined, walletActions.actionCreateWalletOk()))
            .toEqual(
                initialState    
                    .setIn(['wallet', 'creating'], false) 
            );
    });

    it(`${types.WALLET_QUERY}`, () => {
        expect(reducer(undefined, dbActions.actionWalletQuery()))
            .toEqual(
                initialState    
                    .setIn(['wallet', 'loading'], true)
                    .setIn(['wallet', 'error'], null)
                    .setIn(['wallet', 'keyPair'], null)  
            );
    });

    it(`${types.WALLET_QUERY_ERR}`, () => {
        expect(reducer(undefined, dbActions.actionWalletQueryErr('error')))
            .toEqual(
                initialState    
                    .setIn(['wallet', 'loading'], false)
                    .setIn(['wallet', 'error'], 'error')  
            );
    });

    it(`${types.WALLET_QUERY_OK}`, () => {
        expect(reducer(undefined, dbActions.actionWalletQueryOk(wallet)))
            .toEqual(
                initialState    
                    .setIn(['wallet', 'loading'], false)
                    .setIn(['wallet', 'address'], wallet.address)
                    .setIn(['wallet', 'seed'], fromJS(wallet.seed))
                    .setIn(['wallet', 'keyPair'], fromJS(wallet.keyPair))  
            );
    });

    it(`${types.WALLET_CLEAR}`, () => {
        expect(reducer(undefined, walletActions.actionWalletClear()))
            .toEqual(
                initialState    
            );
    });

    it(`${types.SET_PIN_CODE}`, () => {
        expect(reducer(undefined, walletActions.actionSetPinCode('1234')))
            .toEqual(
                initialState    
                    .setIn(['wallet', 'pin'], '1234')  
            );
    });

    it(`${types.SET_SEED}`, () => {
        expect(reducer(undefined, walletActions.actionSetSeed(fixtures.seedBase58)))
            .toEqual(
                initialState    
                    .setIn(['wallet', 'seed'], fromJS(fixtures.seedBase58))  
            );
    });

    it(`${types.SET_ADDRESS}`, () => {
        expect(reducer(undefined, walletActions.actionSetAddress(fixtures.address)))
            .toEqual(
                initialState    
                    .setIn(['wallet', 'address'], fixtures.address)
                    .setIn(['wallet', 'loading'], true)  
            );
    });

    it(`${types.SET_ADDRESS_OK}`, () => {
        expect(reducer(undefined, walletActions.actionSetAddressOk(fixtures.address)))
            .toEqual(
                initialState    
                    .setIn(['wallet', 'address'], fixtures.address)
                    .setIn(['wallet', 'loading'], false)  
            );
    });

    it(`${types.SET_ADDRESS_ERR}`, () => {
        expect(reducer(undefined, walletActions.actionSetAddressErr('error')))
            .toEqual(
                initialState    
                    .setIn(['wallet', 'error'], 'error')
                    .setIn(['wallet', 'loading'], false)  
            );
    });

    it(`${types.SET_KEYPAIR}`, () => {
        expect(reducer(undefined, walletActions.actionSetKeyPair(wallet.keyPair)))
            .toEqual(
                initialState    
                    .setIn(['wallet', 'keyPair'], fromJS(wallet.keyPair))  
            );
    });

});