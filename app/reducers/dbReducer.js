import * as types from '../actions/types';
import { fromJS, isImmutable } from 'immutable';
import { checkError } from '../core/request/checkError';

//http://207.154.242.242:9067/api

export const initialState = fromJS({
    auth: {
        loading: false,
        error: null,
        pin: null,
        hash: null,
    },
    db: {
        connecting: false,
        disconnecting: false,
        open: false,
        created: false,
        error: null,
    },
    crud: {
        id: '',
        executing: false,
        table: 'invoices',
        operation: 'insert',
        error: null,
    },
    unique: {
        value: '1',
        valid: 0, // > 0 - YES, < 0 - NO
    },
    maxOrder: {
        loading: false,
        error: null,
    },
    history: {
        loading: false,
        data: fromJS([]),
        error: null,
        pages: 0,
    },
    patterns: {
        loading: false,
        data: fromJS([]),
        error: null, 
    },
    params: {
        loading: false,
        values: {},
        error: null,
    },
    backup: {
        executing: false,
        error: null,
        path: '',
    },
    restore: {
        executing: false,
        error: null,
    },
    requisite: {
        loading: false,
        error: null,
        recipient: '',
        acct: '',
        cacct: '',
        bank: '', 
        bik: '',
        inn: '',
        kpp: '',
    },
});

export default function walletReducer (state = initialState, action) {
    //console.log('QRSP', action );
    switch (action.type) {
    case types.REDUX_DB_RESET:
        return state
            .set('history', initialState.get('history'))
            .set('params', initialState.get('params'))
            .set('patterns', initialState.get('patterns'));
    case types.BACKUP_RESET:
        return state
            .setIn(['backup', 'path'], '')
            .setIn(['backup', 'executing'], false)
            .setIn(['backup', 'error'], null);
    case types.BACKUP_DB:
        return state
            .setIn(['backup', 'path'], '')
            .setIn(['backup', 'executing'], true)
            .setIn(['backup', 'error'], null);
    case types.BACKUP_DB_OK:
        return state
            .setIn(['backup', 'path'], action.payload)
            .setIn(['backup', 'executing'], false);
    case types.BACKUP_DB_ERR:
        return state
            .setIn(['backup', 'executing'], false)
            .setIn(['backup', 'error'], checkError(action.payload));
    case types.RESTORE_RESET:
        return state
            .setIn(['restore', 'executing'], false)
            .setIn(['restore', 'error'], null);
    case types.RESTORE_DB:
        return state
            .setIn(['restore', 'executing'], true)
            .setIn(['restore', 'error'], null);
    case types.RESTORE_DB_OK:
        return state
            .setIn(['restore', 'executing'], false);
    case types.RESTORE_DB_ERR:
        return state
            .setIn(['restore', 'executing'], false)
            .setIn(['restore', 'error'], checkError(action.payload));
    case types.OPEN_DB:
        return state
            .setIn(['db', 'created'], false)
            .setIn(['db', 'open'], false)
            .setIn(['db', 'connecting'], true);
    case types.OPEN_DB_SUCCESS:
        return state
            .setIn(['db', 'open'], true)
            .setIn(['db', 'error'], null)
            .setIn(['db', 'connecting'], false);
    case types.OPEN_DB_ERROR:
        return state
            .setIn(['db', 'open'], false)
            .setIn(['db', 'error'], checkError(action.payload))
            .setIn(['db', 'connecting'], false);
    case types.DB_EXISTS:
        return state
            .setIn(['db', 'created'], true);
    case types.CREATE_DB_SUCCESS:
        return state
            .setIn(['db', 'created'], true);
    case types.CREATE_DB_ERROR:
        return state
            .setIn(['db', 'error'], checkError(action.payload))
            .setIn(['db', 'created'], false);
    case types.CLOSE_DB:
        return state
            .setIn(['db', 'disconnecting'], true);
    case types.CLOSE_DB_SUCCESS:
        return state
            .setIn(['db', 'open'], false)
            .setIn(['db', 'disconnecting'], false);
    case types.CLOSE_DB_ERROR:
        return state
            .setIn(['db', 'error'], checkError(action.payload))
            .setIn(['db', 'disconnecting'], false);
    case types.PIN_INSERT:
    case types.PIN_UPDATE:
    case types.PIN_QUERY:    
        return state
            .setIn(['auth', 'loading'], true);
    case types.PIN_INSERT_OK:
    case types.PIN_UPDATE_OK:
    case types.PIN_QUERY_OK: 
        return state
            .setIn(['auth', 'loading'], false)
            .setIn(['auth', 'pin'], action.pin)
            .setIn(['auth', 'hash'], action.payload);
    case types.PIN_INSERT_ERR:
    case types.PIN_UPDATE_ERR:
    case types.PIN_QUERY_ERR:  
        return state
            .setIn(['auth', 'loading'], false)
            .setIn(['auth', 'error'], checkError(action.payload));
    case types.INSERT_INVOICE:
        return state
            .setIn(['crud', 'error'], null)
            .setIn(['crud', 'id'], '')
            .setIn(['crud', 'executing'], true)
            .setIn(['crud', 'table'], 'invoices')
            .setIn(['crud', 'operation'], 'insert');
    case types.UPDATE_INVOICE:
        return state
            .setIn(['crud', 'error'], null)
            .setIn(['crud', 'id'], '')
            .setIn(['crud', 'executing'], true)
            .setIn(['crud', 'table'], 'invoices')
            .setIn(['crud', 'operation'], 'update');
    case types.UPDATE_INVOICE_OK:
        return state
            .updateIn(['history','data'], arr => {
                return arr.map(item => {
                    const invoice = isImmutable(item) ? item.toJS() : item;
                    if (invoice.signature === action.payload.signature) {
                        const updated = {
                            status: action.payload.status,
                            attempts: action.payload.attempts,
                            lastdt: action.payload.lastdt,
                            confirmations: action.payload.confirmations,
                            confirmSignature: action.payload.confirmSignature,
                        };
                        return fromJS({ ...invoice, ...updated });
                    }
                    return item;
                });
            })
            .setIn(['crud', 'id'], action.payload.qrcode)
            .setIn(['crud', 'executing'], false);
    case types.INSERT_INVOICE_OK:
        return state
            .setIn(['crud', 'id'], action.payload.qrcode)
            .setIn(['crud', 'executing'], false);
    case types.INSERT_INVOICE_ERR:
    case types.UPDATE_INVOICE_ERR:
        return state
            .setIn(['crud', 'error'], checkError(action.payload))
            .setIn(['crud', 'executing'], false);
    case types.CHECK_UNIQUE_ORDER:
        return state
            .setIn(['unique', 'value'], action.payload)
            .setIn(['unique', 'valid'], 0);
    case types.CHECK_UNIQUE_ORDER_OK:
        return state
            .setIn(['unique', 'valid'], 1);
    case types.CHECK_UNIQUE_ORDER_ERR:
        return state
            .setIn(['unique', 'valid'], -1);
    case types.GET_MAX_NUM_ORDER:
        return state
            .setIn(['maxOrder', 'loading'], true)
            .setIn(['maxOrder', 'error'], null);
    case types.GET_MAX_NUM_ORDER_OK:
        return state
            .setIn(['maxOrder', 'loading'], false)
            .setIn(['unique', 'value'], action.payload.toString())
            .setIn(['unique', 'valid'], 1);
    case types.GET_MAX_NUM_ORDER_ERR:
        return state
            .setIn(['unique', 'valid'], -1)
            .setIn(['maxOrder', 'loading'], false)
            .setIn(['maxOrder', 'error'], checkError(action.payload));
    case types.GET_INVOICES:
        // if page === 1
        if (action.payload === 1) {
            return state
                .setIn(['history', 'loading'], true)
                .setIn(['history', 'error'], null)
                .setIn(['history', 'data'], fromJS([]));
        } 
        return state
            .setIn(['history', 'loading'], true)
            .setIn(['history', 'error'], null);
    case types.GET_INVOICES_OK:
        return state
            .setIn(['history', 'loading'], false)
            .updateIn(['history', 'data'], arr => arr.concat(fromJS(action.payload)));
    case types.GET_INVOICES_ERR:
        return state
            .setIn(['history', 'loading'], false)
            .setIn(['history', 'error'], checkError(action.payload));
    case types.GET_INVOICES_PAGES:
        return state
            .setIn(['history', 'loading'], true)
            .setIn(['history', 'error'], null);
    case types.GET_INVOICES_PAGES_OK:
        return state
            .setIn(['history', 'loading'], false)
            .setIn(['history', 'pages'], action.payload);
    case types.GET_INVOICES_PAGES_ERR:
        return state
            .setIn(['history', 'loading'], false)
            .setIn(['history', 'error'], checkError(action.payload));
    case types.PATTERNS_QUERY:
        return state
            .setIn(['patterns', 'loading'], true)
            .setIn(['patterns', 'error'], null);
    case types.PATTERNS_QUERY_OK:
        return state
            .setIn(['patterns', 'loading'], false)
            .setIn(['patterns', 'data'], fromJS(action.payload));
    case types.PATTERNS_QUERY_ERR:
        return state
            .setIn(['patterns', 'loading'], false)
            .setIn(['patterns', 'error'], checkError(action.payload));
    case types.PATTERN_INSERT:
    case types.PATTERN_UPDATE:
    case types.PATTERN_DELETE:
        return state
            .setIn(['patterns', 'loading'], true)
            .setIn(['patterns', 'error'], null);

    case types.PATTERN_INSERT_OK:
        return state
            .setIn(['patterns', 'loading'], false)
            .updateIn(['patterns','data'], arr => {
                //console.log(arr);
                return arr.push(fromJS(action.payload));
            });
    case types.PATTERN_UPDATE_OK:
        return state
            .setIn(['patterns', 'loading'], false)
            .updateIn(['patterns','data'], arr => {
                return arr.map(item => {
                    const pattern = isImmutable(item) ? item.toJS() : item;
                    if (pattern.id === action.payload.id) {
                        const updated = {
                            name: action.payload.name,
                            pattern: action.payload.pattern,
                            inwork: action.payload.inwork,
                        };
                        return fromJS({ ...pattern, ...updated });
                    }
                    return item;
                });
            });
    case types.PATTERN_DELETE_OK:
        return state
            .setIn(['patterns', 'loading'], false)
            .updateIn(['patterns','data'], arr => {
                return arr.filter(item => {
                    const pattern = isImmutable(item) ? item.toJS() : item;
                    return pattern.id !== action.payload.id;
                });
            });
    case types.PATTERN_INSERT_ERR:
    case types.PATTERN_UPDATE_ERR:
    case types.PATTERN_DELETE_ERR:
        return state
            .setIn(['patterns', 'loading'], false)
            .setIn(['patterns', 'error'], checkError(action.payload));
    case types.GET_PARAM:
        return state
            .setIn(['params', 'loading'], true)
            .setIn(['params', 'error'], null);
    case types.GET_PARAM_OK:
        return state
            .setIn(['params', 'loading'], false)
            .setIn(['params', 'values', action.payload.name], action.payload.val);
    case types.GET_PARAM_ERR:
        return state
            .setIn(['params', 'loading'], false)
            .setIn(['params', 'values', action.name], null)
            .setIn(['params', 'error'], checkError(action.payload));
    case types.SET_PARAM:
        return state
            .setIn(['params', 'loading'], true)
            .setIn(['params', 'error'], null);
    case types.SET_PARAM_OK:
        return state
            .setIn(['params', 'loading'], false)
            .setIn(['params', 'values', action.payload.name], action.payload.val);
    case types.SET_PARAM_ERR:
        return state
            .setIn(['params', 'loading'], false)
            .setIn(['params', 'error'], checkError(action.payload));
    case types.ADD_REQUISITE:
    case types.UPDATE_REQUISITE:
    case types.GET_REQUISITE:
        return state
            .setIn(['requisite', 'error'], null)
            .setIn(['requisite', 'loading'], true);
    case types.ADD_REQUISITE_OK:
    case types.UPDATE_REQUISITE_OK:
    case types.GET_REQUISITE_OK:
        return state
            .setIn(['requisite', 'loading'], false)
            .setIn(['requisite', 'recipient'], action.payload.recipient)
            .setIn(['requisite', 'acct'], action.payload.acct)
            .setIn(['requisite', 'bank'], action.payload.bank)
            .setIn(['requisite', 'cacct'], action.payload.cacct)
            .setIn(['requisite', 'bik'], action.payload.bik)
            .setIn(['requisite', 'inn'], action.payload.inn)
            .setIn(['requisite', 'kpp'], action.payload.kpp);
    case types.ADD_REQUISITE_ERR:
    case types.UPDATE_REQUISITE_ERR:
    case types.GET_REQUISITE_ERR:
        return state
            .setIn(['requisite', 'error'], checkError(action.payload))
            .setIn(['requisite', 'loading'], false);
    default:
        return state;
    }
}