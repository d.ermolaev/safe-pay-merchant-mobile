import * as types from '../actions/types';
import { fromJS } from 'immutable';
import { checkError } from '../core/request/checkError';

export const initialState = fromJS({
    wallet: {
        creating: false,
        pin: null,     // string
        seed: null,    // string
        keyPair: null, // KeyPair
        address: null, // string
        error: null,
        loading: false,
    }
});

export default function walletReducer (state = initialState, action) {
    switch (action.type) {
    case types.RESTORE_REDUX:
        return state
            .mergeDeep({ wallet: fromJS(action.payload).getIn(['wallet', 'wallet'], initialState.get('wallet')) });
    case types.CREATE_WALLET:
        return state
            .setIn(['wallet', 'creating'], true)
            .setIn(['wallet', 'error'], null);
    case types.SET_PIN_CODE:
        return state.setIn(['wallet', 'pin'], action.payload);
    case types.SET_SEED:
        return state.setIn(['wallet', 'seed'], fromJS(action.payload));
    case types.SET_KEYPAIR:
        return state.setIn(['wallet', 'keyPair'], fromJS(action.payload));
    case types.SET_ADDRESS:
        return state
            .setIn(['wallet', 'address'], action.payload)
            .setIn(['wallet', 'loading'], true);
    case types.SET_ADDRESS_OK:
        return state
            .setIn(['wallet', 'address'], action.payload)
            .setIn(['wallet', 'loading'], false); 
    case types.SET_ADDRESS_ERR:
        return state
            .setIn(['wallet', 'error'], checkError(action.payload))
            .setIn(['wallet', 'loading'], false); 
    case types.CREATE_WALLET_OK:
        return state
            .setIn(['wallet', 'creating'], false);
    case types.CREATE_WALLET_ERROR:
        return state
            .setIn(['wallet', 'creating'], false)
            .setIn(['wallet', 'error'], checkError(action.payload));
    case types.WALLET_QUERY:
        return state
            .setIn(['wallet', 'loading'], true)
            .setIn(['wallet', 'error'], null)
            .setIn(['wallet', 'keyPair'], null);
    case types.WALLET_QUERY_OK:
        return state
            .setIn(['wallet', 'loading'], false)
            .setIn(['wallet', 'address'], action.payload.address)
            .setIn(['wallet', 'seed'], fromJS(action.payload.seed))
            .setIn(['wallet', 'keyPair'], fromJS(action.payload.keyPair));
    case types.WALLET_QUERY_ERR:
        return state
            .setIn(['wallet', 'loading'], false)
            .setIn(['wallet', 'error'], checkError(action.payload));
    case types.WALLET_CLEAR:
        return state
            .setIn(['wallet', 'seed'], null)
            .setIn(['wallet', 'address'], null)
            .setIn(['wallet', 'keyPair'], null)
            .setIn(['wallet', 'loading'], false)
            .setIn(['wallet', 'error'], null);
    default:
        return state;
    }
}
