/* 
 * combines all th existing reducers
 */

import telegramReducer from './telegramReducer';
import configReducer from './configReducer';
import walletReducer from './walletReducer';
import serversReducer from './serversReducer';
import invoiceReducer from './invoiceReducer';
import dbReducer from './dbReducer';
import blockchainReducer from './blockchainReducer';
import { combineReducers } from 'redux-immutable';

/**
 * Merges the main reducer with the router state and dynamically injected reducers
 */
export default function createReducer() {
    const rootReducer = combineReducers({
        db: dbReducer,
        invoice: invoiceReducer,
        telegram: telegramReducer,
        config: configReducer,
        wallet: walletReducer,
        servers: serversReducer,
        blockchain: blockchainReducer,
    });
    return rootReducer;
}