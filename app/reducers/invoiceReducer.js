import * as types from '../actions/types';
import { fromJS } from 'immutable';
import { strings } from '../config/localize';
import moment from 'moment/min/moment-with-locales';
import { checkError } from '../core/request/checkError';

//http://207.154.242.242:9067/api

export const convertPattern = (invoice, pattern) => {
    let s = pattern.replace(/@date/g, moment(invoice.date).format('YYYY-MM-DD'));
    s = s.replace(/@num/g, invoice.num.toString());
    s = s.replace(/@sum/g, invoice.sum.toString());
    s = s.replace(/@user/g, invoice.user);
    s = s.replace(/@vat/g, strings['Pattern including VAT']);
    s = s.replace(/@novat/g, strings['Pattern without VAT']);
    return s;
};

export const initialState = fromJS({
    invoice: {
        date: '',
        num: 1,
        qrcode: undefined,
        expire: '',
        user: '',
        curr: 643,
        sum: 0,
        title: '',
        description: '',
        detail: '',
        confirmations: 0,
        confirmSignature: '',
    },
    tasks: {
        checking: false,
        error: null,
        result: false,
    }, 
    restore: {
        executing: false,
        error: null,
        result: 0,
    },
    gui: {
        showQRCode: false,
    }
});

export default function walletReducer (state = initialState, action) {
    switch (action.type) {

    case types.GUI_INVOICE_QRCODE:
        return state
            .setIn(['gui', 'showQRCode'], action.payload);

    case types.INSERT_INVOICE_OK:
        return state
            .setIn(['gui', 'showQRCode'], true);
    
    case types.CHECK_EXPIRED:
        return state
            .setIn(['tasks', 'result'], false)
            .setIn(['tasks', 'error'], null)
            .setIn(['tasks', 'checking'], true);
    case types.CHECK_EXPIRED_OK:   
        return state
            .setIn(['tasks', 'result'], true)
            .setIn(['tasks', 'checking'], false);    
    case types.CHECK_EXPIRED_ERR:   
        return state
            .setIn(['tasks', 'error'], checkError(action.payload))
            .setIn(['tasks', 'checking'], false);  
    case types.RESTORE_INVOICES:
        return state
            .setIn(['restore', 'result'], 0)
            .setIn(['restore', 'error'], null)
            .setIn(['restore', 'executing'], true);
    case types.RESTORE_INVOICES_OK:
        return state
            .setIn(['restore', 'result'], action.payload)
            .setIn(['restore', 'executing'], false);
    case types.RESTORE_INVOICES_ERR:
        return state
            .setIn(['restore', 'error'], checkError(action.payload))
            .setIn(['restore', 'executing'], false);
    default:
        return state;
    }
}
