import * as types from '../actions/types';
import { fromJS } from 'immutable';
import { checkError } from '../core/request/checkError';

export const initialState = fromJS({
    common: {
        count: 10,
        loading: false,
        error: null,
        lastBlock: 1,
        type: 31,
    },
    unconfirmed: {
        loading: false,
        error: null,
        data: [],
    },
    incoming: {
        loading: false,
        error: null,
        data: [],
    },
    poll: {
        executing: false,
        error: null,
    },
    schedulePoll: {
        executing: false,
        error: null,
    },
});

export default function walletReducer (state = initialState, action) {
    switch (action.type) {
    case types.RESTORE_REDUX:
        return state
            .mergeDeep({ common: fromJS(action.payload).getIn(['blockchain', 'common'], initialState.get('common')) });
    case types.SCHEDULE_POLL:
        return state
            .setIn(['schedulePoll', 'error'], null)
            .setIn(['schedulePoll', 'executing'], true);
    case types.SCHEDULE_POLL_STOP:
        return state
            .setIn(['schedulePoll', 'executing'], false);
    case types.SCHEDULE_POLL_OK:
        return state
            .setIn(['schedulePoll', 'executing'], false);
    case types.SCHEDULE_POLL_ERR:
        return state
            .setIn(['schedulePoll', 'error'], checkError(action.payload))
            .setIn(['schedulePoll', 'executing'], false);            
    case types.START_POLL:
        return state
            .setIn(['poll', 'error'], null)
            .setIn(['poll', 'executing'], true);
    case types.POLL_OK:
        return state
            .setIn(['poll', 'executing'], false);
    case types.POLL_ERR:
        return state
            .setIn(['poll', 'error'], checkError(action.payload))
            .setIn(['poll', 'executing'], false);
    case types.GET_HEIGHT_CHAIN:
        return state
            .setIn(['common', 'error'], null)
            .setIn(['common', 'loading'], true);
    case types.GET_HEIGHT_CHAIN_OK:
        return state
            .setIn(['common', 'lastBlock'], action.payload)
            .setIn(['common', 'loading'], false);
    case types.GET_HEIGHT_CHAIN_ERR:
        return state
            .setIn(['common', 'error'], checkError(action.payload))
            .setIn(['common', 'loading'], false);
    case types.GET_UNCONFIRMEDCOMES:
        return state
            .setIn(['unconfirmed', 'data'], fromJS([]))
            .setIn(['unconfirmed', 'loading'], true)
            .setIn(['unconfirmed', 'error'], null);
    case types.GET_UNCONFIRMEDCOMES_OK:
        return state
            .setIn(['unconfirmed', 'data'], fromJS(action.payload))
            .setIn(['unconfirmed', 'loading'], false);
    case types.GET_UNCONFIRMEDCOMES_ERR:
        return state
            .setIn(['unconfirmed', 'error'], checkError(action.payload))
            .setIn(['unconfirmed', 'loading'], false);
    case types.GET_INCOMINGFROMBLOCK:
        return state
            .setIn(['incoming', 'data'], fromJS([]))
            .setIn(['incoming', 'loading'], true);
    case types.GET_INCOMINGFROMBLOCK_OK:
        return state
            .setIn(['incoming', 'data'], fromJS(action.payload))
            .setIn(['incoming', 'loading'], false);
    case types.GET_INCOMINGFROMBLOCK_ERR:
        return state
            .setIn(['incoming', 'error'], checkError(action.payload))
            .setIn(['incoming', 'loading'], false);
    case types.HEIGHT_SAVE:
    case types.HEIGHT_RESTORE:
        return state
            .setIn(['common', 'loading'], true)
            .setIn(['common', 'error'], null);
    case types.HEIGHT_RESTORE_OK:
        return state
            .setIn(['common', 'loading'], false)
            .setIn(['common', 'lastBlock'], action.payload);
    case types.HEIGHT_SAVE_OK:
        return state
            .setIn(['common', 'loading'], false);
    case types.HEIGHT_RESTORE_ERR:
    case types.HEIGHT_SAVE_ERR:
        return state
            .setIn(['common', 'loading'], false)
            .setIn(['common', 'error'], checkError(action.payload));      
    default:
        return state;
    }
}
