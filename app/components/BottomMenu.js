import React, {Component} from 'react';
import PropTypes from 'prop-types';
import {Modal, ScrollView, Text, TouchableOpacity, View} from 'react-native';
import {dHeight, styles} from '../config/styles';
import Toast from 'react-native-simple-toast';

export class BottomMenu extends Component {
    close = () => {
        this.setState({visible: false});
        this.props.onClose && this.props.onClose();
    };

    toggle = () => {
        this.setState({visible: !this.state.visible && this.props.actions.length !== 0});
        if (this.props.actions.length === 0 && this.props.emptyMessage) {
            Toast.show(this.props.emptyMessage);
        }
    };

    constructor(props) {
        super(props);
        this.state = {visible: false};
    }

    UNSAFE_componentWillReceiveProps(props) {
        this.setState({visible: props.visible === undefined ? this.state.visible : props.visible});
    }

    render() {
        const actions = this.props.actions;
        const content = this.renderContent();
        const maxHeight = dHeight * 0.70;
        //console.log({ maxHeight: height });
        return (
            <View>
                <Modal animationType={'none'} visible={this.state.visible} transparent={true}
                    onRequestClose={this.close}>
                    <TouchableOpacity style={styles.register.wholeContainer} activeOpacity={1} onPress={this.close}>
                        <View style={{ alignSelf: 'stretch' }}>
                            <ScrollView style={{ maxHeight: maxHeight }} contentContainerStyle={styles.register.menuContainer}>
                                {actions.map((action, n) => {
                                    if (action.visible && !action.visible()) return null;

                                    return (
                                        <View key={n} style={{alignSelf: 'stretch'}}>
                                            <TouchableOpacity onPress={this.action(action)}
                                                style={styles.register.menuItemContainer}>
                                                <Text style={styles.register.menuText}
                                                    numberOfLines={1}>{`${action.name} `}</Text>
                                            </TouchableOpacity>
                                            {this.props.actions.length === n + 1 ? null :
                                                <View style={styles.shared.line}/>}
                                        </View>
                                    );
                                })}
                            </ScrollView>
                        </View>
                    </TouchableOpacity>
                </Modal>

                {content}
            </View>
        );
    }

    renderContent() {
        if (!this.props.children) {
            return null;
        }

        return (
            <TouchableOpacity onPress={this.toggle}>{this.props.children}</TouchableOpacity>
        );
    }

    action = (action) => {
        return () => {
            this.close();
            this.props.onSelect(action);
        };
    }
}

BottomMenu.propTypes = {
    visible: PropTypes.bool,
    onClose: PropTypes.func,
    actions: PropTypes.array,
    emptyMessage: PropTypes.string,
    children: PropTypes.object,
    onSelect: PropTypes.func,
};
