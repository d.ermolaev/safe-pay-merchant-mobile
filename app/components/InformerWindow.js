/**
 * Created by kirill on 18.10.2017.
 */
import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { ActivityIndicator, Animated, Modal, TouchableOpacity, View } from 'react-native';
import { styles, whiteColor } from '../config/styles';
import { ImageResources } from './ImageResources';

export class InformerWindow extends Component {
    constructor(props) {
        super(props);
        this.state = {visible: false};
    }

    render() {
        return (<View style={{width: 44, height: 44, alignItems: 'center', justifyContent: 'center'}}>
            <TouchableOpacity style={{width: 44, height: 44, alignItems: 'center', justifyContent: 'center'}}
                onPress={this.onPress}>
                <Animated.Image source={ImageResources.icon_qustion}
                    style={{...styles.navigationBar.iconButton, ...{tintColor: whiteColor}}}/>
            </TouchableOpacity>
            <Modal animationType={'none'} visible={this.state.visible} transparent={true}
                onRequestClose={this.close}>
                <TouchableOpacity style={styles.register.wholeContainer} activeOpacity={1} onPress={this.close}>
                    <View style={styles.register.menuContainer}>
                        {
                            this.props.loading ?
                                <ActivityIndicator animating={true} color={'#aaa'} size={'large'}/> :
                                <Animated.Text style={styles.navigationBar.tip}>{this.props.tip}</Animated.Text>
                        }
                    </View>
                </TouchableOpacity>
            </Modal>
        </View>);
    }

    onPress = () => {
        this.props.onPress();
        this.setState({visible: true});
    };

    close = () => {
        this.setState({visible: false});
    };

}

InformerWindow.propTypes = {
    tip: PropTypes.string,
    onPress: PropTypes.func,
    loading: PropTypes.bool,
};

