import { Component } from 'react';
import { BackHandler, Keyboard } from 'react-native';
import * as navigationActions from '../actions/navigationActions';

export class BackHandled extends Component {

    static navigationOptions = {
        header: false,
        drawerLockMode: 'locked-closed',
    };

    componentDidMount() {
        BackHandler.addEventListener('hardwareBackPress', this.back);
    }

    componentWillUnmount() {
        BackHandler.removeEventListener('hardwareBackPress', this.back);
    }

    // return : true
    back = () => {
        //console.log('onBack');
        //navigationActions.back();
        //Keyboard.dismiss();
        
        const activeRoute = navigationActions.getActiveRoute();
        if (activeRoute !== 'MyWallet') {
            navigationActions.back();
            Keyboard.dismiss();
        } else {
            BackHandler.exitApp();
        }
        
        return true;
    }

}
