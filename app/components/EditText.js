import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Animated, Image, TextInput, TouchableOpacity, View, Clipboard } from 'react-native';
import { FontNames } from '../config/FontNames';
import { primaryColor3, styles, textPlaceholderColor, fontSizeLabel, fontSize47 } from '../config/styles';
import { 
    ImageResources,
    Toaster
} from '../components';
import { strings } from '../config/localize';


export class EditText extends Component {

    UNSAFE_componentWillMount() {
        this.setState({height: fontSize47, text: this.props.placeholder});
    }

    copy = () => {
        Clipboard.setString(this.state.text);
        console.log(this.state.text);
        Toaster.error(strings['Copied to clipboard']);
    };

    render() {
        const big = this.props.big;
        const copy = this.props.allowPaste ? (
            <TouchableOpacity onPress={this.copy} style={{ ...styles.register.button }}>
                <Image style={styles.sent.image} source={ImageResources.icon_copy}/>
            </TouchableOpacity> )
            : <View/>;

        const text = (
            <View style={{
                ...styles.register.border, ...{
                    flex: 1,
                    flexDirection: 'row',
                    paddingVertical: 8,
                }
            }}>
                <TextInput
                    multiline={big}
                    numberOfLines={big ? 100 : 1}
                    
                    value={this.state.text}
                    style={{
                        ...styles.register.textEdit,
                        ...{
                            flex: 1,
                        }
                    }}
                    placeholderTextColor={textPlaceholderColor}
                    placeholder={this.props.placeholder}
                    underlineColorAndroid={'transparent'}
                    onChangeText={this.textChanged}
                    autoCompleteType={this.props.autoCompleteType?this.props.autoCompleteType:'off'}
                />
                {copy}
            </View>);

        return (

            <View style={{paddingVertical: 8}}>
                <Animated.Text style={{
                    fontFamily: FontNames.ProstoLight,
                    fontSize: fontSizeLabel,
                    color: primaryColor3,
                    marginBottom: 5
                }}>{this.props.title}</Animated.Text>
                <View style={{ justifyContent: 'flex-end', alignItems: 'center' }}>
                    {text}
                </View>
            </View>
        );
    }

    textChanged = (text) => {
        if (this.props.maxLength) {
            if (this.props.maxLength < text.length) {
                this.setState({text: text.slice(0, this.props.maxLength)});
                this.props.onTextChange(text);
                return;
            }
        }
        this.setState({text});
        this.props.onTextChange(text);
    }

    updateSize = (height) => {
        this.setState({
            height
        });
    }
}

EditText.propTypes = {
    title: PropTypes.string,
    placeholder: PropTypes.string,
    onTextChange: PropTypes.func,
    big: PropTypes.bool,
    allowPaste: PropTypes.bool,
    maxLength: PropTypes.number,
    autoCompleteType: PropTypes.string,
};


