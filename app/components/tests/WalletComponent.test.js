import React from 'react';
import renderer from 'react-test-renderer';
import { Provider } from 'react-redux';
import configureStore from 'redux-mock-store';
import { fromJS } from 'immutable';
import { WalletComponent } from '../WalletComponent';
import { initialState as walletState } from '../../reducers/walletReducer';
import { initialState as dbState } from '../../reducers/dbReducer';

const middlewares = [];
const mockStore = configureStore(middlewares);
const store = mockStore(fromJS({ ...walletState.toJS(), ...dbState.toJS() }));

it('WalletComponent renders correctly', async () => {
    const r = renderer
        .create(
            <Provider store={store}>
                <WalletComponent />
            </Provider>
        );
    const json = r.toJSON();
    expect(json).toMatchSnapshot();
});