import React from 'react';
import renderer from 'react-test-renderer';
import { FooterBtn } from '../FooterBtn';

it('FooterBtn renders correctly and onPress', () => {
    const onPressEvent = jest.fn();
    const r = renderer
        .create(<FooterBtn 
            onPress={onPressEvent}
        />
        );
    const json = r.toJSON();
    const i = r.getInstance();
    i.onPressTest();
    expect(json).toMatchSnapshot();
    expect(onPressEvent).toBeCalled(); 
});