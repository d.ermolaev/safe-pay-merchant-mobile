import React from 'react';
import renderer from 'react-test-renderer';
import { InfoItem } from '../InfoItem';

it('InfoItem renders correctly and onPress', () => {
    const onPressEvent = jest.fn();
    const r = renderer
        .create(<InfoItem 
            title='title'
            value='value'
            onPress={onPressEvent}
        />
        );
    const json = r.toJSON();
    const i = r.getInstance();
    i.copyToClipboard();
    expect(json).toMatchSnapshot();
    expect(onPressEvent).toBeCalled(); 
});