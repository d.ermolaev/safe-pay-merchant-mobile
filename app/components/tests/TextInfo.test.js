import React from 'react';
import renderer from 'react-test-renderer';
import { TextInfo } from '../TextInfo';

it('TextInfo renders correctly and onPress', async () => {
    const r = renderer
        .create(<TextInfo 
            title={'Title'}
            displayText={'Display text'}
            copyText={'Copy text'}
            big={false}
            allowCopy={true}
            qrCode={true}
        />
        );
    const json = r.toJSON();
    expect(json).toMatchSnapshot();
});