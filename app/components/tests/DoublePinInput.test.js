import React from 'react';
import renderer from 'react-test-renderer';
import { DoublePinInput } from '../DoublePinInput';

it('DoublePinInput renders correctly and onPress', async () => {
    const onComplete = jest.fn();
    const r = renderer
        .create(<DoublePinInput 
            onComplete={onComplete}
        />
        );
    const json = r.toJSON();
    const i = r.getInstance();
    i.setPin('1234');
    await i.nextStep();
    i.setPin('1234');
    await i.nextStep();
    expect(json).toMatchSnapshot();
    expect(onComplete).toBeCalled(); 
});