import React from 'react';
import renderer from 'react-test-renderer';
import { LargeLogo } from '../LargeLogo';

it('LargeLogo renders correctly and onPress', async () => {
    const r = renderer
        .create(<LargeLogo></LargeLogo>);
    const json = r.toJSON();
    expect(json).toMatchSnapshot();
});