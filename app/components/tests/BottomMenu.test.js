import React from 'react';
import renderer from 'react-test-renderer';
import { BottomMenu } from '../BottomMenu';

const actions = [
    {
        id: 1,
        name: 'name 1',
        pattern: 'pattern 1',
    },
    {
        id: 2,
        name: 'name 2',
        pattern: 'pattern 2',
    },
];

it('BottomMenu renders correctly and onSelect', () => {
    const onSelect = jest.fn((action) => {
        console.log(action);
    });
    const r = renderer
        .create(<BottomMenu 
            actions={actions}
            visible={true}
            onSelect={onSelect}
        />
        );
    const json = r.toJSON();
    const i = r.getInstance();
    i.action(actions[0]).call();
    expect(json).toMatchSnapshot();
    expect(onSelect).toBeCalled(); 
});




