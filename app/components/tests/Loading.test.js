import React from 'react';
import renderer from 'react-test-renderer';
import { Loading } from '../Loading';

it('Loading renders correctly', () => {
    const r = renderer
        .create(<Loading 
            state={true}
        />
        );
    const json = r.toJSON();
    expect(json).toMatchSnapshot();
});