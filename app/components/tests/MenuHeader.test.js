import React from 'react';
import renderer from 'react-test-renderer';
import { MenuHeader } from '../MenuHeader';

it('MenuHeader renders correctly', () => {
    const r = renderer
        .create(<MenuHeader />
        );
    const json = r.toJSON();
    expect(json).toMatchSnapshot();
});