import React from 'react';
import renderer from 'react-test-renderer';
import { Splash } from '../Splash';

it('Splash renders correctly', () => {
    const r = renderer
        .create(<Splash />
        );
    const json = r.toJSON();
    expect(json).toMatchSnapshot();
});