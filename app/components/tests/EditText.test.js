import React from 'react';
import renderer from 'react-test-renderer';
import { EditText } from '../EditText';

it('EditText renders correctly and onPress', async () => {
    const onTextChange = jest.fn();
    const r = renderer
        .create(<EditText 
            title='Edit text'
            placeholder='Enter text'
            onTextChange={onTextChange}
            big={false}
            allowPaste={false}
            maxLength={20}
        />
        );
    const json = r.toJSON();
    const i = r.getInstance();
    i.textChanged('1234');
    expect(json).toMatchSnapshot();
    expect(onTextChange).toBeCalled(); 
});