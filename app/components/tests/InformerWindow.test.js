import React from 'react';
import renderer from 'react-test-renderer';
import { InformerWindow } from '../InformerWindow';

it('InformerWindow renders correctly and onPress', () => {
    const onPressEvent = jest.fn();
    const r = renderer
        .create(<InformerWindow 
            tip='Tip'
            onPress={onPressEvent}
            loading={false}
        />
        );
    const json = r.toJSON();
    const i = r.getInstance();
    i.onPress();
    expect(json).toMatchSnapshot();
    expect(onPressEvent).toBeCalled(); 
});