import React from 'react';
import renderer from 'react-test-renderer';
import { PinInput } from '../PinInput';

it('PinInput renders correctly and onPress', async () => {
    const onPinChaged = jest.fn();
    const r = renderer
        .create(<PinInput 
            pin={''}
            placeholder={'enter PIN'}
            onPinChaged={onPinChaged}
        />
        );
    const json = r.toJSON();
    const i = r.getInstance();
    i.setPin('1234');
    expect(json).toMatchSnapshot();
    expect(onPinChaged).toBeCalled(); 
});