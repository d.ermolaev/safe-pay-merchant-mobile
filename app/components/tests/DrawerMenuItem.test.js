import React from 'react';
import renderer from 'react-test-renderer';
import { ImageResources } from '../../components';
import { DrawerMenuItem } from '../DrawerMenuItem';

it('DrawerMenuItem renders correctly and onPress', async () => {
    const onPress = jest.fn((item) => item);
    const item = {
        id: '1',
        route: 'route',
        icon: ImageResources.icon_wallet,
        title: 'title',
    };
    const r = renderer
        .create(<DrawerMenuItem 
            onPress={onPress}
            id={item.id}
            route={item.route}
            icon={item.icon}
            title={item.title}
        />
        );
    const json = r.toJSON();
    const i = r.getInstance();
    i.onPressTest(item);
    expect(json).toMatchSnapshot();
    expect(onPress).toBeCalled(); 
});