import React from 'react';
import renderer from 'react-test-renderer';
import { CopySecretKey } from '../CopySecretKey';
import KeyPair from '../../core/account/KeyPair'; 
import { fixtures } from '../../core/crypt/tests/fixtures';

it('CopySecretKey renders correctly and onPress', async () => {
    const keyPair = new KeyPair(fixtures.privateKey, fixtures.publicKey);
    const keys = await keyPair.toString();
    const onComplete = jest.fn();
    const r = renderer
        .create(<CopySecretKey 
            keys={keys}
            onComplete={onComplete}
        />
        );
    const json = r.toJSON();
    const i = r.getInstance();
    i.copyToClipboard();
    await i.pasteFromClipboard();
    expect(json).toMatchSnapshot();
    expect(onComplete).toBeCalled(); 
});