import React from 'react';
import renderer from 'react-test-renderer';
import { ImageResources } from '../../components';
import { NavigationBar } from '../NavigationBar';

it('NavigationBar renders correctly and onPress', () => {
    const onLeft = jest.fn();
    const onRight = jest.fn();

    const r = renderer
        .create(<NavigationBar 
            title={'Title'}
            leftButton={ImageResources.icon_menu}
            rightButton={ImageResources.icon_menu}
            onLeftButtonPress={onLeft}
            onRightButtonPress={onRight}
        />
        );
    const json = r.toJSON();
    const i = r.getInstance();
    i.onLeftPressTest();
    i.onRightPressTest();
    expect(json).toMatchSnapshot();
    expect(onLeft).toBeCalled(); 
    expect(onRight).toBeCalled(); 
});