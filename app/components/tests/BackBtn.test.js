import React from 'react';
import renderer from 'react-test-renderer';
import { BackBtn } from '../BackBtn';

it('BackBtn renders correctly and onPress', () => {
    const onPressEvent = jest.fn();
    const r = renderer
        .create(<BackBtn 
            onPress={onPressEvent}
        />
        );
    const json = r.toJSON();
    const i = r.getInstance();
    i.onPress();
    expect(json).toMatchSnapshot();
    expect(onPressEvent).toBeCalled(); 
});