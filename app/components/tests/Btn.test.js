import React from 'react';
import renderer from 'react-test-renderer';
import { Btn } from '../Btn/Btn';

it('Btn renders correctly and onPress', () => {
    const onPressEvent = jest.fn();
    const r = renderer
        .create(<Btn 
            title={'Next'} 
            fill={true} 
            disabled={false}
            customBtnStyles={{
                width: 300,
                marginBottom: 60
            }}
            onPress={onPressEvent}
        />
        );
    const json = r.toJSON();
    const i = r.getInstance();
    i.onPress();
    expect(json).toMatchSnapshot();
    expect(onPressEvent).toBeCalled(); 
});