import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { ActivityIndicator, View } from 'react-native';
import { AlignItems, JustifyContent, Positions } from '../config/RNStyles';
import { dHeight, dWidth } from '../config/styles';

export class Loading extends Component {
    constructor(props) {
        super(props);
        this.state = {loading: false};
    }

    UNSAFE_componentWillReceiveProps(nextProps) {
        this.setState({loading: nextProps.state});
    }

    render() {
        if (this.state.loading) {
            return ( <View style={styles}><ActivityIndicator animating={true} color={'#fff'} size={'large'}/></View> );
        } else {
            return ( <View/> );
        }
    }
}

Loading.propTypes = {
    state: PropTypes.bool
};

const styles = {
    position: Positions.absolute,
    top: 0,
    left: 0,
    width: dWidth,
    height: dHeight,
    backgroundColor: 'rgba(87,99,115,0.40)',
    alignItems: AlignItems.center,
    justifyContent: JustifyContent.center,
    zIndex: 1
};
