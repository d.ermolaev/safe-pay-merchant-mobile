import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Animated, Text, TouchableOpacity, View } from 'react-native';
import { styles } from '../config/styles';

export class NavigationBar extends Component {

    constructor(props) {
        super(props);
        this.state = {
            tip: '',
        };
    }

    onLeftPressTest() {
        this.props.onLeftButtonPress();
    }

    onRightPressTest() {
        this.props.onRightButtonPress();
    }

    render() {

        return (
            <View style={styles.navigationBar.container}>
                <View style={styles.navigationBar.navigationHeader}>
                    <View style={{flex: 1, flexDirection: 'row', alignItems: 'center', justifyContent: 'flex-start'}}>
                        {this.leftButton()}
                        {this.props.rightButton ? <View style={{width: 44}}/> : null}
                        {this.props.rightWidget ? <View style={{width: 44}}/> : null}
                    </View>
                    <Text style={styles.navigationBar.title} >{`${this.props.title} `}</Text>
                    <View style={{flex: 1, flexDirection: 'row', alignItems: 'center', justifyContent: 'flex-end'}}>
                        {/*<InformerWindow onPress={this.onTipPress} tip={this.state.tip}/>*/}
                        {this.rightButton()}
                        {this.props.rightWidget ? this.props.rightWidget : null}
                    </View>
                </View>
            </View>);
    }

    onTipPress = () => {
        //stores.tipStore.load().then(tip => this.setState({tip: stores.tipStore.tip}));
    }

    leftButton() {
        if (this.props.leftButton) {
            return (
                <TouchableOpacity onPress={this.props.onLeftButtonPress}>
                    <View>
                        <Animated.Image
                            source={this.props.leftButton}
                            style={styles.navigationBar.iconButton}
                            resizeMode='center'
                        />
                    </View>
                </TouchableOpacity>
            );
        }

        return (
            <View style={{width: 44}}/>
        );
    }

    rightButton() {
        if (this.props.rightButton) {
            return (
                <TouchableOpacity style={{
                    width: 44,
                    height: 44,
                }} onPress={this.props.onRightButtonPress}>
                    <Animated.Image
                        source={this.props.rightButton}
                        style={styles.navigationBar.iconButton}
                        resizeMode='center'
                    />
                </TouchableOpacity>
            );
        } else {
            return null;
        }
    }
}

NavigationBar.propTypes = {
    leftButton: PropTypes.number,
    rightButton: PropTypes.object,
    rightWidget: PropTypes.object,
    title: PropTypes.string,
    onLeftButtonPress: PropTypes.func,
    onRightButtonPress: PropTypes.func,
};