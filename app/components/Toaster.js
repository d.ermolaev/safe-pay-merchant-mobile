import Toast from 'react-native-simple-toast';

export class Toaster {
    static errorCodes = {
        'Invalid pin': 'Пин-код введен не верно'
    };

    // error: Error | string, 
    // silent: boolean = false
    static error(error, silent) {
        if (!silent) {
            const message = error instanceof Error ? Toaster.getErrorCode(error.message) : error.toString();
            Toast.show(message);
        } else {
            /* eslint:disable-next-line */
            console.error(error);
        }
    }

    // code: string
    // result : string
    static getErrorCode(code) {
        return Toaster.errorCodes[code] || code;
    }
}
