import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Text, View, TouchableOpacity } from 'react-native';
import { disableColor } from '../../config/styles';
import { BtnStyles } from './styles';

export class Btn extends Component {

    onPress = () => {
        this.props.onPress();
    }

    render() {
        let textStyle = {...BtnStyles.text, ...(this.props.customTextStyles || {})};
        let btnStyle = {...BtnStyles.container, ...(this.props.customBtnStyles || {})};
        if (this.props.fill) {
            textStyle = {...textStyle, ...BtnStyles.textFill};
            btnStyle = {...btnStyle, ...BtnStyles.containerFill};
            if (this.props.disabled) {
                btnStyle = {...btnStyle, backgroundColor: disableColor};
            } else if (this.props.fillColor) {
                btnStyle = {...btnStyle, backgroundColor: this.props.fillColor};
            }
        }
        if (this.props.link) {
            textStyle = {...textStyle, ...BtnStyles.textLink};
            btnStyle = {...btnStyle, ...BtnStyles.containerLink};
        }

        const inner = this.props.children || <Text numberOfLines={1} style={textStyle}>{`${this.props.title} `}</Text>;
        
        if ( this.props.hide ) {
            return <View/>;
        } else {
            return ( <TouchableOpacity disabled={this.props.disabled === true} onPress={this.onPress} style={btnStyle}>{inner}</TouchableOpacity> );
        }
    }
}

Btn.propTypes = {
    title: PropTypes.string,
    children: PropTypes.object,
    customTextStyles: PropTypes.object,
    customBtnStyles: PropTypes.object,
    disabled: PropTypes.bool,
    hide: PropTypes.bool,
    link: PropTypes.string,
    fill: PropTypes.bool,
    fillColor: PropTypes.string,
    onPress: PropTypes.func,
};

