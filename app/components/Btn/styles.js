/* tslint:disable no-object-literal-type-assertion */
import { Platform } from 'react-native';
import {secondaryColor, textBorderColor, textPlaceholderColor, fontSize21, fontSizeButton, fontSize47} from '../../config/styles';
import {AlignItems, FlexDirection, JustifyContent} from '../../config/RNStyles';
import {FontNames} from '../../config/FontNames';

export const BtnStyles = {
    container: {
        borderColor: textBorderColor,
        borderWidth: 1,
        borderRadius: 4,
        height: fontSize47,
        paddingHorizontal: 16,
        alignItems: AlignItems.center,
        justifyContent: JustifyContent.center,
        flexDirection: FlexDirection.row,
        ...Platform.select({
            ios: {
                paddingTop: 4,
            },
            android: {}
        })
    },
    containerFill: {
        backgroundColor: secondaryColor,
        borderWidth: 0,
    },
    text: {
        fontFamily: FontNames.ProstoLight,
        fontSize: fontSizeButton,
        color: textPlaceholderColor,
        lineHeight: fontSize21,
        textAlignVertical: 'center',
        textAlign: 'center',
        flex: 1,
    },
    textFill: {
        color: '#fff'
    },
    textLink: {
        color: secondaryColor,
        borderBottomColor: secondaryColor,
        borderBottomWidth: 1,
    },
    containerLink: {
        paddingHorizontal: 0,
        backgroundColor: 'transparent',
        borderWidth: 0,
    },
};
