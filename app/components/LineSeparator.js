import React, { Component } from 'react';
import { View } from 'react-native';
import { styles } from '../config/styles';

export class LineSeparator extends Component {
    render() {
        return (<View style={styles.shared.line}/>);
    }
}

