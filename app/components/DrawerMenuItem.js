/**
 * Created by kirill on 28.06.2017.
 */
import React, {Component} from 'react';
import PropTypes from 'prop-types';
import {Animated, Image, TouchableOpacity, View} from 'react-native';
import {menuItemBackgroudSelected, styles} from '../config/styles';

export class DrawerMenuItem extends Component {

    onPressTest(item) {
        this.props.onPress(item);
    }

    render() {
        //console.log(this.props);
        const selfHierarch = this.props.id.split('.');
        //const hierarchy = stores.navigationStore.menuId.split('.');
        const hierarchy = [0];
        const icon = this.props.icon;

        if (selfHierarch.length === 1) {
            //const selected = stores.navigationStore.menuId === this.props.id;
            const selected = false;
            return (
                <View
                    style={{...styles.menu.item.container, ...(selected ? {backgroundColor: menuItemBackgroudSelected} : {backgroundColor: 'transparent'})}}>
                    <TouchableOpacity style={styles.menu.item.button} onPress={this.props.onPress}>
                        <View
                            style={{...styles.menu.item.leftLine, ...(selected ? {} : {backgroundColor: 'transparent'})}}/>
                        {icon ? <Image style={styles.menu.item.icon} source={icon}/> : <View/>}
                        <Animated.Text style={styles.menu.item.title} numberOfLines={1}>{this.props.title}</Animated.Text>
                    </TouchableOpacity>
                </View>
            );
        } else {
            //const selected = stores.navigationStore.menuId === this.props.id;
            const selected = false;
            if (selfHierarch[0] === hierarchy[0]) {
                return <View
                    style={{...styles.menu.item.container, ...(selected ? {backgroundColor: menuItemBackgroudSelected} : {backgroundColor: 'transparent'})}}>
                    <TouchableOpacity style={styles.menu.item.button} onPress={this.props.onPress}>
                        <View
                            style={{...styles.menu.item.leftLine, ...(selected ? {} : {backgroundColor: 'transparent'})}}/>
                        {icon ? <Image source={icon}/> : <View/>}
                        <Animated.Text style={styles.menu.item.title}>{this.props.title}</Animated.Text>
                    </TouchableOpacity>
                </View>;
            } else {
                return <View/>;
            }

        }
    }
}

DrawerMenuItem.propTypes = {
    onPress: PropTypes.func,
    id: PropTypes.string,
    route: PropTypes.string,
    icon: PropTypes.number,
    title: PropTypes.string,
    subItem: PropTypes.bool,
    related: PropTypes.string,
};

