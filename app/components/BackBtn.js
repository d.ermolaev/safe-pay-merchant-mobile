import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Image, TouchableOpacity } from 'react-native';
import { styles } from '../config/styles';
import { Positions } from '../config/RNStyles';
import { ImageResources } from './ImageResources';

export class BackBtn extends Component {

    onPress = () => {
        this.props.onPress();
    }

    render() {
        const style = {...styles.backBtn.container, ...(this.props.float ? {position: Positions.absolute} : {})};
        return (
            <TouchableOpacity
                style={style}
                onPress={this.onPress}
            >
                <Image source={ImageResources.icon_back} style={styles.backBtn.icon}/>
            </TouchableOpacity>
        );
    }
}

BackBtn.propTypes = {
    float: PropTypes.bool,
    onPress: PropTypes.func,
};


