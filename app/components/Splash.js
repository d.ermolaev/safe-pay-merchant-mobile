import React, { Component } from 'react';
import { ImageBackground } from 'react-native';
import { ImageResources } from './ImageResources';

export class Splash extends Component {

    render() {
        return (
            <ImageBackground source={ImageResources.splash_screen} style={{width: '100%', height: '100%'}}/>
        );
    }
}
