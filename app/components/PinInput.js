import React, {Component} from 'react';
import PropTypes from 'prop-types';
import { Text, TextInput, View } from 'react-native';
import { styles } from '../config/styles';

export class PinInput extends Component {

    textInput = null;

    // pin: string
    setPin = (pin) => {
        const pins = pin.replace(/[^0-9]+/g, '');
        this && this.props && this.props.onPinChaged(pins);
    };

    componentDidMount() {    
        setTimeout(() => {
            if (!this.textInput.isFocused()) {
                this.textInput.focus();
            }
        }, 250);
    }

    componentDidUpdate(prevProps) {
        if (prevProps.pin !== this.props.pin && this.textInput) {
            this.textInput.focus();
        }
    }

    render() {
        const pin = this.props.pin;
        const placeholder = this.props.placeholder;

        return (
            <View style={styles.login.containerForm}>
                {pin.length <= 0 ? <Text style={styles.login.placeholder}>{`${placeholder} `}</Text> : null}

                <View style={styles.login.inputShow}>
                    <View style={styles.login.digit}>
                        {pin.length > 0 ? <Text style={styles.login.digitValue}>*</Text> : null}
                    </View>
                    <View style={styles.login.digit}>
                        {pin.length > 1 ? <Text style={styles.login.digitValue}>*</Text> : null}
                    </View>
                    <View style={styles.login.digit}>
                        {pin.length > 2 ? <Text style={styles.login.digitValue}>*</Text> : null}
                    </View>
                    <View style={styles.login.digit}>
                        {pin.length > 3 ? <Text style={styles.login.digitValue}>*</Text> : null}
                    </View>
                </View>

                <TextInput
                    style={styles.login.input}
                    underlineColorAndroid={'transparent'}
                    maxLength={4}
                    returnKeyType={'done'}
                    selectTextOnFocus={false}
                    keyboardType={'numeric'}
                    onChangeText={this.setPin}
                    value={pin}
                    caretHidden={true}
                    ref={(ref) => this.textInput = ref}
                />
            </View>
        );
    }

}

PinInput.propTypes = {
    pin: PropTypes.string,
    onPinChaged: PropTypes.func,
    placeholder: PropTypes.string,
};


