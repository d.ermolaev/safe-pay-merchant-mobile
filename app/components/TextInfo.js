import React, {Component} from 'react';
import PropTypes from 'prop-types';
import { Animated, Clipboard, Image, Modal, TouchableOpacity, View, Platform } from 'react-native';
import { FontNames } from '../config/FontNames';
import { dWidth, primaryColor3, styles, fontSizeTitle, fontSize8 } from '../config/styles';
import { 
    ImageResources,
    Toaster
} from '../components';
import { strings } from '../config/localize';
import QRCode from 'react-native-qrcode-svg';
import { AlignItems, FlexDirection } from '../config/RNStyles';

export class TextInfo extends Component {

    copy = () => {
        let text = this.props.copyText ? this.props.copyText : this.props.displayText;
        Clipboard.setString(text);
        Toaster.error(strings['Copied to clipboard']);
    };

    close = () => {
        this.setState({visible: false});
    };

    updateSize = (height) => {
        this.setState({
            height
        });
    };

    // }
    constructor() {
        super();
        this.state = {height: 47, visible: false};
    }

    render() {
        const big = this.props.big;
        const copy = this.props.allowCopy ? (
            <TouchableOpacity onPress={this.copy} style={{...styles.register.buttonNonAbsolute}}>
                <Image style={styles.sent.image1} source={ImageResources.icon_copy_gray}/>
            </TouchableOpacity>)
            : <View/>;
        const qr = this.props.qrCode ? (
            <TouchableOpacity style={{...styles.register.buttonNonAbsolute}}
                onPress={() => this.setState({visible: true})}>
                <Image source={ImageResources.qr_code} style={styles.sent.image2}/>
            </TouchableOpacity>) : null;
        const qrModal = this.props.qrCode ? (
            <Modal animationType={'none'} visible={this.state.visible} transparent={true}
                onRequestClose={this.close}>
                <TouchableOpacity style={{...styles.register.wholeContainer, ...{alignItems: 'center'}}}
                    activeOpacity={1}
                    onPress={this.close}>
                    <View style={{
                        ...styles.register.menuContainer, ...{
                            paddingVertical: 32,
                            paddingHorizontal: 32,
                            marginHorizontal: undefined
                        }
                    }}>
                        <QRCode
                            value={this.props.displayText?this.props.displayText:this.props.copyText}
                            logoSize={dWidth * 0.6}
                            logoBackgroundColor='black'
                        />
                    </View>
                </TouchableOpacity>
            </Modal>) : null;


        const text = (
            <View style={{
                ...styles.register.border, ...{
                    flex: 1,
                    flexDirection: FlexDirection.row,
                    paddingVertical: fontSize8,
                }
            }}>
                <Animated.Text
                    multiline={big}
                    numberOfLines={big ? 100 : 1}
                    onContentSizeChange={(e) => this.updateSize(e.nativeEvent.contentSize.height)}
                    style={{
                        ...styles.register.text,
                        ...{
                            flex: 1,
                            paddingLeft: 12,
                            alignSelf: 'center'
                        },
                        ...Platform.select({
                            ios: {
                                paddingTop: 4,
                            },
                            android: {

                            }
                        })
                    }}

                >{this.props.displayText}</Animated.Text>
                <View style={{
                    flexDirection: FlexDirection.row,
                    alignItems: AlignItems.center,
                }}>
                    {copy}
                    {qr}
                </View>
            </View>);

        return (

            <View style={{paddingVertical: fontSize8}}>
                <Animated.Text style={{
                    ...{
                        fontFamily: FontNames.ProstoLight,
                        fontSize: fontSizeTitle,
                        color: this.props.titleColor ? this.props.titleColor : primaryColor3,
                        marginBottom: 5
                    }, ...this.props.titleStyle
                }}>{this.props.title}</Animated.Text>
                {text}
                {qrModal}
            </View>
        );
    }
}

TextInfo.propTypes = {
    title: PropTypes.string,
    displayText: PropTypes.string,
    copyText: PropTypes.string,
    big: PropTypes.bool,
    allowCopy: PropTypes.bool,
    titleColor: PropTypes.string,
    titleStyle: PropTypes.object,
    qrCode: PropTypes.bool,
};


