import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Animated, Clipboard, Image, TextInput, TouchableOpacity, View } from 'react-native';
import { styles, textPlaceholderColor } from '../config/styles';
import { Toaster } from './Toaster';
import { strings } from '../config/localize';
import { ImageResources } from './ImageResources';
import { Btn } from './Btn/Btn';
import { Base58 } from '../core/crypt/Base58';
import Toast from 'react-native-simple-toast';

export class CopySecretKey extends Component {
    
    copyToClipboard = () => {
        Clipboard.setString(this.state.key);
        Toaster.error(strings['Copied to clipboard']);
    };

    pasteFromClipboard = async () => {
        const content = await Clipboard.getString();
        const keyBytes = await Base58.decode(content);
        const valid = keyBytes.length === 64;
        //const valid = await PrivateKeyAccount.validate(content);
        this.setState({copiedKey: content, validKey: valid});
        Toast.show(strings['Copied from clipboard']);
        this.props.onComplete(content.localeCompare(this.state.key) === 0);
    };

    constructor(props) {
        super(props);
        const keys = this.props.keys;
        this.state = {copiedKey: '', key: keys.secretKey, validKey: false};
    }
    
    render() {
        return (
            <View>
                <View style={{marginHorizontal: 16}}>
                    <Btn onPress={this.copyToClipboard} title={strings['Copy to clipboard']} fill={true}/>
                </View>
                <View>
                    <Animated.Text style={styles.register.completeBigMessage}>{strings['message_4']}</Animated.Text>
                </View>
                <View style={{
                    ...styles.register.border, ...{
                        flex: 1,
                        flexDirection: 'row',
                        paddingVertical: 8,
                    }
                }}>
                    <TextInput
                        placeholderTextColor={textPlaceholderColor}
                        style={{
                            ...styles.register.textEdit,
                            ...{
                                flex: 1,
                            }
                        }}
                        underlineColorAndroid={'transparent'}
                        value={this.state.copiedKey.slice(0, 20) + '...'}
                        editable={false}
                    />
                    <TouchableOpacity activeOpacity={0.9} onPress={this.pasteFromClipboard}
                        style={styles.register.button1}>
                        <Image style={{width: 28, height: 30}}
                            source={ImageResources.icon_insert}/>
                    </TouchableOpacity>
                </View>
            </View>
        );
    }
}

CopySecretKey.propTypes = {
    keys: PropTypes.object,
    onComplete: PropTypes.func,
};

