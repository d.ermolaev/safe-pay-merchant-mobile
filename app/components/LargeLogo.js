import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { View, Image } from 'react-native';
import {
    ImageResources
} from './ImageResources';
import { styles } from '../config/styles';

export class LargeLogo extends Component {
    render() {
        return (
            <View style={styles.login.largeLogo}>
                <Image style={styles.login.logo} resizeMode="contain" source={ImageResources.logo}/>
                {this.props.children}
            </View>
        );
    }
}

LargeLogo.propTypes = {
    children: PropTypes.object,
};

