import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Keyboard, Platform, View } from 'react-native';
import { styles } from '../config/styles';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import { LargeLogo } from './LargeLogo';
import { PinInput } from './PinInput';
import Toast from 'react-native-simple-toast';
import { strings } from '../config/localize';
import { BackBtn } from './BackBtn';

export class DoublePinInput extends Component {

    keyboardDidShowListener = null;
    keyboardDidHideListener = null;

    prevStep = () => {
        if (this.state.step <= 0) {
            return;
        }
        this.setState({step: this.state.step - 1, pins: ['', ''], keyboardVisible: true});
    };

    setPin = (pin) => {
        const pins = this.state.pins;
        pins[this.state.step] = pin;
        this.setState({pins});
        if (pin.length === 4) {
            Keyboard.dismiss();
            setTimeout(async () => {
                await this.nextStep();
            }, 250);
        }
    };

    nextStep = async () => {
        if (this.state.step > 0) {
            if (this.state.pins[0] === this.state.pins[1]) {
                await this.props.onComplete(this.state.pins[0]);
                return;
            } else {
                this.state.pins[1] = '';
                this.setState({invalidPin: true, step: this.state.step - 1, pins: ['', '']});
                Toast.show(strings['pins do not match']);
            }
        } else {
            this.setState({step: this.state.step + 1});
        }
    };

    _keyboardDidShow = () => {
        this.setState({keyboardVisible: true});
    };

    _keyboardDidHide = () => {
        this.setState({keyboardVisible: false});
    };

    constructor(props) {
        super(props);

        this.keyboardDidShowListener = Keyboard.addListener('keyboardDidShow', this._keyboardDidShow);
        this.keyboardDidHideListener = Keyboard.addListener('keyboardDidHide', this._keyboardDidHide);

        //DC-209
        this.state = {
            pins: '',
            step: 0,
            invalidPin: false,
            keyboardVisible: true
        };
        this.state = {pins: ['', ''], step: 0, invalidPin: false};
    }

    componentWillUnmount() {
        this.keyboardDidShowListener.remove();
        this.keyboardDidHideListener.remove();
    }

    render() {
        const step = this.state.step;
        const placeholders = [
            strings['create pin'],
            strings['confirm pin'],
            strings['pins do not match'],
        ];
        const pin = this.state.pins[step];

        const placeholder = placeholders[step]; //this.state.invalidPin ? placeholders[2] :

        const chldern = (<View style={{flex: 1}}>
            <View style={styles.login.containerTop}>
                <LargeLogo>
                    {this.state.step === 0 ? null : <BackBtn float={true} onPress={this.prevStep}/>}
                </LargeLogo>
            </View>

            <View
                style={{
                    ...styles.login.containerBottom, ...{
                        alignItems: 'center',
                        justifyContent: 'center',
                        paddingTop: 32
                    }
                }}>
                <PinInput placeholder={placeholder} onPinChaged={this.setPin} pin={pin}/>
            </View>

            {this.state.keyboardVisible && Platform.OS === 'ios' ? <View style={{flex: 1}}/> : null}
        </View>);

        return (
            <View style={styles.container}>
                {Platform.OS === 'android' ?
                    <KeyboardAwareScrollView
                        contentContainerStyle={{flex: 1}}
                        keyboardShouldPersistTaps='handled'
                        bounces={false}
                    >
                        {chldern}
                    </KeyboardAwareScrollView> : chldern}

            </View>
        );
    }
}

DoublePinInput.propTypes = {
    onComplete: PropTypes.func,
};

