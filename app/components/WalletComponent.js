import React, {Component} from 'react';
import PropTypes from 'prop-types';
import { Text, View } from 'react-native';
import { styles } from '../config/styles';
import { InfoItem } from './InfoItem';
import { strings } from '../config/localize';
import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect';
import {  
    makeSelectWallet
} from '../selectors/walletSelectors';
import {  
    makeSelectHistory
} from '../selectors/dbSelectors';

class WalletInfo extends Component {

    render() {
        const historyStyle = {...styles.infoItem.container};
        const { data } = this.props.history;
        const lable = data.length > 0 ?
            null :
            <Text style={styles.myWallet.noHistoryText}>{strings['no records']}</Text>;

        return (
            <View style={styles.container}>
                <InfoItem
                    title={`${strings['Yours correct address']} `}
                    value={this.props.wallet.address}
                />
                <View style={styles.shared.line}/>
                <View style={historyStyle}>
                    <Text style={styles.infoItem.title}>{strings['Transactions history']}</Text>
                </View>
                <View>{lable}</View>
            </View>
        );
    }
}

WalletInfo.propTypes = {
    wallet: PropTypes.object,
    history: PropTypes.object,
};

const mapStateToProps = createStructuredSelector({
    wallet: makeSelectWallet(),
    history: makeSelectHistory(),

});

export const WalletComponent = connect(
    mapStateToProps,
    null
)(WalletInfo);
