import { StyleSheet } from 'react-native';

const styles = StyleSheet.create({
    icon: {
        paddingLeft: 10,
    },
});

export default styles;
