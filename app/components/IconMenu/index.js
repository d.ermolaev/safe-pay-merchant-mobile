import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { TouchableOpacity, View } from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome';
import styles from './styles';

class IconMenu extends Component {
    
    render() {
        return (
            <View style={styles.icon} >
                <TouchableOpacity onPress={() => { this.props.onPress(); } }>
                    <Icon 
                        name="bars" 
                        type="font-awesome" 
                        size={25} 
                        color={'#666699'}
                    />
                </TouchableOpacity>
            </View>
        );
    }
}

IconMenu.propTypes = {
    onPress: PropTypes.func,
};

export default IconMenu;
