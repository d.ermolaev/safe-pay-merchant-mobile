import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Animated, Clipboard, Image, TouchableOpacity, View } from 'react-native';
import { styles } from '../config/styles';
import { ImageResources } from './ImageResources';
import Toast from 'react-native-simple-toast';
import { strings } from '../config/localize';

// const dWidth = Dimensions.get('window').width;

export class InfoItem extends Component {
    copyToClipboard = () => {
        if (this.props.onPress) {
            this.props.onPress();
        } else {
            Clipboard.setString(this.props.value);
            Toast.show(strings['Copied to clipboard']);
        }
    };

    render() {
        return (
            <View style={styles.infoItem.container}>
                <Animated.Text multiline={false}
                    numberOfLines={1} style={styles.infoItem.title}>{this.props.title}</Animated.Text>
                <View style={styles.infoItem.valueContainer}>
                    <Animated.Text
                        multiline={false}
                        numberOfLines={1}
                        style={styles.infoItem.value}
                    >
                        {this.props.value}
                    </Animated.Text>
                    <TouchableOpacity style={styles.infoItem.button} onPress={this.copyToClipboard}>
                        <Image style={styles.infoItem.button} source={ImageResources.icon_copy}/>
                    </TouchableOpacity>
                </View>
            </View>
        );
    }
}

InfoItem.propTypes = {
    title: PropTypes.string,
    value: PropTypes.string,
    onPress: PropTypes.func,
};
