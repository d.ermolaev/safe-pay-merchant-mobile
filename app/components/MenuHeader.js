import React, { Component } from 'react';
import { Image, TouchableOpacity, View } from 'react-native';
import { styles } from '../config/styles';
import { ImageResources } from './ImageResources';

export class MenuHeader extends Component {

    render() {
        return (
            <View>
                <TouchableOpacity onPress={this.myAccount} activeOpacity={0.95}>
                    <Image style={styles.menu.backgroundImage} source={ImageResources.image}/>
                    <View style={styles.menu.backgroundEmpty}>
                        <View style={styles.menu.textContainer}>
                        </View>
                    </View>
                    <View style={styles.menu.userView}>
                        <View>
                            {this.renderAvatar()}
                            {/*<Image style={styles.menu.userEditButton} source={ImageResources.icon_user_edit}/>*/}
                        </View>
                    </View>
                </TouchableOpacity>
            </View>
        );
    }

    renderAvatar() {

        return <Image source={ImageResources.icon_menulogo} style={styles.menu.userImage}/>;
    }
}
