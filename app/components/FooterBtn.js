import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Text, TouchableOpacity, View, Platform } from 'react-native';
import { AlignItems, FlexDirection, JustifyContent, Positions } from '../config/RNStyles';
import { dWidth, disableColor, primaryColor3, whiteColor, fontSize18, fontSize7 } from '../config/styles';
import { FontNames } from '../config/FontNames';

export class FooterBtn extends Component {

    onPressTest() {
        this.props.onPress();
    }

    render() {
        const content = this.props.title ? <Text style={styles.text} numberOfLines={1}>{this.props.title}</Text> : this.props.children;
        return (
            <View style={styles.container}>
                <TouchableOpacity disabled={this.props.disabled} onPress={this.props.onPress} style={[styles.button, { backgroundColor: this.props.disabled?disableColor:primaryColor3 }]} activeOpacity={0.9}>{content}</TouchableOpacity>
            </View>
        );
    }
}

const styles = {
    container: {
        position: Positions.absolute,
        bottom: 0,
        left: 0,
        right: 0,
        flexDirection: FlexDirection.row,
        alignItems: AlignItems.stretch,
        justifyContent: JustifyContent.center,
    },
    button: {
        flexDirection: FlexDirection.row,
        flex: 1,
        alignItems: AlignItems.center,
        justifyContent: JustifyContent.center,
        paddingVertical: fontSize7,
        ...Platform.select({
            ios: {
                paddingTop: 4,
            },
            android: {

            }
        })
    },
    text: {
        color: whiteColor,
        fontFamily: FontNames.ProstoRegular,
        fontSize: fontSize18,
        marginHorizontal: 10,
        maxWidth: dWidth *.8
    }
};

FooterBtn.propTypes = {
    title: PropTypes.string,
    onPress: PropTypes.func,
    children: PropTypes.array,
    disabled: PropTypes.bool,
};

