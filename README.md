
Safe Pay 
===========================================

[![React Native](https://img.shields.io/badge/React%20Native-v0.59.4-blue.svg)](https://facebook.github.io/react-native/)
[![React Navigation V3](https://img.shields.io/badge/React%20Navigation-v3.8-blue.svg)](https://reactnavigation.org/)


This project is configured with redux, reselect, redux saga and redux immutable. 

## Features

* [Redux](http://redux.js.org/)
* [Reselect](https://github.com/reduxjs/reselect)
* [Redux Saga](https://redux-saga.js.org/)
* [Redux Immutable](https://github.com/gajus/redux-immutable/)
* [React Navigation](https://reactnavigation.org/) 
* [React Native Gesture Handler](https://github.com/kmagiera/react-native-gesture-handler) 
* [Jest](https://facebook.github.io/jest/)
* [Eslint](http://eslint.org/) ([Airbnb config](https://github.com/airbnb/javascript/tree/master/packages/eslint-config-airbnb))

## Prerequisites

* [Node](https://nodejs.org) v8.10 (it is recommended to install it via [NVM](https://github.com/creationix/nvm))
* [React Native CLI](https://www.npmjs.com/package/react-native-cli) (npm -g install react-native-cli)
* [Yarn](https://yarnpkg.com/)
* A development machine set up for React Native by following [these instructions](https://facebook.github.io/react-native/docs/getting-started.html)

## Getting Started

1. Clone this repo, `git clone https://lab.erachain.org/safe_pay/merchant_mobile.git`
2. Go to project's root directory, `cd merchant_mobile`
3. Run `yarn` or `npm install` to install dependencies.
4. Start the packager with `npm start`
5. Run the test application:
  * On Android:
    * Run `react-native run-android` on Android simulator (Android Studio required).
  * On iOS:
    * Run `react-native run-ios` on iOS simulator (XCode required).
6. Run test: `yarn test`
7. Build release:
  * On Android:
    * Run `cd android && ./gradlew assembleRelease`  
  * On iOS:
    * Open `ios/qrsp.xcworkspace` in Xcode
    * Hit `Product -> Archive`
8. Enjoy!!! 

