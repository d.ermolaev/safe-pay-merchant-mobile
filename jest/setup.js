import { fixtures } from '../app/core/crypt/tests/fixtures';


//import DB from '../app/store/DB';

jest.mock('react-native-simple-toast', () => {
    return {
        show: jest.fn()
    };
});

jest.mock('react-native-fetch-blob', () => {
    return {
        DocumentDir: () => {},
        polyfill: () => {},
    };
});

let clipboard = '';
jest.mock('Clipboard', () => {
    return {
        setString: jest.fn((s) => {
            clipboard = s;
        }),
        getString: jest.fn(() => {
            return new Promise((resolve) => {
                resolve(clipboard);
            });
        })
    };
});

jest.mock('NativeModules', () => {
    return {
        ChainUtilities: {
            base58encode: jest.fn(() => {
                return new Promise((resolve) => {
                    resolve(fixtures.base58String);
                });
            }),
            base58decode: jest.fn(() => {
                return new Promise((resolve) => {
                    resolve(fixtures.base58Array);
                });
            }),
        },
        ReactLocalization: {
            language: 'en_US'
        }
    };
});

const map = {};
jest.mock('Keyboard', () => {
    return {
        addListener: jest.fn((event, cb) => {
            map[event] = cb;
        }),
        dismiss: jest.fn(),
    };
});

jest.mock('TextInput', () => {
    const RealComponent = require.requireActual('TextInput');
    const React = require('react');
  
    class TextInput extends React.Component {
        focus() {}

        render() {
            return React.createElement('TextInput', {...this.props, autoFocus: false}, this.props.children);
        }
    }
    TextInput.propTypes = RealComponent.propTypes;
    return TextInput;
});

jest.mock('NativeAnimatedHelper');

jest.mock('realm', () => {

    class Realm {

        constructor(params) {
            this.schema = {};
            this.callbackList = [];
            this.data = {};
            this.schemaCallbackList = {};
            params.schema.forEach((schema) => {
                this.data[schema.name] = [];
            });
            params.schema.forEach((schema) => {
                this.schema[schema.name] = schema;
            });
            this.lastLookedUpModel = null;
        }

        static open(params) {
            return new Promise((resolve) => {
                resolve(new Realm(params));
            });
        }
      
        objects(schemaName) {
            this.lastLookedUpModel = schemaName;
            const objects = Object.values(this.data[schemaName]);
            objects.values = () => objects;
            objects.addListener = (cb) => {
                if (this.schemaCallbackList[schemaName]) {
                    this.schemaCallbackList[schemaName].push(cb);
                } else {
                    this.schemaCallbackList[schemaName] = [cb];
                }
            };
            objects.removeListener = () => {};
            objects.filtered = () => {
                return objects;
            };
            objects.sorted = () => {
                return objects;
            };
            objects.max = (key) => {
                let result = 0;
                for (let i = 0; i < objects.length; i++) {
                    const obj = objects[i];
                    if (obj[key] > result) {
                        result = obj[key];
                    }
                }
                return result;
            };
            objects.slice = () => objects;
            return objects;
        }
      
        write(fn) {
            this.writing = true;
            fn();
            this.writing = false;
        }
      
        create(schemaName, object) {
            const modelObject = object;
            const properties = this.schema[schemaName].properties;
            Object.keys(properties).forEach((key) => {
                
                if (modelObject[key] && modelObject[key].model) {
                    this.data[modelObject[key].model][modelObject[key].id] = this.create(
                        modelObject[key].model, modelObject[key],
                    );
                } else if (modelObject[key] && modelObject[key].length && modelObject[key][0].model) {
                    modelObject[key].forEach((obj) => {
                        this.data[modelObject[key][0].model][obj.id] = obj;
                    });
                    modelObject[key].filtered = () => modelObject[key];
                    modelObject[key].sorted = () => modelObject[key].sort();
                } else if (modelObject[key] === undefined) {
                    if (typeof properties[key] === 'object' && properties[key].optional) {
                        modelObject[key] = null;
                    }
                    if (typeof properties[key] === 'object' && ['list', 'linkingObjects'].includes(properties[key].type)) {
                        modelObject[key] = [];
                        modelObject[key].filtered = () => [];
                        modelObject[key].sorted = () => [];
                    }
                }
            });
            //console.log(schemaName, this.data);
            this.data[schemaName].push(modelObject);
            if (this.writing) {
                if (this.schemaCallbackList[schemaName]) {
                    this.schemaCallbackList[schemaName].forEach(cb => cb(schemaName, {
                        insertions: { length: 1 },
                        modifications: { length: 0 },
                        deletions: { length: 0 },
                    }));
                }
                this.callbackList.forEach((cb) => { cb(); });
            }
            //console.log(this.data);
            return modelObject;
        }
      
        objectForPrimaryKey(model, id) {
            this.lastLookedUpModel = model;
            for (let i = 0; i < this.data[model].length; i++) {
                if (this.data[model][i].id === id) {
                    //console.log(this.data[model][i]);
                    return this.data[model][i];
                }
            }
            return undefined;
        }
      
        delete(object) {
            if (this.lastLookedUpModel || object.model) {
                const model = object.model ? object.model : this.lastLookedUpModel;
                if (Array.isArray(object)) {
                    object.forEach((item) => {
                        delete this.data[model][item.id];
                    });
                }
                delete this.data[model][object.id];
                if (this.writing) {
                    if (this.schemaCallbackList[model]) {
                        this.schemaCallbackList[model].forEach(cb => cb(model, {
                            insertions: { length: 0 },
                            modifications: { length: 0 },
                            deletions: { length: 1 },
                        }));
                    }
                    this.callbackList.forEach((cb) => { cb(); });
                }
            }
        }
      
        deleteAll() {
            Object.keys(this.schema).forEach((key) => {
                if (this.writing && this.schemaCallbackList[this.schema[key].name]) {
                    this.schemaCallbackList[this.schema[key].name].forEach(cb => cb(key, {
                        insertions: { length: 0 },
                        modifications: { length: 0 },
                        deletions: { length: Object.values(this.data[this.schema[key].name]).length },
                    }));
                }
                this.data[this.schema[key].name] = [];
            });
            if (this.writing) this.callbackList.forEach((cb) => { cb(); });
        }
      
        addListener(event, callback) {
            this.callbackList.push(callback);
        }
      
        prepareData(schemaName, objects) {
            objects.forEach((object) => {
                this.create(schemaName, object);
            });
        }
    }

    return Realm;
});

jest.mock('../app/store/LocalStore', () => {

    class LocalStore {

        static data = null;

        static backup(filename, data) {
            return new Promise((resolve, reject) => {
                LocalStore.data = data;
                if (!filename) {
                    reject();
                }
                resolve(filename);
            });
        }
    
        static restore() {
            return new Promise(async (resolve) => {
                resolve(LocalStore.data);
            });
        }
    }

    return LocalStore;
});

jest.mock('../app/core/request/NodeBaseRequest', () => {
    return {
        nodeBaseRequest: () => {
            return new Promise((resolve) => {
                const r = {
                    error: null,
                    respInfo: {
                        status: 200,
                    },
                    txs: [], 
                    height: 0,
                    signature: '25GVaWn9xrs6UaFo1aseSpD4Wk4sJXE3emU971f7MqirhG4YHFrvNSdgHJcJRqwfVtSB3VEbTHEZa6p5N9vMtRu4',
                    status: 'ok'
                };
                resolve(r);
            });
        },
    };
});

jest.mock('../app/core/request/BaseRequest', () => {
    return {
        baseRequest: ({ url }) => {
            return new Promise((resolve) => {
                const r = {
                    error: null,
                    respInfo: {
                        status: 200,
                    },
                    data: {},
                };
                if (url.indexOf('height') !== -1) {
                    resolve('2');
                } else {
                    resolve(r);
                }
            });
        },
    };
});