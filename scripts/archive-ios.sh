#!/usr/bin/env bash
set -xeo pipefail

source $(dirname $0)/config.sh

cd ./ios

export FORCE_BUNDLING=true

if [ -f ./Podfile ]; then
  pod install
fi

xcodebuild clean archive -archivePath build/$APP_NAME_CODE -scheme $APP_NAME_CODE
xcodebuild -exportArchive -archivePath build/$APP_NAME_CODE.xcarchive -exportOptionsPlist exportOptionsAdHoc.plist -exportPath build/$APP_NAME_CODE.ipa PROVISIONING_PROFILE="ProfileSafePay"