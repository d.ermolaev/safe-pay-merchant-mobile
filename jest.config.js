//const {defaults} = require('jest-config');
//console.log(defaults);
module.exports = {
    preset: 'react-native',
    setupFiles: ['./jest/setup.js'],
    transform: {
        '^.+\\.js$': require.resolve('react-native/jest/preprocessor.js'),
    }
};